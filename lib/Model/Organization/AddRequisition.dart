import 'package:blu_way/Functions/network/api_constants.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class AddRequisition {
  String status;
  String message;
  Data data;
  String token;

  AddRequisition({this.status, this.message, this.data, this.token});

  AddRequisition.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['token'] = this.token;
    return data;
  }
}

class Data {
  int deleted;
  int active;
  String sId;
  List<Products> products;
  String organization;
  String createdDate;
  int iV;

  Data(
      {this.deleted,
      this.active,
      this.sId,
      this.products,
      this.organization,
      this.createdDate,
      this.iV});

  Data.fromJson(Map<String, dynamic> json) {
    deleted = json['deleted'];
    active = json['active'];
    sId = json['_id'];
    if (json['products'] != null) {
      products = new List<Products>();
      json['products'].forEach((v) {
        products.add(new Products.fromJson(v));
      });
    }
    organization = json['organization'];
    createdDate = json['created_date'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['deleted'] = this.deleted;
    data['active'] = this.active;
    data['_id'] = this.sId;
    if (this.products != null) {
      data['products'] = this.products.map((v) => v.toJson()).toList();
    }
    data['organization'] = this.organization;
    data['created_date'] = this.createdDate;
    data['__v'] = this.iV;
    return data;
  }
}

class Products {
  String sId;
  String product;
  int box;

  Products({this.sId, this.product, this.box});

  Products.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    product = json['product'];
    box = json['box'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['product'] = this.product;
    data['box'] = this.box;
    return data;
  }
}

Future<AddRequisition> addRequisition(
    List<Map<String, String>> products, String orgId, token) async {
  var decode;
  var response = await http.post(Uri.encodeFull(AppConstants.addRequisition),
      body: json.encode({
        "products": products,
        "organization": orgId,
      }),
      headers: {"authorization": token});
  if (response.statusCode == 200) {
    decode = AddRequisition.fromJson(json.decode(response.body));
    print("Response of added data is: " + response.body);
    return AddRequisition.fromJson(json.decode(response.body));
  } else {
    print("There is an error");
  }
  return decode;
}
