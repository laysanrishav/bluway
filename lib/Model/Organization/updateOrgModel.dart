import 'package:blu_way/Functions/network/api_constants.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class UpdateOrganizationModel {
  String status;
  String message;
  OrgUpdateData data;

  UpdateOrganizationModel({this.status, this.message, this.data});

  UpdateOrganizationModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    data =
        json['data'] != null ? new OrgUpdateData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class OrgUpdateData {
  Position position;
  int active;
  int deleted;
  String sId;
  String state;
  String district;
  String name;
  String pincode;
  String orgType;
  String orgOtherType;
  String regNumber;
  String gstNumber;
  String hub;
  String createdDate;
  int iV;
  String address1;
  String address2;
  String servicePincode;

  OrgUpdateData(
      {this.position,
      this.active,
      this.deleted,
      this.sId,
      this.state,
      this.district,
      this.name,
      this.pincode,
      this.orgType,
      this.orgOtherType,
      this.regNumber,
      this.gstNumber,
      this.hub,
      this.createdDate,
      this.iV,
      this.address1,
      this.address2,
      this.servicePincode});

  OrgUpdateData.fromJson(Map<String, dynamic> json) {
    position = json['position'] != null
        ? new Position.fromJson(json['position'])
        : null;
    active = json['active'];
    deleted = json['deleted'];
    sId = json['_id'];
    state = json['state'];
    district = json['district'];
    name = json['name'];
    pincode = json['pincode'];
    orgType = json['org_type'];
    orgOtherType = json['org_other_type'];
    regNumber = json['reg_number'];
    gstNumber = json['gst_number'];
    hub = json['hub'];
    createdDate = json['created_date'];
    iV = json['__v'];
    address1 = json['address1'];
    address2 = json['address2'];
    servicePincode = json['service_pincode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.position != null) {
      data['position'] = this.position.toJson();
    }
    data['active'] = this.active;
    data['deleted'] = this.deleted;
    data['_id'] = this.sId;
    data['state'] = this.state;
    data['district'] = this.district;
    data['name'] = this.name;
    data['pincode'] = this.pincode;
    data['org_type'] = this.orgType;
    data['org_other_type'] = this.orgOtherType;
    data['reg_number'] = this.regNumber;
    data['gst_number'] = this.gstNumber;
    data['hub'] = this.hub;
    data['created_date'] = this.createdDate;
    data['__v'] = this.iV;
    data['address1'] = this.address1;
    data['address2'] = this.address2;
    data['service_pincode'] = this.servicePincode;
    return data;
  }
}

class Position {
  String type;
  List<int> coordinates;

  Position({this.type, this.coordinates});

  Position.fromJson(Map<String, dynamic> json) {
    type = json['type'];
    coordinates = json['coordinates'].cast<int>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['type'] = this.type;
    data['coordinates'] = this.coordinates;
    return data;
  }
}
