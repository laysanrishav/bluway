// import 'dart:convert';
// import 'package:http/http.dart' as http;

// class DistrictListResponse {
//   String status;
//   String message;
//   List<DistrictResult> result;

//   DistrictListResponse({this.status, this.message, this.result});

//   DistrictListResponse.fromJson(Map<String, dynamic> json) {
//     status = json['status'];
//     message = json['message'];
//     if (json['data'] != null) {
//       result = new List<DistrictResult>();
//       json['data'].forEach((v) {
//         result.add(new DistrictResult.fromJson(v));
//       });
//     }
//   }

//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['status'] = this.status;
//     data['message'] = this.message;
//     if (this.result != null) {
//       data['data'] = this.result.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }

// class DistrictResult {
//   int active;
//   int deleted;
//   String sId;
//   String state;
//   String name;
//   String stateName;
//   String createdDate;

//   DistrictResult(
//       {this.active,
//       this.deleted,
//       this.sId,
//       this.state,
//       this.name,
//       this.stateName,
//       this.createdDate});

//   DistrictResult.fromJson(Map<String, dynamic> json) {
//     active = json['active'];
//     deleted = json['deleted'];
//     sId = json['_id'];
//     state = json['state'];
//     name = json['name'];
//     stateName = json['state_name'];
//     createdDate = json['created_date'];
//   }

//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['active'] = this.active;
//     data['deleted'] = this.deleted;
//     data['_id'] = this.sId;
//     data['state'] = this.state;
//     data['name'] = this.name;
//     data['state_name'] = this.stateName;
//     data['created_date'] = this.createdDate;
//     return data;
//   }
// }

// Future<DistrictListResponse> fetchDistrictListModel(String id) async {
//   var body = jsonEncode({"id": id});

//   final response =
//       await http.post('http://139.59.75.40:4040/app/district-list', body: body);
//   print(response.body);
//   if (response.statusCode == 200) {
//     return DistrictListResponse.fromJson(json.decode(response.body));
//   } else {
//     throw Exception("Failed to load District");
//   }
// }
