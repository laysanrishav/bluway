import 'dart:convert';
import 'package:blu_way/Functions/network/api_constants.dart';
import 'package:http/http.dart' as http;

class GetSeListModel {
  String status;
  String token;
  List<SEResult> seresult;

  GetSeListModel({this.status, this.token, this.seresult});

  GetSeListModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    token = json['token'];
    if (json['result'] != null) {
      seresult = new List<SEResult>();
      json['result'].forEach((v) {
        seresult.add(new SEResult.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['token'] = this.token;
    if (this.seresult != null) {
      data['result'] = this.seresult.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class SEResult {
  int active;
  int otp;
  int verified;
  Supervisor supervisor;
  int deleted;
  String sId;
  String firstName;
  String lastName;
  String email;
  String mobile;
  String userType;
  String state;
  String district;
  String pincode;
  String master;
  String masterName;
  String address;
  String village;
  String postOffice;
  String thana;
  String licenceNumber;
  String document;
  String documentImage;
  String profileImage;
  String createdDate;
  int iV;

  SEResult(
      {this.active,
      this.otp,
      this.verified,
      this.supervisor,
      this.deleted,
      this.sId,
      this.firstName,
      this.lastName,
      this.email,
      this.mobile,
      this.userType,
      this.state,
      this.district,
      this.pincode,
      this.master,
      this.masterName,
      this.address,
      this.village,
      this.postOffice,
      this.thana,
      this.licenceNumber,
      this.document,
      this.documentImage,
      this.profileImage,
      this.createdDate,
      this.iV});

  SEResult.fromJson(Map<String, dynamic> json) {
    active = json['active'];
    otp = json['otp'];
    verified = json['verified'];
    supervisor = json['supervisor'] != null
        ? new Supervisor.fromJson(json['supervisor'])
        : null;
    deleted = json['deleted'];
    sId = json['_id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    email = json['email'];
    mobile = json['mobile'];
    userType = json['user_type'];
    state = json['state'];
    district = json['district'];
    pincode = json['pincode'];
    master = json['master'];
    masterName = json['master_name'];
    address = json['address'];
    village = json['village'];
    postOffice = json['post_office'];
    thana = json['thana'];
    licenceNumber = json['licence_number'];
    document = json['document'];
    documentImage = json['document_image'];
    profileImage = json['profile_image'];
    createdDate = json['created_date'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['active'] = this.active;
    data['otp'] = this.otp;
    data['verified'] = this.verified;
    if (this.supervisor != null) {
      data['supervisor'] = this.supervisor.toJson();
    }
    data['deleted'] = this.deleted;
    data['_id'] = this.sId;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['email'] = this.email;
    data['mobile'] = this.mobile;
    data['user_type'] = this.userType;
    data['state'] = this.state;
    data['district'] = this.district;
    data['pincode'] = this.pincode;
    data['master'] = this.master;
    data['master_name'] = this.masterName;
    data['address'] = this.address;
    data['village'] = this.village;
    data['post_office'] = this.postOffice;
    data['thana'] = this.thana;
    data['licence_number'] = this.licenceNumber;
    data['document'] = this.document;
    data['document_image'] = this.documentImage;
    data['profile_image'] = this.profileImage;
    data['created_date'] = this.createdDate;
    data['__v'] = this.iV;
    return data;
  }
}

class Supervisor {
  String sId;
  String firstName;
  String lastName;
  String email;
  String mobile;

  Supervisor(
      {this.sId, this.firstName, this.lastName, this.email, this.mobile});

  Supervisor.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    email = json['email'];
    mobile = json['mobile'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['email'] = this.email;
    data['mobile'] = this.mobile;
    return data;
  }
}

Future<GetSeListModel> getSElistModel(String orgId, String token) async {
  GetSeListModel getSeListModel = GetSeListModel();
  var response = await http.post(Uri.encodeFull(AppConstants.seList),
      body: json.encode({"id": "$orgId"}),
      headers: {"authorization": "$token"});
  if (response.statusCode == 200) {
    return GetSeListModel.fromJson(json.decode(response.body));
  } else {
    print("There is an error");
  }
  return getSeListModel;
}
