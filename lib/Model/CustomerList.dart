import 'package:blu_way/Functions/network/api_constants.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class CustomerList {
  String status;
  String token;
  List<CustomerResult> result;

  CustomerList({this.status, this.token, this.result});

  CustomerList.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    token = json['token'];
    if (json['result'] != null) {
      result = new List<CustomerResult>();
      json['result'].forEach((v) {
        result.add(new CustomerResult.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['token'] = this.token;
    if (this.result != null) {
      data['result'] = this.result.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CustomerResult {
  Position position;
  int otp;
  int deleted;
  int verified;
  String sId;
  String fullName;
  String mobile;
  String address;
  String village;
  String postOffice;
  String thana;
  String state;
  String district;
  String pincode;
  String createdDate;
  int iV;

  CustomerResult(
      {this.position,
      this.otp,
      this.deleted,
      this.verified,
      this.sId,
      this.fullName,
      this.mobile,
      this.address,
      this.village,
      this.postOffice,
      this.thana,
      this.state,
      this.district,
      this.pincode,
      this.createdDate,
      this.iV});

  CustomerResult.fromJson(Map<String, dynamic> json) {
    position = json['position'] != null
        ? new Position.fromJson(json['position'])
        : null;
    otp = json['otp'];
    deleted = json['deleted'];
    verified = json['verified'];
    sId = json['_id'];
    fullName = json['full_name'];
    mobile = json['mobile'];
    address = json['address'];
    village = json['village'];
    postOffice = json['post_office'];
    thana = json['thana'];
    state = json['state'];
    district = json['district'];
    pincode = json['pincode'];
    createdDate = json['created_date'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.position != null) {
      data['position'] = this.position.toJson();
    }
    data['otp'] = this.otp;
    data['deleted'] = this.deleted;
    data['verified'] = this.verified;
    data['_id'] = this.sId;
    data['full_name'] = this.fullName;
    data['mobile'] = this.mobile;
    data['address'] = this.address;
    data['village'] = this.village;
    data['post_office'] = this.postOffice;
    data['thana'] = this.thana;
    data['state'] = this.state;
    data['district'] = this.district;
    data['pincode'] = this.pincode;
    data['created_date'] = this.createdDate;
    data['__v'] = this.iV;
    return data;
  }
}

class Position {
  String type;
  List<int> coordinates;

  Position({this.type, this.coordinates});

  Position.fromJson(Map<String, dynamic> json) {
    type = json['type'];
    coordinates = json['coordinates'].cast<int>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['type'] = this.type;
    data['coordinates'] = this.coordinates;
    return data;
  }
}

Future<CustomerList> getCustomerList(String orgId) async {
  CustomerList customerList;
  var response = await http.post(Uri.encodeFull(AppConstants.customerList),
      body: json.encode({"organization": orgId}));
  if (response.statusCode == 200) {
    print("Customer list is: " + json.decode(response.body).toString());

    return CustomerList.fromJson(json.decode(response.body));
  } else {
    print("There is an error with status code ${response.statusCode}");
  }
  return customerList;
}
