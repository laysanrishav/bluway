import 'package:blu_way/Functions/network/api_call.dart';
import 'package:blu_way/Functions/network/api_constants.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class GetAllProductModel {
  String status;
  String token;
  List<ProductResult> result;

  GetAllProductModel({this.status, this.token, this.result});

  GetAllProductModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    token = json['token'];
    if (json['result'] != null) {
      result = new List<ProductResult>();
      json['result'].forEach((v) {
        result.add(new ProductResult.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['token'] = this.token;
    if (this.result != null) {
      data['result'] = this.result.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ProductResult {
  ParentGroup parentGroup;
  ParentGroup childGroup;
  ParentGroup brand;
  Unit unit;
  ParentGroup state;
  int active;
  int qty;
  int deleted;
  String sId;
  String name;
  String unitValue;
  String vendor;
  String manufacturingPrice;
  String sellingPriceOrg;
  String sellingPrice;
  String cgst;
  String sgst;
  String igst;
  String batchCode;
  String totalProductPack;
  String totalPackBox;
  String boxHeight;
  String boxLength;
  String boxWidth;
  String city;
  String image;
  String createdDate;
  int iV;

  ProductResult(
      {this.parentGroup,
      this.childGroup,
      this.brand,
      this.unit,
      this.state,
      this.active,
      this.qty,
      this.deleted,
      this.sId,
      this.name,
      this.unitValue,
      this.vendor,
      this.manufacturingPrice,
      this.sellingPriceOrg,
      this.sellingPrice,
      this.cgst,
      this.sgst,
      this.igst,
      this.batchCode,
      this.totalProductPack,
      this.totalPackBox,
      this.boxHeight,
      this.boxLength,
      this.boxWidth,
      this.city,
      this.image,
      this.createdDate,
      this.iV});

  ProductResult.fromJson(Map<String, dynamic> json) {
    parentGroup = json['parent_group'] != null
        ? new ParentGroup.fromJson(json['parent_group'])
        : null;
    childGroup = json['child_group'] != null
        ? new ParentGroup.fromJson(json['child_group'])
        : null;
    brand =
        json['brand'] != null ? new ParentGroup.fromJson(json['brand']) : null;
    unit = json['unit'] != null ? new Unit.fromJson(json['unit']) : null;
    state =
        json['state'] != null ? new ParentGroup.fromJson(json['state']) : null;
    active = json['active'];
    qty = json['qty'];
    deleted = json['deleted'];
    sId = json['_id'];
    name = json['name'];
    unitValue = json['unit_value'];
    vendor = json['vendor'];
    manufacturingPrice = json['manufacturing_price'];
    sellingPriceOrg = json['selling_price_org'];
    sellingPrice = json['selling_price'];
    cgst = json['cgst'];
    sgst = json['sgst'];
    igst = json['igst'];
    batchCode = json['batch_code'];
    totalProductPack = json['total_product_pack'];
    totalPackBox = json['total_pack_box'];
    boxHeight = json['box_height'];
    boxLength = json['box_length'];
    boxWidth = json['box_width'];
    city = json['city'];
    image = json['image'];
    createdDate = json['created_date'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.parentGroup != null) {
      data['parent_group'] = this.parentGroup.toJson();
    }
    if (this.childGroup != null) {
      data['child_group'] = this.childGroup.toJson();
    }
    if (this.brand != null) {
      data['brand'] = this.brand.toJson();
    }
    if (this.unit != null) {
      data['unit'] = this.unit.toJson();
    }
    if (this.state != null) {
      data['state'] = this.state.toJson();
    }
    data['active'] = this.active;
    data['qty'] = this.qty;
    data['deleted'] = this.deleted;
    data['_id'] = this.sId;
    data['name'] = this.name;
    data['unit_value'] = this.unitValue;
    data['vendor'] = this.vendor;
    data['manufacturing_price'] = this.manufacturingPrice;
    data['selling_price_org'] = this.sellingPriceOrg;
    data['selling_price'] = this.sellingPrice;
    data['cgst'] = this.cgst;
    data['sgst'] = this.sgst;
    data['igst'] = this.igst;
    data['batch_code'] = this.batchCode;
    data['total_product_pack'] = this.totalProductPack;
    data['total_pack_box'] = this.totalPackBox;
    data['box_height'] = this.boxHeight;
    data['box_length'] = this.boxLength;
    data['box_width'] = this.boxWidth;
    data['city'] = this.city;
    data['image'] = this.image;
    data['created_date'] = this.createdDate;
    data['__v'] = this.iV;
    return data;
  }
}

class ParentGroup {
  String sId;
  String name;

  ParentGroup({this.sId, this.name});

  ParentGroup.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['name'] = this.name;
    return data;
  }
}

class Unit {
  String sId;
  String unit;

  Unit({this.sId, this.unit});

  Unit.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    unit = json['unit'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['unit'] = this.unit;
    return data;
  }
}

Future<GetAllProductModel> getProductList(String orgId, String token) async {
  GetAllProductModel getAllProductModel;
  var response = await http.post(Uri.encodeFull(AppConstants.allProducts),
      body: json.encode({"org": "$orgId"}),
      headers: {"authorization": "$token"});
  var decodeResponse;
  print("Http post inside Product list has been done successfully");
  if (response.statusCode == 200) {
    decodeResponse = GetAllProductModel.fromJson(json.decode(response.body));
    return GetAllProductModel.fromJson(json.decode(response.body));
  } else {
    print("Post was not successfull and the result status is:" +
        decodeResponse.toString());
  }
  return getAllProductModel;
}
