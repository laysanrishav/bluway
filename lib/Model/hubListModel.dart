import 'dart:convert';
import 'package:http/http.dart' as http;

class HubListModel {
  String status;
  List<HubResult> hubResult;

  HubListModel({this.status, this.hubResult});

  HubListModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['result'] != null) {
      hubResult = new List<HubResult>();
      json['result'].forEach((v) {
        hubResult.add(new HubResult.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.hubResult != null) {
      data['result'] = this.hubResult.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class HubResult {
  Position position;
  int active;
  int deleted;
  String sId;
  String state;
  String district;
  String name;
  String pincode;
  String address1;
  String createdDate;
  int iV;

  HubResult(
      {this.position,
      this.active,
      this.deleted,
      this.sId,
      this.state,
      this.district,
      this.name,
      this.pincode,
      this.address1,
      this.createdDate,
      this.iV});

  HubResult.fromJson(Map<String, dynamic> json) {
    position = json['position'] != null
        ? new Position.fromJson(json['position'])
        : null;
    active = json['active'];
    deleted = json['deleted'];
    sId = json['_id'];
    state = json['state'];
    district = json['district'];
    name = json['name'];
    pincode = json['pincode'];
    address1 = json['address1'];
    createdDate = json['created_date'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.position != null) {
      data['position'] = this.position.toJson();
    }
    data['active'] = this.active;
    data['deleted'] = this.deleted;
    data['_id'] = this.sId;
    data['state'] = this.state;
    data['district'] = this.district;
    data['name'] = this.name;
    data['pincode'] = this.pincode;
    data['address1'] = this.address1;
    data['created_date'] = this.createdDate;
    data['__v'] = this.iV;
    return data;
  }
}

class Position {
  String type;
  List<int> coordinates;

  Position({this.type, this.coordinates});

  Position.fromJson(Map<String, dynamic> json) {
    type = json['type'];
    coordinates = json['coordinates'].cast<int>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['type'] = this.type;
    data['coordinates'] = this.coordinates;
    return data;
  }
}

Future<HubListModel> hubListModel() async {
  final response = await http.post('http://139.59.75.40:4040/app/hub-list');
  print(response.body);
  if (response.statusCode == 200) {
    // If the call to the server was successful, parse the JSON
    print(response.body);

    return HubListModel.fromJson(json.decode(response.body));
    // print("Status OK");
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to load post');
  }
}
