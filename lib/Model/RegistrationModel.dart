// import 'dart:convert';
//
// import 'package:http/http.dart' as http;
// import 'package:shared_preferences/shared_preferences.dart';
//
// // {
// //     "status": "error",
// //     "message": "Phone number alreday in use."
// // }
//
// class Registration {
//   String status;
//   String message;
//   Data data;
//   int oTP;
//
//   Registration({this.status, this.message, this.data, this.oTP});
//
//   Registration.fromJson(Map<String, dynamic> json) {
//     status = json['status'];
//     message = json['message'];
//     data = json['data'] != null ? new Data.fromJson(json['data']) : null;
//     oTP = json['OTP'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['status'] = this.status;
//     data['message'] = this.message;
//     if (this.data != null) {
//       data['data'] = this.data.toJson();
//     }
//     data['OTP'] = this.oTP;
//     return data;
//   }
// }
//
// class Data {
//   String organizationName;
//   String orgType;
//   String orgOtherType;
//   String regNumber;
//   String gstNumber;
//   String bankName;
//   String accountNumber;
//   String ifsc;
//   String hub;
//   String firstName;
//   String lastName;
//   String email;
//   String gender;
//   String dob;
//   String fatherName;
//   String address1;
//   String village;
//   String postOffice;
//   String thana;
//   String country;
//   String state;
//   String district;
//   String pincode;
//   String occupation;
//   String document;
//   String mobile;
//   String documentImage;
//
//   Data(
//       {this.organizationName,
//       this.orgType,
//       this.orgOtherType,
//       this.regNumber,
//       this.gstNumber,
//       this.bankName,
//       this.accountNumber,
//       this.ifsc,
//       this.hub,
//       this.firstName,
//       this.lastName,
//       this.email,
//       this.gender,
//       this.dob,
//       this.fatherName,
//       this.address1,
//       this.village,
//       this.postOffice,
//       this.thana,
//       this.country,
//       this.state,
//       this.district,
//       this.pincode,
//       this.occupation,
//       this.document,
//       this.mobile,
//       this.documentImage});
//
//   Data.fromJson(Map<String, dynamic> json) {
//     organizationName = json['organization_name'];
//     orgType = json['org_type'];
//     orgOtherType = json['org_other_type'];
//     regNumber = json['reg_number'];
//     gstNumber = json['gst_number'];
//     bankName = json['bank_name'];
//     accountNumber = json['account_number'];
//     ifsc = json['ifsc'];
//     hub = json['hub'];
//     firstName = json['first_name'];
//     lastName = json['last_name'];
//     email = json['email'];
//     gender = json['gender'];
//     dob = json['dob'];
//     fatherName = json['father_name'];
//     address1 = json['address1'];
//     village = json['village'];
//     postOffice = json['post_office'];
//     thana = json['thana'];
//     country = json['country'];
//     state = json['state'];
//     district = json['district'];
//     pincode = json['pincode'];
//     occupation = json['occupation'];
//     document = json['document'];
//     mobile = json['mobile'];
//     documentImage = json['document_image'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['organization_name'] = this.organizationName;
//     data['org_type'] = this.orgType;
//     data['org_other_type'] = this.orgOtherType;
//     data['reg_number'] = this.regNumber;
//     data['gst_number'] = this.gstNumber;
//     data['bank_name'] = this.bankName;
//     data['account_number'] = this.accountNumber;
//     data['ifsc'] = this.ifsc;
//     data['hub'] = this.hub;
//     data['first_name'] = this.firstName;
//     data['last_name'] = this.lastName;
//     data['email'] = this.email;
//     data['gender'] = this.gender;
//     data['dob'] = this.dob;
//     data['father_name'] = this.fatherName;
//     data['address1'] = this.address1;
//     data['village'] = this.village;
//     data['post_office'] = this.postOffice;
//     data['thana'] = this.thana;
//     data['country'] = this.country;
//     data['state'] = this.state;
//     data['district'] = this.district;
//     data['pincode'] = this.pincode;
//     data['occupation'] = this.occupation;
//     data['document'] = this.document;
//     data['mobile'] = this.mobile;
//     data['document_image'] = this.documentImage;
//     return data;
//   }
// }
//
// // Future<Registration> resgistrationModel({
// //   // String transporter_name,
//
// //   String organization_name,
// //   String org_type,
// //   String org_other_type,
// //   String reg_number,
// //   String gst_number,
// //   String bank_name,
// //   String account_number,
// //   String ifsc,
// //   String hub,
// //   String first_name,
// //   String last_name,
// //   String email,
// //   String gender,
// //   String dob,
// //   String father_name,
// //   String address1,
// //   String village,
// //   String post_office,
// //   String thana,
// //   String country,
// //   String state,
// //   String district,
// //   String pincode,
// //   String occupation,
// //   String document,
// //   String mobile,
// //   String documentImage,
// // }) async {
// //   var body = jsonEncode({
// //     "organization_name": organization_name,
// //     "org_type": org_type,
// //     "org_other_type": org_type,
// //     "reg_number": reg_number,
// //     "gst_number": gst_number,
// //     "bank_name": bank_name,
// //     "account_number": account_number,
// //     "ifsc": ifsc,
// //     "hub": hub,
// //     "first_name": first_name,
// //     "last_name": last_name,
// //     "email": email,
// //     "gender": gender,
// //     "dob": dob,
// //     "father_name": father_name,
// //     "address1": address1,
// //     "village": village,
// //     "post_office": post_office,
// //     "thana": thana,
// //     "country": country,
// //     "state": state,
// //     "district": district,
// //     "pincode": pincode,
// //     "occupation": occupation,
// //     "document": document,
// //     "mobile": mobile,
// //     "documentImage": documentImage,
// //   });
// //   final response = await http
// //       .post('http://139.59.75.40:4040/app/organization-register', body: body);
// //   print(response.body);
// //   if (response.statusCode == 200) {
// //     SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
//
// //     // If the call to the server was successful, parse the JSON
//
// //     return Registration.fromJson(json.decode(response.body));
// //     // print("Status OK");
// //   } else {
// //     // If that call was not successful, throw an error.
// //     throw Exception('Failed to load post');
// //   }
// // }
