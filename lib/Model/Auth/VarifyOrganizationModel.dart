import 'dart:convert';
import 'package:http/http.dart' as http;

class VarifyOrganizationModel {
  String status;
  String message;
  String token;
  Result result;
  // Organizations orgData;

  VarifyOrganizationModel({this.status, this.message, this.token, this.result});

  VarifyOrganizationModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    token = json['token'];
    result =
        json['result'] != null ? new Result.fromJson(json['result']) : null;
    // orgData =
    // json['orgData'] != null ? new Organizations.fromJson(json['orgData']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    data['token'] = this.token;
    if (this.result != null) {
      data['result'] = this.result.toJson();
    }
    // if (this.orgData != null) {
    //   data['orgData'] = this.result.toJson();
    // }
    return data;
  }
}

class Result {
  String userType;
  int active;
  String sId;
  String firstName;
  String lastName;
  String email;
  String fatherName;
  String village;
  String postOffice;
  String thana;
  String state;
  String district;
  String address;
  String pincode;
  String occupation;
  String document;
  String mobile;
  String createdDate;
  Null profileImage;
  String documentImage;
  Organizations organizations;

  Result(
      {this.userType,
      this.active,
      this.sId,
      this.firstName,
      this.lastName,
      this.email,
      this.fatherName,
      this.village,
      this.postOffice,
      this.thana,
      this.state,
      this.district,
      this.address,
      this.pincode,
      this.occupation,
      this.document,
      this.mobile,
      this.createdDate,
      this.profileImage,
      this.documentImage,
      this.organizations});

  Result.fromJson(Map<String, dynamic> json) {
    userType = json['user_type'];
    active = json['active'];
    sId = json['_id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    email = json['email'];
    fatherName = json['father_name'];
    village = json['village'];
    postOffice = json['post_office'];
    thana = json['thana'];
    state = json['state'];
    district = json['district'];
    address = json['address'];
    pincode = json['pincode'];
    occupation = json['occupation'];
    document = json['document'];
    mobile = json['mobile'];
    createdDate = json['created_date'];
    profileImage = json['profile_image'];
    documentImage = json['document_image'];
    organizations = json['organizations'] != null
        ? new Organizations.fromJson(json['organizations'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_type'] = this.userType;
    data['active'] = this.active;
    data['_id'] = this.sId;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['email'] = this.email;
    data['father_name'] = this.fatherName;
    data['village'] = this.village;
    data['post_office'] = this.postOffice;
    data['thana'] = this.thana;
    data['state'] = this.state;
    data['district'] = this.district;
    data['address'] = this.address;
    data['pincode'] = this.pincode;
    data['occupation'] = this.occupation;
    data['document'] = this.document;
    data['mobile'] = this.mobile;
    data['created_date'] = this.createdDate;
    data['profile_image'] = this.profileImage;
    data['document_image'] = this.documentImage;
    if (this.organizations != null) {
      data['organizations'] = this.organizations.toJson();
    }
    return data;
  }
}

class Organizations {
  Position position;
  int active;
  int deleted;
  String sId;
  String state;
  String district;
  String name;
  String pincode;
  String orgType;
  String orgOtherType;
  String regNumber;
  String gstNumber;
  String hub;
  String createdDate;
  int iV;

  Organizations(
      {this.position,
      this.active,
      this.deleted,
      this.sId,
      this.state,
      this.district,
      this.name,
      this.pincode,
      this.orgType,
      this.orgOtherType,
      this.regNumber,
      this.gstNumber,
      this.hub,
      this.createdDate,
      this.iV});

  Organizations.fromJson(Map<String, dynamic> json) {
    position = json['position'] != null
        ? new Position.fromJson(json['position'])
        : null;
    active = json['active'];
    deleted = json['deleted'];
    sId = json['_id'];
    state = json['state'];
    district = json['district'];
    name = json['name'];
    pincode = json['pincode'];
    orgType = json['org_type'];
    orgOtherType = json['org_other_type'];
    regNumber = json['reg_number'];
    gstNumber = json['gst_number'];
    hub = json['hub'];
    createdDate = json['created_date'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.position != null) {
      data['position'] = this.position.toJson();
    }
    data['active'] = this.active;
    data['deleted'] = this.deleted;
    data['_id'] = this.sId;
    data['state'] = this.state;
    data['district'] = this.district;
    data['name'] = this.name;
    data['pincode'] = this.pincode;
    data['org_type'] = this.orgType;
    data['org_other_type'] = this.orgOtherType;
    data['reg_number'] = this.regNumber;
    data['gst_number'] = this.gstNumber;
    data['hub'] = this.hub;
    data['created_date'] = this.createdDate;
    data['__v'] = this.iV;
    return data;
  }
}

class Position {
  String type;
  List<int> coordinates;

  Position({this.type, this.coordinates});

  Position.fromJson(Map<String, dynamic> json) {
    type = json['type'];
    coordinates = json['coordinates'].cast<int>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['type'] = this.type;
    data['coordinates'] = this.coordinates;
    return data;
  }
}

Future<VarifyOrganizationModel> varifyOrganizationModel({
  String mobileNo,
  String otp,
  String id,
}) async {
  var body = jsonEncode({
    "mobile": mobileNo,
    "otp": otp,
  });
  final response = await http
      .post('http://139.59.75.40:4040/app/verify-organization', body: body);
  print("Login response is: " + response.body);
  if (response.statusCode == 200) {
    // If the call to the server was successful, parse the JSON
    print(response.body);

    return VarifyOrganizationModel.fromJson(json.decode(response.body));
    // print("Status OK");
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to load post');
  }
}
