import 'package:http/http.dart' as http;
import 'dart:convert';

class VarifySEModel {
  String status;
  String message;
  String token;
  Result result;

  VarifySEModel({this.status, this.message, this.token, this.result});

  VarifySEModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    token = json['token'];
    result =
        json['result'] != null ? new Result.fromJson(json['result']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    data['token'] = this.token;
    if (this.result != null) {
      data['result'] = this.result.toJson();
    }
    return data;
  }
}

class Result {
  String userType;
  int active;
  String sId;
  String firstName;
  String lastName;
  String email;
  String fatherName;
  String village;
  String postOffice;
  String thana;
  String state;
  String district;
  String address;
  String pincode;
  String occupation;
  String document;
  String mobile;
  String createdDate;
  Null profileImage;
  String documentImage;
  Null transpor;

  Result(
      {this.userType,
      this.active,
      this.sId,
      this.firstName,
      this.lastName,
      this.email,
      this.fatherName,
      this.village,
      this.postOffice,
      this.thana,
      this.state,
      this.district,
      this.address,
      this.pincode,
      this.occupation,
      this.document,
      this.mobile,
      this.createdDate,
      this.profileImage,
      this.documentImage,
      this.transpor});

  Result.fromJson(Map<String, dynamic> json) {
    userType = json['user_type'];
    active = json['active'];
    sId = json['_id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    village = json['village'];
    postOffice = json['post_office'];
    thana = json['thana'];
    state = json['state'];
    district = json['district'];
    pincode = json['pincode'];
    document = json['document'];
    mobile = json['mobile'];
    createdDate = json['created_date'];
    profileImage = json['profile_image'];
    transpor = json['transpor'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_type'] = this.userType;
    data['user_type'] = this.userType;
    data['active'] = this.active;
    data['_id'] = this.sId;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['email'] = this.email;
    data['father_name'] = this.fatherName;
    data['village'] = this.village;
    data['post_office'] = this.postOffice;
    data['thana'] = this.thana;
    data['state'] = this.state;
    data['district'] = this.district;
    data['address'] = this.address;
    data['pincode'] = this.pincode;
    data['occupation'] = this.occupation;
    data['document'] = this.document;
    data['mobile'] = this.mobile;
    data['created_date'] = this.createdDate;
    data['profile_image'] = this.profileImage;
    data['document_image'] = this.documentImage;
    data['transpor'] = this.transpor;
    return data;
  }
}

Future<VarifySEModel> varifySEModel({
  String mobileNo,
  String otp,
  String id,
}) async {
  var body = jsonEncode({
    "mobile": mobileNo,
    "otp": otp,
  });
  final response =
      await http.post('http://139.59.75.40:4040/app/verify-se', body: body);
  print("Login response is: " + response.body);
  if (response.statusCode == 200) {
    // If the call to the server was successful, parse the JSON
    print(response.body);

    return VarifySEModel.fromJson(json.decode(response.body));
    // print("Status OK");
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to load post');
  }
}
