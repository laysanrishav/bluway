// To parse this JSON data, do
//
//     final loginResponse = loginResponseFromJson(jsonString);
import 'dart:convert';
import 'package:http/http.dart' as http;

class FetchOtp {
  String status;
  int oTP;
  String message;

  FetchOtp({this.status, this.oTP, this.message});

  FetchOtp.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    oTP = json['OTP'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['OTP'] = this.oTP;
    data['message'] = this.message;
    return data;
  }
}

Future<FetchOtp> fetchOrganizationOtpModel({String mobileNo}) async {
  var body = jsonEncode({"mobile": mobileNo});
  final response = await http
      .post('http://139.59.75.40:4040/app/organization-login', body: body);
  print(response.body);
  if (response.statusCode == 200) {
    // If the call to the server was successful, parse the JSON
    print(response.body);

    return FetchOtp.fromJson(json.decode(response.body));
    // print("Status OK");
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to load post');
  }
}

Future<FetchOtp> fetchSupervisorOtpModel({String mobileNo}) async {
  var body = jsonEncode({"mobile": mobileNo});
  final response = await http
      .post('http://139.59.75.40:4040/app/supervisor-login', body: body);
  print(response.body);
  if (response.statusCode == 200) {
    // If the call to the server was successful, parse the JSON
    print(response.body);

    return FetchOtp.fromJson(json.decode(response.body));
    // print("Status OK");
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to load post');
  }
}

Future<FetchOtp> fetchSEOtpModel({String mobileNo}) async {
  var body = jsonEncode({"mobile": mobileNo});
  final response =
      await http.post('http://139.59.75.40:4040/app/se-login', body: body);
  print(response.body);
  if (response.statusCode == 200) {
    // If the call to the server was successful, parse the JSON
    print(response.body);

    return FetchOtp.fromJson(json.decode(response.body));
    // print("Status OK");
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to load post');
  }
}
