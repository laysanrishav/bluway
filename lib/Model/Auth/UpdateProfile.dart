class UpdateProfile {
  String status;
  String message;
  Data data;
  String token;

  UpdateProfile({this.status, this.message, this.data, this.token});

  UpdateProfile.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['token'] = this.token;
    return data;
  }
}

class Data {
  int active;
  String sId;
  String firstName;
  String lastName;
  String dob;
  String fatherName;
  String village;
  String postOffice;
  String thana;
  String state;
  String district;
  String pincode;
  String occupation;
  String document;
  String mobile;
  String createdDate;
  String profileImage;

  Data(
      {this.active,
      this.sId,
      this.firstName,
      this.lastName,
      this.dob,
      this.fatherName,
      this.village,
      this.postOffice,
      this.thana,
      this.state,
      this.district,
      this.pincode,
      this.occupation,
      this.document,
      this.mobile,
      this.createdDate,
      this.profileImage});

  Data.fromJson(Map<String, dynamic> json) {
    active = json['active'];
    sId = json['_id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    dob = json['dob'];
    fatherName = json['father_name'];
    village = json['village'];
    postOffice = json['post_office'];
    thana = json['thana'];
    state = json['state'];
    district = json['district'];
    pincode = json['pincode'];
    occupation = json['occupation'];
    document = json['document'];
    mobile = json['mobile'];
    createdDate = json['created_date'];
    profileImage = json['profile_image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['active'] = this.active;
    data['_id'] = this.sId;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['dob'] = this.dob;
    data['father_name'] = this.fatherName;
    data['village'] = this.village;
    data['post_office'] = this.postOffice;
    data['thana'] = this.thana;
    data['state'] = this.state;
    data['district'] = this.district;
    data['pincode'] = this.pincode;
    data['occupation'] = this.occupation;
    data['document'] = this.document;
    data['mobile'] = this.mobile;
    data['created_date'] = this.createdDate;
    data['profile_image'] = this.profileImage;
    return data;
  }
}
