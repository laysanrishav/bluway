import 'package:blu_way/Functions/network/api_constants.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class DeleteSupervisor {
  String status;
  String message;
  String token;

  DeleteSupervisor({this.status, this.message, this.token});

  DeleteSupervisor.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    data['token'] = this.token;
    return data;
  }
}

Future deleteSupervisor(String id, String token) async {
  var response = await http.post(Uri.encodeFull(AppConstants.deleteSupervisor),
      body: jsonEncode({"id": id}), headers: {"authorization": token});
  if (response.statusCode == 200) {
    Map<String, dynamic> decode = json.decode(response.body);
    // print("Delete output is: " +
    //     decode.status.t +
    //     " with a message" +
    //     decode.message);
    return decode;
  } else {
    print("There is an error");
  }
}
