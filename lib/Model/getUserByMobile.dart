import 'package:blu_way/Functions/network/api_constants.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class GetUserByMobile {
  String status;
  Data data;

  GetUserByMobile({this.status, this.data});

  GetUserByMobile.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  Position position;
  int otp;
  int deleted;
  String sId;
  String fullName;
  String mobile;
  String address;
  String village;
  String postOffice;
  String thana;
  String state;
  String district;
  String pincode;
  String createdDate;
  int iV;

  Data(
      {this.position,
      this.otp,
      this.deleted,
      this.sId,
      this.fullName,
      this.mobile,
      this.address,
      this.village,
      this.postOffice,
      this.thana,
      this.state,
      this.district,
      this.pincode,
      this.createdDate,
      this.iV});

  Data.fromJson(Map<String, dynamic> json) {
    position = json['position'] != null
        ? new Position.fromJson(json['position'])
        : null;
    otp = json['otp'];
    deleted = json['deleted'];
    sId = json['_id'];
    fullName = json['full_name'];
    mobile = json['mobile'];
    address = json['address'];
    village = json['village'];
    postOffice = json['post_office'];
    thana = json['thana'];
    state = json['state'];
    district = json['district'];
    pincode = json['pincode'];
    createdDate = json['created_date'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.position != null) {
      data['position'] = this.position.toJson();
    }
    data['otp'] = this.otp;
    data['deleted'] = this.deleted;
    data['_id'] = this.sId;
    data['full_name'] = this.fullName;
    data['mobile'] = this.mobile;
    data['address'] = this.address;
    data['village'] = this.village;
    data['post_office'] = this.postOffice;
    data['thana'] = this.thana;
    data['state'] = this.state;
    data['district'] = this.district;
    data['pincode'] = this.pincode;
    data['created_date'] = this.createdDate;
    data['__v'] = this.iV;
    return data;
  }
}

class Position {
  String type;
  List<int> coordinates;

  Position({this.type, this.coordinates});

  Position.fromJson(Map<String, dynamic> json) {
    type = json['type'];
    coordinates = json['coordinates'].cast<int>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['type'] = this.type;
    data['coordinates'] = this.coordinates;
    return data;
  }
}

Future<GetUserByMobile> fetchUsers(String id, token) async {
  var response = await http.post(Uri.encodeFull(AppConstants.getCustomerData),
      body: json.encode({"mobile": "3453454353"}),
      headers: {"authorization": token});
  if (response.statusCode == 200) {
    return GetUserByMobile.fromJson(json.decode(response.body));
  } else {
    print("There is an error with status code ${response.statusCode}");
  }
}
