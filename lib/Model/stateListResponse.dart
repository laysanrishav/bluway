// // To parse this JSON data, do
// //
// //     final stateListResponse = stateListResponseFromJson(jsonString);

// // import 'dart:convert';

// // StateListResponse stateListResponseFromJson(String str) =>
// //     StateListResponse.fromJson(json.decode(str));

// // String stateListResponseToJson(StateListResponse data) =>
// //     json.encode(data.toJson());

// // class StateListResponse {
// //   StateListResponse({
// //     this.status,
// //     this.message,
// //     this.data,
// //   });

// //   String status;
// //   String message;
// //   List<Datum> data;

// //   factory StateListResponse.fromJson(Map<String, dynamic> json) =>
// //       StateListResponse(
// //         status: json["status"],
// //         message: json["message"],
// //         data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
// //       );

// //   Map<String, dynamic> toJson() => {
// //         "status": status,
// //         "message": message,
// //         "data": List<dynamic>.from(data.map((x) => x.toJson())),
// //       };
// // }

// // class Datum {
// //   Datum({
// //     this.active,
// //     this.id,
// //     this.name,
// //     this.createdDate,
// //     this.v,
// //   });

// //   int active;
// //   String id;
// //   String name;
// //   DateTime createdDate;
// //   int v;

// //   factory Datum.fromJson(Map<String, dynamic> json) => Datum(
// //         active: json["active"],
// //         id: json["_id"],
// //         name: json["name"],
// //         createdDate: DateTime.parse(json["created_date"]),
// //         v: json["__v"],
// //       );

// //   Map<String, dynamic> toJson() => {
// //         "active": active,
// //         "_id": id,
// //         "name": name,
// //         "created_date": createdDate.toIso8601String(),
// //         "__v": v,
// //       };
// // }

// // Use private fields
// import 'dart:convert';
// import 'package:http/http.dart' as http;

// class StateListResponse {
//   String status;
//   List<Result> result;

//   StateListResponse({this.status, this.result});
//   // static Future<StateListResponse> postState() async {
//   //   final response = await http.post(
//   //     Uri.encodeFull(AppConstants.stateList),
//   //     headers: {
//   //       "accept": "application/json",
//   //     },
//   //   );
//   //   if (response.statusCode == 200) {
//   //     final String responseString = response.body;
//   //     return stateListResponseFromJson(responseString);
//   //   } else {
//   //     return null;
//   //   }
//   // }
//   StateListResponse.fromJson(Map<String, dynamic> json) {
//     status = json['status'];
//     if (json['result'] != null) {
//       result = new List<Result>();
//       json['result'].forEach((v) {
//         result.add(new Result.fromJson(v));
//       });
//     }
//   }

//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['status'] = this.status;
//     if (this.result != null) {
//       data['result'] = this.result.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }

// class Result {
//   int active;
//   int deleted;
//   String sId;
//   String name;
//   String createdDate;

//   Result({this.active, this.deleted, this.sId, this.name, this.createdDate});

//   Result.fromJson(Map<String, dynamic> json) {
//     active = json['active'];
//     deleted = json['deleted'];
//     sId = json['_id'];
//     name = json['name'];
//     createdDate = json['created_date'];
//   }

//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['active'] = this.active;
//     data['deleted'] = this.deleted;
//     data['_id'] = this.sId;
//     data['name'] = this.name;
//     data['created_date'] = this.createdDate;
//     return data;
//   }
// }

// Future<StateListResponse> fetchStateListModel() async {
//   final response = await http.post('http://139.59.75.40:4040/app/state-list');
//   print(response.body);
//   if (response.statusCode == 200) {
//     // If the call to the server was successful, parse the JSON

//     return StateListResponse.fromJson(json.decode(response.body));
//     // print("Status OK");
//   } else {
//     // If that call was not successful, throw an error.
//     throw Exception('Failed to load post');
//   }
// }
