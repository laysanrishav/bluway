import 'dart:convert';
import 'package:blu_way/Functions/network/api_constants.dart';
import 'package:http/http.dart' as http;

class CreateOrderResponse {
  String status;
  String message;
  Data data;
  String token;

  CreateOrderResponse({this.status, this.message, this.data, this.token});

  CreateOrderResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['token'] = this.token;
    return data;
  }
}

class Data {
  Position position;
  Position startingPosition;
  Position endingPosition;
  int deleted;
  int status;
  int otp;
  String sId;
  List<Products> products;
  String organization;
  String se;
  String customer;
  String createdDate;
  int iV;

  Data(
      {this.position,
      this.startingPosition,
      this.endingPosition,
      this.deleted,
      this.status,
      this.otp,
      this.sId,
      this.products,
      this.organization,
      this.se,
      this.customer,
      this.createdDate,
      this.iV});

  Data.fromJson(Map<String, dynamic> json) {
    position = json['position'] != null
        ? new Position.fromJson(json['position'])
        : null;
    startingPosition = json['starting_position'] != null
        ? new Position.fromJson(json['starting_position'])
        : null;
    endingPosition = json['ending_position'] != null
        ? new Position.fromJson(json['ending_position'])
        : null;
    deleted = json['deleted'];
    status = json['status'];
    otp = json['otp'];
    sId = json['_id'];
    if (json['products'] != null) {
      products = new List<Products>();
      json['products'].forEach((v) {
        products.add(new Products.fromJson(v));
      });
    }
    organization = json['organization'];
    se = json['se'];
    customer = json['customer'];
    createdDate = json['created_date'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.position != null) {
      data['position'] = this.position.toJson();
    }
    if (this.startingPosition != null) {
      data['starting_position'] = this.startingPosition.toJson();
    }
    if (this.endingPosition != null) {
      data['ending_position'] = this.endingPosition.toJson();
    }
    data['deleted'] = this.deleted;
    data['status'] = this.status;
    data['otp'] = this.otp;
    data['_id'] = this.sId;
    if (this.products != null) {
      data['products'] = this.products.map((v) => v.toJson()).toList();
    }
    data['organization'] = this.organization;
    data['se'] = this.se;
    data['customer'] = this.customer;
    data['created_date'] = this.createdDate;
    data['__v'] = this.iV;
    return data;
  }
}

class Position {
  String type;
  List<int> coordinates;

  Position({this.type, this.coordinates});

  Position.fromJson(Map<String, dynamic> json) {
    type = json['type'];
    coordinates = json['coordinates'].cast<int>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['type'] = this.type;
    data['coordinates'] = this.coordinates;
    return data;
  }
}

class Products {
  String sId;
  String product;
  int qty;

  Products({this.sId, this.product, this.qty});

  Products.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    product = json['product'];
    qty = json['qty'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['product'] = this.product;
    data['qty'] = this.qty;
    return data;
  }
}

Future<CreateOrderResponse> getCreatedOrderResponse(
    {List<Map<String, String>> products,
    String orgId,
    seId,
    number,
    token}) async {
  CreateOrderResponse createOrderResponse = CreateOrderResponse();
  var response = await http.post(Uri.encodeFull(AppConstants.addCustomerorder),
      body: json.encode({
        "products": products,
        "organization": orgId,
        "se": seId,
        "customer": number,
      }),
      headers: {"authorization": token});
  if (response.statusCode == 200) {
    print("Response of added data is: " + response.body);
    return CreateOrderResponse.fromJson(json.decode(response.body));
  } else {
    print("There is an error");
  }
  return createOrderResponse;
}
