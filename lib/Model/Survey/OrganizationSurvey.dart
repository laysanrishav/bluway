import 'dart:convert';
import 'package:http/http.dart' as http;

class OrganizationSurveyList {
  String status;
  List<OrganizationProducts> organizationProducts;
  List<OrganizationResult> organizationResult;
  String token;

  OrganizationSurveyList(
      {this.status,
      this.organizationProducts,
      this.organizationResult,
      this.token});

  OrganizationSurveyList.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['products'] != null) {
      organizationProducts = new List<OrganizationProducts>();
      json['products'].forEach((v) {
        organizationProducts.add(new OrganizationProducts.fromJson(v));
      });
    }
    if (json['result'] != null) {
      organizationResult = new List<OrganizationResult>();
      json['result'].forEach((v) {
        organizationResult.add(new OrganizationResult.fromJson(v));
      });
    }
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.organizationProducts != null) {
      data['products'] =
          this.organizationProducts.map((v) => v.toJson()).toList();
    }
    if (this.organizationResult != null) {
      data['result'] = this.organizationResult.map((v) => v.toJson()).toList();
    }
    data['token'] = this.token;
    return data;
  }
}

class OrganizationProducts {
  String parentGroup;
  String childGroup;
  String brand;
  String unit;
  String state;
  int active;
  int qty;
  int deleted;
  String sId;
  String name;
  String unitValue;
  String vendor;
  String manufacturingPrice;
  String sellingPriceOrg;
  String sellingPrice;
  String cgst;
  String sgst;
  String igst;
  String batchCode;
  String totalProductPack;
  String totalPackBox;
  String boxHeight;
  String boxLength;
  String boxWidth;
  String city;
  String image;
  String createdDate;
  int iV;

  OrganizationProducts(
      {this.parentGroup,
      this.childGroup,
      this.brand,
      this.unit,
      this.state,
      this.active,
      this.qty,
      this.deleted,
      this.sId,
      this.name,
      this.unitValue,
      this.vendor,
      this.manufacturingPrice,
      this.sellingPriceOrg,
      this.sellingPrice,
      this.cgst,
      this.sgst,
      this.igst,
      this.batchCode,
      this.totalProductPack,
      this.totalPackBox,
      this.boxHeight,
      this.boxLength,
      this.boxWidth,
      this.city,
      this.image,
      this.createdDate,
      this.iV});

  OrganizationProducts.fromJson(Map<String, dynamic> json) {
    parentGroup = json['parent_group'];
    childGroup = json['child_group'];
    brand = json['brand'];
    unit = json['unit'];
    state = json['state'];
    active = json['active'];
    qty = json['qty'];
    deleted = json['deleted'];
    sId = json['_id'];
    name = json['name'];
    unitValue = json['unit_value'];
    vendor = json['vendor'];
    manufacturingPrice = json['manufacturing_price'];
    sellingPriceOrg = json['selling_price_org'];
    sellingPrice = json['selling_price'];
    cgst = json['cgst'];
    sgst = json['sgst'];
    igst = json['igst'];
    batchCode = json['batch_code'];
    totalProductPack = json['total_product_pack'];
    totalPackBox = json['total_pack_box'];
    boxHeight = json['box_height'];
    boxLength = json['box_length'];
    boxWidth = json['box_width'];
    city = json['city'];
    image = json['image'];
    createdDate = json['created_date'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['parent_group'] = this.parentGroup;
    data['child_group'] = this.childGroup;
    data['brand'] = this.brand;
    data['unit'] = this.unit;
    data['state'] = this.state;
    data['active'] = this.active;
    data['qty'] = this.qty;
    data['deleted'] = this.deleted;
    data['_id'] = this.sId;
    data['name'] = this.name;
    data['unit_value'] = this.unitValue;
    data['vendor'] = this.vendor;
    data['manufacturing_price'] = this.manufacturingPrice;
    data['selling_price_org'] = this.sellingPriceOrg;
    data['selling_price'] = this.sellingPrice;
    data['cgst'] = this.cgst;
    data['sgst'] = this.sgst;
    data['igst'] = this.igst;
    data['batch_code'] = this.batchCode;
    data['total_product_pack'] = this.totalProductPack;
    data['total_pack_box'] = this.totalPackBox;
    data['box_height'] = this.boxHeight;
    data['box_length'] = this.boxLength;
    data['box_width'] = this.boxWidth;
    data['city'] = this.city;
    data['image'] = this.image;
    data['created_date'] = this.createdDate;
    data['__v'] = this.iV;
    return data;
  }
}

class OrganizationResult {
  List<String> user;
  int active;
  int deleted;
  String sId;
  Survey survey;
  String surveyName;
  String group;
  String createdDate;
  int iV;

  OrganizationResult(
      {this.user,
      this.active,
      this.deleted,
      this.sId,
      this.survey,
      this.surveyName,
      this.group,
      this.createdDate,
      this.iV});

  OrganizationResult.fromJson(Map<String, dynamic> json) {
    user = json['user'].cast<String>();
    active = json['active'];
    deleted = json['deleted'];
    sId = json['_id'];
    survey =
        json['survey'] != null ? new Survey.fromJson(json['survey']) : null;
    surveyName = json['survey_name'];
    group = json['group'];
    createdDate = json['created_date'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user'] = this.user;
    data['active'] = this.active;
    data['deleted'] = this.deleted;
    data['_id'] = this.sId;
    if (this.survey != null) {
      data['survey'] = this.survey.toJson();
    }
    data['survey_name'] = this.surveyName;
    data['group'] = this.group;
    data['created_date'] = this.createdDate;
    data['__v'] = this.iV;
    return data;
  }
}

class Survey {
  int active;
  int deleted;
  String sId;
  String name;
  List<Fields> fields;
  String createdDate;
  int iV;

  Survey(
      {this.active,
      this.deleted,
      this.sId,
      this.name,
      this.fields,
      this.createdDate,
      this.iV});

  Survey.fromJson(Map<String, dynamic> json) {
    active = json['active'];
    deleted = json['deleted'];
    sId = json['_id'];
    name = json['name'];
    if (json['fields'] != null) {
      fields = new List<Fields>();
      json['fields'].forEach((v) {
        fields.add(new Fields.fromJson(v));
      });
    }
    createdDate = json['created_date'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['active'] = this.active;
    data['deleted'] = this.deleted;
    data['_id'] = this.sId;
    data['name'] = this.name;
    if (this.fields != null) {
      data['fields'] = this.fields.map((v) => v.toJson()).toList();
    }
    data['created_date'] = this.createdDate;
    data['__v'] = this.iV;
    return data;
  }
}

class Fields {
  String filedName;
  String filedType;
  String filedValue;
  String filedRequired;
  List<String> formValuesComma;

  Fields(
      {this.filedName,
      this.filedType,
      this.filedValue,
      this.filedRequired,
      this.formValuesComma});

  Fields.fromJson(Map<String, dynamic> json) {
    filedName = json['filed_name'];
    filedType = json['filed_type'];
    filedValue = json['filed_value'];
    filedRequired = json['filed_required'];
    formValuesComma = json['form_values_comma'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['filed_name'] = this.filedName;
    data['filed_type'] = this.filedType;
    data['filed_value'] = this.filedValue;
    data['filed_required'] = this.filedRequired;
    data['form_values_comma'] = this.formValuesComma;
    return data;
  }
}

Future<OrganizationSurveyList> organizationSurveyList() async {
  final response =
      await http.post('http://139.59.75.40:4040/app/organization-surevy-list');
  print(response.body);
  if (response.statusCode == 200) {
    // If the call to the server was successful, parse the JSON
    print(response.body);

    return OrganizationSurveyList.fromJson(json.decode(response.body));
    // print("Status OK");
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to load post');
  }
}
