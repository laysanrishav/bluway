// // To parse this JSON data, do
// //
// //     final signUpResponse = signUpResponseFromJson(jsonString);
//
// // import 'dart:convert';
//
// // SignUpResponse signUpResponseFromJson(String str) =>
// //     SignUpResponse.fromJson(json.decode(str));
//
// // String signUpResponseToJson(SignUpResponse data) => json.encode(data.toJson());
//
// // // class SignUpResponse {
// // //   SignUpResponse({
// // //     this.status,
// // //     this.message,
// // //     this.data,
// // //     this.otp,
// // //   });
//
// // //   String status;
// // //   String message;
// // //   Data data;
// // //   int otp;
//
// // //   factory SignUpResponse.fromJson(Map<String, dynamic> json) => SignUpResponse(
// // //         status: json["status"],
// // //         message: json["message"],
// // //         data: Data.fromJson(json["data"]),
// // //         otp: json["OTP"],
// // //       );
//
// // //   Map<String, dynamic> toJson() => {
// // //         "status": status,
// // //         "message": message,
// // //         "data": data.toJson(),
// // //         "OTP": otp,
// // //       };
// // // }
//
// // // class Data {
// // //   Data({
// // //     this.userType,
// // //     this.active,
// // //     this.otp,
// // //     this.id,
// // //     this.firstName,
// // //     this.lastName,
// // //     this.gender,
// // //     this.dob,
// // //     this.fatherName,
// // //     this.address1,
// // //     this.village,
// // //     this.postOffice,
// // //     this.thana,
// // //     this.pincode,
// // //     this.occupation,
// // //     this.identity,
// // //     this.document,
// // //     this.mobile,
// // //     this.level,
// // //     this.createdDate,
// // //     this.v,
// // //   });
//
// // //   int userType;
// // //   int active;
// // //   int otp;
// // //   String id;
// // //   String firstName;
// // //   String lastName;
// // //   String gender;
// // //   String dob;
// // //   String fatherName;
// // //   String address1;
// // //   String village;
// // //   String postOffice;
// // //   String thana;
// // //   String pincode;
// // //   String occupation;
// // //   String identity;
// // //   String document;
// // //   String mobile;
// // //   String level;
// // //   DateTime createdDate;
// // //   int v;
//
// // //   factory Data.fromJson(Map<String, dynamic> json) => Data(
// // //         userType: json["user_type"],
// // //         active: json["active"],
// // //         otp: json["otp"],
// // //         id: json["_id"],
// // //         firstName: json["first_name"],
// // //         lastName: json["last_name"],
// // //         gender: json["gender"],
// // //         dob: json["dob"],
// // //         fatherName: json["father_name"],
// // //         address1: json["address1"],
// // //         village: json["village"],
// // //         postOffice: json["post_office"],
// // //         thana: json["thana"],
// // //         pincode: json["pincode"],
// // //         occupation: json["occupation"],
// // //         identity: json["identity"],
// // //         document: json["document"],
// // //         mobile: json["mobile"],
// // //         level: json["level"],
// // //         createdDate: DateTime.parse(json["created_date"]),
// // //         v: json["__v"],
// // //       );
//
// // //   Map<String, dynamic> toJson() => {
// // //         "user_type": userType,
// // //         "active": active,
// // //         "otp": otp,
// // //         "_id": id,
// // //         "first_name": firstName,
// // //         "last_name": lastName,
// // //         "gender": gender,
// // //         "dob": dob,
// // //         "father_name": fatherName,
// // //         "address1": address1,
// // //         "village": village,
// // //         "post_office": postOffice,
// // //         "thana": thana,
// // //         "pincode": pincode,
// // //         "occupation": occupation,
// // //         "identity": identity,
// // //         "document": document,
// // //         "mobile": mobile,
// // //         "level": level,
// // //         "created_date": createdDate.toIso8601String(),
// // //         "__v": v,
// // //       };
// // // }
//
// // class SignUpResponse {
// //   String status;
// //   String message;
// //   UserData data;
// //   int oTP;
//
// //   SignUpResponse({this.status, this.message, this.data, this.oTP});
//
// //   SignUpResponse.fromJson(Map<String, dynamic> json) {
// //     status = json['status'];
// //     message = json['message'];
// //     data = json['data'] != null ? new UserData.fromJson(json['data']) : null;
// //     oTP = json['OTP'];
// //   }
//
// //   Map<String, dynamic> toJson() {
// //     final Map<String, dynamic> data = new Map<String, dynamic>();
// //     data['status'] = this.status;
// //     data['message'] = this.message;
// //     if (this.data != null) {
// //       data['data'] = this.data.toJson();
// //     }
// //     data['OTP'] = this.oTP;
// //     return data;
// //   }
// // }
//
// // class UserData {
// //   int active;
// //   int otp;
// //   int verified;
// //   Null supervisor;
// //   int deleted;
// //   String sId;
// //   String firstName;
// //   String lastName;
// //   String village;
// //   String postOffice;
// //   String thana;
// //   String state;
// //   String district;
// //   String pincode;
// //   String document;
// //   String documentImage;
// //   String mobile;
// //   String userType;
// //   String master;
// //   String masterName;
// //   String createdDate;
// //   int iV;
//
// //   UserData(
// //       {this.active,
// //       this.otp,
// //       this.verified,
// //       this.supervisor,
// //       this.deleted,
// //       this.sId,
// //       this.firstName,
// //       this.lastName,
// //       this.village,
// //       this.postOffice,
// //       this.thana,
// //       this.state,
// //       this.district,
// //       this.pincode,
// //       this.document,
// //       this.documentImage,
// //       this.mobile,
// //       this.userType,
// //       this.master,
// //       this.masterName,
// //       this.createdDate,
// //       this.iV});
//
// //   UserData.fromJson(Map<String, dynamic> json) {
// //     active = json['active'];
// //     otp = json['otp'];
// //     verified = json['verified'];
// //     supervisor = json['supervisor'];
// //     deleted = json['deleted'];
// //     sId = json['_id'];
// //     firstName = json['first_name'];
// //     lastName = json['last_name'];
// //     village = json['village'];
// //     postOffice = json['post_office'];
// //     thana = json['thana'];
// //     state = json['state'];
// //     district = json['district'];
// //     pincode = json['pincode'];
// //     document = json['document'];
// //     documentImage = json['document_image'];
// //     mobile = json['mobile'];
// //     userType = json['user_type'];
// //     master = json['master'];
// //     masterName = json['master_name'];
// //     createdDate = json['created_date'];
// //     iV = json['__v'];
// //   }
//
// //   Map<String, dynamic> toJson() {
// //     final Map<String, dynamic> data = new Map<String, dynamic>();
// //     data['active'] = this.active;
// //     data['otp'] = this.otp;
// //     data['verified'] = this.verified;
// //     data['supervisor'] = this.supervisor;
// //     data['deleted'] = this.deleted;
// //     data['_id'] = this.sId;
// //     data['first_name'] = this.firstName;
// //     data['last_name'] = this.lastName;
// //     data['village'] = this.village;
// //     data['post_office'] = this.postOffice;
// //     data['thana'] = this.thana;
// //     data['state'] = this.state;
// //     data['district'] = this.district;
// //     data['pincode'] = this.pincode;
// //     data['document'] = this.document;
// //     data['document_image'] = this.documentImage;
// //     data['mobile'] = this.mobile;
// //     data['user_type'] = this.userType;
// //     data['master'] = this.master;
// //     data['master_name'] = this.masterName;
// //     data['created_date'] = this.createdDate;
// //     data['__v'] = this.iV;
// //     return data;
// //   }
// // }
//
// import 'dart:convert';
//
// import 'package:http/http.dart' as http;
// import 'package:shared_preferences/shared_preferences.dart';
//
// class RegisterUserModel {
//   String status;
//   String message;
//   UserRegistrationData data;
//   int oTP;
//
//   RegisterUserModel({this.status, this.message, this.data, this.oTP});
//
//   RegisterUserModel.fromJson(Map<String, dynamic> json) {
//     status = json['status'];
//     message = json['message'];
//     data = json['data'] != null ? new UserRegistrationData.fromJson(json['data']) : null;
//     oTP = json['OTP'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['status'] = this.status;
//     data['message'] = this.message;
//     if (this.data != null) {
//       data['data'] = this.data.toJson();
//     }
//     data['OTP'] = this.oTP;
//     return data;
//   }
// }
//
// class UserRegistrationData {
//   int active;
//   int otp;
//   int verified;
//   Null supervisor;
//   int deleted;
//   String sId;
//   String firstName;
//   String lastName;
//   String village;
//   String postOffice;
//   String thana;
//   String state;
//   String district;
//   String pincode;
//   String document;
//   String documentImage;
//   String mobile;
//   String userType;
//   String master;
//   String masterName;
//   String createdDate;
//   int iV;
//
//   UserRegistrationData(
//       {this.active,
//       this.otp,
//       this.verified,
//       this.supervisor,
//       this.deleted,
//       this.sId,
//       this.firstName,
//       this.lastName,
//       this.village,
//       this.postOffice,
//       this.thana,
//       this.state,
//       this.district,
//       this.pincode,
//       this.document,
//       this.documentImage,
//       this.mobile,
//       this.userType,
//       this.master,
//       this.masterName,
//       this.createdDate,
//       this.iV});
//
//   UserRegistrationData.fromJson(Map<String, dynamic> json) {
//     active = json['active'];
//     otp = json['otp'];
//     verified = json['verified'];
//     supervisor = json['supervisor'];
//     deleted = json['deleted'];
//     sId = json['_id'];
//     firstName = json['first_name'];
//     lastName = json['last_name'];
//     village = json['village'];
//     postOffice = json['post_office'];
//     thana = json['thana'];
//     state = json['state'];
//     district = json['district'];
//     pincode = json['pincode'];
//     document = json['document'];
//     documentImage = json['document_image'];
//     mobile = json['mobile'];
//     userType = json['user_type'];
//     master = json['master'];
//     masterName = json['master_name'];
//     createdDate = json['created_date'];
//     iV = json['__v'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['active'] = this.active;
//     data['otp'] = this.otp;
//     data['verified'] = this.verified;
//     data['supervisor'] = this.supervisor;
//     data['deleted'] = this.deleted;
//     data['_id'] = this.sId;
//     data['first_name'] = this.firstName;
//     data['last_name'] = this.lastName;
//     data['village'] = this.village;
//     data['post_office'] = this.postOffice;
//     data['thana'] = this.thana;
//     data['state'] = this.state;
//     data['district'] = this.district;
//     data['pincode'] = this.pincode;
//     data['document'] = this.document;
//     data['document_image'] = this.documentImage;
//     data['mobile'] = this.mobile;
//     data['user_type'] = this.userType;
//     data['master'] = this.master;
//     data['master_name'] = this.masterName;
//     data['created_date'] = this.createdDate;
//     data['__v'] = this.iV;
//     return data;
//   }
// }
//
// Future<RegisterUserModel> resgistrationModel({
//   // String transporter_name,
//
//   // String organization_name,
//   String organizationname,
//   String org_type,
//   String org_other_type,
//   String reg_number,
//   String gst_number,
//   String bank_name,
//   String account_number,
//   String ifsc,
//   String hub,
//   String first_name,
//   String last_name,
//   String email,
//   String gender,
//   String dob,
//   String father_name,
//   String address1,
//   String village,
//   String post_office,
//   String thana,
//   String country,
//   String state,
//   String district,
//   String pincode,
//   String occupation,
//   String document,
//   String mobile,
//   String documentImage,
// }) async {
//   var body = jsonEncode({
//     "organization_name": organizationname,
//     "org_type": org_type,
//     "org_other_type": org_type,
//     "reg_number": reg_number,
//     "gst_number": gst_number,
//     "bank_name": bank_name,
//     "account_number": account_number,
//     "ifsc": ifsc,
//     "hub": hub,
//     "first_name": first_name,
//     "last_name": last_name,
//     "email": email,
//     "gender": gender,
//     "dob": dob,
//     "father_name": father_name,
//     "address1": address1,
//     "village": village,
//     "post_office": post_office,
//     "thana": thana,
//     "country": country,
//     "state": state,
//     "district": district,
//     "pincode": pincode,
//     "occupation": occupation,
//     "document": document,
//     "mobile": mobile,
//     "documentImage": "data:image/png;base64, $documentImage",
//   });
//   final response = await http
//       .post('http://139.59.75.40:4040/app/organization-register', body: body);
//   print(response.body);
//   if (response.statusCode == 200) {
//     SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
//     print(response.body);
//     // if(User)
//
//     // If the call to the server was successful, parse the JSON
//
//     return RegisterUserModel.fromJson(json.decode(response.body));
//     // print("Status OK");
//   } else {
//     // If that call was not successful, throw an error.
//     throw Exception('Failed to load post');
//   }
// }
