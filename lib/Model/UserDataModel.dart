class UserDataModel {
  String status;
  String message;
  UserData data;
  int oTP;

  UserDataModel({this.status, this.message, this.data, this.oTP});

  UserDataModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? new UserData.fromJson(json['data']) : null;
    oTP = json['OTP'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['OTP'] = this.oTP;
    return data;
  }
}

class UserData {
  int active;
  int otp;
  int verified;
  Null supervisor;
  int deleted;
  String sId;
  String firstName;
  String lastName;
  String village;
  String postOffice;
  String thana;
  String state;
  String district;
  String pincode;
  String document;
  String documentImage;
  String mobile;
  String userType;
  String master;
  String masterName;
  String createdDate;
  int iV;

  UserData(
      {this.active,
      this.otp,
      this.verified,
      this.supervisor,
      this.deleted,
      this.sId,
      this.firstName,
      this.lastName,
      this.village,
      this.postOffice,
      this.thana,
      this.state,
      this.district,
      this.pincode,
      this.document,
      this.documentImage,
      this.mobile,
      this.userType,
      this.master,
      this.masterName,
      this.createdDate,
      this.iV});

  UserData.fromJson(Map<String, dynamic> json) {
    active = json['active'];
    otp = json['otp'];
    verified = json['verified'];
    supervisor = json['supervisor'];
    deleted = json['deleted'];
    sId = json['_id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    village = json['village'];
    postOffice = json['post_office'];
    thana = json['thana'];
    state = json['state'];
    district = json['district'];
    pincode = json['pincode'];
    document = json['document'];
    documentImage = json['document_image'];
    mobile = json['mobile'];
    userType = json['user_type'];
    master = json['master'];
    masterName = json['master_name'];
    createdDate = json['created_date'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['active'] = this.active;
    data['otp'] = this.otp;
    data['verified'] = this.verified;
    data['supervisor'] = this.supervisor;
    data['deleted'] = this.deleted;
    data['_id'] = this.sId;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['village'] = this.village;
    data['post_office'] = this.postOffice;
    data['thana'] = this.thana;
    data['state'] = this.state;
    data['district'] = this.district;
    data['pincode'] = this.pincode;
    data['document'] = this.document;
    data['document_image'] = this.documentImage;
    data['mobile'] = this.mobile;
    data['user_type'] = this.userType;
    data['master'] = this.master;
    data['master_name'] = this.masterName;
    data['created_date'] = this.createdDate;
    data['__v'] = this.iV;
    return data;
  }
}
