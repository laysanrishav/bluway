import 'package:blu_way/Constants/ConstantList.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class InputContainerForm extends StatelessWidget {
  String containerName;
  Function onSaved;
  Function validator;
  InputContainerForm(
      {@required this.containerName,
      @required this.onSaved,
      @required this.validator});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: Text(
              containerName,
              style: GoogleFonts.actor(
                fontSize: 25,
                color: Colors.blue,
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.75,
            decoration: kSurveyFormContainerDecoration,
            margin: const EdgeInsets.only(top: 20, bottom: 20),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                decoration: kSurveyFormInputDecoration,
                keyboardType: TextInputType.text,
                validator: validator,
                // initialValue:
                // selectedItems[index],
                onSaved: onSaved,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
