import 'package:blu_way/Constants/Style.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class MyAppBar extends StatelessWidget with PreferredSizeWidget {
  @override
  final Size preferredSize;

  final String title;
  MyAppBar(
    this.title, {
    Key key,
  })  : preferredSize = Size.fromHeight(50.0),
        super(key: key);
  @override
  Widget build(BuildContext context) {
    return AppBar(
      // leading: IconButton(icon: Icon(Icons.arrow_back, color: Colors.white,),
      // onPressed: (){
      //   Navigator.of(context).pop();
      // },),
      flexibleSpace: Container(
        decoration: BoxDecoration(gradient: kAppBarGradient),
        child: SafeArea(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Image.asset('assets/images/Icon-Holder.png'),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 3.0),
                child: Image.asset('assets/images/Header-Logo.png'),
              ),
              SizedBox(
                width: 20,
              ),
              Center(
                child: Text(
                  title,
                  style: GoogleFonts.roboto(
                      color: Color(0xff031e48),
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                ),
              ),
              // SizedBox(
              //   width: 30,
              // ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Image.asset('assets/images/Icon-Holder.png'),
                    Icon(
                      Icons.close,
                      color: Colors.white,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  // @override
  // // TODO: implement preferredSize
  // // Size get preferredSize => throw UnimplementedError();
}
