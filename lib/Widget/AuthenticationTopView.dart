import 'package:flutter/material.dart';

class AuthenticationTopView extends StatelessWidget {
  const AuthenticationTopView({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 0,
      bottom: MediaQuery.of(context).size.height * 0.60,
      left: 0,
      right: 0,
      child: Container(
        width: MediaQuery.of(context).size.width,
        // height: MediaQuery.of(context).size.height*0.40,
        child: Container(
          height: 200,
          width: 200,
          padding: const EdgeInsets.all(24.0),
          child: Image.asset('assets/images/logologin.png'),
        ),
        alignment: Alignment.topRight,
        decoration: BoxDecoration(
            image: DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage('assets/images/logintopsegment.png'))),
      ),
    );
  }
}
