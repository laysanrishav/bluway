// import '../custom_navigation_drawer.dart';
import 'package:flutter/material.dart';

class CollapsingListTile extends StatefulWidget {
  String itemImage;
  String itemName;
  // Function ontap;
  AnimationController animationController;
  CollapsingListTile({@required this.itemImage, @required this.itemName, @required this.animationController });
  @override
  _CollapsingListTileState createState() => _CollapsingListTileState();
}

class _CollapsingListTileState extends State<CollapsingListTile> {

  Animation<double> widthAnimation;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    widthAnimation = Tween<double>(begin: 0 , end: 250).animate(widget.animationController);
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height/13,
  width: widthAnimation.value,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            // SizedBox(width: 10,),
            (widthAnimation.value>=250)?Card(
              elevation: 10,
                shape: RoundedRectangleBorder(
                  // side: BorderSide(color: Colors.white70, width: 1),
                  borderRadius: BorderRadius.circular(150),
                ),
                child: Container(
                    // height: (MediaQuery.of(context).size.height)/23,
                    child: Image.asset(widget.itemImage , fit: BoxFit.contain,))):Container(),
            // (widthAnimation.value>=250)?SizedBox(width: 10,):Container(),
            (widthAnimation.value>=250)?Text(widget.itemName,
              style: TextStyle( fontFamily: 'Roboto',color: Colors.white),
              textAlign: TextAlign.right,):Container(),
            SizedBox(width: 10,),
          ],
        ),
      ),
    );
  }
}


// class CollapsingListTile extends StatefulWidget {
//   final String title;
//   final IconData icon;
//   final AnimationController animationController;
//   final bool isSelected;
//   final Function onTap;
//
//   CollapsingListTile(
//       {@required this.title,
//         @required this.icon,
//         @required this.animationController,
//         this.isSelected = false,
//         this.onTap});
//
//   @override
//   _CollapsingListTileState createState() => _CollapsingListTileState();
// }
//
// class _CollapsingListTileState extends State<CollapsingListTile> {
//   Animation<double> widthAnimation, sizedBoxAnimation;
//
//   @override
//   void initState() {
//     super.initState();
//     widthAnimation =
//         Tween<double>(begin: 200, end: 70).animate(widget.animationController);
//     sizedBoxAnimation =
//         Tween<double>(begin: 10, end: 0).animate(widget.animationController);
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return InkWell(
//       onTap: widget.onTap,
//       child: Container(
//         decoration: BoxDecoration(
//           borderRadius: BorderRadius.all(Radius.circular(16.0)),
//           color: widget.isSelected
//               ? Colors.transparent.withOpacity(0.3)
//               : Colors.transparent,
//         ),
//         width: widthAnimation.value,
//         margin: EdgeInsets.symmetric(horizontal: 8.0),
//         padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
//         child: Row(
//           children: <Widget>[
//             Icon(
//               widget.icon,
//               color: widget.isSelected ? selectedColor : Colors.white30,
//               size: 38.0,
//             ),
//             SizedBox(width: sizedBoxAnimation.value),
//             (widthAnimation.value >= 190)
//                 ? Text(widget.title,
//                 style: widget.isSelected
//                     ? listTitleSelectedTextStyle
//                     : listTitleDefaultTextStyle)
//                 : Container()
//           ],
//         ),
//       ),
//     );
//   }
// }