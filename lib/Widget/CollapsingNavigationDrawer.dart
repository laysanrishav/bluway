import 'package:blu_way/Constants/ConstantList.dart';
import 'package:blu_way/Model/GetAllProductModel.dart';
import 'package:blu_way/Screens/CreateOrder.dart';
import 'package:blu_way/Screens/EditProfile.dart';
import 'package:blu_way/Screens/LoginScreen.dart';
import 'package:blu_way/Screens/sign_out.dart';
import 'package:blu_way/Widget/Dialog.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'CollapsingListTile.dart';

class CollapsingNavigationDrawer extends StatefulWidget {
  AnimationController animationController;
  double maxwidth;
  double minwidth;
  Animation<double> widthAnimation;
  var model;

  CollapsingNavigationDrawer({
    @required this.animationController,
    @required this.minwidth,
    @required this.maxwidth,
    @required this.widthAnimation,
    @required this.model,
  });

  @override
  _CollapsingNavigationDrawerState createState() =>
      _CollapsingNavigationDrawerState();
}

class _CollapsingNavigationDrawerState extends State<CollapsingNavigationDrawer>
    with SingleTickerProviderStateMixin {
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  List<String> names = [
    'StockInHand',
    'CreateOrder',
    'SurveyList',
    'Add Employee',
    'Add Requisition',
    'Return',
    'Report',
    'Track',
    'Forecast',
    'AboutUs',
    'EditProfile',
    'LogOut',
  ];
  String body = ' ';
  String header = ' ';
  List<ProductResult> productList = new List<ProductResult>();

  getDataFromSharedPreference() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    // setState(() {
    body = sharedPreferences.getString("Organization_id");
    header = sharedPreferences.getString("token");

    print("Body is :" + body);
    print("Header is: " + header);
    // return {'body': body, 'header': header};
  }

  @override
  void initState() {
    // TODO: implement initState
    getDataFromSharedPreference();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: widget.animationController,
      builder: (context, widget) {
        return getDrawer(context, widget);
      },
    );
  }

  Widget getDrawer(context, Widget) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
            topRight: Radius.circular(10), bottomRight: Radius.circular(10)),
        color: (widget.widthAnimation.value >= widget.maxwidth)
            ? Color(0xff0062b3)
            : Color(0xffffffff),
      ),
      width: widget.widthAnimation.value,

      child: ListView.separated(
        itemCount: images.length,
        separatorBuilder: (BuildContext context, int index) => Divider(
          color: Colors.white,
        ),
        itemBuilder: (BuildContext ctx, int index) {
          return GestureDetector(
              onTap: () async {
                if (names[index] == 'SurveyList') {
                  Navigator.pushNamed(context, '/${names[index]}');
                } else if (names[index] == "LogOut") {
                  SignOut();
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) {
                    return LoginScreen();
                  }));
                } else if (names[index] == "LogOut") {
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) {
                    return EditProfile(
                      model: widget.model,
                    );
                  }));
                } else if (names[index] == 'CreateOrder') {
                  Set<ProductResult> searchedProductList = Set<ProductResult>();

                  getProductList(body, header).then((value) {
                    productList.addAll(value.result);
                  });

                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (context) {
                    return CreateOrder(
                      orgId: body,
                      token: header,
                      name: "CreateOrder",
                    );
                  }));
                  // });
                } else if (names[index] == 'Add Requisition') {
                  Set<ProductResult> searchedProductList = Set<ProductResult>();

                  getProductList(body, header).then((value) {
                    productList.addAll(value.result);
                  });

                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (context) {
                    return CreateOrder(
                      orgId: body,
                      token: header,
                      name: "Add Requisition",
                    );
                  }));
                  // });
                } else {
                  Navigator.pushNamed(context, '/${names[index]}');
                }
              },
              child: CollapsingListTile(
                  itemImage: images[index],
                  itemName: names[index],
                  animationController: widget.animationController));
        },
      ),
      // child:
    );
  }
}

// class CollapsingNavigationDrawer extends StatefulWidget {
//   @override
//   CollapsingNavigationDrawerState createState() {
//     return new CollapsingNavigationDrawerState();
//   }
// }
//
// class CollapsingNavigationDrawerState extends State<CollapsingNavigationDrawer>
//     with SingleTickerProviderStateMixin {
//   double maxWidth = 210;
//   double minWidth = 70;
//   bool isCollapsed = false;
//   AnimationController _animationController;
//   Animation<double> widthAnimation;
//   int currentSelectedIndex = 0;
//
//   @override
//   void initState() {
//     super.initState();
//     _animationController = AnimationController(
//         vsync: this, duration: Duration(milliseconds: 300));
//     widthAnimation = Tween<double>(begin: maxWidth, end: minWidth)
//         .animate(_animationController);
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return AnimatedBuilder(
//       animation: _animationController,
//       builder: (context, widget) => getWidget(context, widget),
//     );
//   }
//
//   Widget getWidget(context, widget) {
//     return Material(
//       elevation: 80.0,
//       child: Container(
//         width: widthAnimation.value,
//         color: drawerBackgroundColor,
//         child: Column(
//           children: <Widget>[
//             CollapsingListTile(title: 'Techie', icon: Icons.person, animationController: _animationController,),
//             Divider(color: Colors.grey, height: 40.0,),
//             Expanded(
//               child: ListView.separated(
//                 separatorBuilder: (context, counter) {
//                   return Divider(height: 12.0);
//                 },
//                 itemBuilder: (context, counter) {
//                   return CollapsingListTile(
//                     onTap: () {
//                       setState(() {
//                         currentSelectedIndex = counter;
//                       });
//                     },
//                     isSelected: currentSelectedIndex == counter,
//                     title: navigationItems[counter].title,
//                     icon: navigationItems[counter].icon,
//                     animationController: _animationController,
//                   );
//                 },
//                 itemCount: navigationItems.length,
//               ),
//             ),
//             InkWell(
//               onTap: () {
//                 setState(() {
//                   isCollapsed = !isCollapsed;
//                   isCollapsed
//                       ? _animationController.forward()
//                       : _animationController.reverse();
//                 });
//               },
//               child: AnimatedIcon(
//                 icon: AnimatedIcons.close_menu,
//                 progress: _animationController,
//                 color: Colors.amber,
//                 size: 50.0,
//               ),
//             ),
//             SizedBox(
//               height: 50.0,
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
