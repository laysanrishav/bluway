import 'package:flutter/material.dart';

final String kStockInHand = "assets/images/Stock-In-Hand-Icon.png";
final String kCreateOrder = "assets/images/Create-Order-Icon.png";
final String kReturn = "assets/images/Return-01.png";
final String kReport = "assets/images/Report-Icon.png";
final String kTrack = "assets/images/Track-Icon.png";
final String kForecast = "assets/images/Forecast-Icon.png";
final String kAboutus = "assets/images/About-Icon.png";
final String kEditProfile = "assets/images/Profile-Icon.png";
final String kLogOut = "assets/images/LogOut-Icon.png";
final String kActiveSurvey = "assets/images/ActiveSurveyIcon.png";
final String kAddEmployee = "assets/images/driver.png";
final String kAddRequisition = "assets/images/addRequisition.png";
List<String> images = [
  kStockInHand,
  kCreateOrder,
  kActiveSurvey,
  kAddEmployee,
  kAddRequisition,
  kReturn,
  kReport,
  kTrack,
  kForecast,
  kAboutus,
  kEditProfile,
  kLogOut,
];

List<String> names = [
  'STOCK IN HAND',
  'CREATE ORDER',
  'Active Survey',
  'RETURN',
  'REPORT',
  'TRACK',
  'FORECAST',
  'ABOUT US',
  'EDIT PROFILE',
  'LOGOUT',
];

// List<String> userType = ['Organization' , 'Supervisor' , 'Sales Executive'];

class CustomContainer extends StatelessWidget {
  Widget child;
  CustomContainer({this.child});
  @override
  Widget build(BuildContext context) {
    return Container(
      child: child,
      width: MediaQuery.of(context).size.width,
      // height: MediaQuery.of(context).size.height,
      height: 700,
      decoration: BoxDecoration(
          color: Colors.black,
          image: DecorationImage(
            image: AssetImage('assets/images/background.png'),
            fit: BoxFit.cover,
            colorFilter: new ColorFilter.mode(
                Colors.black.withOpacity(0.9), BlendMode.dstATop),
          )),
    );
  }
}

var kSurveyFormContainerDecoration = BoxDecoration(
  borderRadius: BorderRadius.circular(10),
  color: Color(0xffbdd5f1),
);

var kSurveyFormInputDecoration = InputDecoration(
  border: InputBorder.none,
);

// class TextFormInput extends StatelessWidget {
//   final String labelText;
//   bool readOnly = false;
//
//   TextFormInput({
//     Key key,
//     @required this.labelText,
//     this.readOnly,
//   }) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return Column(
//       crossAxisAlignment: CrossAxisAlignment.start,
//       children: [
//         TextFormField(
//           controller: null,
//           readOnly: readOnly,
//           // autovalidateMode: autovalidate,
//           keyboardType: TextInputType.phone,
//           style: TextStyle(
//               fontFamily: 'Arial', color: AppColors.whiteColor, fontSize: 22),
//           validator: (val) {
//             if (val.trim().isEmpty) {
//               return 'Please enter Mobile no';
//             } else if (val.length > 10 || val.length < 10) {
//               return 'Please enter valid mobile No';
//             }
//             return null;
//           },
//           decoration: InputDecoration(
//             labelText: labelText,
//             labelStyle: TextStyle(
//                 fontFamily: 'Shruti',
//                 fontWeight: FontWeight.w600,
//                 fontSize: 20,
//                 color: AppColors.textFeildcolor),
//             border: InputBorder.none,
//           ),
//         ),
//         Container(
//             child: Image.asset(
//               'assets/images/Loginsignup/Line-1.png',
//             )),
//       ],
//     );
//   }
// }
