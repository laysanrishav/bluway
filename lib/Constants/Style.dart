import 'package:blu_way/utlis/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Gradient kAppBarGradient = LinearGradient(colors: [
  Color(0xffffffff),
  Color(0xffc2e9ff),
]);

TextStyle style = GoogleFonts.roboto(
  color: Colors.black,
  fontSize: 20,
);
