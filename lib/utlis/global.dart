import 'package:blu_way/Model/Auth/UserDataModel.dart';
import 'package:blu_way/Model/Auth/VarifyOrganizationModel.dart';
import 'package:blu_way/Model/Auth/VarifySEModel.dart';
import 'package:blu_way/Model/Auth/VarifySupervisorModel.dart';
import 'package:blu_way/Model/Survey/OrganizationSurvey.dart';

class Global {
  static bool isLoading = false;
  static String signupno = '';

  static String otp;

  static UserDataModel userDataModel;

  static String id;

  static VarifyOrganizationModel loginModel;

  static String accessToken;

  static VarifySupervisorModel supervisorModel;

  static VarifySEModel seModel;

  static OrganizationResult orgResultList;
  // static res.SurveyProducts surveyProducts;
}
