import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'Screens/AboutUs.dart';
import 'Screens/CreateOrder.dart';
import 'Screens/EditProfile.dart';
import 'Screens/Forecast.dart';

import 'Screens/HomePage.dart';
import 'Screens/LandingPage.dart';
import 'Screens/LoginScreen.dart';
import 'Screens/Report.dart';
import 'Screens/Return.dart';
import 'Screens/SignupDetails.dart';
import 'Screens/StockInHand.dart';
import 'Screens/Supervisors.dart';
import 'Screens/Survey/Survey.dart';
import 'Screens/Track.dart';
import 'Screens/formSubmitted.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

Future main() async {
  runApp(ProviderScope(
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Blu Way',
      theme: ThemeData(
        unselectedWidgetColor: Color(0xff819ffd),
        backgroundColor: Colors.blue,
        primarySwatch: Colors.blue,
      ),
      initialRoute: 'landing_page',
      routes: {
        'registration': (context) => SignupDetails(),
        'login': (context) => LoginScreen(),
        '/StockInHand': (context) => StockInHand(),
        // '/CreateOrder': (context) => CreateOrder(

        // ),
        '/Return': (context) => Return(),
        '/Report': (context) => Report(),
        '/Track': (context) => Track(),
        '/Forecast': (context) => ForeCast(),
        '/AboutUs': (context) => AboutUs(),
        // '/EditProfile': (context) => EditProfile(),
        '/SurveyList': (context) => Survey(),
        'homepage': (context) => MyHomePage(
              title: "ORGANIZATION",
            ),

        'formsubmitted': (context) => FormSubmitted(),
        'landing_page': (context) => LandingPage(),
        '/Add Employee': (context) => SupervisorsList(),
        // AddEmployee(title: "Add Employee",),
        // '/LogOut': (context) => LoginScreen(),
      },
      // home: MyHomePage(),
    );
  }
}
