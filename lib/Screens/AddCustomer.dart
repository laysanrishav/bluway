import 'dart:convert';
import 'package:blu_way/Constants/ConstantList.dart';
import 'package:blu_way/Functions/network/api_constants.dart';
import 'package:blu_way/Model/DistictList.dart';
import 'package:blu_way/Model/GetSEListResponse.dart';
import 'package:blu_way/Model/StateList.dart';
import 'package:blu_way/Model/createCustomerModel.dart';
import 'package:blu_way/Screens/HomePage.dart';
import 'package:blu_way/Widget/MyAppBar.dart';
import 'package:blu_way/utlis/theme.dart';
import 'package:cool_alert/cool_alert.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:validators/validators.dart';
import 'package:http/http.dart' as http;

class AddCustomer extends StatefulWidget {
  final String orgId;
  final String auth;
  final List<Map<String, String>> product;
  final List<SEResult> seResult;

  AddCustomer({this.orgId, this.auth, this.product, this.seResult});

  @override
  _AddCustomerState createState() => _AddCustomerState();
}

class _AddCustomerState extends State<AddCustomer> {
  StateList stateList = StateList();
  DistrictList distictList = DistrictList();
  String _selectedState = 'Select State';
  List<StateResult> stateresult = [];
  List<DistrictResult> districtresult = [];
  StateResult value;
  DistrictResult test = DistrictResult();
  TextEditingController fullname = new TextEditingController();
  TextEditingController pincode = new TextEditingController();
  TextEditingController mobile = new TextEditingController();

  TextEditingController address = new TextEditingController();

  TextEditingController village = new TextEditingController();

  TextEditingController postOffice = new TextEditingController();

  TextEditingController thana = new TextEditingController();

  String body = ' ';
  String header = ' ';


  Map<String, String> supervisorName = new Map<String, String>();

  Future<Map<String, String>> getDataFromSharedPreference() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    // setState(() {
    body = sharedPreferences.getString("Organization_id");
    header = sharedPreferences.getString("token");

    print("orgId add customer is :" + body);
    print("Header is: " + header);
    return {'body': body, 'header': header};
  }

  String state_id = ' ';
  String district_id = ' ';
  String selectedState;

  final _formkey = GlobalKey<FormState>();
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  @override
  void initState() {
    // TODO: implement initState
    getDataFromSharedPreference();
    fetchStateListModel().then((value) {
      setState(() {
        stateList = value;
        stateresult = stateList.result;
      });
    });
    super.initState();
  }

  Future<CreateCustomerModel> createCustomer() async {
    print("Submit pressed");
    EasyLoading.show(status: 'loading...');
    // Dialogs.showLoadingDialog(
    //     context, _keyLoader);
    var response = await http
        .post(Uri.encodeFull(Uri.encodeFull(AppConstants.createCustomer)),
            body: json.encode({
              "organization": body,
              "full_name": fullname.text,
              "pincode": pincode.text,
              "mobile": mobile.text,
              "address": address.text,
              "village": village.text,
              "post_office": postOffice.text,
              "thana": thana.text,
              "state": state_id,
              "district": district_id,
            }),
            headers: {
          "authorization": header,
        });
    CreateCustomerModel decoded = CreateCustomerModel.fromJson(json.decode(response.body));
    if (response.statusCode == 200) {
      EasyLoading.dismiss().then((_) {
        return EasyLoading.showSuccess('''${decoded.status} \n ${decoded.message}''').then((_) {
          return Navigator.of(context).push(MaterialPageRoute(builder: (context){
            return MyHomePage(title: "ORGANIZER");
          }));
        });
      });

      if (decoded.status == "success") {
        // Navigator.of(_keyLoader.currentContext, rootNavigator: true)
        //     .pop();



      } else {
        EasyLoading.dismiss();
        // Navigator.of(_keyLoader.currentContext, rootNavigator: true)
        //     .pop();
        return CoolAlert.show(
          context: context,
          type: CoolAlertType.error,
          text: decoded.status + "\n" + decoded.message,
        );
      }
      return CreateCustomerModel.fromJson(json.decode(response.body));
    }
    return CreateCustomerModel.fromJson(json.decode(response.body));
  }

  fetchDistrict(String id) {
    fetchDistrictListModel(id).then((value) {
      setState(() {
        distictList = value;
        districtresult = distictList.result;
        print(distictList.result.isEmpty);
        test = distictList.result[0];
      });
    });
  }



  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      builder: EasyLoading.init(),
      home: Scaffold(
        appBar: MyAppBar("Create Customer"),
        body: Form(
            key: _formkey,
            child: CustomContainer(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: ListView(
                  children: [
                    TextFormField(
                      controller: fullname,
                      readOnly: false,
                      // inputFormatters: [
                      //   new LengthLimitingTextInputFormatter(widget.maxlength),
                      // ],
                      // autovalidateMode: autovalidate,
                      keyboardType: TextInputType.name,
                      style: TextStyle(
                          fontFamily: 'Arial',
                          color: AppColors.whiteColor,
                          fontSize: 22),
                      validator: (value) {
                        if (!isAlpha(value)) {
                          return "Enter a valid full name";
                        } else
                          return null;
                      },
                      onSaved: (value) {
                        setState(() {
                          fullname.text = value;
                        });
                      },
                      decoration: InputDecoration(
                        labelText: "Full Name",
                        labelStyle: TextStyle(
                            fontFamily: 'Shruti',
                            fontWeight: FontWeight.w600,
                            fontSize: 20,
                            color: AppColors.textFeildcolor),
                        border: InputBorder.none,
                      ),
                    ),
                    Container(
                        child: Image.asset(
                      'assets/images/Loginsignup/Line-1.png',
                    )),
                    SizedBox(
                      height: 20,
                    ),

                    TextFormField(
                      controller: pincode,
                      // readOnly: false,
                      inputFormatters: [
                        new LengthLimitingTextInputFormatter(6),
                      ],
                      // autovalidateMode: autovalidate,
                      keyboardType: TextInputType.number,
                      style: TextStyle(
                          fontFamily: 'Arial',
                          color: AppColors.whiteColor,
                          fontSize: 22),
                      validator: (value) {
                        if (!isNumeric(value)) {
                          return "Enter a valid pincode name";
                        } else
                          return null;
                      },
                      onSaved: (value) {
                        setState(() {
                          pincode.text = value;
                        });
                      },
                      decoration: InputDecoration(
                        labelText: "PINCODE",
                        labelStyle: TextStyle(
                            fontFamily: 'Shruti',
                            fontWeight: FontWeight.w600,
                            fontSize: 20,
                            color: AppColors.textFeildcolor),
                        border: InputBorder.none,
                      ),
                    ),
                    Container(
                        child: Image.asset(
                      'assets/images/Loginsignup/Line-1.png',
                    )),
                    // TextFormInput(
                    //   textInputType: TextInputType.number,
                    //   labelText: "Pincode",
                    //   maxlength: 6,
                    //   validator: (value) {
                    //     if (!isNumeric(value)) {
                    //       return "Enter a valid last name";
                    //     } else
                    //       return null;
                    //   },
                    //   onSaved: (value) {
                    //     setState(() {
                    //       // last_name = value;
                    //       pincode = value;
                    //     });
                    //   },
                    // ),
                    SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      controller: mobile,
                      inputFormatters: [
                        new LengthLimitingTextInputFormatter(10),
                      ],
                      // autovalidateMode: autovalidate,
                      keyboardType: TextInputType.number,
                      style: TextStyle(
                          fontFamily: 'Arial',
                          color: AppColors.whiteColor,
                          fontSize: 22),
                      validator: (value) {
                        if (!isNumeric(value)) {
                          return "Enter a valid number";
                        } else
                          return null;
                      },
                      onSaved: (value) {
                        setState(() {
                          mobile.text = value;
                        });
                      },
                      decoration: InputDecoration(
                        labelText: "Mobile",
                        labelStyle: TextStyle(
                            fontFamily: 'Shruti',
                            fontWeight: FontWeight.w600,
                            fontSize: 20,
                            color: AppColors.textFeildcolor),
                        border: InputBorder.none,
                      ),
                    ),
                    Container(
                        child: Image.asset(
                      'assets/images/Loginsignup/Line-1.png',
                    )),
                    // TextFormInput(
                    //   textInputType: TextInputType.number,
                    //   labelText: "Mobile",
                    //   maxlength: 10,
                    //   validator: (value) {
                    //     if (!isNumeric(value)) {
                    //       return "Enter a valid last name";
                    //     } else
                    //       return null;
                    //   },
                    //   onSaved: (value) {
                    //     setState(() {
                    //       mobile = value;
                    //       // last_name = value;
                    //     });
                    //   },
                    // ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      'STATE',
                      style: TextStyle(
                          fontFamily: 'Shruti',
                          fontWeight: FontWeight.w600,
                          fontSize: 20,
                          color: AppColors.textFeildcolor),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.80,
                      padding: EdgeInsets.fromLTRB(18, 0, 8, 8),
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: ExactAssetImage(
                                  'assets/images/Loginsignup/Field-01.png'),
                              fit: BoxFit.fill)),
                      child: new DropdownButtonFormField<StateResult>(
                        isDense: true,
                        style: TextStyle(
                            fontFamily: 'Shruti',
                            fontWeight: FontWeight.w600,
                            fontSize: 20,
                            color: AppColors.textFeildcolor),
                        hint: Text('Select State',
                            style: TextStyle(
                                fontFamily: 'Shruti',
                                fontWeight: FontWeight.w600,
                                fontSize: 18,
                                color: AppColors.textFeildcolor)),
                        decoration:
                            InputDecoration(enabledBorder: InputBorder.none),
                        icon: Padding(
                          padding: const EdgeInsets.only(right: 8.0),
                          child: Image.asset(
                            'assets/images/Loginsignup/DropDown-Icon-01.png',
                            height: 15,
                            width: 15,
                          ),
                        ),
                        validator: (value) =>
                            value == null ? 'Feild Required' : null,
                        items: stateresult.map<DropdownMenuItem<StateResult>>(
                            (StateResult item) {
                          return DropdownMenuItem<StateResult>(
                            value: item,
                            child: Text(item.name),
                          );
                        }).toList(),
                        onChanged: (value) {
                          print(value.sId);
                          setState(() {
                            state_id = value.sId;
                            fetchDistrict(state_id);
                          });
                        },
                        onSaved: (value) {
                          print(value.sId);
                          setState(() {
                            state_id = value.sId;
                            // fetchDistrict(state_id);
                          });
                        },
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),

                    Text(
                      'DISTRICT',
                      style: TextStyle(
                          fontFamily: 'Shruti',
                          fontWeight: FontWeight.w600,
                          fontSize: 20,
                          color: AppColors.textFeildcolor),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.80,
                      padding: EdgeInsets.fromLTRB(18, 0, 8, 8),
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: ExactAssetImage(
                                  'assets/images/Loginsignup/Field-01.png'),
                              fit: BoxFit.fill)),
                      child: new DropdownButtonFormField<DistrictResult>(
                        isDense: true,
                        value: test,
                        style: TextStyle(
                            fontFamily: 'Shruti',
                            fontWeight: FontWeight.w600,
                            fontSize: 20,
                            color: AppColors.textFeildcolor),
                        hint: Text('Select District',
                            style: TextStyle(
                                fontFamily: 'Shruti',
                                fontWeight: FontWeight.w600,
                                fontSize: 18,
                                color: AppColors.textFeildcolor)),
                        decoration:
                            InputDecoration(enabledBorder: InputBorder.none),
                        icon: Padding(
                          padding: const EdgeInsets.only(right: 8.0),
                          child: Image.asset(
                            'assets/images/Loginsignup/DropDown-Icon-01.png',
                            height: 15,
                            width: 15,
                          ),
                        ),
                        validator: (value) =>
                            value == null ? 'Feild Required' : null,
                        items: districtresult
                            .map<DropdownMenuItem<DistrictResult>>(
                                (DistrictResult item) {
                          return DropdownMenuItem<DistrictResult>(
                            value: item,
                            child: Text(item.name),
                          );
                        }).toList(),
                        onChanged: state_id.isEmpty
                            ? null
                            : (value) {
                                print(value.sId);
                                setState(() {
                                  test = value;
                                  district_id = value.sId;
                                });
                              },
                        onSaved: state_id.isEmpty
                            ? null
                            : (value) {
                                print(value.sId);
                                setState(() {
                                  test = value;
                                  district_id = value.sId;
                                });
                              },
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      controller: address,
                      readOnly: false,
                      // inputFormatters: [
                      //   new LengthLimitingTextInputFormatter(widget.maxlength),
                      // ],
                      // autovalidateMode: autovalidate,
                      keyboardType: TextInputType.name,
                      style: TextStyle(
                          fontFamily: 'Arial',
                          color: AppColors.whiteColor,
                          fontSize: 22),
                      validator: (value) {
                        if (!isAlpha(value)) {
                          return "Enter a valid address";
                        } else
                          return null;
                      },
                      onSaved: (value) {
                        setState(() {
                          address.text = value;
                        });
                      },
                      decoration: InputDecoration(
                        labelText: "Address",
                        labelStyle: TextStyle(
                            fontFamily: 'Shruti',
                            fontWeight: FontWeight.w600,
                            fontSize: 20,
                            color: AppColors.textFeildcolor),
                        border: InputBorder.none,
                      ),
                    ),
                    Container(
                        child: Image.asset(
                      'assets/images/Loginsignup/Line-1.png',
                    )),
                    // TextFormInput(
                    //   labelText: "Address",
                    //   validator: (value) {
                    //     if (!isAlpha(value)) {
                    //       return "Enter a valid last name";
                    //     } else
                    //       return null;
                    //   },
                    //   onSaved: (value) {
                    //     setState(() {
                    //       // last_name = value;
                    //       address = value;
                    //     });
                    //   },
                    // ),
                    SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      controller: village,
                      keyboardType: TextInputType.name,
                      style: TextStyle(
                          fontFamily: 'Arial',
                          color: AppColors.whiteColor,
                          fontSize: 22),
                      validator: (value) {
                        if (!isAlpha(value)) {
                          return "Enter a valid village name";
                        } else
                          return null;
                      },
                      onSaved: (value) {
                        setState(() {
                          village.text = value;
                        });
                      },
                      decoration: InputDecoration(
                        labelText: "Village",
                        labelStyle: TextStyle(
                            fontFamily: 'Shruti',
                            fontWeight: FontWeight.w600,
                            fontSize: 20,
                            color: AppColors.textFeildcolor),
                        border: InputBorder.none,
                      ),
                    ),
                    Container(
                        child: Image.asset(
                      'assets/images/Loginsignup/Line-1.png',
                    )),
                    // TextFormInput(
                    //   labelText: "Village",
                    //   validator: (value) {
                    //     if (!isAlpha(value)) {
                    //       return "Enter a valid last name";
                    //     } else
                    //       return null;
                    //   },
                    //   onSaved: (value) {
                    //     setState(() {
                    //       village = value;
                    //       // last_name = value;
                    //     });
                    //   },
                    // ),
                    SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      controller: postOffice,
                      // inputFormatters: [
                      //   new LengthLimitingTextInputFormatter(widget.maxlength),
                      // ],
                      // autovalidateMode: autovalidate,
                      keyboardType: TextInputType.name,
                      style: TextStyle(
                          fontFamily: 'Arial',
                          color: AppColors.whiteColor,
                          fontSize: 22),
                      validator: (value) {
                        if (!isAlpha(value)) {
                          return "Enter a valid Post Office Name";
                        } else
                          return null;
                      },
                      onSaved: (value) {
                        setState(() {
                          postOffice.text = value;
                        });
                      },
                      decoration: InputDecoration(
                        labelText: "Post Office",
                        labelStyle: TextStyle(
                            fontFamily: 'Shruti',
                            fontWeight: FontWeight.w600,
                            fontSize: 20,
                            color: AppColors.textFeildcolor),
                        border: InputBorder.none,
                      ),
                    ),
                    Container(
                        child: Image.asset(
                      'assets/images/Loginsignup/Line-1.png',
                    )),
                    // TextFormInput(
                    //   labelText: "Post Office",
                    //   validator: (value) {
                    //     if (!isAlpha(value)) {
                    //       return "Enter a valid last name";
                    //     } else
                    //       return null;
                    //   },
                    //   onSaved: (value) {
                    //     setState(() {
                    //       postOffice = value;
                    //       // last_name = value;
                    //     });
                    //   },
                    // ),
                    SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      controller: thana,
                      readOnly: false,
                      // inputFormatters: [
                      //   new LengthLimitingTextInputFormatter(widget.maxlength),
                      // ],
                      // autovalidateMode: autovalidate,
                      keyboardType: TextInputType.name,
                      style: TextStyle(
                          fontFamily: 'Arial',
                          color: AppColors.whiteColor,
                          fontSize: 22),
                      validator: (value) {
                        if (!isAlpha(value)) {
                          return "Enter a valid thana name";
                        } else
                          return null;
                      },
                      onSaved: (value) {
                        setState(() {
                          thana.text = value;
                        });
                      },
                      decoration: InputDecoration(
                        labelText: "Thana",
                        labelStyle: TextStyle(
                            fontFamily: 'Shruti',
                            fontWeight: FontWeight.w600,
                            fontSize: 20,
                            color: AppColors.textFeildcolor),
                        border: InputBorder.none,
                      ),
                    ),
                    Container(
                        child: Image.asset(
                      'assets/images/Loginsignup/Line-1.png',
                    )),
                    // TextFormInput(
                    //   labelText: "Thana",
                    //   validator: (value) {
                    //     if (!isAlpha(value)) {
                    //       return "Enter a valid last name";
                    //     } else
                    //       return null;
                    //   },
                    //   onSaved: (value) {
                    //     setState(() {
                    //       // last_name = value;
                    //       thana = value;
                    //     });
                    //   },
                    // ),

                    SizedBox(
                      height: 20,
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 30, bottom: 30),
                      child: Center(
                        child: GestureDetector(
                          onTap: () {
                            // addEmployee();

                            if (_formkey.currentState.validate()) {
                              _formkey.currentState.save();
                              // setState(() {
                              //   Dialogs.showLoadingDialog(context, _keyLoader);
                              // });
                              createCustomer();
                            }
                          },
                          child: Container(
                            width: 230,
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: ExactAssetImage(
                                        'assets/images/Loginsignup/BT-1.png'),
                                    fit: BoxFit.fill)),
                            child: Padding(
                              padding: const EdgeInsets.all(15.0),
                              child: Center(
                                child: Text(
                                  'SUBMIT',
                                  style: TextStyle(
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.w600,
                                      color: AppColors.whiteColor,
                                      fontSize: 18),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )),
      ),
    );
  }
}
