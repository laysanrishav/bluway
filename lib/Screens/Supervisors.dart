import 'package:blu_way/Constants/ConstantList.dart';
import 'package:blu_way/Constants/Style.dart';
import 'package:blu_way/Model/Auth/deleteSupervisor.dart';
import 'package:blu_way/Model/Supervisor/GetSupervisorModel.dart';
import 'package:blu_way/Screens/Supervisor/editSupervisor.dart';
import 'package:blu_way/utlis/Country.dart';
import 'package:blu_way/utlis/theme.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:http/http.dart' as http;

import 'dart:convert';
import 'dart:collection';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

import 'AddEmployee.dart';

class SupervisorsList extends StatefulWidget {
  @override
  _SupervisorsListState createState() => _SupervisorsListState();
}

class _SupervisorsListState extends State<SupervisorsList> {
  String body = ' ';
  String header = ' ';
  Key cardkey;
  GetSupervisorModel getSupervisorModel = GetSupervisorModel();
  Map<String, String> supervisorName = new Map<String, String>();

  Future<Map<String, String>> getDataFromSharedPreference() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    // setState(() {
    body = sharedPreferences.getString("Organization_id");
    header = sharedPreferences.getString("token");

    print("Body is :" + body);
    print("Header is: " + header);
    return {'body': body, 'header': header};
  }

  @override
  void initState() {
    // getDataFromSharedPreference();
    // getSupervisorList();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          flexibleSpace: Container(
            decoration: BoxDecoration(gradient: kAppBarGradient),
            child: SafeArea(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Image.asset('assets/images/Icon-Holder.png'),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 3.0),
                    child: Image.asset('assets/images/Header-Logo.png'),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Center(
                    child: Text(
                      "SupervisorsList",
                      style: GoogleFonts.roboto(
                          color: Color(0xff031e48),
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  SizedBox(
                    width: 60,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Image.asset('assets/images/Icon-Holder.png'),
                        Icon(
                          Icons.close,
                          color: Colors.white,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        floatingActionButton: Container(
          height: 80.0,
          width: 80.0,
          child: FloatingActionButton(
            elevation: 10,
            backgroundColor: AppColors.textFeildcolor,
            onPressed: () {
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) {
                return AddEmployee(
                  title: "Add Employee",
                  supervisor: supervisorName,
                  header: header,
                  body: body,
                );
              }));
            },
            isExtended: true,
            child: Center(
              child: Icon(Icons.add),
            ),
          ),
        ),
        body: CustomContainer(
          child: FutureBuilder(
              future: getDataFromSharedPreference(),
              builder: (context, snapshot) {
                if (snapshot.hasData &&
                    snapshot.connectionState == ConnectionState.done) {
                  Map<String, String> bodyData = snapshot.data;

                  return FutureBuilder(
                      future: getSupervisorList(
                          bodyData['body'], bodyData['header']),
                      builder: (con, snap) {
                        // print("")

                        if (snap.hasData) {
                          GetSupervisorModel obj = snap.data;
                          // print("image is:" + obj.result[0].profileImage);
                          if (obj.result.length > 0) {
                            for (int i = 0; i < obj.result.length; i++) {
                              supervisorName = {
                                obj.result[i].sId.toString():
                                    obj.result[i].firstName.toString()
                              };
                            }
                            var info = obj.result;
                          } else {
                            return Container(
                              child: Center(
                                child: Text(
                                  "Sorry No Supervisor Added",
                                  style: style.copyWith(
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            );
                          }
                          return CustomContainer(
                            child: (obj.result.length.toString().isEmpty)
                                ? Container(
                                    child: Center(
                                      child: Text(
                                        "Sorry No Supervisor Added",
                                        style: style.copyWith(
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  )
                                : Container(
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 8.0, vertical: 15),
                                      child: Container(
                                          height: MediaQuery.of(context)
                                              .size
                                              .height,
                                          width:
                                              MediaQuery.of(context).size.width,
                                          decoration: BoxDecoration(
                                            // color: Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(10),
                                          ),
                                          child: ListView.builder(
                                              itemCount: obj.result.length,
                                              itemBuilder: (context, i) {
                                                return GestureDetector(
                                                  onTap: () {},
                                                  child: Dismissible(
                                                    key:
                                                        ValueKey(obj.result[i]),
                                                    confirmDismiss:
                                                        (DismissDirection
                                                            direction) async {
                                                      if (direction ==
                                                          DismissDirection
                                                              .startToEnd) {
                                                        return await showDialog(
                                                          context: context,
                                                          builder: (BuildContext
                                                              context) {
                                                            return AlertDialog(
                                                              title: const Text(
                                                                  "Confirm"),
                                                              content: const Text(
                                                                  "Are you sure you wish to delete this Driver?"),
                                                              actions: <Widget>[
                                                                FlatButton(
                                                                    onPressed: () =>
                                                                        Navigator.of(context).pop(
                                                                            true),
                                                                    child: const Text(
                                                                        "DELETE")),
                                                                FlatButton(
                                                                  onPressed: () =>
                                                                      Navigator.of(
                                                                              context)
                                                                          .pop(
                                                                              false),
                                                                  child: const Text(
                                                                      "CANCEL"),
                                                                ),
                                                              ],
                                                            );
                                                          },
                                                        );
                                                      }
                                                      return await showDialog(
                                                        context: context,
                                                        builder: (BuildContext
                                                            context) {
                                                          return AlertDialog(
                                                            title: const Text(
                                                                "EDIT"),
                                                            content: const Text(
                                                                "Do you want to edit the Driver?"),
                                                            actions: <Widget>[
                                                              FlatButton(
                                                                  onPressed:
                                                                      () {
                                                                    Navigator.of(
                                                                            context)
                                                                        .push(MaterialPageRoute(builder:
                                                                            (context) {
                                                                      return EditSupervisor(
                                                                        address: obj
                                                                            .result[i]
                                                                            .address,
                                                                        district_name: obj
                                                                            .result[i]
                                                                            .district,
                                                                        document: obj
                                                                            .result[i]
                                                                            .document,
                                                                        document_image_data: obj
                                                                            .result[i]
                                                                            .documentImage,
                                                                        email: obj
                                                                            .result[i]
                                                                            .email,
                                                                        fname: obj
                                                                            .result[i]
                                                                            .firstName,
                                                                        id: obj
                                                                            .result[i]
                                                                            .sId,
                                                                        image_data: obj
                                                                            .result[i]
                                                                            .profileImage,
                                                                        licence_number: obj
                                                                            .result[i]
                                                                            .licenceNumber,
                                                                        lname: obj
                                                                            .result[i]
                                                                            .lastName,
                                                                        mobile: obj
                                                                            .result[i]
                                                                            .mobile,
                                                                        pincode: obj
                                                                            .result[i]
                                                                            .pincode,
                                                                        post_office: obj
                                                                            .result[i]
                                                                            .postOffice,
                                                                        state_name: obj
                                                                            .result[i]
                                                                            .state,
                                                                        thana: obj
                                                                            .result[i]
                                                                            .thana,
                                                                        village: obj
                                                                            .result[i]
                                                                            .village,
                                                                      );
                                                                    }));
                                                                  },
                                                                  child: const Text(
                                                                      "EDIT")),
                                                              FlatButton(
                                                                onPressed: () =>
                                                                    Navigator.of(
                                                                            context)
                                                                        .pop(
                                                                            false),
                                                                child: const Text(
                                                                    "CANCEL"),
                                                              ),
                                                            ],
                                                          );
                                                        },
                                                      );
                                                    },
                                                    onDismissed:
                                                        (direction) async {
                                                      deleteSupervisor(
                                                              obj.result[i].sId,
                                                              obj.token)
                                                          .then((value) {
                                                        if (value.status ==
                                                            "success") {
                                                          Toast.show(
                                                            "Deleted Supervisor Successfully",
                                                            context,
                                                            backgroundColor:
                                                                Colors.white,
                                                            textColor:
                                                                Colors.black,
                                                            duration: 5,
                                                          );
                                                        }
                                                      });
                                                      obj.result.removeAt(i);
                                                    },

                                                    // onDismissed: ,

                                                    child: Card(
                                                      color: Color(0xffCBECFE),
                                                      shape:
                                                          RoundedRectangleBorder(
                                                        borderRadius:
                                                            const BorderRadius
                                                                .all(
                                                          Radius.circular(8.0),
                                                        ),
                                                      ),
                                                      key: cardkey,
                                                      elevation: 10,
                                                      child: Container(
                                                        // height: 50,
                                                        margin: EdgeInsets
                                                            .symmetric(
                                                                horizontal: 20,
                                                                vertical: 20),
                                                        width: MediaQuery.of(
                                                                    context)
                                                                .size
                                                                .width *
                                                            .80,
                                                        decoration:
                                                            BoxDecoration(
                                                          color:
                                                              Color(0xffCBECFE),
                                                        ),
                                                        child: Row(
                                                          children: [
                                                            CircleAvatar(
                                                              maxRadius: 40,
                                                              minRadius: 40,
                                                              backgroundColor:
                                                                  Colors
                                                                      .transparent,
                                                              child:
                                                                  Image.network(
                                                                '''http://139.59.75.40:4040/uploads/profile_image/${obj.result[i].profileImage}''',
                                                                fit: BoxFit
                                                                    .contain,
                                                              ),
                                                            ),
                                                            SizedBox(
                                                              width: 10,
                                                            ),
                                                            Column(
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .start,
                                                              children: [
                                                                Container(
                                                                  decoration:
                                                                      BoxDecoration(),
                                                                  child: Text(
                                                                    obj.result[i].firstName
                                                                            .toUpperCase() +
                                                                        " " +
                                                                        obj.result[i]
                                                                            .lastName
                                                                            .toUpperCase(),
                                                                    style: GoogleFonts
                                                                        .roboto(
                                                                      color: Colors
                                                                          .black,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                      fontSize:
                                                                          25,
                                                                    ),
                                                                  ),
                                                                ),
                                                                Container(
                                                                  decoration:
                                                                      BoxDecoration(),
                                                                  child: Row(
                                                                    children: [
                                                                      Text(
                                                                        "Email : ",
                                                                        style: style.copyWith(
                                                                            // fontWeight:
                                                                            //     FontWeight.bold,
                                                                            color: Colors.black,
                                                                            fontSize: 20),
                                                                      ),
                                                                      Text(
                                                                        obj.result[i]
                                                                            .email,
                                                                        style: style.copyWith(
                                                                            // fontWeight:
                                                                            //     FontWeight.bold,
                                                                            color: Colors.black,
                                                                            fontSize: 20),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                                Container(
                                                                  decoration:
                                                                      BoxDecoration(),
                                                                  child: Row(
                                                                    children: [
                                                                      Text(
                                                                        "Mobile : ",
                                                                        style: style.copyWith(
                                                                            // fontWeight:
                                                                            //     FontWeight.bold,
                                                                            color: Colors.black,
                                                                            fontSize: 20),
                                                                      ),
                                                                      Text(
                                                                        obj.result[i]
                                                                            .mobile
                                                                            .toUpperCase(),
                                                                        style: style.copyWith(
                                                                            // fontWeight:
                                                                            //     FontWeight.bold,
                                                                            color: Colors.black,
                                                                            fontSize: 20),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                    background: Container(
                                                      decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(20),
                                                        color: Colors.red,
                                                      ),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .start,
                                                        children: [
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .only(
                                                                    left: 15.0),
                                                            child: Icon(
                                                                Icons.cancel),
                                                          ),
                                                        ],
                                                      ),
                                                    ),

                                                    secondaryBackground:
                                                        Container(
                                                      decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(20),
                                                        color: Colors.yellow,
                                                      ),
                                                    ),
                                                  ),
                                                );
                                              })),
                                    ),
                                  ),
                          );
                        }
                        // else if(snap.data.){

                        // }
                        return Center(
                          child: CircularProgressIndicator(
                            valueColor: new AlwaysStoppedAnimation(
                              Theme.of(context).colorScheme.primary,
                            ),
                          ),
                        );
                      });
                } else {
                  return Center(
                    child: CircularProgressIndicator(
                      valueColor: new AlwaysStoppedAnimation(
                        Theme.of(context).colorScheme.primary,
                      ),
                    ),
                  );
                }
              }),
        ));
  }
}
