import 'package:blu_way/Constants/ConstantList.dart';
import 'package:blu_way/Model/CreateOrderReponseModel.dart';
import 'package:blu_way/Model/CustomerList.dart';
import 'package:blu_way/Model/GetSEListResponse.dart';
import 'package:blu_way/Model/Supervisor/GetSupervisorModel.dart';
import 'package:blu_way/Screens/AddCustomer.dart';
import 'package:blu_way/Screens/HomePage.dart';
import 'package:blu_way/utlis/theme.dart';
import 'package:cool_alert/cool_alert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:google_fonts/google_fonts.dart';

class SubmitOrder extends StatefulWidget {
  final List<Map<String, String>> product;
  final List<SEResult> seResult;
  final String orgId;
  List<Color> activeColor;

  final String token;
  SubmitOrder(
      {@required this.product,
      @required this.seResult,
      @required this.orgId,
      @required this.token});
  @override
  _SubmitOrderState createState() => _SubmitOrderState();
}

class _SubmitOrderState extends State<SubmitOrder> {
  List<SupervisorResult> supervisorList = [];
  List<CustomerResult> customers = new List<CustomerResult>();
  String number = '';

  List<CustomerResult> searchedCustomer = new List<CustomerResult>();
  CustomerList customerList = new CustomerList();
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  submitOrder() async {
    // setState(() {
    //   Dialogs.showLoadingDialog(context, _keyLoader);
    // });
    EasyLoading.show(status: 'loading...');
    if (number.isEmpty) {
      CoolAlert.show(
        context: context,
        type: CoolAlertType.warning,
        text: "Please Select Customer First",
      );
      EasyLoading.dismiss();
      // setState(() {
      // Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
      // });
    } else {
      var response = await getCreatedOrderResponse(
        number: number,
        orgId: widget.orgId,
        products: widget.product,
        token: widget.token,
        seId: seId,
      );
      if (response.status == "success") {
        EasyLoading.dismiss();
        CoolAlert.show(
            context: context,
            type: CoolAlertType.success,
            text: "${response.message}",
            onConfirmBtnTap: () {
              Navigator.of(context)
                  .pushReplacement(MaterialPageRoute(builder: (context) {
                return MyHomePage(
                  title: "ORGANIZATION",
                );
              }));
            });

        print("Created order id is:" +
            "${response.data.products[0].product}" +
            "quantity is" +
            "${response.data.products[0].qty}");
      } else {
        setState(() {
          Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
        });
        CoolAlert.show(
          context: context,
          type: CoolAlertType.error,
          text: "${response.message}",
        );
      }
    }
  }

  String seId = '';
  int length;
  @override
  void initState() {
    print("orgId  inside submit order" + widget.orgId);
    print("token inside submit order" + widget.token);
    // TODO: implement initState
    print("Entered product detail is: " + widget.product.toString());
    getSupervisorList(widget.orgId, widget.token).then((value) {
      print(value.result[0].firstName);
      setState(() {
        supervisorList = value.result;
      });
      getCustomerList(widget.orgId).then((value) {
        setState(() {
          customerList = value;

          customers.addAll(value.result);
          searchedCustomer = customers;
          length = searchedCustomer.length + 1;
          // print("searched customer length is:" + length.toString());
        });
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      builder: EasyLoading.init(),
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        length: 2,
        child: SafeArea(
          child: Scaffold(
            // appBar: AppBar(
            //   bottom: TabBar(
            //     tabs: [
            //       Tab(
            //         child: Container(
            //           child: Text("Customer"),
            //         ),
            //       ),
            //       Tab(
            //         child: Container(
            //           child: Text("Organization"),
            //         ),
            //       )
            //     ],
            //   ),
            // ),
            body: CustomContainer(
              child: Container(
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 8.0, vertical: 15),
                  child: Column(
                    children: [
                      Expanded(
                        child: ListView(
                          children: [
                            Container(
                              height: 500,
                              child: (customerList.status == "success")
                                  ? ListView.builder(
                                      itemCount: searchedCustomer.length + 1,
                                      itemBuilder: (context, index) {
                                        widget.activeColor =
                                            List<Color>(length);
                                        for (int i = 0; i < length; i++) {
                                          widget.activeColor[index] =
                                              Colors.blue[100];
                                        }
                                        return (index == 0)
                                            ? Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: Row(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment
                                                          .baseline,
                                                  children: [
                                                    Expanded(
                                                      flex: 5,
                                                      child: Container(
                                                        decoration:
                                                            BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(6),
                                                          color: Colors.white,
                                                        ),
                                                        height: 50,
                                                        child: Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                  .all(8.0),
                                                          child: TextField(
                                                            keyboardType:
                                                                TextInputType
                                                                    .number,
                                                            style: GoogleFonts
                                                                .roboto(
                                                                    color: Color(
                                                                        0xff3e4368)),
                                                            decoration: InputDecoration(
                                                                border:
                                                                    InputBorder
                                                                        .none,
                                                                hintText:
                                                                    "Enter Mobile Number of Customer",
                                                                hintStyle: GoogleFonts.roboto(
                                                                    color: Color(
                                                                        0xff3e4368),
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold)),
                                                            onChanged: (text) {
                                                              text = text
                                                                  .toLowerCase();
                                                              setState(() {
                                                                searchedCustomer =
                                                                    customers.where(
                                                                        (localCustomers) {
                                                                  var title =
                                                                      localCustomers
                                                                          .mobile
                                                                          .toLowerCase();

                                                                  return title
                                                                      .contains(
                                                                          text);
                                                                }).toList();
                                                              });
                                                            },
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      width: 3,
                                                    ),
                                                    Expanded(
                                                      flex: 1,
                                                      child: Container(
                                                        height: 50,
                                                        decoration: BoxDecoration(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        6),
                                                            color: Color(
                                                                0xff658DB3)),
                                                        child: GestureDetector(
                                                          onTap: () {
                                                            Navigator.of(
                                                                    context)
                                                                .push(MaterialPageRoute(
                                                                    builder:
                                                                        (context) {
                                                              return AddCustomer(
                                                                orgId: widget
                                                                    .orgId,
                                                                auth: widget
                                                                    .token,
                                                                product: widget
                                                                    .product,
                                                                seResult: widget
                                                                    .seResult,
                                                              );
                                                            }));
                                                          },
                                                          child: Row(
                                                            children: [
                                                              Icon(
                                                                Icons.add,
                                                                color: Colors
                                                                    .white,
                                                              ),
                                                              Text(
                                                                "Add",
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold),
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              )
                                            : GestureDetector(
                                                onTap: () {
                                                  setState(() {
                                                    number = searchedCustomer[
                                                            index - 1]
                                                        .mobile;
                                                  });
                                                  setState(() {
                                                    widget.activeColor[index -
                                                        1] = Colors.blue[400];

                                                    if (widget.activeColor[
                                                            index - 1] ==
                                                        Colors.blue[400]) {
                                                      print(
                                                          "Active color changed with contact number $number");
                                                    }
                                                  });
                                                },
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.all(8.0),
                                                  child: _customersList(
                                                    index - 1,
                                                    searchedCustomer,
                                                    number,
                                                    // isTapped,
                                                    widget.activeColor,
                                                  ),
                                                ),
                                              );
                                      })
                                  : Container(
                                      child: Column(
                                        // mainAxisAlignment:
                                        // MainAxisAlignment.spaceEvenly,
                                        children: [
                                          Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.baseline,
                                            children: [
                                              Expanded(
                                                flex: 5,
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            6),
                                                    color: Colors.white,
                                                  ),
                                                  height: 50,
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            8.0),
                                                    child: TextField(
                                                      keyboardType:
                                                          TextInputType.number,
                                                      style: GoogleFonts.roboto(
                                                          color: Color(
                                                              0xff3e4368)),
                                                      decoration: InputDecoration(
                                                          border:
                                                              InputBorder.none,
                                                          hintText:
                                                              "Enter Mobile Number of Customer",
                                                          hintStyle: GoogleFonts
                                                              .roboto(
                                                                  color: Color(
                                                                      0xff3e4368),
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold)),
                                                      onChanged: (text) {
                                                        text =
                                                            text.toLowerCase();
                                                        setState(() {
                                                          searchedCustomer =
                                                              customers.where(
                                                                  (localCustomers) {
                                                            var title =
                                                                localCustomers
                                                                    .mobile
                                                                    .toLowerCase();

                                                            return title
                                                                .contains(text);
                                                          }).toList();
                                                        });
                                                      },
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                width: 3,
                                              ),
                                              Expanded(
                                                flex: 1,
                                                child: Container(
                                                  height: 50,
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              6),
                                                      color: Color(0xff658DB3)),
                                                  child: GestureDetector(
                                                    onTap: () {
                                                      Navigator.of(context).push(
                                                          MaterialPageRoute(
                                                              builder:
                                                                  (context) {
                                                        return AddCustomer(
                                                          orgId: widget.orgId,
                                                          auth: widget.token,
                                                          product:
                                                              widget.product,
                                                          seResult:
                                                              widget.seResult,
                                                        );
                                                      }));
                                                    },
                                                    child: Row(
                                                      children: [
                                                        Icon(
                                                          Icons.add,
                                                          color: Colors.white,
                                                        ),
                                                        Text(
                                                          "Add",
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.white,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold),
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                          SizedBox(
                                            height: 50,
                                          ),
                                          Text(
                                            "Please add a customer",
                                            style: TextStyle(
                                                fontFamily: 'Shruti',
                                                color: Colors.white,
                                                fontSize: 30),
                                          ),
                                        ],
                                      ),
                                    ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.95,
                              padding: EdgeInsets.only(left: 18),
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      color: Colors.white, width: 2)),
                              child:
                                  new DropdownButtonFormField<SupervisorResult>(
                                dropdownColor: Colors.blue[900],
                                isDense: true,
                                style: TextStyle(
                                    fontFamily: 'Shruti',
                                    fontWeight: FontWeight.w600,
                                    fontSize: 20,
                                    color: AppColors.blueColor),
                                hint: Text('Select Supervisor',
                                    style: TextStyle(
                                        fontFamily: 'Shruti',
                                        fontWeight: FontWeight.w600,
                                        fontSize: 18,
                                        color: AppColors.whiteColor)),
                                decoration: InputDecoration(
                                    fillColor: Colors.blue[900],
                                    enabledBorder: InputBorder.none),
                                icon: Padding(
                                  padding: const EdgeInsets.only(right: 8.0),
                                  child: Image.asset(
                                    'assets/images/Loginsignup/DropDown-Icon-01.png',
                                    height: 15,
                                    width: 15,
                                  ),
                                ),
                                validator: (value) =>
                                    value == null ? 'Feild Required' : null,
                                items: supervisorList
                                    .map<DropdownMenuItem<SupervisorResult>>(
                                        (SupervisorResult item) {
                                  return DropdownMenuItem<SupervisorResult>(
                                    value: item,
                                    child: Text(
                                      item.firstName + " " + item.lastName,
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  );
                                }).toList(),
                                onChanged: (value) {
                                  print(value.sId);
                                  setState(() {
                                    seId = value.sId;
                                  });
                                },
                              ),
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: GestureDetector(
                              onTap: () {
                                submitOrder();
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                    color: Colors.blue[900],
                                    borderRadius: BorderRadius.circular(10)),
                                height:
                                    MediaQuery.of(context).size.height * 0.09,
                                child: Center(
                                  child: Text("Submit"),
                                ),
                              )),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

_customersList(
  index,
  List customer,
  String number,
  // List<bool> isTapped,
  List<Color> activeColor,
) {
  return Card(
    child: Container(
      padding: EdgeInsets.only(left: 10),
      decoration: BoxDecoration(
        color: activeColor[index],
        borderRadius: BorderRadius.circular(10),
      ),
      height: 100,
      child: Row(
        children: [
          CircleAvatar(
            maxRadius: 40,
            backgroundColor: Color(0xff6e90bc),
            child: Container(
              width: 60,
              height: 60,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 16.0, top: 8),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  customer[index].fullName,
                  style: GoogleFonts.roboto(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                  ),
                ),
                RichText(
                    text: TextSpan(children: [
                  TextSpan(
                    text: "Mobile: ",
                    style: GoogleFonts.roboto(
                      color: Colors.black,
                      fontSize: 15,
                    ),
                  ),
                  TextSpan(
                    text: customer[index].mobile,
                    style: GoogleFonts.roboto(
                      color: Colors.black,
                      // fontWeight:
                      //     FontWeight
                      //         .bold,
                      fontSize: 15,
                    ),
                  )
                ])),
                RichText(
                    text: TextSpan(children: [
                  TextSpan(
                    text: "Address: ",
                    style: GoogleFonts.roboto(
                      color: Colors.black,
                      fontSize: 15,
                    ),
                  ),
                  TextSpan(
                    text: customer[index].address,
                    style: GoogleFonts.roboto(
                      color: Colors.black,
                      // fontWeight:
                      //     FontWeight
                      //         .bold,
                      fontSize: 15,
                    ),
                  ),
                ])),
                RichText(
                    text: TextSpan(children: [
                  TextSpan(
                    text: "PIN: ",
                    style: GoogleFonts.roboto(
                      color: Colors.black,
                      fontSize: 15,
                    ),
                  ),
                  TextSpan(
                    text: customer[index].pincode,
                    style: GoogleFonts.roboto(
                      color: Colors.black,
                      fontSize: 15,
                    ),
                  )
                ]))
              ],
            ),
          ),
        ],
      ),
    ),
  );
}
