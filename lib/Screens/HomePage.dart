import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:shared_preferences/shared_preferences.dart';

import '../Constants/Style.dart';
import '../Widget/CollapsingNavigationDrawer.dart';
import 'Organization/Update.dart';

class MyHomePage extends StatefulWidget {
  String token;
  var model;
  String title;
  MyHomePage({this.token, this.model, @required this.title});
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
    with SingleTickerProviderStateMixin {
  String title;
  initilizeDate() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString("accessToken", widget.token);
  }

  double maxwidth = 250;
  double minwidth = 0;
  AnimationController _animationController;
  bool isCollapsed = false;
  Animation<double> widthAnimation;
  var height;
  @override
  void initState() {
    // getState();
    // print("Model is: "+ widget.model);
    // TODO: implement initState
    super.initState();
    setState(() {
      title = widget.title;
      print("Title of the page is:" + title);
    });
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 100));
    widthAnimation = Tween<double>(begin: minwidth, end: maxwidth)
        .animate(_animationController);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: Container(),
          flexibleSpace: Container(
            decoration: BoxDecoration(gradient: kAppBarGradient),
            child: SafeArea(
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
                // const EdgeInsets.only(left: 40),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: () {
                        isCollapsed = !isCollapsed;
                        (isCollapsed)
                            ? _animationController.reverse()
                            : _animationController.forward();
                      },
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          Image.asset('assets/images/Icon-Holder.png'),
                          AnimatedIcon(
                            icon: AnimatedIcons.menu_close,
                            color: Colors.white,
                            progress: _animationController,
                          ),
                        ],
                      ),
                    ),
                    Image.asset('assets/images/Header-Logo.png'),
                    Center(
                      child: Text(
                        "BLU WAY $title",
                        style: GoogleFonts.roboto(
                          color: Color(0xff031e48),

                          fontSize: 20,
                          // fontFamily: 'Roboto'
                        ),
                      ),
                    ),
                    // SizedBox(
                    //   width: 60,
                    // ),
                    Stack(
                      alignment: Alignment.center,
                      children: [
                        Image.asset('assets/images/Icon-Holder.png'),
                        IconButton(
                          icon: Icon(Icons.miscellaneous_services),
                          color: Colors.white,
                          onPressed: () {
                            Navigator.of(context)
                                .push(MaterialPageRoute(builder: (context) {
                              return UpdateOrganization();
                            }));
                          },
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        // drawer: CollapsingNavigationDrawer(),
        body: Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      colors: [Color(0xff00005e), Color(0xff00006b)])),
              child: SingleChildScrollView(
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              "Gallery",
                              style: TextStyle(
                                  fontFamily: 'Roboto',
                                  fontSize: 25,
                                  color: Color(0xffaab5dd)),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          decoration: BoxDecoration(
                              border:
                                  Border.all(color: Colors.orange, width: 3),
                              gradient: LinearGradient(colors: [
                                Color(0xff0075c9),
                                Color(0xff003687)
                              ])),
                        ),
                      ),
                      SizedBox(
                        height: 200,
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: 10,
                          itemBuilder: (context, index) {
                            return Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 16.0),
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Colors.lightBlue, width: 3),
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(10)),
                                height: 200,
                                width: 150,
                              ),
                            );
                          },
                        ),
                      ),
                      SizedBox(
                        height: 40,
                      ),
                      Expanded(
                        child: Container(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "WHAT IS LOREM IPSUM?",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 20,
                                          fontFamily: 'Roboto'),
                                      textAlign: TextAlign.start,
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    Text(
                                      "Lorem ipsum is good To enable the details of this specific error message to be viewable on remote machines, please create a <customErrors> tag within a configuration file located in the root directory of the current web application. This <customErrors> tag should then have its mode attribute set to Off",
                                      style: TextStyle(
                                          color: Color(0xffa1d1ff),
                                          fontSize: 15),
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                  ],
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 30.0),
                                  child: Container(
                                    alignment: Alignment.bottomRight,
                                    height: 45,
                                    width: MediaQuery.of(context).size.width *
                                        0.48,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(14),
                                        color: Colors.white,
                                        border: Border.all(
                                            color: Colors.lightBlue, width: 3)),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.orange, width: 3),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            CollapsingNavigationDrawer(
              model: widget.model,
              animationController: _animationController,
              maxwidth: maxwidth,
              minwidth: minwidth,
              widthAnimation: widthAnimation,
            ),
          ],
        ) // This trailing comma makes auto-formatting nicer for build methods.
        );
  }
}
