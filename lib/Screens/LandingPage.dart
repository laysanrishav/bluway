import 'package:flutter/material.dart';

import 'package:shared_preferences/shared_preferences.dart';

import 'HomePage.dart';
import 'LoginScreen.dart';

class LandingPage extends StatefulWidget {
  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  String isSigned = " ";
  Future getPage;

  Future<String> getLandingPage() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool isSigned = sharedPreferences.getBool("isSigned");

    this.isSigned = isSigned.toString();
    // print("User is signed" + this.isSigned.toString());
    return this.isSigned;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getPage = getLandingPage();
  }

  @override
  Widget build(BuildContext context) {
    // (isSigned) ? MyHomePage() : LoginScreen();
    return FutureBuilder(
        future: getLandingPage(),
        builder: (context, snapshot) {
          if (snapshot.hasData != null) {
            // print("snapshot.data: "+snapshot.data);
            if (snapshot.data == "true") {
              // print("snapshot.data: "+snapshot.data);
              return MyHomePage(
                title: "Organization",
              );
            }
            // print("snapshot.data out: "+snapshot.data);
            return LoginScreen();
          }
          return CircularProgressIndicator();
        });
  }
}
