import 'package:blu_way/Constants/ConstantList.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class FormSubmitted extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: CustomContainer(
      child: Center(
        child: Card(
          elevation: 10,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              child: Text("Yeah Form Submitted!",
                  style: GoogleFonts.actor(
                    fontSize: 30,
                    color: Colors.black,
                  )),
            ),
          ),
        ),
      ),
    ));
  }

  moveTohome(BuildContext context) {
    Navigator.pushReplacementNamed(context, 'homepage');
  }
}
