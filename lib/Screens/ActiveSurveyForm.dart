// import 'dart:collection';
// import 'dart:convert';
// // import 'dart:html';
// import 'dart:io';
// import 'dart:io' as Io;
// import 'package:flutter/material.dart';
// import 'package:google_fonts/google_fonts.dart';
// import 'package:image_picker/image_picker.dart';
// import 'package:intl/intl.dart';
// import 'package:geolocator/geolocator.dart';
// import 'package:navigationdrawer/Constants/ConstantList.dart';
// import 'package:navigationdrawer/Constants/Style.dart';
// import 'package:navigationdrawer/Functions/network/api_call.dart';
// import 'package:navigationdrawer/Model/SurveyListResponse.dart';
// import 'package:navigationdrawer/Model/districtListResponse.dart';
// import 'package:navigationdrawer/Model/stateListResponse.dart';
// import 'package:navigationdrawer/Model/surveySubmitResponse.dart';
// import 'package:navigationdrawer/Widget/Dialog.dart';
// import 'package:navigationdrawer/Widget/InputContainerForm.dart';
// import 'package:navigationdrawer/Widget/custom_view.dart';
// import 'package:validators/validators.dart' as validator;

// import 'package:shared_preferences/shared_preferences.dart';

// class ActiveSurveyForm extends StatefulWidget {
//   List surveyForm, products;
//   String surveyName;
//   String surveyId;
//   StateListResponse stateListResponse;
//   String country;
//   String fullName;
//   String address;
//   String village;
//   String postOffice;
//   String thana;
//   String state;
//   String district;
//   String pincode;
//   String mobileNumber;
//   String img64;

//   ActiveSurveyForm(
//       {Key key,
//       @required this.surveyForm,
//       @required this.surveyName,
//       @required this.products,
//       @required this.surveyId,
//       @required this.stateListResponse,
//       @required this.country,
//       @required this.pincode,
//       @required this.thana,
//       @required this.postOffice,
//       @required this.village,
//       @required this.address,
//       @required this.mobileNumber,
//       @required this.district,
//       @required this.fullName,
//       @required this.state,
//       @required this.img64})
//       : super(key: key);
//   @override
//   _ActiveSurveyFormState createState() => _ActiveSurveyFormState();
// }

// class _ActiveSurveyFormState extends State<ActiveSurveyForm> {
//   List _surveyForm, products;
//   final _formKey = GlobalKey<FormState>();
//   int item = 1;
//   List<String> selectedItems;
//   List<String> dropDownValues = new List();
//   final format = DateFormat("yyyy/MM/dd");
//   List<String> getDropDownItems(String val) {
//     return val.split(',');
//   }

//   final GlobalKey<State> _keyLoader = new GlobalKey<State>();

//   List<String> stateList = List();
//   List<String> districtList = List();
//   List<String> pincodeData = List();
//   List<String> listData = List();
//   List<String> idData = List();
//   // List<String> fieldName = ['Full name' , 'address', 'mobile number' , 'village' , 'post office' , 'thana', ' pincode' , 'district' , 'state' , 'country'];
//   // List<String> fieldType = ['Text Box', 'Text Box' , 'Text Box' , 'Text Box', 'Text Box', 'Text Box', 'Text Box' , 'Select Box', 'Select Box' ,'Select Box'];
//   List<String> dynamicFieldName = new List();
//   List<String> dynamicFieldType = new List();
//   // AddDynamicValuesToList()
//   // {
//   //   for(int i=0;i<widget.surveyForm.length;i++)
//   //     {
//   //       dynamicFieldName.add(widget.surveyForm[i].filedName);
//   //       dynamicFieldType.add(widget.surveyForm[i].filedType);
//   //     }
//   // }
//   // AddValuesToMap()
//   // {
//   //   Map<String, String> map;
//   //   map = Map.fromIterables(fieldName, fieldType);
//   //   // map = Map.fromIterables(dynamicFieldName, dynamicFieldType);
//   //   // for(int i=0;i<widget.surveyForm.length;i++)
//   //   // {
//   //   //   map = Map.fromIterable(widget.surveyForm, key: widget.surveyForm[i].filedName ,
//   //   //       value: widget.surveyForm[i].filedType
//   //   //   );
//   //   //   }
//   //
//   //   print("Map value is: "+map[2]);
//   // }
//   String country;
//   String fullName;
//   String address;
//   String village;
//   String postOffice;
//   String thana;
//   String state;
//   String district;
//   String pincode;
//   String mobileNumber;

//   bool isLoading = true;

//   List<String> countryList = List();
//   List<int> bytes;
//   // List listData;

//   List blank;
//   Map<String, Object> inputDoc = new HashMap();
//   List<String> productList = List();
//   String lng, lat;
//   String img64;

//   void getcurrentLocation() async {
//     final position = await GeolocatorPlatform.instance.getCurrentPosition(
//       desiredAccuracy: LocationAccuracy.high,
//     );
//     print(position);

//     setState(() {
//       lng = position.longitude.toString();
//       print(lng);
//       lat = position.latitude.toString();
//       print(lat);
//     });
//   }

//   File _image;
//   Future getImage(BuildContext context) async {
//     showDialog(
//         context: context,
//         builder: (context) {
//           return AlertDialog(
//               content: SizedBox(
//             height: 150,
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.center,
//               mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//               children: <Widget>[
//                 FlatButton(
//                   child: Text(
//                     "Camera",
//                     style: TextStyle(fontSize: 26),
//                   ),
//                   onPressed: () async {
//                     var image = await ImagePicker().getImage(
//                       source: ImageSource.camera,
//                     );
//                     setState(() {
//                       // _image = image;
//                       _image = File(image.path);
//                       print(_image);
//                       List<int> bytes = Io.File(_image.path).readAsBytesSync();
//                       img64 = base64UrlEncode(bytes);
//                       Navigator.pop(context);
//                     });
//                   },
//                 ),
//                 FlatButton(
//                   child: Text(
//                     "Gallery",
//                     style: TextStyle(fontSize: 26, color: Colors.blue),
//                   ),
//                   onPressed: () async {
//                     var image = await ImagePicker().getImage(
//                       source: ImageSource.gallery,
//                       imageQuality: 50,
//                     );
//                     setState(() {
//                       _image = File(image.path);

//                       List<int> bytes = Io.File(_image.path).readAsBytesSync();
//                       img64 = base64UrlEncode(bytes);
//                       print('img64 : $img64');
//                       print('bytes $bytes');
//                       print('image1 : $_image');
//                       Navigator.pop(context);
//                     });
//                   },
//                 ),
//                 _image != null
//                     ? FlatButton(
//                         child: Text(
//                           "Remove Profile",
//                           style: TextStyle(fontSize: 26),
//                         ),
//                         onPressed: () async {
//                           setState(() {
//                             _image = null;
//                             Navigator.pop(context);
//                           });
//                         },
//                       )
//                     : Text(
//                         "",
//                       ),
//               ],
//             ),
//           ));
//         });
//   }

//   String accessToken;
//   getDataFromSharedPrefs() async {
//     SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
//     setState(() {
//       accessToken = sharedPreferences.getString("access_token");
//     });
//   }

//   String id;
//   DistrictListResponse districtListResponse1 = DistrictListResponse();
//   @override
//   void initState() {
//     // AddDynamicValuesToList();
//     // AddValuesToMap();
//     // print("length of map is: " + map.length.toString());
//     getDataFromSharedPrefs();
//     selectedItems = new List<String>(widget.surveyForm.length);

//     for (int i = 0; i < widget.stateListResponse.hubResult.length; i++) {
//       listData.add(widget.stateListResponse.hubResult[i].name);
//       idData.add(widget.stateListResponse.hubResult[i].sId);
//       print('listData :  $listData');
//     }

//     getcurrentLocation();
//     fullName = '';
//     address = '';
//     village = '';
//     postOffice = '';
//     thana = '';
//     // state = '';
//     // district = '';
//     // pincode = '';
//     mobileNumber = '';
//     products = widget.products;
//     _surveyForm = widget.surveyForm;
//     print(_surveyForm.length);

//     super.initState();
//   }

//   @override
//   void dispose() {
//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return IgnorePointer(
//         ignoring: !isLoading,
//         child: Scaffold(
//           appBar: AppBar(
//             flexibleSpace: Container(
//               decoration: BoxDecoration(gradient: kAppBarGradient),
//               child: SafeArea(
//                 child: Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: [
//                     Padding(
//                       padding: const EdgeInsets.all(8.0),
//                       child: Stack(
//                         alignment: Alignment.center,
//                         children: [
//                           Image.asset('assets/images/Icon-Holder.png'),
//                           // Icon(Icons.chevron_left_sharp, color: Colors.white,),
//                         ],
//                       ),
//                     ),
//                     Padding(
//                       padding: const EdgeInsets.symmetric(vertical: 3.0),
//                       child: Image.asset('assets/images/Header-Logo.png'),
//                     ),
//                     SizedBox(
//                       width: 20,
//                     ),
//                     Center(
//                       child: Text(
//                         "Survey Form",
//                         style: GoogleFonts.roboto(
//                             color: Color(0xff031e48),
//                             fontSize: 20,
//                             fontWeight: FontWeight.bold
//                             // fontFamily: 'Roboto'
//                             ),
//                       ),
//                     ),
//                     SizedBox(
//                       width: 60,
//                     ),
//                     Padding(
//                       padding: const EdgeInsets.all(8.0),
//                       child: Stack(
//                         alignment: Alignment.center,
//                         children: [
//                           Image.asset('assets/images/Icon-Holder.png'),
//                           Icon(
//                             Icons.close,
//                             color: Colors.white,
//                           ),
//                         ],
//                       ),
//                     ),
//                   ],
//                 ),
//               ),
//             ),
//           ),
//           body: CustomContainer(
//             child: Padding(
//               padding:
//                   const EdgeInsets.symmetric(horizontal: 10.0, vertical: 15),
//               child: Container(
//                 decoration: BoxDecoration(
//                     borderRadius: BorderRadius.circular(10),
//                     color: Colors.white),
//                 child: Form(
//                   key: _formKey,
//                   child: Container(
//                     height: MediaQuery.of(context).size.height,
//                     child: Column(
//                       // crossAxisAlignment: CrossAxisAlignment.start,
//                       children: [
//                         // Padding(
//                         Expanded(
//                           child: ListView.builder(
//                               itemCount: _surveyForm.length,
//                               itemBuilder: (context, index) {
//                                 if (_surveyForm[index].filedType ==
//                                     'Text Box') {
//                                   return Padding(
//                                     padding: const EdgeInsets.symmetric(
//                                         horizontal: 10.0, vertical: 10),
//                                     child: Column(
//                                       crossAxisAlignment:
//                                           CrossAxisAlignment.start,
//                                       children: [
//                                         Container(
//                                           child: Text(
//                                             _surveyForm[index].filedName,
//                                             style: GoogleFonts.actor(
//                                               fontSize: 25,
//                                               color: Colors.blue,
//                                             ),
//                                           ),
//                                         ),
//                                         Container(
//                                           width: MediaQuery.of(context)
//                                                   .size
//                                                   .width *
//                                               0.90,
//                                           decoration:
//                                               kSurveyFormContainerDecoration,
//                                           margin: const EdgeInsets.only(
//                                               top: 20, bottom: 20),
//                                           child: Padding(
//                                             padding: const EdgeInsets.all(8.0),
//                                             child: TextFormField(
//                                               decoration:
//                                                   kSurveyFormInputDecoration,
//                                               keyboardType: TextInputType.text,
//                                               validator: (value) {
//                                                 if (value.length <= 0) {
//                                                   return "Enter a valid ${_surveyForm[index].filedName}";
//                                                 } else
//                                                   return null;
//                                               },
//                                               initialValue:
//                                                   selectedItems[index],
//                                               onSaved: (value) {
//                                                 selectedItems[index] = value;
//                                                 print(selectedItems);
//                                               },
//                                             ),
//                                           ),
//                                         ),
//                                       ],
//                                     ),
//                                   );
//                                 } else if (_surveyForm[index].filedType ==
//                                     'Select Box') {
//                                   return Padding(
//                                     padding: const EdgeInsets.symmetric(
//                                         horizontal: 10.0),
//                                     child: Column(
//                                       crossAxisAlignment:
//                                           CrossAxisAlignment.start,
//                                       children: [
//                                         Container(
//                                           child: Text(
//                                             _surveyForm[index].filedName,
//                                             style: GoogleFonts.actor(
//                                               fontSize: 25,
//                                               color: Colors.blue,
//                                             ),
//                                           ),
//                                         ),
//                                         Container(
//                                           margin: const EdgeInsets.only(
//                                               top: 20, bottom: 20),
//                                           width: MediaQuery.of(context)
//                                                   .size
//                                                   .width *
//                                               0.90,
//                                           decoration:
//                                               kSurveyFormContainerDecoration,
//                                           child: Padding(
//                                             padding: const EdgeInsets.all(8.0),
//                                             child: DropdownButton<String>(
//                                               // value: selectedValue,
//                                               hint: Text(
//                                                 _surveyForm[index].filedName,
//                                                 style: TextStyle(
//                                                     fontSize: 20,
//                                                     color: Colors.blue),
//                                               ),
//                                               items: getDropDownItems(
//                                                 _surveyForm[index]
//                                                     .filedValue
//                                                     .toString(),
//                                               ).map((dynamic val) {
//                                                 return DropdownMenuItem(
//                                                     value: val.toString(),
//                                                     child:
//                                                         Text(val.toString()));
//                                               }).toList(),
//                                               value: selectedItems.isEmpty
//                                                   ? _surveyForm[index].filedName
//                                                   : selectedItems[index],
//                                               onChanged: (value) {
//                                                 setState(() {
//                                                   selectedItems[index] = value;
//                                                 });
//                                               },
//                                               // value: ,
//                                             ),
//                                           ),
//                                         ),
//                                       ],
//                                     ),
//                                   );
//                                 } else if (_surveyForm[index].filedType ==
//                                     'Radio Box') {
//                                   return Radio(
//                                       // value: _surveyForm[index].filedValue.toString(),
//                                       // onChanged: (value){
//                                       //
//                                       // },
//                                       );
//                                 } else {
//                                   return Checkbox(value: null, onChanged: null);
//                                 }
//                               }),
//                         ),
//                         Align(
//                           alignment: Alignment.bottomRight,
//                           child: Container(
//                               width: MediaQuery.of(context).size.width * 0.40,
//                               margin:
//                                   const EdgeInsets.only(right: 20, bottom: 20),
//                               alignment: Alignment.bottomRight,
//                               child: GestureDetector(
//                                 onTap: () async {
//                                   if (_formKey.currentState.validate()) {
//                                     _formKey.currentState.save();
//                                     Map<String, dynamic> formData =
//                                         new Map<String, dynamic>();
//                                     for (int i = 0;
//                                         i < _surveyForm.length;
//                                         i++) {
//                                       formData.putIfAbsent(
//                                           _surveyForm[i].filedName,
//                                           () => selectedItems[i]);
//                                     }

//                                     String products = "";

//                                     List<String> pTemp = new List<String>();
//                                     for (Product temp in widget.products) {
//                                       pTemp.add(temp.name);
//                                     }

//                                     products = pTemp.join(",");
//                                     print("Final product => " + products);

//                                     String qty = "1";

//                                     Dialogs.showLoadingDialog(
//                                         context, _keyLoader);
//                                     print("surveyId: " + widget.surveyId);
//                                     print("fullName: " + widget.fullName);
//                                     print("address: " + widget.address);
//                                     print("village: " + widget.village);
//                                     print("postOffice: " + widget.postOffice);
//                                     print("thana: " + widget.thana);
//                                     print("country: " + widget.country);
//                                     print("state: " + widget.state);
//                                     print("district: " + widget.district);
//                                     print("pincode: " + widget.pincode);
//                                     print(
//                                         "mobileNumber: " + widget.mobileNumber);
//                                     print("formData: $formData");
//                                     print("products: $products");
//                                     print("qty: $qty");
//                                     print("lng: $lng");
//                                     print("lat: $lat");
//                                     String reqImage =
//                                         'data:image/png;base64, ' +
//                                                     widget.img64 ==
//                                                 null
//                                             ? ''
//                                             : widget.img64;
//                                     print("image: $reqImage");
//                                     SurveySubmitResponse surveySubmitResponse =
//                                         await ApiCall.getSurveySubmit(
//                                             surveyId: widget.surveyId,
//                                             fullName: widget.fullName,
//                                             address: widget.address,
//                                             village: widget.village,
//                                             postOffice: widget.postOffice,
//                                             thana: widget.thana,
//                                             country: widget.country,
//                                             state: widget.state,
//                                             district: widget.district,
//                                             pincode: widget.pincode,
//                                             mobileNumber: widget.mobileNumber,
//                                             formData: formData,
//                                             products: products,
//                                             qty: qty,
//                                             lng: lng,
//                                             lat: lat,
//                                             image: reqImage);
//                                     if (surveySubmitResponse.status ==
//                                         "success") {
//                                       Navigator.of(_keyLoader.currentContext,
//                                               rootNavigator: true)
//                                           .pop();
//                                       print("Survey Submitted => " +
//                                           surveySubmitResponse.message);
//                                       print("Survey Result => " +
//                                           surveySubmitResponse.token);
//                                       Navigator.pushReplacementNamed(
//                                           context, 'formsubmitted');
//                                     } else {
//                                       Navigator.of(_keyLoader.currentContext,
//                                               rootNavigator: true)
//                                           .pop();
//                                       if (surveySubmitResponse != null) {
//                                         print("Survey Error => " +
//                                             surveySubmitResponse.status);
//                                         print("Survey Error => " +
//                                             surveySubmitResponse.message
//                                                 .toString());
//                                       } else
//                                         print("respnse is null");
//                                     }
//                                   }
//                                 },
//                                 child: Container(
//                                   decoration: BoxDecoration(
//                                     borderRadius: BorderRadius.circular(10),
//                                     color: Colors.blue[800],
//                                   ),
//                                   child: Padding(
//                                     padding: const EdgeInsets.all(16.0),
//                                     child: Text(
//                                       "NEXT",
//                                       style: GoogleFonts.actor(
//                                         color: Colors.white,
//                                         fontSize: 20,
//                                       ),
//                                     ),
//                                   ),
//                                 ),
//                               )),
//                         )
//                       ],
//                     ),
//                   ),
//                 ),
//               ),
//             ),
//           ),
//         ));
//   }
// }
