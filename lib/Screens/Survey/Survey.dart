import 'package:blu_way/Model/Survey/OrganizationSurvey.dart';
import 'package:blu_way/Widget/MyAppBar.dart';
import 'package:blu_way/main.dart';
import 'package:blu_way/utlis/Size_config.dart';
import 'package:blu_way/utlis/global.dart';
import 'package:blu_way/utlis/theme.dart';
import 'package:flutter/material.dart';

import 'package:toast/toast.dart';

import 'staticSurveyForm.dart';

class Survey extends StatefulWidget {
  Survey({Key key}) : super(key: key);

  @override
  _SurveyState createState() => _SurveyState();
}

class _SurveyState extends State<Survey> {
  OrganizationSurveyList orgList = OrganizationSurveyList();

  @override
  void initState() {
    super.initState();
    // orgList.organizationResult
    initdata();
  }

  initdata() {
    organizationSurveyList().then((value) {
      if (value.status == 'success') {
        setState(() {
          orgList = value;
        });
      } else {
        Toast.show(value.status, context);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: SafeArea(
        child: Scaffold(
            backgroundColor: Colors.white,
            appBar: MyAppBar("SURVEY LIST"),
            body: FutureBuilder(
              future: organizationSurveyList(),
              builder: (context, snapshot) {
                return !snapshot.hasData
                    ? Container(
                        child: Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              CircularProgressIndicator(),
                              Text(
                                "Fetching the List",
                                style:
                                    TextStyle(color: Colors.blue, fontSize: 30),
                              )
                            ],
                          ),
                        ),
                      )
                    : (LayoutBuilder(
                        builder: (BuildContext context,
                            BoxConstraints viewportConstraints) {
                          orgList = snapshot.data;
                          return Container(
                            padding: EdgeInsets.fromLTRB(12, 12, 12, 0),
                            width: double.infinity,
                            height: SizeConfig.screenHeight,
                            child: SingleChildScrollView(
                              child: ConstrainedBox(
                                constraints: BoxConstraints(
                                  minHeight: viewportConstraints.maxHeight,
                                ),
                                child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.max,
                                    children: [
                                      Container(
                                        height:
                                            SizeConfig.safeBlockVertical * 90,
                                        child: ListView.builder(
                                            itemCount: orgList
                                                .organizationResult.length,
                                            shrinkWrap: true,
                                            itemBuilder: (context, index) {
                                              return GestureDetector(
                                                onTap: () {
                                                  Global.orgResultList = orgList
                                                          .organizationResult[
                                                      index];
                                                  Navigator.push(context,
                                                      MaterialPageRoute(
                                                          builder: (context) {
                                                    return StaticSurveyForm(
                                                      products: orgList
                                                          .organizationProducts,
                                                      index: index,
                                                      surveyId: orgList
                                                          .organizationResult[
                                                              index]
                                                          .sId,
                                                      surveyName: orgList
                                                          .organizationResult[
                                                              index]
                                                          .surveyName,
                                                      surveyResult: orgList
                                                          .organizationResult,
                                                    );
                                                  }));
                                                },
                                                child: Card(
                                                  elevation: 10,
                                                  shape: RoundedRectangleBorder(
                                                    side: BorderSide(
                                                        color: Colors.white70,
                                                        width: 1),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10),
                                                  ),
                                                  child: Container(
                                                    margin: EdgeInsets.all(10),
                                                    child: Center(
                                                      child: Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                  .all(8.0),
                                                          child: Text(orgList
                                                              .organizationResult[
                                                                  index]
                                                              .surveyName
                                                              .toString()
                                                              .toUpperCase())),
                                                    ),
                                                  ),
                                                ),
                                              );
                                            }),
                                      )
                                    ]),
                              ),
                            ),
                          );
                        },
                      ));
              },
            )),
      ),
    );
  }
}
