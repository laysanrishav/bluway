import 'dart:collection';
import 'dart:convert';
import 'dart:io';
import 'dart:io' as Io;

import 'package:blu_way/Model/DistictList.dart';
import 'package:blu_way/Model/StateList.dart';
import 'package:blu_way/Model/Survey/OrganizationSurvey.dart';
import 'package:blu_way/Widget/MyAppBar.dart';
import 'package:blu_way/utlis/Country.dart';
import 'package:blu_way/utlis/Size_config.dart';
import 'package:blu_way/utlis/global.dart';
import 'package:blu_way/utlis/theme.dart';
import 'package:geolocator/geolocator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:toast/toast.dart';

import 'dynamic_survey_form.dart';

class StaticSurveyForm extends StatefulWidget {
  int index;
  List surveyResult;
  List products;
  String surveyName;
  String surveyId;

  StaticSurveyForm({
    Key key,
    @required this.index,
    @required this.products,
    @required this.surveyResult,
    @required this.surveyName,
    @required this.surveyId,
  }) : super(key: key);

  @override
  _EditTransportorProfileState createState() => _EditTransportorProfileState();
}

class _EditTransportorProfileState extends State<StaticSurveyForm> {
  TextEditingController fnameName = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController village = TextEditingController();
  TextEditingController postoffice = TextEditingController();
  TextEditingController thana = TextEditingController();
  TextEditingController address = TextEditingController();
  // final Geolocator _geolocator = Geolocator();
  String selectedCountry = '';
  String stateid = '';
  String districtid = '';
  TextEditingController pincode = TextEditingController();
  // String profileimage = '';
  String gender;
  File _image;
  File croppedFile;
  File result;
  bool makeVisible = false;
  DateTime selectedDate = DateTime.now();
  TextEditingController dob = TextEditingController();
  StateList stateList = StateList();
  DistrictList distictList = DistrictList();
  List<StateResult> stateresult = [];
  List<DistrictResult> districtresult = [];
  List countries = [];
  String lat = '';
  String long = '';
  String type;
  String img64;
  final _formKey = GlobalKey<FormState>();
  bool readonly = false;
  DistrictResult test = DistrictResult();
  Map<String, Object> inputDoc = new HashMap();
  @override
  void initState() {
    super.initState();
    print("Length of survey is: " +
        widget.surveyResult[widget.index].survey.fields.length.toString());
    getLocation();
    initData();
  }

  fetchDistrict(String id) {
    fetchDistrictListModel(id).then((value) {
      setState(() {
        distictList = value;
        districtresult = distictList.result;
        print(distictList.result.isEmpty);
        test = distictList.result[0];
      });
    });
  }

  getLocation() async {
    final ph = PermissionHandler();
    ph.requestPermissions([PermissionGroup.location]);
    Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
        .then((Position position) {
      print(position.latitude);
      print(position.longitude);
    });
  }

  initData() {
    fetchStateListModel().then((value) {
      setState(() {
        stateList = value;
        stateresult = stateList.result;
      });
    });
  }

  getImageFile(ImageSource source) async {
    //pickImage(source: source);
    final picker = ImagePicker();
    var selectedimage = await picker.getImage(source: source);

    if (selectedimage != null) {
      croppedFile = await ImageCropper.cropImage(
          sourcePath: selectedimage.path,
          aspectRatioPresets: [
            CropAspectRatioPreset.square,
            CropAspectRatioPreset.ratio3x2,
            CropAspectRatioPreset.original,
            CropAspectRatioPreset.ratio4x3,
            CropAspectRatioPreset.ratio16x9
          ],
          androidUiSettings: AndroidUiSettings(
              toolbarTitle: 'Cropper',
              toolbarColor: AppColors.appBarColor,
              toolbarWidgetColor: Colors.white,
              initAspectRatio: CropAspectRatioPreset.square,
              lockAspectRatio: false),
          iosUiSettings: IOSUiSettings(
            minimumAspectRatio: 1.0,
          ));

      if (croppedFile != null) {
        File compressedFile = await FlutterNativeImage.compressImage(
            croppedFile.path,
            quality: 88,
            percentage: 80);
        print(compressedFile.lengthSync());
        final bytes = await Io.File(selectedimage.path).readAsBytesSync();
        img64 = base64Encode(bytes);
        setState(() {
          _image = compressedFile;
          print("This the Image $_image");
          print("This is the compressed image size :-${_image?.lengthSync()}");
          makeVisible = true;
          // final bytes = await Io.File(croppedFile.path).readAsBytesSync();

          // profileimage = img64;
        });
      }
    }
  }

  // Bottom Sheet
  void imageModalBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            height: 150.0,
            color: Colors.white,
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: FloatingActionButton.extended(
                    label: Text('Camera'),
                    onPressed: () {
                      Navigator.of(context, rootNavigator: true).pop();
                      //image = "";
                      getImageFile(ImageSource.camera);
                    },
                    heroTag: UniqueKey(),
                    icon: Icon(Icons.camera),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: FloatingActionButton.extended(
                    label: Text('Gallery'),
                    onPressed: () {
                      Navigator.of(context, rootNavigator: true).pop();

                      getImageFile(ImageSource.gallery);
                    },
                    heroTag: UniqueKey(),
                    icon: Icon(Icons.photo_library),
                  ),
                )
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            appBar: MyAppBar(widget.surveyName.toUpperCase()),
            body: LayoutBuilder(builder:
                (BuildContext context, BoxConstraints viewportConstraints) {
              return Container(
                  padding: EdgeInsets.fromLTRB(12, 12, 12, 0),
                  width: double.infinity,
                  height: SizeConfig.screenHeight,
                  child: SingleChildScrollView(
                      child: ConstrainedBox(
                          constraints: BoxConstraints(
                            minHeight: viewportConstraints.maxHeight,
                          ),
                          child: Form(
                            key: _formKey,
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  Padding(
                                    padding: EdgeInsets.only(
                                        top: 10.0,
                                        left:
                                            SizeConfig.safeBlockHorizontal * 8),
                                    child: GestureDetector(
                                      onTap: () {
                                        imageModalBottomSheet(context);
                                      },
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Column(
                                            children: [
                                              _image != null
                                                  ? ClipRRect(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              45),
                                                      child: Image.file(
                                                        _image,
                                                        height: 90,
                                                        fit: BoxFit.cover,
                                                        width: 90,
                                                      ))
                                                  : ClipRRect(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              50),
                                                      child: Container(
                                                        color:
                                                            AppColors.blueColor,
                                                        child: Image.network(
                                                          'https://leadslive.io/wp-content/uploads/2017/05/Miniclip-8-Ball-Pool-Avatar-11.png',
                                                          height: 100,
                                                          width: 100,
                                                        ),
                                                      ),
                                                    ),
                                              Padding(
                                                padding:
                                                    EdgeInsets.only(top: 15),
                                                child: Text(
                                                  'Upload',
                                                  style: TextStyle(
                                                    fontFamily: 'Arial',
                                                    fontSize: 20,
                                                    fontWeight: FontWeight.w500,
                                                    color: Colors.orange[200],
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                          Container(
                                            padding: EdgeInsets.all(8),
                                            margin: EdgeInsets.all(8),
                                            decoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                                color: AppColors.darkColor),
                                            child: Icon(
                                              Icons.camera_alt,
                                              size: 20,
                                              color: Colors.white,
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: 20),
                                    child: TextFormField(
                                      controller: fnameName,
                                      style: TextStyle(
                                          fontFamily: 'Arial',
                                          fontSize: 20,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.black),
                                      onSaved: (value) {
                                        inputDoc["full_name"] = value;
                                        print(inputDoc["full_name"]);
                                      },
                                      validator: (val) {
                                        if (val.trim().isEmpty) {
                                          return 'Please Enter you FullName';
                                        }
                                        return null;
                                      },
                                      // readOnly: readonly,
                                      decoration: InputDecoration(
                                        labelText: 'Full Name',
                                        labelStyle: TextStyle(
                                            fontFamily: 'Arial',
                                            fontSize: 18,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.grey),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              width: 1.5, color: Colors.grey),
                                          borderRadius: const BorderRadius.all(
                                            const Radius.circular(10.0),
                                          ),
                                        ),
                                        border: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              width: 1.5, color: Colors.grey),
                                          borderRadius: const BorderRadius.all(
                                            const Radius.circular(10.0),
                                          ),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              width: 1.5, color: Colors.grey),
                                          borderRadius: const BorderRadius.all(
                                            const Radius.circular(10.0),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: 10),
                                    child: DropdownButtonFormField<String>(
                                      isDense: true,
                                      isExpanded: true,
                                      style: TextStyle(
                                        fontFamily: 'Shruti',
                                        fontWeight: FontWeight.w600,
                                        fontSize: 20,
                                        color: AppColors.textFeildcolor,
                                      ),
                                      hint: Text('Select Country',
                                          style: TextStyle(
                                              fontFamily: 'Arial',
                                              fontSize: 18,
                                              fontWeight: FontWeight.w500,
                                              color: Colors.grey)),
                                      decoration: InputDecoration(
                                          border: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            width: 1.5, color: Colors.grey),
                                        borderRadius: const BorderRadius.all(
                                          const Radius.circular(10.0),
                                        ),
                                      )),
                                      validator: (value) => value == null
                                          ? 'Feild Required'
                                          : null,
                                      items: countryList.map((String value) {
                                        return new DropdownMenuItem<String>(
                                          value: value,
                                          child: new Text(value),
                                        );
                                      }).toList(),
                                      onChanged: (val) {
                                        setState(() {
                                          selectedCountry = val;
                                          inputDoc["country"] = selectedCountry;
                                          print(inputDoc);
                                        });
                                      },
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 10.0),
                                    child: new DropdownButtonFormField<String>(
                                      isDense: true,
                                      style: TextStyle(
                                          fontFamily: 'Arial',
                                          fontSize: 18,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.black),
                                      hint: Text(
                                        'Select Gender',
                                        style: TextStyle(
                                            fontFamily: 'Arial',
                                            fontSize: 18,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.grey),
                                      ),
                                      decoration: InputDecoration(
                                          border: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            width: 1.5, color: Colors.grey),
                                        borderRadius: const BorderRadius.all(
                                          const Radius.circular(10.0),
                                        ),
                                      )),
                                      validator: (value) => value == null
                                          ? 'Feild Required'
                                          : null,
                                      items: <String>[
                                        'Male',
                                        'Female',
                                        'Not Specify',
                                      ].map((String value) {
                                        return new DropdownMenuItem<String>(
                                          value: value,
                                          child: new Text(value),
                                        );
                                      }).toList(),
                                      onChanged: readonly
                                          ? null
                                          : (val) {
                                              setState(() {
                                                gender = val;
                                                inputDoc["gender"] = gender;
                                                print(inputDoc);
                                              });
                                            },
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: 10),
                                    child: TextFormField(
                                      controller: address,
                                      readOnly: readonly,
                                      style: TextStyle(
                                          fontFamily: 'Arial',
                                          fontSize: 20,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.black),
                                      validator: (val) {
                                        if (val.trim().isEmpty) {
                                          return 'Please Enter the Address';
                                        }
                                        return null;
                                      },
                                      onSaved: (value) {
                                        inputDoc["address"] = value;
                                        print(inputDoc);
                                      },
                                      decoration: InputDecoration(
                                        labelText: 'Address',
                                        labelStyle: TextStyle(
                                            fontFamily: 'Arial',
                                            fontSize: 18,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.grey),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              width: 1.5, color: Colors.grey),
                                          borderRadius: const BorderRadius.all(
                                            const Radius.circular(10.0),
                                          ),
                                        ),
                                        border: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              width: 1.5, color: Colors.grey),
                                          borderRadius: const BorderRadius.all(
                                            const Radius.circular(10.0),
                                          ),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              width: 1.5, color: Colors.grey),
                                          borderRadius: const BorderRadius.all(
                                            const Radius.circular(10.0),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: 10),
                                    child: TextFormField(
                                      controller: postoffice,
                                      readOnly: readonly,
                                      style: TextStyle(
                                          fontFamily: 'Arial',
                                          fontSize: 20,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.black),
                                      validator: (val) {
                                        if (val.trim().isEmpty) {
                                          return 'Please Enter the Postoffice';
                                        }
                                        return null;
                                      },
                                      onSaved: (value) {
                                        inputDoc["post_office"] = value;
                                        print(inputDoc);
                                      },
                                      decoration: InputDecoration(
                                        labelText: 'Post Office',
                                        labelStyle: TextStyle(
                                            fontFamily: 'Arial',
                                            fontSize: 18,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.grey),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              width: 1.5, color: Colors.grey),
                                          borderRadius: const BorderRadius.all(
                                            const Radius.circular(10.0),
                                          ),
                                        ),
                                        border: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              width: 1.5, color: Colors.grey),
                                          borderRadius: const BorderRadius.all(
                                            const Radius.circular(10.0),
                                          ),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              width: 1.5, color: Colors.grey),
                                          borderRadius: const BorderRadius.all(
                                            const Radius.circular(10.0),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: 10),
                                    child: TextFormField(
                                      controller: thana,
                                      readOnly: readonly,
                                      style: TextStyle(
                                          fontFamily: 'Arial',
                                          fontSize: 20,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.black),
                                      validator: (val) {
                                        if (val.trim().isEmpty) {
                                          return 'Please Enter the Thana';
                                        }
                                        return null;
                                      },
                                      onSaved: (value) {
                                        inputDoc["thana"] = value;
                                        print(inputDoc);
                                      },
                                      decoration: InputDecoration(
                                        labelText: 'Thana',
                                        labelStyle: TextStyle(
                                            fontFamily: 'Arial',
                                            fontSize: 18,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.grey),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              width: 1.5, color: Colors.grey),
                                          borderRadius: const BorderRadius.all(
                                            const Radius.circular(10.0),
                                          ),
                                        ),
                                        border: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              width: 1.5, color: Colors.grey),
                                          borderRadius: const BorderRadius.all(
                                            const Radius.circular(10.0),
                                          ),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              width: 1.5, color: Colors.grey),
                                          borderRadius: const BorderRadius.all(
                                            const Radius.circular(10.0),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: 10),
                                    child: new DropdownButtonFormField<
                                        StateResult>(
                                      isDense: true,
                                      style: TextStyle(
                                          fontFamily: 'Arial',
                                          fontSize: 18,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.black),
                                      hint: Text('Select State',
                                          style: TextStyle(
                                              fontFamily: 'Arial',
                                              fontSize: 18,
                                              fontWeight: FontWeight.w500,
                                              color: Colors.grey)),
                                      decoration: InputDecoration(
                                          border: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            width: 1.5, color: Colors.grey),
                                        borderRadius: const BorderRadius.all(
                                          const Radius.circular(10.0),
                                        ),
                                      )),
                                      validator: (value) => value == null
                                          ? 'Feild Required'
                                          : null,
                                      items: stateresult
                                          .map<DropdownMenuItem<StateResult>>(
                                              (StateResult item) {
                                        return DropdownMenuItem<StateResult>(
                                          value: item,
                                          child: Text(item.name),
                                        );
                                      }).toList(),
                                      onChanged: readonly
                                          ? null
                                          : (value) {
                                              inputDoc["state"] = value.name;
                                              print(inputDoc);
                                              print(value.sId);
                                              setState(() {
                                                stateid = value.sId;
                                                fetchDistrict(stateid);
                                              });
                                            },
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: 10),
                                    child: new DropdownButtonFormField<
                                        DistrictResult>(
                                      isDense: true,
                                      style: TextStyle(
                                          fontFamily: 'Arial',
                                          fontSize: 18,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.black),
                                      hint: Text('Select District',
                                          style: TextStyle(
                                              fontFamily: 'Arial',
                                              fontSize: 18,
                                              fontWeight: FontWeight.w500,
                                              color: Colors.grey)),
                                      decoration: InputDecoration(
                                          border: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            width: 1.5, color: Colors.grey),
                                        borderRadius: const BorderRadius.all(
                                          const Radius.circular(10.0),
                                        ),
                                      )),
                                      value: test,
                                      validator: (value) => value == null
                                          ? 'Feild Required'
                                          : null,
                                      items: districtresult.map<
                                              DropdownMenuItem<DistrictResult>>(
                                          (DistrictResult item) {
                                        return DropdownMenuItem<DistrictResult>(
                                          value: item,
                                          child: Text(item.name),
                                        );
                                      }).toList(),
                                      onChanged: stateid.isEmpty
                                          ? null
                                          : (value) {
                                              inputDoc["district"] = value.name;
                                              print(inputDoc);
                                              print(value.sId);
                                              setState(() {
                                                test = value;
                                                districtid = value.sId;
                                              });
                                            },
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: 10),
                                    child: TextFormField(
                                      maxLength: 6,
                                      controller: pincode,
                                      readOnly: readonly,
                                      style: TextStyle(
                                          fontFamily: 'Arial',
                                          fontSize: 20,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.black),
                                      validator: (val) {
                                        if (val.trim().isEmpty) {
                                          return 'Please Enter the Pincode';
                                        }
                                        return null;
                                      },
                                      onSaved: (value) {
                                        inputDoc["pincode"] = value;
                                        print(inputDoc);
                                      },
                                      keyboardType: TextInputType.phone,
                                      decoration: InputDecoration(
                                        labelText: 'Pincode',
                                        labelStyle: TextStyle(
                                            fontFamily: 'Arial',
                                            fontSize: 18,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.grey),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              width: 1.5, color: Colors.grey),
                                          borderRadius: const BorderRadius.all(
                                            const Radius.circular(10.0),
                                          ),
                                        ),
                                        border: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              width: 1.5, color: Colors.grey),
                                          borderRadius: const BorderRadius.all(
                                            const Radius.circular(10.0),
                                          ),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              width: 1.5, color: Colors.grey),
                                          borderRadius: const BorderRadius.all(
                                            const Radius.circular(10.0),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: 10),
                                    child: TextFormField(
                                      controller: village,
                                      readOnly: readonly,
                                      style: TextStyle(
                                          fontFamily: 'Arial',
                                          fontSize: 20,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.black),
                                      validator: (val) {
                                        if (val.trim().isEmpty) {
                                          return 'Please Enter the Village';
                                        }
                                        return null;
                                      },
                                      onSaved: (value) {
                                        inputDoc["village"] = value;
                                        print(inputDoc);
                                      },
                                      decoration: InputDecoration(
                                        labelText: 'Village',
                                        labelStyle: TextStyle(
                                            fontFamily: 'Arial',
                                            fontSize: 18,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.grey),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              width: 1.5, color: Colors.grey),
                                          borderRadius: const BorderRadius.all(
                                            const Radius.circular(10.0),
                                          ),
                                        ),
                                        border: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              width: 1.5, color: Colors.grey),
                                          borderRadius: const BorderRadius.all(
                                            const Radius.circular(10.0),
                                          ),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              width: 1.5, color: Colors.grey),
                                          borderRadius: const BorderRadius.all(
                                            const Radius.circular(10.0),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: 10),
                                    child: TextFormField(
                                      maxLength: 10,
                                      controller: phone,
                                      readOnly: readonly,
                                      style: TextStyle(
                                          fontFamily: 'Arial',
                                          fontSize: 20,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.black),
                                      validator: (val) {
                                        if (val.trim().isEmpty) {
                                          return 'Please Enter the mobile No';
                                        } else if (val.trim().length < 10 ||
                                            val.trim().length > 10) {
                                          return 'Please enter a valid mobile no';
                                        }
                                        return null;
                                      },
                                      onSaved: (value) {
                                        inputDoc["mobile"] = value;
                                        print(inputDoc);
                                      },
                                      keyboardType: TextInputType.phone,
                                      decoration: InputDecoration(
                                        labelText: 'Mobile',
                                        labelStyle: TextStyle(
                                            fontFamily: 'Arial',
                                            fontSize: 18,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.grey),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              width: 1.5, color: Colors.grey),
                                          borderRadius: const BorderRadius.all(
                                            const Radius.circular(10.0),
                                          ),
                                        ),
                                        border: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              width: 1.5, color: Colors.grey),
                                          borderRadius: const BorderRadius.all(
                                            const Radius.circular(10.0),
                                          ),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              width: 1.5, color: Colors.grey),
                                          borderRadius: const BorderRadius.all(
                                            const Radius.circular(10.0),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    height: 140,
                                    child: ListView.builder(
                                        itemCount: widget.products.length,
                                        itemBuilder: (context, index) {
                                          String image =
                                              widget.products[index].image;
                                          String name =
                                              widget.products[index].name;
                                          String price = widget
                                              .products[index].sellingPrice;
                                          int _itemCount = 0;
                                          return Card(
                                            elevation: 10,
                                            child: Container(
                                              height: 130,
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: Row(
                                                  children: [
                                                    CircleAvatar(
                                                      child: Image.network(
                                                          '''http://139.59.75.40:4040/uploads/image/$image'''),
                                                    ),
                                                    SizedBox(
                                                      width: 20,
                                                    ),
                                                    Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Text(
                                                          name,
                                                          style: GoogleFonts
                                                              .roboto(
                                                            // color: Colors
                                                            //     .grey,
                                                            fontSize: 25,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                          ),
                                                        ),
                                                        RichText(
                                                          text: TextSpan(
                                                            text: 'Price:  ',
                                                            style: GoogleFonts
                                                                .roboto(
                                                                    fontSize:
                                                                        20,
                                                                    color: Colors
                                                                        .black),
                                                            children: <
                                                                TextSpan>[
                                                              TextSpan(
                                                                  text:
                                                                      '''₹$price''',
                                                                  style: GoogleFonts.roboto(
                                                                      fontSize:
                                                                          20,
                                                                      color: Colors
                                                                          .black,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold)),
                                                            ],
                                                          ),
                                                        ),
                                                        Row(
                                                          children: <Widget>[
                                                            IconButton(
                                                              icon: new Icon(
                                                                  Icons.remove),
                                                              onPressed: () {
                                                                setState(() {
                                                                  _itemCount =
                                                                      _itemCount--;
                                                                });
                                                              },
                                                            ),
                                                            Text(_itemCount
                                                                .toString()),
                                                            IconButton(
                                                              icon: new Icon(
                                                                  Icons.add),
                                                              onPressed: () {
                                                                setState(() {
                                                                  _itemCount =
                                                                      _itemCount++;
                                                                });
                                                              },
                                                            )
                                                          ],
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          );
                                        }),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 15.0),
                                    child: Center(
                                      child: RaisedButton(
                                          color: Colors.indigo[900],
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(50),
                                          ),
                                          child: Padding(
                                            padding: const EdgeInsets.all(12.0),
                                            child: Text(
                                              'NEXT',
                                              style: TextStyle(
                                                  fontFamily: 'Arial',
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 20),
                                            ),
                                          ),
                                          onPressed: () {
                                            if (_formKey.currentState
                                                .validate()) {
                                              _formKey.currentState.save();
                                              // Navigator.push(context,
                                              //     MaterialPageRoute(
                                              //         builder: (context) {
                                              //   return DynamicSurveyForm(
                                              //     surveyName: widget.surveyName,
                                              //     inputDoc: inputDoc,
                                              //     surveyId: widget.surveyId,
                                              //     surveyFormFeild: widget
                                              //         .surveyResult[
                                              //             widget.index]
                                              //         .survey
                                              //         .fields,
                                              //     lat: lat,
                                              //     long: long,
                                              //     image: img64,
                                              //   );
                                              // }));
                                            }
                                          }),
                                    ),
                                  )
                                ]),
                          ))));
            })));

    // ModalProgressHUD(
    //   inAsyncCall: Global.isLoading,
    //   color: AppColors.buttonBg,
    //   progressIndicator: CircularProgressIndicator(),
    //   child: GestureDetector(
    //       onTap: () => FocusScope.of(context).unfocus(),
    //       child:
    //   ).
    // );
  }
}
