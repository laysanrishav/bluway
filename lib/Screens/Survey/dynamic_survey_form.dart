// import 'package:blu_way/Model/Survey/OrganizationSurvey.dart';
// import 'package:blu_way/Widget/MyAppBar.dart';
// import 'package:blu_way/main.dart';
// import 'package:blu_way/utlis/values/styles.dart';
// import 'package:flutter/material.dart';

// class DynamicSurveyForm extends StatefulWidget {
//   final String surveyName;
//   final Map inputDoc;
//   final int index;
//   final List surveyFormFeild;
//   final String surveyId;
//   final String lat;
//   final String long;
//   final String image;

//   const DynamicSurveyForm(
//       {Key key,
//       this.surveyName,
//       this.inputDoc,
//       this.index,
//       this.surveyFormFeild,
//       this.surveyId,
//       this.lat,
//       this.long,
//       this.image})
//       : super(key: key);
//   @override
//   _DynamicSurveyFormState createState() => _DynamicSurveyFormState();
// }

// class _DynamicSurveyFormState extends State<DynamicSurveyForm> {
//   List<Fields> _surveyFieldList;
//   @override
//   void initState() {
//     // TODO: implement initState
//     _surveyFieldList = widget.surveyFormFeild;
//     print("Survey field lists are inside dynamic: " +
//         widget.surveyFormFeild.length.toString());

//     List commaValue = [];
//     // List<bool> inputValues = [];
//     // String cValue = _surveyFieldList[2].formValuesComma;

//     for (int i = 0; i < _surveyFieldList[2].formValuesComma.length; i++) {
//       commaValue.add(_surveyFieldList[2].formValuesComma[i].split(','));
//     }

//     print("list of comma values are: " + commaValue.toString());

//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: MyAppBar(widget.surveyName),
//       body: Container(
//         child: ListView.builder(
//             itemCount: widget.surveyFormFeild.length,
//             itemBuilder: (context, index) {
//               String fieldName = _surveyFieldList[index].filedName;
//               String fieldType = _surveyFieldList[index].filedType;
//               String fieldValue = _surveyFieldList[index].filedValue;
//               String fieldRequired = _surveyFieldList[index].filedRequired;

//               if (fieldType.toLowerCase() == "check box") {
//                 List commaValue = [];
//                 List<bool> inputValues = [];

//                 for (int i = 0;
//                     i < _surveyFieldList[index].formValuesComma.length;
//                     i++) {
//                   commaValue.add(
//                       _surveyFieldList[index].formValuesComma[i].split(','));
//                 }
//                 print("Value of comma separated value ");
//                 for (int i = 0; i < commaValue.length; i++) {
//                   print(commaValue[i].toString().split(','));
//                 }
//                 for (int i = 0; i < commaValue.length; i++) {
//                   inputValues.add(false);
//                 }
//                 return Column(
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   mainAxisAlignment: MainAxisAlignment.start,
//                   children: [
//                     fieldRequired.toString().toLowerCase() == "no"
//                         ? Container(
//                             margin: const EdgeInsets.only(top: 20, left: 20),
//                             child: Text(
//                               '$fieldName(Optional) :'.toUpperCase(),
//                               style: kheadingStyle.apply(
//                                 fontSizeFactor: 1.2,
//                                 fontWeightDelta: 5,
//                               ),
//                             ),
//                           )
//                         : Container(
//                             margin: const EdgeInsets.only(top: 20, left: 20),
//                             child: Text(
//                               '$fieldName :'.toUpperCase(),
//                               style: kheadingStyle.apply(
//                                 fontSizeFactor: 1.2,
//                                 fontWeightDelta: 5,
//                               ),
//                             ),
//                           ),
//                   ],
//                 );
//               }
//             }),
//       ),
//     );
//   }
// }
