import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'dart:ui';

import 'package:blu_way/Model/Auth/Registration.dart';
import 'package:blu_way/Model/Auth/UserDataModel.dart';
import 'package:blu_way/Model/DistictList.dart';
import 'package:blu_way/Model/StateList.dart';
import 'package:blu_way/Model/hubListModel.dart';
import 'package:blu_way/utlis/Country.dart';
import 'package:blu_way/utlis/Size_config.dart';
import 'package:blu_way/utlis/global.dart';
import 'package:blu_way/utlis/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:image_picker/image_picker.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:intl/intl.dart';

import 'dart:io' as Io;

import 'package:toast/toast.dart';
import 'package:validators/validators.dart';

import 'Signup.dart';

class SignupDetails extends StatefulWidget {
  SignupDetails({Key key}) : super(key: key);

  @override
  _SignupDetailsState createState() => _SignupDetailsState();
}

class _SignupDetailsState extends State<SignupDetails> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController mobileNo = TextEditingController();
  TextEditingController tname = TextEditingController();
  TextEditingController otp = TextEditingController();
  TextEditingController fname = TextEditingController();
  TextEditingController lname = TextEditingController();
  TextEditingController fHname = TextEditingController();
  TextEditingController address = TextEditingController();
  TextEditingController village = TextEditingController();
  TextEditingController postoffice = TextEditingController();
  TextEditingController thana = TextEditingController();
  TextEditingController pincode = TextEditingController();
  TextEditingController occupation = TextEditingController();
  TextEditingController indentityDoc = TextEditingController();
  TextEditingController dob = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController anyotherOrg = TextEditingController();
  TextEditingController rnum = TextEditingController();
  TextEditingController gst_num = TextEditingController();
  TextEditingController bankname = TextEditingController();
  TextEditingController anum = TextEditingController();
  TextEditingController ifsc_num = TextEditingController();
  UserDataModel userDataModel = UserDataModel();
  DateTime selectedDate = DateTime.now();
  bool autoValidate = false;
  AutovalidateMode autovalidate = AutovalidateMode.disabled;
  List<String> organization = ['test1', 'test2', 'test3'];
  List<String> role = ['test1', 'test2', 'test3'];
  StateList stateList = StateList();
  DistrictList distictList = DistrictList();
  List<StateResult> stateresult = [];
  List<DistrictResult> districtresult = [];
  // List<HubResult> transportorlist = [];
  List<HubResult> hublist = [];
  DistrictResult test = DistrictResult();
  List countries = [];
  String selectedCountry = '';
  String hubname = '';
  File _image;
  File croppedFile;
  File result;
  bool makeVisible = false;
  String stateid = '';
  String districtId = '';
  String img64 = '';
  String gender = '';
  String selectedOrg = '';
  String transporter_name = '';
  List<String> documenttype = [
    'Aadhar Card',
    'Voter Id Card',
    'PAN Card',
    'Driving License'
  ];
  List<String> listOfOrganization = [
    'Property firm',
    'Partnership firm',
    'LLB Pvt Ltd',
    'Pvt Ltd',
    'NGO',
    'Trust',
    'SHG',
    'Any Other'
  ];
  String selectedDocument = '';
  @override
  void initState() {
    super.initState();
    initData();
  }

  initData() {
    fetchStateListModel().then((value) {
      setState(() {
        stateList = value;
        stateresult = stateList.result;
      });
    });
    hubListModel().then((value) {
      setState(() {
        hublist = value.hubResult;
      });
    });
  }

  fetchDistrict(String id) {
    fetchDistrictListModel(id).then((value) {
      setState(() {
        distictList = value;
        districtresult = distictList.result;
        print(distictList.result.isEmpty);
        test = distictList.result[0];
      });
    });
  }

  Future getImage(BuildContext context) async {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
              content: SizedBox(
            height: 150,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                FlatButton(
                  child: Text(
                    "Camera",
                    style: TextStyle(fontSize: 26),
                  ),
                  onPressed: () async {
                    var image = await ImagePicker().getImage(
                      source: ImageSource.camera,
                      imageQuality: 50,
                    );
                    // var image = await ImagePicker()
                    //     .getImage(source: ImageSource.camera);
                    setState(() {
                      // _image = image;
                      _image = File(image.path);
                      List<int> bytes = Io.File(_image.path).readAsBytesSync();
                      img64 = base64UrlEncode(bytes);
                      // var base64String =
                      //     base64UrlEncode(_image.readAsBytesSync());
                      // base64String = base64Encode(_image.readAsBytesSync());
                      // print('document $img64');
                      // print('base64String $base64String');
                      Navigator.pop(context);
                    });
                  },
                ),
                FlatButton(
                  child: Text(
                    "Gallery",
                    style: TextStyle(fontSize: 26),
                  ),
                  onPressed: () async {
                    var image = await ImagePicker().getImage(
                      source: ImageSource.gallery,
                      imageQuality: 50,
                    );
                    // var image = await ImagePicker()
                    //     .getImage(source: ImageSource.gallery);
                    setState(() {
                      _image = File(image.path);
                      // print(_image.uri.toString());
                      List<int> bytes = Io.File(_image.path).readAsBytesSync();
                      img64 = base64UrlEncode(bytes);
                      print('img64 : $img64');
                      print('bytes $bytes');
                      // print('base64Image : $img64');
                      // base64String = base64Encode(_image.readAsBytesSync());
                      // print('document $img64');
                      // print('base64String $base64String');
                      print('image1 : $_image');
                      Navigator.pop(context);
                    });
                  },
                ),
                _image != null
                    ? FlatButton(
                        child: Text(
                          "Remove Profile",
                          style: TextStyle(fontSize: 26),
                        ),
                        onPressed: () async {
                          setState(() {
                            _image = null;
                            Navigator.pop(context);
                          });
                        },
                      )
                    : Text(
                        "",
                      ),
              ],
            ),
          ));
        });
  }

  void loginSubmitted() {
    setState(() {
      Global.isLoading = true;
    });

    print(email.text);
    if (_formKey.currentState.validate() || _image != null) {
      resgistrationModel(
              hub: hubname,
              account_number: int.tryParse(anum.text),
              bank_name: bankname.text,
              reg_number: rnum.text,
              ifsc: ifsc_num.text,
              gst_number: gst_num.text,
              organization_name: tname.text,
              org_name: selectedOrg,
              org_type_other: anyotherOrg.text,
              fname: fname.text,
              lname: lname.text,
              email: email.text,
              gender: gender,
              dob: dob.text,
              fathername: fHname.text,
              address: address.text,
              village: village.text,
              postoffice: postoffice.text,
              thana: thana.text,
              country: selectedCountry,
              state: stateid,
              distict: districtId,
              pincode: pincode.text,
              mobile: mobileNo.text,
              occupation: occupation.text,
              document: selectedDocument,
              docimage: img64)
          .then((value) {
        if (value.status == 'success') {
          final data = value;

          print("Otp obtained is: " + value.oTP.toString());
          setState(() {
            Global.otp = value.oTP.toString();
          });

          userDataModel = UserDataModel(
              active: value.data.active,
              gst_number: value.data.gst_number,
              reg_number: value.data.reg_number,
              ifsc: value.data.ifsc,
              bank_name: value.data.bank_name,
              account_num: value.data.account_number,
              otp: value.data.otp,
              verified: value.data.verified,
              deleted: value.data.deleted,
              transportername: value.data.organization_name,
              sId: value.data.sId,
              orgname: value.data.orgtype,
              firstName: value.data.firstName,
              lastName: value.data.lastName,
              village: value.data.village,
              postOffice: value.data.postOffice,
              thana: value.data.thana,
              state: value.data.state,
              district: value.data.district,
              pincode: value.data.pincode,
              document: value.data.document,
              documentImage: value.data.documentImage,
              mobile: value.data.mobile,
              userType: value.data.userType,
              master: value.data.master,
              masterName: value.data.masterName,
              createdDate: value.data.createdDate,
              iV: value.data.iV);
          print("User data model is:" + userDataModel.otp.toString());
          setState(() {
            Global.isLoading = false;
          });
          Toast.show(value.message, context);
          Global.signupno = value.data.mobile;
          Global.otp = userDataModel.otp.toString();
          print("Signup number is:" + Global.signupno);
          print("Otp is:" + Global.otp);

          Global.userDataModel = userDataModel;
          _formKey.currentState.reset();
          //;
          mobileNo.clear();
          otp.clear();
          fname.clear();
          lname.clear();
          tname.clear();
          fHname.clear();
          address.clear();
          village.clear();
          postoffice.clear();
          thana.clear();
          pincode.clear();
          occupation.clear();
          indentityDoc.clear();
          dob.clear();
          email.clear();
          anyotherOrg.clear();
          Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (context) {
            return Signup(
              globalOtp: value.oTP,
            );
          }));
        } else {
          setState(() {
            Global.isLoading = false;
          });
          Toast.show(value.message, context);
        }
      }).catchError((onError) {
        setState(() {
          Global.isLoading = false;
        });
        Toast.show('Something went wrong', context);
      });
    } else {
      setState(() {
        Global.isLoading = false;
      });
      if (_image == null) {
        Toast.show('Please upload the document', context);
      }
    }
  }

  _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime(1980),
      lastDate: DateTime(2025),
    ).then((selectedDate) {
      if (selectedDate != null) {
        dob.text = DateFormat('dd/MM/yyyy').format(selectedDate);
      }
    });
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return ModalProgressHUD(
        inAsyncCall: Global.isLoading,
        color: AppColors.buttonBg,
        progressIndicator: CircularProgressIndicator(),
        child: GestureDetector(
            onTap: () => FocusScope.of(context).unfocus(),
            child: SafeArea(
                child: Scaffold(
                    resizeToAvoidBottomInset: true,
                    body: Form(
                      key: _formKey,
                      child: Container(
                          width: double.infinity,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: ExactAssetImage(
                                      'assets/images/login/BG-1.png'),
                                  fit: BoxFit.fill)),
                          height: SizeConfig.screenHeight,
                          child: LayoutBuilder(builder: (BuildContext context,
                              BoxConstraints viewportConstraints) {
                            return SingleChildScrollView(
                              child: ConstrainedBox(
                                constraints: BoxConstraints(
                                  minHeight: viewportConstraints.maxHeight,
                                ),
                                child: Container(
                                  child: Stack(
                                    children: [
                                      Container(
                                          child: Image.asset(
                                              'assets/images/login/BG-2.png',
                                              width: SizeConfig.screenWidth)),
                                      Positioned(
                                        right: 20,
                                        top: 0,
                                        child: Padding(
                                          padding: EdgeInsets.only(
                                              top:
                                                  SizeConfig.safeBlockVertical *
                                                      1,
                                              left: 10),
                                          child: Container(
                                              child: Image.asset(
                                            'assets/images/Loginsignup/Blu-logo.png',
                                            height: 140,
                                            width: 140,
                                          )),
                                        ),
                                      ),
                                      Container(
                                          //height: SizeConfig.safeBlockVertical*76,
                                          child: Padding(
                                        padding: EdgeInsets.only(
                                            left: 12.0,
                                            right: 12.0,
                                            top: SizeConfig.safeBlockVertical *
                                                22),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  right: 8.0),
                                              child: Text(
                                                'REGISTRATION',
                                                style: TextStyle(
                                                  fontSize: 30,
                                                  fontFamily: 'Shruti',
                                                  color: Color(0xffaedcff),
                                                  shadows: <Shadow>[
                                                    Shadow(
                                                      offset: Offset(1.0, 1.0),
                                                      blurRadius: 3.0,
                                                      color: Color.fromARGB(
                                                          255, 0, 0, 0),
                                                    ),
                                                    Shadow(
                                                      offset: Offset(1.0, 1.0),
                                                      blurRadius: 8.0,
                                                      color: Color.fromARGB(
                                                          125, 0, 0, 255),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),

                                            TextFormField(
                                              controller: fname,
                                              // autovalidateMode: autovalidate,
                                              style: TextStyle(
                                                  fontFamily: 'Arial',
                                                  color: AppColors.whiteColor,
                                                  fontSize: 22),
                                              validator: (val) {
                                                if (val.trim().isEmpty) {
                                                  Global.isLoading = false;
                                                  return 'Please enter your name';
                                                } else if (!isAlpha(val)) {
                                                  return "Enter a valid name";
                                                }

                                                return null;
                                              },
                                              decoration: InputDecoration(
                                                labelText: 'FIRST NAME',
                                                labelStyle: TextStyle(
                                                    fontFamily: 'Shruti',
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 20,
                                                    color: AppColors
                                                        .textFeildcolor),
                                                border: InputBorder.none,
                                              ),
                                            ),
                                            Container(
                                                child: Image.asset(
                                              'assets/images/Loginsignup/Line-1.png',
                                            )),
                                            TextFormField(
                                              controller: lname,
                                              // autovalidateMode: autovalidate,
                                              style: TextStyle(
                                                  fontFamily: 'Arial',
                                                  color: AppColors.whiteColor,
                                                  fontSize: 22),
                                              validator: (val) {
                                                if (val.trim().isEmpty) {
                                                  return 'Please enter your Lastname';
                                                } else if (!isAlpha(val)) {
                                                  return "Please enter a valid last name";
                                                }

                                                return null;
                                              },
                                              decoration: InputDecoration(
                                                labelText: 'LAST NAME',
                                                labelStyle: TextStyle(
                                                    fontFamily: 'Shruti',
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 20,
                                                    color: AppColors
                                                        .textFeildcolor),
                                                border: InputBorder.none,
                                              ),
                                            ),
                                            Container(
                                                child: Image.asset(
                                              'assets/images/Loginsignup/Line-1.png',
                                            )),
                                            TextFormField(
                                              controller: tname,
                                              // autovalidateMode: autovalidate,
                                              style: TextStyle(
                                                  fontFamily: 'Arial',
                                                  color: AppColors.whiteColor,
                                                  fontSize: 22),
                                              validator: (val) {
                                                if (val.trim().isEmpty) {
                                                  Global.isLoading = false;
                                                  return 'Please enter an Organization name';
                                                } else if (!isAlpha(val)) {
                                                  return "Please enter a valid Organization name";
                                                }

                                                return null;
                                              },
                                              decoration: InputDecoration(
                                                labelText: 'Organization name',
                                                labelStyle: TextStyle(
                                                    fontFamily: 'Shruti',
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 20,
                                                    color: AppColors
                                                        .textFeildcolor),
                                                border: InputBorder.none,
                                              ),
                                            ),
                                            Container(
                                                child: Image.asset(
                                              'assets/images/Loginsignup/Line-1.png',
                                            )),

                                            TextFormField(
                                              controller: rnum,
                                              autovalidateMode: autovalidate,
                                              style: TextStyle(
                                                  fontFamily: 'Arial',
                                                  color: AppColors.whiteColor,
                                                  fontSize: 22),
                                              validator: (val) {
                                                if (val.trim().isEmpty) {
                                                  return null;
                                                }

                                                return null;
                                              },
                                              decoration: InputDecoration(
                                                labelText: 'Registration num',
                                                labelStyle: TextStyle(
                                                    fontFamily: 'Shruti',
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 20,
                                                    color: AppColors
                                                        .textFeildcolor),
                                                border: InputBorder.none,
                                              ),
                                            ),
                                            Container(
                                                child: Image.asset(
                                              'assets/images/Loginsignup/Line-1.png',
                                            )),

                                            TextFormField(
                                              controller: bankname,
                                              autovalidateMode: autovalidate,
                                              style: TextStyle(
                                                  fontFamily: 'Arial',
                                                  color: AppColors.whiteColor,
                                                  fontSize: 22),
                                              validator: (val) {
                                                if (val.trim().isEmpty) {
                                                  return null;
                                                }
                                                return null;
                                              },
                                              decoration: InputDecoration(
                                                labelText: 'Bank Name',
                                                labelStyle: TextStyle(
                                                    fontFamily: 'Shruti',
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 20,
                                                    color: AppColors
                                                        .textFeildcolor),
                                                border: InputBorder.none,
                                              ),
                                            ),
                                            Container(
                                                child: Image.asset(
                                              'assets/images/Loginsignup/Line-1.png',
                                            )),

                                            TextFormField(
                                              controller: anum,
                                              autovalidateMode: autovalidate,
                                              keyboardType:
                                                  TextInputType.number,
                                              style: TextStyle(
                                                  fontFamily: 'Arial',
                                                  color: AppColors.whiteColor,
                                                  fontSize: 22),
                                              validator: (val) {
                                                if (val.trim().isEmpty) {
                                                  return null;
                                                }

                                                return null;
                                              },
                                              decoration: InputDecoration(
                                                labelText: 'Account Num',
                                                labelStyle: TextStyle(
                                                    fontFamily: 'Shruti',
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 20,
                                                    color: AppColors
                                                        .textFeildcolor),
                                                border: InputBorder.none,
                                              ),
                                            ),
                                            Container(
                                                child: Image.asset(
                                              'assets/images/Loginsignup/Line-1.png',
                                            )),

                                            TextFormField(
                                              controller: ifsc_num,
                                              autovalidateMode: autovalidate,
                                              style: TextStyle(
                                                  fontFamily: 'Arial',
                                                  color: AppColors.whiteColor,
                                                  fontSize: 22),
                                              validator: (val) {
                                                if (val.trim().isEmpty) {
                                                  return null;
                                                }

                                                return null;
                                              },
                                              decoration: InputDecoration(
                                                labelText: 'IFSC NUM',
                                                labelStyle: TextStyle(
                                                    fontFamily: 'Shruti',
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 20,
                                                    color: AppColors
                                                        .textFeildcolor),
                                                border: InputBorder.none,
                                              ),
                                            ),
                                            Container(
                                                child: Image.asset(
                                              'assets/images/Loginsignup/Line-1.png',
                                            )),

                                            TextFormField(
                                              controller: gst_num,
                                              autovalidateMode: autovalidate,
                                              style: TextStyle(
                                                  fontFamily: 'Arial',
                                                  color: AppColors.whiteColor,
                                                  fontSize: 22),
                                              validator: (val) {
                                                if (val.trim().isEmpty) {
                                                  return null;
                                                }

                                                return null;
                                              },
                                              decoration: InputDecoration(
                                                labelText: 'Gst Num',
                                                labelStyle: TextStyle(
                                                    fontFamily: 'Shruti',
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 20,
                                                    color: AppColors
                                                        .textFeildcolor),
                                                border: InputBorder.none,
                                              ),
                                            ),
                                            Container(
                                                child: Image.asset(
                                              'assets/images/Loginsignup/Line-1.png',
                                            )),
                                            TextFormField(
                                              controller: email,
                                              // autovalidateMode: autovalidate,
                                              keyboardType:
                                                  TextInputType.emailAddress,
                                              style: TextStyle(
                                                  fontFamily: 'Arial',
                                                  color: AppColors.whiteColor,
                                                  fontSize: 22),
                                              validator: (val) {
                                                if (val.trim().isEmpty) {
                                                  Global.isLoading = false;
                                                  return 'Please enter your Email';
                                                } else if (!isEmail(val)) {
                                                  return "Please enter a valid Email";
                                                }
                                                return null;
                                                // return Validators.mustEmail(
                                                //     val);
                                                return null;
                                              },
                                              decoration: InputDecoration(
                                                labelText: 'Email',
                                                labelStyle: TextStyle(
                                                    fontFamily: 'Shruti',
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 20,
                                                    color: AppColors
                                                        .textFeildcolor),
                                                border: InputBorder.none,
                                              ),
                                            ),
                                            Container(
                                                child: Image.asset(
                                              'assets/images/Loginsignup/Line-1.png',
                                            )),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 8.0),
                                              child: Text(
                                                'GENDER',
                                                style: TextStyle(
                                                    fontFamily: 'Shruti',
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 20,
                                                    color: AppColors
                                                        .textFeildcolor),
                                              ),
                                            ),

                                            Container(
                                              width: double.infinity,
                                              padding: EdgeInsets.fromLTRB(
                                                  18, 0, 8, 8),
                                              decoration: BoxDecoration(
                                                  image: DecorationImage(
                                                      image: ExactAssetImage(
                                                          'assets/images/Loginsignup/Field-01.png'),
                                                      fit: BoxFit.fill)),
                                              child:
                                                  new DropdownButtonFormField<
                                                      String>(
                                                isDense: true,
                                                style: TextStyle(
                                                    fontFamily: 'Shruti',
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 20,
                                                    color: AppColors
                                                        .textFeildcolor),
                                                hint: Text('SELECT GENDER',
                                                    style: TextStyle(
                                                        fontFamily: 'Shruti',
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        fontSize: 18,
                                                        color: AppColors
                                                            .textFeildcolor)),
                                                decoration: InputDecoration(
                                                    enabledBorder:
                                                        InputBorder.none),
                                                icon: Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          right: 8.0),
                                                  child: Image.asset(
                                                    'assets/images/Loginsignup/DropDown-Icon-01.png',
                                                    height: 15,
                                                    width: 15,
                                                  ),
                                                ),
                                                validator: (value) {
                                                  if (value == null) {
                                                    return "Choose gender please";
                                                  }
                                                  return null;
                                                },
                                                items: <String>[
                                                  'Male',
                                                  'Female',
                                                  'Not Specify',
                                                ].map((String value) {
                                                  return new DropdownMenuItem<
                                                      String>(
                                                    value: value,
                                                    child: new Text(value),
                                                  );
                                                }).toList(),
                                                onChanged: (val) {
                                                  setState(() {
                                                    gender = val;
                                                  });
                                                },
                                              ),
                                            ),
                                            Row(
                                              children: [
                                                Expanded(
                                                  child: TextFormField(
                                                    controller: dob,

                                                    // autovalidateMode:
                                                    //     autovalidate,
                                                    keyboardType:
                                                        TextInputType.phone,
                                                    style: TextStyle(
                                                        fontFamily: 'Arial',
                                                        color: AppColors
                                                            .whiteColor,
                                                        fontSize: 22),
                                                    validator: (val) {
                                                      if (val.trim().isEmpty) {
                                                        Global.isLoading =
                                                            false;
                                                        return 'Please enter your Date of Birth';
                                                      }

                                                      return null;
                                                    },
                                                    // inputFormatters: [DateTextFormatter],
                                                    decoration: InputDecoration(
                                                      labelText:
                                                          'DATE OF BIRTH',
                                                      labelStyle: TextStyle(
                                                          fontFamily: 'Shruti',
                                                          fontWeight:
                                                              FontWeight.w600,
                                                          fontSize: 20,
                                                          color: AppColors
                                                              .textFeildcolor),
                                                      border: InputBorder.none,
                                                    ),
                                                  ),
                                                ),
                                                GestureDetector(
                                                    onTap: () {
                                                      _selectDate(context);
                                                    },
                                                    child: Image.asset(
                                                      'assets/images/Loginsignup/Calender-Icon-01.png',
                                                      height: 30,
                                                      width: 30,
                                                    ))
                                              ],
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 8.0),
                                              child: TextFormField(
                                                controller: fHname,
                                                style: TextStyle(
                                                    fontFamily: 'Arial',
                                                    color: AppColors.whiteColor,
                                                    fontSize: 22),
                                                validator: (val) {
                                                  if (val.trim().isEmpty) {
                                                    Global.isLoading = false;
                                                    return 'Please enter Name ';
                                                  }
                                                  return null;
                                                },
                                                decoration: InputDecoration(
                                                  labelText:
                                                      'FATHER/HUSBAND NAME',
                                                  labelStyle: TextStyle(
                                                      fontFamily: 'Shruti',
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      fontSize: 20,
                                                      color: AppColors
                                                          .textFeildcolor),
                                                  border: InputBorder.none,
                                                ),
                                              ),
                                            ),
                                            Container(
                                                child: Image.asset(
                                              'assets/images/Loginsignup/Line-1.png',
                                            )),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 8.0),
                                              child: TextFormField(
                                                controller: address,
                                                style: TextStyle(
                                                    fontFamily: 'Arial',
                                                    color: AppColors.whiteColor,
                                                    fontSize: 22),
                                                validator: (val) {
                                                  if (val.trim().isEmpty) {
                                                    Global.isLoading = false;
                                                    return 'Please enter Address ';
                                                  }
                                                  return null;
                                                },
                                                decoration: InputDecoration(
                                                  labelText: 'ADDRESS1',
                                                  labelStyle: TextStyle(
                                                      fontFamily: 'Shruti',
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      fontSize: 20,
                                                      color: AppColors
                                                          .textFeildcolor),
                                                  border: InputBorder.none,
                                                ),
                                              ),
                                            ),
                                            Container(
                                                child: Image.asset(
                                              'assets/images/Loginsignup/Line-1.png',
                                            )),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 8.0),
                                              child: TextFormField(
                                                controller: village,
                                                style: TextStyle(
                                                    fontFamily: 'Arial',
                                                    color: AppColors.whiteColor,
                                                    fontSize: 22),
                                                validator: (val) {
                                                  if (val.trim().isEmpty) {
                                                    Global.isLoading = false;
                                                    return 'Please enter Village ';
                                                  }
                                                  return null;
                                                },
                                                decoration: InputDecoration(
                                                  labelText: 'VILLAGE',
                                                  labelStyle: TextStyle(
                                                      fontFamily: 'Shruti',
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      fontSize: 20,
                                                      color: AppColors
                                                          .textFeildcolor),
                                                  border: InputBorder.none,
                                                ),
                                              ),
                                            ),
                                            Container(
                                                child: Image.asset(
                                              'assets/images/Loginsignup/Line-1.png',
                                            )),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 8.0),
                                              child: TextFormField(
                                                controller: postoffice,
                                                style: TextStyle(
                                                    fontFamily: 'Arial',
                                                    color: AppColors.whiteColor,
                                                    fontSize: 22),
                                                validator: (val) {
                                                  if (val.trim().isEmpty) {
                                                    Global.isLoading = false;
                                                    return 'Please enter Post Office ';
                                                  }
                                                  return null;
                                                },
                                                decoration: InputDecoration(
                                                  labelText: 'POST OFFICE',
                                                  labelStyle: TextStyle(
                                                      fontFamily: 'Shruti',
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      fontSize: 20,
                                                      color: AppColors
                                                          .textFeildcolor),
                                                  border: InputBorder.none,
                                                ),
                                              ),
                                            ),
                                            Container(
                                                child: Image.asset(
                                              'assets/images/Loginsignup/Line-1.png',
                                            )),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 8.0),
                                              child: TextFormField(
                                                controller: thana,
                                                style: TextStyle(
                                                    fontFamily: 'Arial',
                                                    color: AppColors.whiteColor,
                                                    fontSize: 22),
                                                validator: (val) {
                                                  if (val.trim().isEmpty) {
                                                    Global.isLoading = false;
                                                    return 'Please enter Thane ';
                                                  }
                                                  return null;
                                                },
                                                decoration: InputDecoration(
                                                  labelText: 'THANA',
                                                  labelStyle: TextStyle(
                                                      fontFamily: 'Shruti',
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      fontSize: 20,
                                                      color: AppColors
                                                          .textFeildcolor),
                                                  border: InputBorder.none,
                                                ),
                                              ),
                                            ),
                                            Container(
                                                child: Image.asset(
                                              'assets/images/Loginsignup/Line-1.png',
                                            )),
                                            Text(
                                              'Organization',
                                              style: TextStyle(
                                                  fontFamily: 'Shruti',
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 20,
                                                  color:
                                                      AppColors.textFeildcolor),
                                            ),

                                            Container(
                                              width: double.infinity,
                                              padding: EdgeInsets.fromLTRB(
                                                  18, 0, 8, 8),
                                              decoration: BoxDecoration(
                                                  image: DecorationImage(
                                                      image: ExactAssetImage(
                                                          'assets/images/Loginsignup/Field-01.png'),
                                                      fit: BoxFit.fill)),
                                              child: DropdownButtonFormField<
                                                  String>(
                                                items: listOfOrganization.map(
                                                    (String
                                                        dropDownStringItem) {
                                                  return DropdownMenuItem<
                                                      String>(
                                                    value: dropDownStringItem,
                                                    child: Text(
                                                      dropDownStringItem,
                                                      style: TextStyle(
                                                          fontFamily: 'Shruti',
                                                          fontWeight:
                                                              FontWeight.w600,
                                                          fontSize: 18,
                                                          color: AppColors
                                                              .textFeildcolor),
                                                    ),
                                                  );
                                                }).toList(),
                                                onChanged: (value) {
                                                  setState(() {
                                                    selectedOrg = value;
                                                  });
                                                },
                                                validator: (value) =>
                                                    value == null
                                                        ? 'Feild Required'
                                                        : null,
                                                hint: Text(
                                                    'Select Organization',
                                                    style: TextStyle(
                                                        fontFamily: 'Shruti',
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        fontSize: 18,
                                                        color: AppColors
                                                            .textFeildcolor)),
                                              ),

                                              //     new DropdownButtonFormField<
                                              //         TransporterListResult>(
                                              //   isDense: true,
                                              //   isExpanded: true,
                                              //   style: TextStyle(
                                              //     fontFamily: 'Shruti',
                                              //     fontWeight: FontWeight.w600,
                                              //     fontSize: 20,
                                              //     color:
                                              //         AppColors.textFeildcolor,
                                              //   ),
                                              //   hint: Text('Select Transporter',
                                              //       style: TextStyle(
                                              //           fontFamily: 'Shruti',
                                              //           fontWeight:
                                              //               FontWeight.w600,
                                              //           fontSize: 18,
                                              //           color: AppColors
                                              //               .textFeildcolor)),
                                              //   decoration: InputDecoration(
                                              //       enabledBorder:
                                              //           InputBorder.none),
                                              //   icon: Padding(
                                              //     padding:
                                              //         const EdgeInsets.only(
                                              //             right: 8.0),
                                              //     child: Image.asset(
                                              //       'assets/images/Loginsignup/DropDown-Icon-01.png',
                                              //       height: 15,
                                              //       width: 15,
                                              //     ),
                                              //   ),
                                              //   validator: (value) =>
                                              //       value == null
                                              //           ? 'Feild Required'
                                              //           : null,
                                              //   items: transportorlist.map<
                                              //           DropdownMenuItem<
                                              //               TransporterListResult>>(
                                              //       (TransporterListResult
                                              //           value) {
                                              //     return new DropdownMenuItem<
                                              //         TransporterListResult>(
                                              //       value: value,
                                              //       child: new Text(value.name),
                                              //     );
                                              //   }).toList(),
                                              //   onChanged: (val) {
                                              //     setState(() {
                                              //       // selectedOrg = val,
                                              //       // selectedTransporter =
                                              //       //     val.sId;
                                              //     });
                                              //   },
                                              // ),
                                            ),
                                            (selectedOrg == 'Any Other')
                                                ? Text(
                                                    'Organization Name',
                                                    style: TextStyle(
                                                        fontFamily: 'Shruti',
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        fontSize: 20,
                                                        color: AppColors
                                                            .textFeildcolor),
                                                  )
                                                : Container(),

                                            (selectedOrg == 'Any Other')
                                                ? TextFormField(
                                                    controller: anyotherOrg,
                                                    style: TextStyle(
                                                        fontFamily: 'Arial',
                                                        color: AppColors
                                                            .whiteColor,
                                                        fontSize: 22),
                                                    validator: (val) {
                                                      if (val.trim().isEmpty) {
                                                        Global.isLoading =
                                                            false;
                                                        return 'Please enter Organization ';
                                                      }
                                                      return null;
                                                    },
                                                    decoration: InputDecoration(
                                                      border: InputBorder.none,
                                                      // labelText: 'Organization Name',
                                                      labelStyle: TextStyle(
                                                          fontFamily: 'Shruti',
                                                          fontWeight:
                                                              FontWeight.w600,
                                                          fontSize: 20,
                                                          color: AppColors
                                                              .textFeildcolor),
                                                      // border: InputBorder.none,
                                                    ),
                                                  )
                                                : Container(),
                                            (selectedOrg == 'Any Other')
                                                ? Container(
                                                    child: Image.asset(
                                                    'assets/images/Loginsignup/Line-1.png',
                                                  ))
                                                : Container(),
                                            Text(
                                              'COUNTRY',
                                              style: TextStyle(
                                                  fontFamily: 'Shruti',
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 20,
                                                  color:
                                                      AppColors.textFeildcolor),
                                            ),

                                            Container(
                                              width: double.infinity,
                                              padding: EdgeInsets.fromLTRB(
                                                  18, 0, 8, 8),
                                              decoration: BoxDecoration(
                                                  image: DecorationImage(
                                                      image: ExactAssetImage(
                                                          'assets/images/Loginsignup/Field-01.png'),
                                                      fit: BoxFit.fill)),
                                              child:
                                                  new DropdownButtonFormField<
                                                      String>(
                                                isDense: true,
                                                isExpanded: true,
                                                style: TextStyle(
                                                  fontFamily: 'Shruti',
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 20,
                                                  color:
                                                      AppColors.textFeildcolor,
                                                ),
                                                hint: Text('Select',
                                                    style: TextStyle(
                                                        fontFamily: 'Shruti',
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        fontSize: 18,
                                                        color: AppColors
                                                            .textFeildcolor)),
                                                decoration: InputDecoration(
                                                    enabledBorder:
                                                        InputBorder.none),
                                                icon: Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          right: 8.0),
                                                  child: Image.asset(
                                                    'assets/images/Loginsignup/DropDown-Icon-01.png',
                                                    height: 15,
                                                    width: 15,
                                                  ),
                                                ),
                                                validator: (value) =>
                                                    value == null
                                                        ? 'Feild Required'
                                                        : null,
                                                items: countryList
                                                    .map((String value) {
                                                  return new DropdownMenuItem<
                                                      String>(
                                                    value: value,
                                                    child: new Text(value),
                                                  );
                                                }).toList(),
                                                onChanged: (val) {
                                                  setState(() {
                                                    selectedCountry = val;
                                                  });
                                                },
                                              ),
                                            ),
                                            Text(
                                              'STATE',
                                              style: TextStyle(
                                                  fontFamily: 'Shruti',
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 20,
                                                  color:
                                                      AppColors.textFeildcolor),
                                            ),

                                            Container(
                                              width: double.infinity,
                                              padding: EdgeInsets.fromLTRB(
                                                  18, 0, 8, 8),
                                              decoration: BoxDecoration(
                                                  image: DecorationImage(
                                                      image: ExactAssetImage(
                                                          'assets/images/Loginsignup/Field-01.png'),
                                                      fit: BoxFit.fill)),
                                              child:
                                                  new DropdownButtonFormField<
                                                      StateResult>(
                                                isDense: true,
                                                style: TextStyle(
                                                    fontFamily: 'Shruti',
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 20,
                                                    color: AppColors
                                                        .textFeildcolor),
                                                hint: Text('Select State',
                                                    style: TextStyle(
                                                        fontFamily: 'Shruti',
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        fontSize: 18,
                                                        color: AppColors
                                                            .textFeildcolor)),
                                                decoration: InputDecoration(
                                                    enabledBorder:
                                                        InputBorder.none),
                                                icon: Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          right: 8.0),
                                                  child: Image.asset(
                                                    'assets/images/Loginsignup/DropDown-Icon-01.png',
                                                    height: 15,
                                                    width: 15,
                                                  ),
                                                ),
                                                validator: (value) =>
                                                    value == null
                                                        ? 'Feild Required'
                                                        : null,
                                                items: stateresult.map<
                                                        DropdownMenuItem<
                                                            StateResult>>(
                                                    (StateResult item) {
                                                  return DropdownMenuItem<
                                                      StateResult>(
                                                    value: item,
                                                    child: Text(item.name),
                                                  );
                                                }).toList(),
                                                onChanged: (value) {
                                                  print(value.sId);
                                                  setState(() {
                                                    stateid = value.sId;
                                                    fetchDistrict(stateid);
                                                  });
                                                },
                                              ),
                                            ),

                                            Text(
                                              'DISTRICT',
                                              style: TextStyle(
                                                  fontFamily: 'Shruti',
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 20,
                                                  color:
                                                      AppColors.textFeildcolor),
                                            ),

                                            Container(
                                              width: double.infinity,
                                              padding: EdgeInsets.fromLTRB(
                                                  18, 0, 8, 8),
                                              decoration: BoxDecoration(
                                                  image: DecorationImage(
                                                      image: ExactAssetImage(
                                                          'assets/images/Loginsignup/Field-01.png'),
                                                      fit: BoxFit.fill)),
                                              child:
                                                  new DropdownButtonFormField<
                                                      DistrictResult>(
                                                isDense: true,
                                                value: test,
                                                style: TextStyle(
                                                    fontFamily: 'Shruti',
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 20,
                                                    color: AppColors
                                                        .textFeildcolor),
                                                hint: Text('Select District',
                                                    style: TextStyle(
                                                        fontFamily: 'Shruti',
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        fontSize: 18,
                                                        color: AppColors
                                                            .textFeildcolor)),
                                                decoration: InputDecoration(
                                                    enabledBorder:
                                                        InputBorder.none),
                                                icon: Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          right: 8.0),
                                                  child: Image.asset(
                                                    'assets/images/Loginsignup/DropDown-Icon-01.png',
                                                    height: 15,
                                                    width: 15,
                                                  ),
                                                ),
                                                validator: (value) =>
                                                    value == null
                                                        ? 'Feild Required'
                                                        : null,
                                                items: districtresult.map<
                                                        DropdownMenuItem<
                                                            DistrictResult>>(
                                                    (DistrictResult item) {
                                                  return DropdownMenuItem<
                                                      DistrictResult>(
                                                    value: item,
                                                    child: Text(item.name),
                                                  );
                                                }).toList(),
                                                onChanged: stateid.isEmpty
                                                    ? null
                                                    : (value) {
                                                        print(value.sId);
                                                        setState(() {
                                                          test = value;
                                                          districtId =
                                                              value.sId;
                                                        });
                                                      },
                                              ),
                                            ),
                                            SizedBox(
                                              height: 30,
                                            ),
                                            Text(
                                              'Hub',
                                              style: TextStyle(
                                                  fontFamily: 'Shruti',
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 20,
                                                  color:
                                                      AppColors.textFeildcolor),
                                            ),

                                            Container(
                                              width: double.infinity,
                                              padding: EdgeInsets.fromLTRB(
                                                  18, 0, 8, 8),
                                              decoration: BoxDecoration(
                                                  image: DecorationImage(
                                                      image: ExactAssetImage(
                                                          'assets/images/Loginsignup/Field-01.png'),
                                                      fit: BoxFit.fill)),
                                              child:
                                                  new DropdownButtonFormField<
                                                      HubResult>(
                                                isDense: true,
                                                style: TextStyle(
                                                    fontFamily: 'Shruti',
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 20,
                                                    color: AppColors
                                                        .textFeildcolor),
                                                hint: Text('Select Hub',
                                                    style: TextStyle(
                                                        fontFamily: 'Shruti',
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        fontSize: 18,
                                                        color: AppColors
                                                            .textFeildcolor)),
                                                decoration: InputDecoration(
                                                    enabledBorder:
                                                        InputBorder.none),
                                                icon: Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          right: 8.0),
                                                  child: Image.asset(
                                                    'assets/images/Loginsignup/DropDown-Icon-01.png',
                                                    height: 15,
                                                    width: 15,
                                                  ),
                                                ),
                                                validator: (value) =>
                                                    value == null
                                                        ? 'Feild Required'
                                                        : null,
                                                items: hublist.map<
                                                        DropdownMenuItem<
                                                            HubResult>>(
                                                    (HubResult item) {
                                                  return DropdownMenuItem<
                                                      HubResult>(
                                                    value: item,
                                                    child: Text(item.name),
                                                  );
                                                }).toList(),
                                                onChanged: (value) {
                                                  print(value.sId);
                                                  setState(() {
                                                    hubname = value.sId;
                                                  });
                                                },
                                              ),
                                            ),
                                            TextFormField(
                                              controller: pincode,
                                              autovalidateMode: autovalidate,
                                              keyboardType: TextInputType.phone,
                                              style: TextStyle(
                                                  fontFamily: 'Arial',
                                                  color: AppColors.whiteColor,
                                                  fontSize: 22),
                                              validator: (val) {
                                                if (val.trim().isEmpty) {
                                                  Global.isLoading = false;
                                                  return 'Please enter Pincode';
                                                }

                                                return null;
                                              },
                                              maxLength: 6,
                                              decoration: InputDecoration(
                                                isDense: true,
                                                labelText: 'PIN CODE',
                                                labelStyle: TextStyle(
                                                    fontFamily: 'Shruti',
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 20,
                                                    color: AppColors
                                                        .textFeildcolor),
                                                border: InputBorder.none,
                                              ),
                                            ),
                                            Container(
                                                child: Image.asset(
                                              'assets/images/Loginsignup/Line-1.png',
                                            )),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 8.0),
                                              child: TextFormField(
                                                controller: mobileNo,
                                                maxLength: 10,
                                                keyboardType:
                                                    TextInputType.phone,
                                                style: TextStyle(
                                                    fontFamily: 'Arial',
                                                    color: AppColors.whiteColor,
                                                    fontSize: 22),
                                                validator: (val) {
                                                  if (val.trim().isEmpty) {
                                                    Global.isLoading = false;
                                                    return 'Please enter Mobile No ';
                                                  }
                                                  return null;
                                                },
                                                decoration: InputDecoration(
                                                  labelText: 'MOBILE NO',
                                                  labelStyle: TextStyle(
                                                      fontFamily: 'Shruti',
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      fontSize: 20,
                                                      color: AppColors
                                                          .textFeildcolor),
                                                  border: InputBorder.none,
                                                ),
                                              ),
                                            ),
                                            Container(
                                                child: Image.asset(
                                              'assets/images/Loginsignup/Line-1.png',
                                            )),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 8.0),
                                              child: TextFormField(
                                                controller: occupation,
                                                keyboardType:
                                                    TextInputType.name,
                                                style: TextStyle(
                                                    fontFamily: 'Arial',
                                                    color: AppColors.whiteColor,
                                                    fontSize: 22),
                                                validator: (val) {
                                                  if (val.trim().isEmpty) {
                                                    Global.isLoading = false;
                                                    return 'Please enter Occupation  ';
                                                  }
                                                  return null;
                                                },
                                                decoration: InputDecoration(
                                                  labelText: 'OCCUPATION',
                                                  labelStyle: TextStyle(
                                                      fontFamily: 'Shruti',
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      fontSize: 20,
                                                      color: AppColors
                                                          .textFeildcolor),
                                                  border: InputBorder.none,
                                                ),
                                              ),
                                            ),
                                            Container(
                                                child: Image.asset(
                                              'assets/images/Loginsignup/Line-1.png',
                                            )),
                                            //
                                            Text(
                                              'IDENTITY DOCUMENT',
                                              style: TextStyle(
                                                  fontFamily: 'Shruti',
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 20,
                                                  color:
                                                      AppColors.textFeildcolor),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 8.0),
                                              child: Container(
                                                width: double.infinity,
                                                padding: EdgeInsets.fromLTRB(
                                                    18, 0, 8, 8),
                                                decoration: BoxDecoration(
                                                    image: DecorationImage(
                                                        image: ExactAssetImage(
                                                            'assets/images/Loginsignup/Field-01.png'),
                                                        fit: BoxFit.fill)),
                                                child:
                                                    new DropdownButtonFormField<
                                                        String>(
                                                  isDense: true,
                                                  isExpanded: true,
                                                  style: TextStyle(
                                                    fontFamily: 'Shruti',
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 20,
                                                    color: AppColors
                                                        .textFeildcolor,
                                                  ),
                                                  hint: Text('Select Document',
                                                      style: TextStyle(
                                                          fontFamily: 'Shruti',
                                                          fontWeight:
                                                              FontWeight.w600,
                                                          fontSize: 18,
                                                          color: AppColors
                                                              .textFeildcolor)),
                                                  decoration: InputDecoration(
                                                      enabledBorder:
                                                          InputBorder.none),
                                                  icon: Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            right: 8.0),
                                                    child: Image.asset(
                                                      'assets/images/Loginsignup/DropDown-Icon-01.png',
                                                      height: 15,
                                                      width: 15,
                                                    ),
                                                  ),
                                                  validator: (value) =>
                                                      value == null
                                                          ? 'Feild Required'
                                                          : null,
                                                  items: documenttype
                                                      .map((String value) {
                                                    return new DropdownMenuItem<
                                                        String>(
                                                      value: value,
                                                      child: new Text(value),
                                                    );
                                                  }).toList(),
                                                  onChanged: (val) {
                                                    setState(() {
                                                      selectedDocument = val;
                                                    });
                                                  },
                                                ),
                                              ),
                                            ),
                                            // Container(
                                            //     child: Image.asset('assets/images/Loginsignup/Line-1.png',)),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 8.0),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Text(
                                                        'UPLOAD DOCUMENT',
                                                        style: TextStyle(
                                                            fontFamily:
                                                                'Shruti',
                                                            fontWeight:
                                                                FontWeight.w600,
                                                            fontSize: 20,
                                                            color: AppColors
                                                                .textFeildcolor),
                                                      ),
                                                      _image != null
                                                          ? Text(
                                                              'Image Uploaded',
                                                              style: TextStyle(
                                                                  fontFamily:
                                                                      'Arial',
                                                                  color: AppColors
                                                                      .whiteColor,
                                                                  fontSize: 22))
                                                          : Container()
                                                    ],
                                                  ),
                                                  GestureDetector(
                                                      onTap: () {
                                                        getImage(context);
                                                      },
                                                      child: Image.asset(
                                                        'assets/images/Loginsignup/Upload-Icon-01.png',
                                                        height: 50,
                                                        width: 50,
                                                      ))
                                                ],
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 8.0),
                                              child: Container(
                                                  child: Image.asset(
                                                'assets/images/Loginsignup/Line-1.png',
                                              )),
                                            ),

                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  bottom: 15.0),
                                              child: Center(
                                                child: Padding(
                                                  padding: EdgeInsets.only(
                                                      top: SizeConfig
                                                              .safeBlockVertical *
                                                          3),
                                                  child: GestureDetector(
                                                    onTap: loginSubmitted,
                                                    child: Container(
                                                      width: 230,
                                                      decoration: BoxDecoration(
                                                          image: DecorationImage(
                                                              image: ExactAssetImage(
                                                                  'assets/images/Loginsignup/BT-1.png'),
                                                              fit:
                                                                  BoxFit.fill)),
                                                      child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .all(15.0),
                                                        child: Center(
                                                          child: Text(
                                                            'SUBMIT',
                                                            style: TextStyle(
                                                                fontFamily:
                                                                    'Arial',
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w600,
                                                                color: AppColors
                                                                    .whiteColor,
                                                                fontSize: 18),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      )),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          })),
                    )))));
  }
}

class DateTextFormatter extends TextInputFormatter {
  static const _maxChars = 8;

  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    var text = _format(newValue.text, '/');
    return newValue.copyWith(text: text, selection: updateCursorPosition(text));
  }

  String _format(String value, String seperator) {
    value = value.replaceAll(seperator, '');
    var newString = '';

    for (int i = 0; i < min(value.length, _maxChars); i++) {
      newString += value[i];
      if ((i == 1 || i == 3) && i != value.length - 1) {
        newString += seperator;
      }
    }

    return newString;
  }

  TextSelection updateCursorPosition(String text) {
    return TextSelection.fromPosition(TextPosition(offset: text.length));
  }
}
