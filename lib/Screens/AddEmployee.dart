import 'dart:convert';
import 'dart:io' as Io;
import 'dart:io';
import 'package:blu_way/Constants/ConstantList.dart';
import 'package:blu_way/Constants/Style.dart';
import 'package:blu_way/Functions/network/api_call.dart';
import 'package:blu_way/Model/Auth/SEModel.dart';
import 'package:blu_way/Model/Auth/SupervisorModel.dart';
import 'package:blu_way/Model/DistictList.dart';
import 'package:blu_way/Model/StateList.dart';
import 'package:blu_way/Model/Supervisor/GetSupervisorModel.dart';
import 'package:blu_way/Model/hubListModel.dart';
import 'package:blu_way/Widget/Dialog.dart';
import 'package:blu_way/utlis/Country.dart';
import 'package:blu_way/utlis/global.dart';
import 'package:blu_way/utlis/theme.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:validators/validators.dart';

import 'Supervisors.dart';

class AddEmployee extends StatefulWidget {
  String title;
  Map supervisor;
  String body;
  String header;

  AddEmployee(
      {@required this.title,
      @required this.supervisor,
      @required this.body,
      @required this.header});

  @override
  _AddEmployeeState createState() => _AddEmployeeState();
}

class _AddEmployeeState extends State<AddEmployee> {
  final _formKey = GlobalKey<FormState>();
  List<HubResult> hublist = [];
  List<SupervisorResult> supervisorList = [];
  // List<SupervisorsList> supervisorList = [];
  StateList stateList = StateList();
  DistrictList distictList = DistrictList();
  List<StateResult> stateresult = [];
  List<DistrictResult> districtresult = [];
  DistrictResult test = DistrictResult();
  DateTime selectedDate = DateTime.now();
  TextEditingController dob = TextEditingController();

  List<String> documenttype = [
    'Aadhar Card',
    'Voter Id Card',
    'PAN Card',
    'Driving License'
  ];
  // List<String> supervisorList = List<String>();

  // GetSupervisorList(){

  // }

  getDatafromSharedPreference() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    token = sharedPreferences.getString("token");
    organization_id = sharedPreferences.getString("Organization_id");
    // body = sharedPreferences.getString("Organization_id");
    // header = sharedPreferences.getString("token");
    print("Organization id is: " + organization_id);
  }

  // TextEditingController dob = TextEditingController();
  String token = '';
  String userimg64 = '';
  String documentimg64 = '';
  File _userimagepath;
  File _documentimagepath;
  String supervisorId = ' ';
  String organization_id = ' ';
  String first_name;
  String last_name;
  String email;
  String gender;
  String usertype = ' ';
  // String dob = ' ';
  String father_name;
  String address;
  String village;
  String post_office;
  String thana;
  String country;
  String state_id = ' ';
  String district_id = ' ';
  String pincode;
  String occupation;
  String document;
  String mobile;
  String licence_number;
  String document_image_data;
  String user_image_data;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  addEmployee() async {
    if (_formKey.currentState.validate() &&
        _userimagepath != null &&
        _documentimagepath != null) {
      _formKey.currentState.save();
      setState(() {
        Dialogs.showLoadingDialog(context, _keyLoader);
      });
      // 'Superviser',
      //                           'Sales Executive',
      if (usertype == 'Superviser') {
        final AddSupervisorModel supervisorModel =
            await ApiCall.addSupervisorModel(token,
                // id: organization_id,
                id: organization_id,
                fname: first_name,
                lname: last_name,
                email: email,
                gender: gender,
                dob: dob.text,
                fathername: father_name,
                postoffice: post_office,
                thana: thana,
                country: country,
                statename: state_id,
                districtname: district_id,
                pincode: pincode,
                occupation: occupation,
                doucment: document,
                documentimage: documentimg64,
                mobileNo: mobile,
                village: village,
                licenceno: licence_number,
                address: address,
                profileimage: userimg64);
        // .then((value) {
        print("Message is: " + supervisorModel.message);
        if (supervisorModel.status == 'success') {
          // print("Added supervisor is")
          Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
          print("sup status " + supervisorModel.status);
          Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (context) {
            return SupervisorsList();
          }));

          // if (supervisorModel.token.isNotEmpty) {
          //   setState(() {
          //     Global.accessToken = supervisorModel.token;
          //   });
          // }
          // Toast.show(value.message, context);
          // _showSuccessDialog(value.message);
        } else {
          Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
          Toast.show(supervisorModel.message, context);
        }
      } else if (usertype == 'Sales Executive') {
        final AddSEModel addSEModel = await ApiCall.addSEModel(token,
            id: organization_id,
            fname: first_name,
            lname: last_name,
            email: email,
            gender: gender,
            dob: dob.text,
            fathername: father_name,
            postoffice: post_office,
            thana: thana,
            country: country,
            statename: state_id,
            districtname: district_id,
            pincode: pincode,
            occupation: occupation,
            doucment: document,
            documentimage: documentimg64,
            mobileNo: mobile,
            village: village,
            licenceno: licence_number,
            address: address,
            profileimage: userimg64,
            supervisorId: supervisorId);
        if (addSEModel.status == 'success') {
          Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
          print(addSEModel.status);
          Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (context) {
            return SupervisorsList();
          }));

          if (addSEModel.token.isNotEmpty) {
            setState(() {
              Global.accessToken = addSEModel.token;
            });
          }
          // Toast.show(value.message, context);
          // _showSuccessDialog(value.message);
        } else {
          Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
          Toast.show(addSEModel.message, context);
        }
      }
      // });
    } else if (userimg64.isEmpty) {
      Toast.show('Please upload ProfilePicture', context);
    } else if (documentimg64.isEmpty) {
      Toast.show('Please upload Document', context);
    }
  }

  // DateTime picked;

  initData() {
    fetchStateListModel().then((value) {
      setState(() {
        stateList = value;
        stateresult = stateList.result;
      });
    });

    hubListModel().then((value) {
      setState(() {
        hublist = value.hubResult;
      });
    });

    getSupervisorList(widget.body, widget.header).then((value) {
      setState(() {
        supervisorList = value.result;
      });
    });
  }

  _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime(1980),
      lastDate: DateTime(2025),
    ).then((selectedDate) {
      if (selectedDate != null) {
        dob.text = DateFormat('yyyy-MM-dd').format(selectedDate);
      }
    });
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  fetchDistrict(String id) {
    fetchDistrictListModel(id).then((value) {
      setState(() {
        distictList = value;
        districtresult = distictList.result;
        print(distictList.result.isEmpty);
        test = distictList.result[0];
      });
    });
  }

  Future getUserImage(BuildContext context) async {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
              content: SizedBox(
            height: 150,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                FlatButton(
                  child: Text(
                    "Camera",
                    style: TextStyle(fontSize: 26),
                  ),
                  onPressed: () async {
                    var image = await ImagePicker().getImage(
                      source: ImageSource.camera,
                      imageQuality: 50,
                    );
                    // var image = await ImagePicker()
                    //     .getImage(source: ImageSource.camera);
                    setState(() {
                      // _image = image;
                      _userimagepath = File(image.path);
                      List<int> bytes =
                          Io.File(_userimagepath.path).readAsBytesSync();
                      userimg64 = base64UrlEncode(bytes);
                      // var base64String =
                      //     base64UrlEncode(_image.readAsBytesSync());
                      // base64String = base64Encode(_image.readAsBytesSync());
                      // print('document $img64');
                      // print('base64String $base64String');
                      Navigator.pop(context);
                    });
                  },
                ),
                FlatButton(
                  child: Text(
                    "Gallery",
                    style: TextStyle(fontSize: 26),
                  ),
                  onPressed: () async {
                    var image = await ImagePicker().getImage(
                      source: ImageSource.gallery,
                      imageQuality: 50,
                    );

                    setState(() {
                      _userimagepath = File(image.path);
                      // print(_image.uri.toString());
                      List<int> bytes =
                          Io.File(_userimagepath.path).readAsBytesSync();
                      userimg64 = base64UrlEncode(bytes);
                      print('img64 : $userimg64');
                      print('bytes $bytes');

                      print('image1 : $_userimagepath');
                      Navigator.pop(context);
                    });
                  },
                ),
                _userimagepath != null
                    ? FlatButton(
                        child: Text(
                          "Remove Profile",
                          style: TextStyle(fontSize: 26),
                        ),
                        onPressed: () async {
                          setState(() {
                            _userimagepath = null;
                            Navigator.pop(context);
                          });
                        },
                      )
                    : Text(
                        "",
                      ),
              ],
            ),
          ));
        });
  }

  Future getDocumentImage(BuildContext context) async {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
              content: SizedBox(
            height: 150,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                FlatButton(
                  child: Text(
                    "Camera",
                    style: TextStyle(fontSize: 26),
                  ),
                  onPressed: () async {
                    var image = await ImagePicker().getImage(
                      source: ImageSource.camera,
                      imageQuality: 50,
                    );
                    // var image = await ImagePicker()
                    //     .getImage(source: ImageSource.camera);
                    setState(() {
                      // _image = image;
                      _documentimagepath = File(image.path);
                      List<int> bytes =
                          Io.File(_documentimagepath.path).readAsBytesSync();
                      documentimg64 = base64UrlEncode(bytes);
                      // var base64String =
                      //     base64UrlEncode(_image.readAsBytesSync());
                      // base64String = base64Encode(_image.readAsBytesSync());
                      // print('document $img64');
                      // print('base64String $base64String');
                      Navigator.pop(context);
                    });
                  },
                ),
                FlatButton(
                  child: Text(
                    "Gallery",
                    style: TextStyle(fontSize: 26),
                  ),
                  onPressed: () async {
                    var image = await ImagePicker().getImage(
                      source: ImageSource.gallery,
                      imageQuality: 50,
                    );
                    // var image = await ImagePicker()
                    //     .getImage(source: ImageSource.gallery);
                    setState(() {
                      _documentimagepath = File(image.path);
                      // print(_image.uri.toString());
                      List<int> bytes =
                          Io.File(_documentimagepath.path).readAsBytesSync();
                      documentimg64 = base64UrlEncode(bytes);
                      print('img64 : $documentimg64');
                      print('bytes $bytes');
                      // print('base64Image : $img64');
                      // base64String = base64Encode(_image.readAsBytesSync());
                      // print('document $img64');
                      // print('base64String $base64String');
                      print('image1 : $_documentimagepath');
                      Navigator.pop(context);
                    });
                  },
                ),
                _documentimagepath != null
                    ? FlatButton(
                        child: Text(
                          "Remove Profile",
                          style: TextStyle(fontSize: 26),
                        ),
                        onPressed: () async {
                          setState(() {
                            _documentimagepath = null;
                            Navigator.pop(context);
                          });
                        },
                      )
                    : Text(
                        "",
                      ),
              ],
            ),
          ));
        });
  }

  @override
  void initState() {
    // TODO: implement initState
    initData();
    getDatafromSharedPreference();
    print("Organization id is: " + organization_id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: Container(
          decoration: BoxDecoration(gradient: kAppBarGradient),
          child: SafeArea(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Image.asset('assets/images/Icon-Holder.png'),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 3.0),
                  child: Image.asset('assets/images/Header-Logo.png'),
                ),
                SizedBox(
                  width: 20,
                ),
                Center(
                  child: Text(
                    widget.title,
                    style: GoogleFonts.roboto(
                        color: Color(0xff031e48),
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(
                  width: 60,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Image.asset('assets/images/Icon-Holder.png'),
                      Icon(
                        Icons.close,
                        color: Colors.white,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      body: CustomContainer(
        child: Container(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 15),
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                // color: Colors.white,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Form(
                key: _formKey,
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'ADD EMPLOYEE',
                        style: TextStyle(
                          fontSize: 30,
                          fontFamily: 'Shruti',
                          color: Color(0xffaedcff),
                          shadows: <Shadow>[
                            Shadow(
                              offset: Offset(1.0, 1.0),
                              blurRadius: 3.0,
                              color: Color.fromARGB(255, 0, 0, 0),
                            ),
                            Shadow(
                              offset: Offset(1.0, 1.0),
                              blurRadius: 8.0,
                              color: Color.fromARGB(125, 0, 0, 255),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      OutlineButton(
                          color: Color(0xFF819ffd),
                          onPressed: () {
                            getUserImage(context);
                          },
                          child: (_userimagepath == null)
                              ? Container(
                                  width: 300,
                                  height: 300,
                                  decoration: BoxDecoration(
                                    color: Color(0xFF819ffd),
                                    borderRadius: BorderRadius.circular(300),
                                  ),
                                  child: Center(
                                      child: Text("Tap to add Image",
                                          style:
                                              TextStyle(color: Colors.black))),
                                )
                              : Container(
                                  width: 300,
                                  height: 300,
                                  // child:
                                  decoration: BoxDecoration(
                                      color: Colors.red,
                                      borderRadius: BorderRadius.circular(300),
                                      image: DecorationImage(
                                          image: FileImage(
                                            _userimagepath,
                                          ),
                                          fit: BoxFit.fill)),
                                )),
                      SizedBox(
                        height: 20,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Text(
                              'EMPLOYEE TYPE',
                              style: TextStyle(
                                  fontFamily: 'Shruti',
                                  fontWeight: FontWeight.w600,
                                  fontSize: 20,
                                  color: AppColors.textFeildcolor),
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width * 0.80,
                            padding: EdgeInsets.fromLTRB(18, 0, 8, 8),
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: ExactAssetImage(
                                        'assets/images/Loginsignup/Field-01.png'),
                                    fit: BoxFit.fill)),
                            child: new DropdownButtonFormField<String>(
                              isDense: true,
                              style: TextStyle(
                                  fontFamily: 'Shruti',
                                  fontWeight: FontWeight.w600,
                                  fontSize: 20,
                                  color: AppColors.textFeildcolor),
                              hint: Text('SELECT EMPLOYEE',
                                  style: TextStyle(
                                      fontFamily: 'Shruti',
                                      fontWeight: FontWeight.w600,
                                      fontSize: 18,
                                      color: AppColors.textFeildcolor)),
                              decoration: InputDecoration(
                                  enabledBorder: InputBorder.none),
                              icon: Padding(
                                padding: const EdgeInsets.only(right: 8.0),
                                child: Image.asset(
                                  'assets/images/Loginsignup/DropDown-Icon-01.png',
                                  height: 15,
                                  width: 15,
                                ),
                              ),
                              validator: (value) {
                                if (value == null) {
                                  return "Choose Employee please";
                                }
                                return null;
                              },
                              items: <String>[
                                'Superviser',
                                'Sales Executive',
                              ].map((String value) {
                                return new DropdownMenuItem<String>(
                                  value: value,
                                  child: new Text(value),
                                );
                              }).toList(),
                              onChanged: (val) {
                                setState(() {
                                  usertype = val;
                                });
                              },
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          (usertype == 'Sales Executive')
                              ? Padding(
                                  padding: const EdgeInsets.only(top: 8.0),
                                  child: Text(
                                    'SUPERVISORS',
                                    style: TextStyle(
                                        fontFamily: 'Shruti',
                                        fontWeight: FontWeight.w600,
                                        fontSize: 20,
                                        color: AppColors.textFeildcolor),
                                  ),
                                )
                              : Container(),
                          (usertype == 'Sales Executive')
                              ? SizedBox(
                                  height: 20,
                                )
                              : Container(),
                          (usertype == 'Sales Executive')
                              ? Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.80,
                                  padding: EdgeInsets.fromLTRB(18, 0, 8, 8),
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                          image: ExactAssetImage(
                                              'assets/images/Loginsignup/Field-01.png'),
                                          fit: BoxFit.fill)),
                                  child: new DropdownButtonFormField<
                                      SupervisorResult>(
                                    isDense: true,
                                    style: TextStyle(
                                        fontFamily: 'Shruti',
                                        fontWeight: FontWeight.w600,
                                        fontSize: 20,
                                        color: AppColors.textFeildcolor),
                                    hint: Text('Select Supervisor',
                                        style: TextStyle(
                                            fontFamily: 'Shruti',
                                            fontWeight: FontWeight.w600,
                                            fontSize: 18,
                                            color: AppColors.textFeildcolor)),
                                    decoration: InputDecoration(
                                        enabledBorder: InputBorder.none),
                                    icon: Padding(
                                      padding:
                                          const EdgeInsets.only(right: 8.0),
                                      child: Image.asset(
                                        'assets/images/Loginsignup/DropDown-Icon-01.png',
                                        height: 15,
                                        width: 15,
                                      ),
                                    ),
                                    validator: (value) =>
                                        value == null ? 'Feild Required' : null,
                                    items: supervisorList.map<
                                            DropdownMenuItem<SupervisorResult>>(
                                        (SupervisorResult item) {
                                      return DropdownMenuItem<SupervisorResult>(
                                        value: item,
                                        child: Text(item.firstName +
                                            " " +
                                            item.lastName),
                                      );
                                    }).toList(),
                                    onChanged: (value) {
                                      print(value.sId);
                                      setState(() {
                                        supervisorId = value.sId;
                                      });
                                    },
                                  ),
                                )
                              : Container(),
                          SizedBox(
                            height: 20,
                          ),
                          TextFormInput(
                            labelText: "First Name",
                            validator: (value) {
                              if (!isAlpha(value)) {
                                return "Enter a valid name";
                              }
                              return null;
                            },
                            onSaved: (value) {
                              setState(() {
                                first_name = value;
                              });
                            },
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          TextFormInput(
                            labelText: "Last Name",
                            validator: (value) {
                              if (!isAlpha(value)) {
                                return "Enter a valid last name";
                              } else
                                return null;
                            },
                            onSaved: (value) {
                              setState(() {
                                last_name = value;
                              });
                            },
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          TextFormInput(
                            labelText: "Email",
                            textInputType: TextInputType.emailAddress,
                            validator: (value) {
                              if (!isEmail(value)) {
                                return "Enter a valid Email";
                              }
                            },
                            onSaved: (value) {
                              setState(() {
                                email = value;
                              });
                            },
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: TextFormField(
                                  controller: dob,

                                  // autovalidateMode:
                                  //     autovalidate,
                                  keyboardType: TextInputType.phone,
                                  style: TextStyle(
                                      fontFamily: 'Arial',
                                      color: AppColors.whiteColor,
                                      fontSize: 22),
                                  validator: (val) {
                                    if (val.trim().isEmpty) {
                                      Global.isLoading = false;
                                      return 'Please enter your Date of Birth';
                                    }

                                    return null;
                                  },
                                  // inputFormatters: [DateTextFormatter],
                                  decoration: InputDecoration(
                                    labelText: 'DATE OF BIRTH',
                                    labelStyle: TextStyle(
                                        fontFamily: 'Shruti',
                                        fontWeight: FontWeight.w600,
                                        fontSize: 20,
                                        color: AppColors.textFeildcolor),
                                    border: InputBorder.none,
                                  ),
                                ),
                              ),
                              GestureDetector(
                                  onTap: () {
                                    _selectDate(context);
                                  },
                                  child: Image.asset(
                                    'assets/images/Loginsignup/Calender-Icon-01.png',
                                    height: 30,
                                    width: 30,
                                  ))
                            ],
                          ),
                          Container(
                              child: Image.asset(
                            'assets/images/Loginsignup/Line-1.png',
                          )),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Text(
                              'GENDER',
                              style: TextStyle(
                                  fontFamily: 'Shruti',
                                  fontWeight: FontWeight.w600,
                                  fontSize: 20,
                                  color: AppColors.textFeildcolor),
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width * 0.80,
                            padding: EdgeInsets.fromLTRB(18, 0, 8, 8),
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: ExactAssetImage(
                                        'assets/images/Loginsignup/Field-01.png'),
                                    fit: BoxFit.fill)),
                            child: new DropdownButtonFormField<String>(
                              isDense: true,
                              style: TextStyle(
                                  fontFamily: 'Shruti',
                                  fontWeight: FontWeight.w600,
                                  fontSize: 20,
                                  color: AppColors.textFeildcolor),
                              hint: Text('SELECT GENDER',
                                  style: TextStyle(
                                      fontFamily: 'Shruti',
                                      fontWeight: FontWeight.w600,
                                      fontSize: 18,
                                      color: AppColors.textFeildcolor)),
                              decoration: InputDecoration(
                                  enabledBorder: InputBorder.none),
                              icon: Padding(
                                padding: const EdgeInsets.only(right: 8.0),
                                child: Image.asset(
                                  'assets/images/Loginsignup/DropDown-Icon-01.png',
                                  height: 15,
                                  width: 15,
                                ),
                              ),
                              validator: (value) {
                                if (value == null) {
                                  return "Choose gender please";
                                }
                                return null;
                              },
                              items: <String>[
                                'Male',
                                'Female',
                                'Not Specify',
                              ].map((String value) {
                                return new DropdownMenuItem<String>(
                                  value: value,
                                  child: new Text(value),
                                );
                              }).toList(),
                              onChanged: (val) {
                                setState(() {
                                  gender = val;
                                });
                              },
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          TextFormInput(
                            labelText: "Father Name",
                            validator: (value) {
                              if (!isAlpha(value)) {
                                return "Enter a valid last name";
                              } else
                                return null;
                            },
                            onSaved: (value) {
                              setState(() {
                                father_name = value;
                              });
                            },
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          TextFormInput(
                            labelText: "Address",
                            validator: (value) {
                              if (!isAlpha(value)) {
                                return "Enter a valid last name";
                              } else
                                return null;
                            },
                            onSaved: (value) {
                              setState(() {
                                address = value;
                              });
                            },
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          TextFormInput(
                            labelText: "Village",
                            validator: (value) {
                              if (!isAlpha(value)) {
                                return "Enter a valid last name";
                              } else
                                return null;
                            },
                            onSaved: (value) {
                              setState(() {
                                village = value;
                              });
                            },
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          TextFormInput(
                            labelText: "Post Office",
                            validator: (value) {
                              if (!isAlpha(value)) {
                                return "Enter a valid last name";
                              } else
                                return null;
                            },
                            onSaved: (value) {
                              setState(() {
                                post_office = value;
                              });
                            },
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          TextFormInput(
                            labelText: "Thana",
                            validator: (value) {
                              if (!isAlpha(value)) {
                                return "Enter a valid last name";
                              } else
                                return null;
                            },
                            onSaved: (value) {
                              setState(() {
                                thana = value;
                              });
                            },
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                            'COUNTRY',
                            style: TextStyle(
                                fontFamily: 'Shruti',
                                fontWeight: FontWeight.w600,
                                fontSize: 20,
                                color: AppColors.textFeildcolor),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width * .80,
                            padding: EdgeInsets.fromLTRB(18, 0, 8, 8),
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: ExactAssetImage(
                                        'assets/images/Loginsignup/Field-01.png'),
                                    fit: BoxFit.fill)),
                            child: new DropdownButtonFormField<String>(
                              isDense: true,
                              isExpanded: true,
                              style: TextStyle(
                                fontFamily: 'Shruti',
                                fontWeight: FontWeight.w600,
                                fontSize: 20,
                                color: AppColors.textFeildcolor,
                              ),
                              hint: Text('Country',
                                  style: TextStyle(
                                      fontFamily: 'Shruti',
                                      fontWeight: FontWeight.w600,
                                      fontSize: 18,
                                      color: AppColors.textFeildcolor)),
                              decoration: InputDecoration(
                                  enabledBorder: InputBorder.none),
                              icon: Padding(
                                padding: const EdgeInsets.only(right: 8.0),
                                child: Image.asset(
                                  'assets/images/Loginsignup/DropDown-Icon-01.png',
                                  height: 15,
                                  width: 15,
                                ),
                              ),
                              validator: (value) =>
                                  value == null ? 'Feild Required' : null,
                              items: countryList.map((String value) {
                                return new DropdownMenuItem<String>(
                                  value: value,
                                  child: new Text(value),
                                );
                              }).toList(),
                              onChanged: (val) {
                                setState(() {
                                  country = val;
                                });
                              },
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                            'STATE',
                            style: TextStyle(
                                fontFamily: 'Shruti',
                                fontWeight: FontWeight.w600,
                                fontSize: 20,
                                color: AppColors.textFeildcolor),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width * 0.80,
                            padding: EdgeInsets.fromLTRB(18, 0, 8, 8),
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: ExactAssetImage(
                                        'assets/images/Loginsignup/Field-01.png'),
                                    fit: BoxFit.fill)),
                            child: new DropdownButtonFormField<StateResult>(
                              isDense: true,
                              style: TextStyle(
                                  fontFamily: 'Shruti',
                                  fontWeight: FontWeight.w600,
                                  fontSize: 20,
                                  color: AppColors.textFeildcolor),
                              hint: Text('Select State',
                                  style: TextStyle(
                                      fontFamily: 'Shruti',
                                      fontWeight: FontWeight.w600,
                                      fontSize: 18,
                                      color: AppColors.textFeildcolor)),
                              decoration: InputDecoration(
                                  enabledBorder: InputBorder.none),
                              icon: Padding(
                                padding: const EdgeInsets.only(right: 8.0),
                                child: Image.asset(
                                  'assets/images/Loginsignup/DropDown-Icon-01.png',
                                  height: 15,
                                  width: 15,
                                ),
                              ),
                              validator: (value) =>
                                  value == null ? 'Feild Required' : null,
                              items: stateresult
                                  .map<DropdownMenuItem<StateResult>>(
                                      (StateResult item) {
                                return DropdownMenuItem<StateResult>(
                                  value: item,
                                  child: Text(item.name),
                                );
                              }).toList(),
                              onChanged: (value) {
                                print(value.sId);
                                setState(() {
                                  state_id = value.sId;
                                  fetchDistrict(state_id);
                                });
                              },
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                            'DISTRICT',
                            style: TextStyle(
                                fontFamily: 'Shruti',
                                fontWeight: FontWeight.w600,
                                fontSize: 20,
                                color: AppColors.textFeildcolor),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width * 0.80,
                            padding: EdgeInsets.fromLTRB(18, 0, 8, 8),
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: ExactAssetImage(
                                        'assets/images/Loginsignup/Field-01.png'),
                                    fit: BoxFit.fill)),
                            child: new DropdownButtonFormField<DistrictResult>(
                              isDense: true,
                              value: test,
                              style: TextStyle(
                                  fontFamily: 'Shruti',
                                  fontWeight: FontWeight.w600,
                                  fontSize: 20,
                                  color: AppColors.textFeildcolor),
                              hint: Text('Select District',
                                  style: TextStyle(
                                      fontFamily: 'Shruti',
                                      fontWeight: FontWeight.w600,
                                      fontSize: 18,
                                      color: AppColors.textFeildcolor)),
                              decoration: InputDecoration(
                                  enabledBorder: InputBorder.none),
                              icon: Padding(
                                padding: const EdgeInsets.only(right: 8.0),
                                child: Image.asset(
                                  'assets/images/Loginsignup/DropDown-Icon-01.png',
                                  height: 15,
                                  width: 15,
                                ),
                              ),
                              validator: (value) =>
                                  value == null ? 'Feild Required' : null,
                              items: districtresult
                                  .map<DropdownMenuItem<DistrictResult>>(
                                      (DistrictResult item) {
                                return DropdownMenuItem<DistrictResult>(
                                  value: item,
                                  child: Text(item.name),
                                );
                              }).toList(),
                              onChanged: state_id.isEmpty
                                  ? null
                                  : (value) {
                                      print(value.sId);
                                      setState(() {
                                        test = value;
                                        district_id = value.sId;
                                      });
                                    },
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          TextFormInput(
                            labelText: "Pincode",
                            textInputType: TextInputType.number,
                            maxlength: 6,
                            validator: (value) {
                              if (!isNumeric(value)) {
                                return "Enter a valid Pincode";
                              } else
                                return null;
                            },
                            onSaved: (value) {
                              setState(() {
                                pincode = value;
                              });
                            },
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          TextFormInput(
                            labelText: "Occupation",
                            validator: (value) {
                              if (!isAlpha(value)) {
                                return "Enter a valid occupation";
                              } else
                                return null;
                            },
                            onSaved: (value) {
                              setState(() {
                                occupation = value;
                              });
                            },
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          TextFormInput(
                            maxlength: 10,
                            textInputType: TextInputType.number,
                            labelText: "Mobile",
                            validator: (value) {
                              if (!isNumeric(value)) {
                                return "Enter a valid number";
                              } else
                                return null;
                            },
                            onSaved: (value) {
                              setState(() {
                                mobile = value;
                              });
                            },
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          TextFormInput(
                            labelText: "Licence Number",
                            validator: (value) {
                              if (!isAlpha(value)) {
                                return "Enter a valid licence number";
                              } else
                                return null;
                            },
                            onSaved: (value) {
                              setState(() {
                                licence_number = value;
                              });
                            },
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                            'IDENTITY DOCUMENT',
                            style: TextStyle(
                                fontFamily: 'Shruti',
                                fontWeight: FontWeight.w600,
                                fontSize: 20,
                                color: AppColors.textFeildcolor),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Container(
                              width: MediaQuery.of(context).size.width * 0.80,
                              padding: EdgeInsets.fromLTRB(18, 0, 8, 8),
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: ExactAssetImage(
                                          'assets/images/Loginsignup/Field-01.png'),
                                      fit: BoxFit.fill)),
                              child: new DropdownButtonFormField<String>(
                                isDense: true,
                                isExpanded: true,
                                style: TextStyle(
                                  fontFamily: 'Shruti',
                                  fontWeight: FontWeight.w600,
                                  fontSize: 20,
                                  color: AppColors.textFeildcolor,
                                ),
                                hint: Text('Select Document',
                                    style: TextStyle(
                                        fontFamily: 'Shruti',
                                        fontWeight: FontWeight.w600,
                                        fontSize: 18,
                                        color: AppColors.textFeildcolor)),
                                decoration: InputDecoration(
                                    enabledBorder: InputBorder.none),
                                icon: Padding(
                                  padding: const EdgeInsets.only(right: 8.0),
                                  child: Image.asset(
                                    'assets/images/Loginsignup/DropDown-Icon-01.png',
                                    height: 15,
                                    width: 15,
                                  ),
                                ),
                                validator: (value) =>
                                    value == null ? 'Feild Required' : null,
                                items: documenttype.map((String value) {
                                  return new DropdownMenuItem<String>(
                                    value: value,
                                    child: new Text(value),
                                  );
                                }).toList(),
                                onChanged: (val) {
                                  setState(() {
                                    document = val;
                                  });
                                },
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                flex: 3,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      (_documentimagepath == null)
                                          ? "Upload Image"
                                          : "Image Uploaded",
                                      style: TextStyle(
                                          fontFamily: 'Shruti',
                                          fontWeight: FontWeight.w600,
                                          fontSize: 20,
                                          color: AppColors.textFeildcolor),
                                    ),
                                    Container(
                                        child: Image.asset(
                                      'assets/images/Loginsignup/Line-1.png',
                                    )),
                                  ],
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: OutlineButton(
                                    // color: Colors.blue[900],
                                    //   borderSide: BorderSide.,
                                    child: (_documentimagepath == null)
                                        ? Container(
                                            height: 100,
                                            width: 100,
                                            decoration: BoxDecoration(
                                              color: Color(0xFF819ffd),
                                              borderRadius:
                                                  BorderRadius.circular(100),
                                            ),
                                            child: Center(
                                                child: Text(
                                              "Tap to add Image",
                                              style: TextStyle(
                                                  color: Colors.black),
                                              textAlign: TextAlign.center,
                                            )),
                                          )
                                        : Container(
                                            width: 80,
                                            height: 80,
                                            // child:
                                            decoration: BoxDecoration(
                                                // color: Colors.red,
                                                borderRadius:
                                                    BorderRadius.circular(300),
                                                image: DecorationImage(
                                                    image: FileImage(
                                                      _documentimagepath,
                                                    ),
                                                    fit: BoxFit.fill)),
                                          ),
                                    onPressed: () {
                                      getDocumentImage(context);
                                    }),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 30, bottom: 30),
                            child: Center(
                              child: GestureDetector(
                                onTap: () {
                                  addEmployee();
                                },
                                child: Container(
                                  width: 230,
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                          image: ExactAssetImage(
                                              'assets/images/Loginsignup/BT-1.png'),
                                          fit: BoxFit.fill)),
                                  child: Padding(
                                    padding: const EdgeInsets.all(15.0),
                                    child: Center(
                                      child: Text(
                                        'SUBMIT',
                                        style: TextStyle(
                                            fontFamily: 'Arial',
                                            fontWeight: FontWeight.w600,
                                            color: AppColors.whiteColor,
                                            fontSize: 18),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class TextFormInput extends StatefulWidget {
  final String labelText;
  final Function validator;
  final Function onSaved;
  final TextInputType textInputType;
  final int maxlength;

  // bool readOnly = false;

  const TextFormInput({
    Key key,
    @required this.labelText,
    @required this.validator,
    @required this.onSaved,
    this.textInputType,
    this.maxlength,
    // this.readOnly,
  }) : super(key: key);

  @override
  _TextFormInputState createState() => _TextFormInputState();
}

class _TextFormInputState extends State<TextFormInput> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TextFormField(
          controller: null,
          readOnly: false,
          inputFormatters: [
            new LengthLimitingTextInputFormatter(widget.maxlength),
          ],
          // autovalidateMode: autovalidate,
          keyboardType: widget.textInputType,
          style: TextStyle(
              fontFamily: 'Arial', color: AppColors.whiteColor, fontSize: 22),
          validator: widget.validator,
          onSaved: widget.onSaved,
          decoration: InputDecoration(
            labelText: widget.labelText,
            labelStyle: TextStyle(
                fontFamily: 'Shruti',
                fontWeight: FontWeight.w600,
                fontSize: 20,
                color: AppColors.textFeildcolor),
            border: InputBorder.none,
          ),
        ),
        Container(
            child: Image.asset(
          'assets/images/Loginsignup/Line-1.png',
        )),
      ],
    );
  }
}
