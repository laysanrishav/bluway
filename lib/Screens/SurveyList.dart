// import 'package:flutter/material.dart';
// import 'package:google_fonts/google_fonts.dart';
// import 'package:navigationdrawer/Constants/ConstantList.dart';
// import 'package:navigationdrawer/Constants/Style.dart';
// import 'package:navigationdrawer/Functions/network/api_call.dart';
// import 'package:navigationdrawer/Model/SurveyListResponse.dart';
// import 'package:navigationdrawer/Model/stateListResponse.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:navigationdrawer/Screens/ActiveSurveyForm.dart';

// import 'ActivityForm1.dart';

// class SurveyList1 extends StatefulWidget {
//   final SurveyListResponse surveyListResponse;
//   SurveyList1(this.surveyListResponse);
//   @override
//   _SurveyListState1 createState() => _SurveyListState1();
// }

// class _SurveyListState1 extends State<SurveyList1> {
//   int _count;
//   String status;
//   List list;
//   List fieldName = new List();
//   String accessToken;

//   getDataFromSharedPrefs() async {
//     SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
//     setState(() {
//       accessToken = sharedPreferences.getString("access_token");
//     });
//   }

//   @override
//   void initState() {
//     status = widget.surveyListResponse.status;
//     _count = widget.surveyListResponse.result.length;
//     print('response ${widget.surveyListResponse.status}');
//     getDataFromSharedPrefs();
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         flexibleSpace: Container(
//           decoration: BoxDecoration(gradient: kAppBarGradient),
//           child: SafeArea(
//             child: Row(
//               mainAxisAlignment: MainAxisAlignment.spaceBetween,
//               children: [
//                 Padding(
//                   padding: const EdgeInsets.all(8.0),
//                   child: Stack(
//                     alignment: Alignment.center,
//                     children: [
//                       Image.asset('assets/images/Icon-Holder.png'),
//                       // Icon(Icons.chevron_left_sharp, color: Colors.white,),
//                     ],
//                   ),
//                 ),
//                 Padding(
//                   padding: const EdgeInsets.symmetric(vertical: 3.0),
//                   child: Image.asset('assets/images/Header-Logo.png'),
//                 ),
//                 SizedBox(
//                   width: 20,
//                 ),
//                 Center(
//                   child: Text(
//                     "Survey Lists",
//                     style: GoogleFonts.roboto(
//                         color: Color(0xff031e48),
//                         fontSize: 20,
//                         fontWeight: FontWeight.bold
//                         // fontFamily: 'Roboto'
//                         ),
//                   ),
//                 ),
//                 SizedBox(
//                   width: 60,
//                 ),
//                 Padding(
//                   padding: const EdgeInsets.all(8.0),
//                   child: Stack(
//                     alignment: Alignment.center,
//                     children: [
//                       Image.asset('assets/images/Icon-Holder.png'),
//                       Icon(
//                         Icons.close,
//                         color: Colors.white,
//                       ),
//                     ],
//                   ),
//                 ),
//               ],
//             ),
//           ),
//         ),
//       ),
//       body: CustomContainer(
//         child: ListView.builder(
//           itemCount: _count,
//           itemBuilder: (BuildContext context, index) {
//             return InkWell(
//               onTap: () async {
//                 // print(widget.surveyListResponse.result[index].survey
//                 //     .fields[index].filedName);
//                 List lst =
//                     widget.surveyListResponse.result[index].survey.fields;
//                 print(lst);
//                 for (int i = 0; i < lst.length; i++) {
//                   print(widget.surveyListResponse.result[index].survey.fields);
//                 }
//                 final StateListResponse stateListResponse =
//                     await ApiCall.postState();
//                 if (stateListResponse.status == "success") {
//                   Navigator.of(context)
//                       .pushReplacement(MaterialPageRoute(builder: (context) {
//                     return StaticPage(
//                       products: widget.surveyListResponse.products,
//                       surveyForm: lst,
//                       surveyName:
//                           widget.surveyListResponse.result[index].survey.name,
//                       surveyId:
//                           widget.surveyListResponse.result[index].survey.id,
//                       stateListResponse: stateListResponse,
//                     );
//                   }));
//                   // Navigator.of(context).push(MaterialPageRoute(
//                   //   builder: (context) => ActiveSurveyForm(
//                   //     products: widget.surveyListResponse.products,
//                   //     surveyForm: lst,
//                   //     surveyName:
//                   //         widget.surveyListResponse.result[index].survey.name,
//                   //     surveyId:
//                   //         widget.surveyListResponse.result[index].survey.id,
//                   //     stateListResponse: stateListResponse,
//                   //   ),
//                   // ));
//                 }
//               },
//               child: Container(
//                 height: MediaQuery.of(context).size.height / 6,
//                 margin: const EdgeInsets.all(20),
//                 child: Card(
//                   shape: RoundedRectangleBorder(
//                     borderRadius: BorderRadius.circular(30),
//                   ),
//                   color: Colors.white,
//                   child: Center(
//                       child: Text(
//                     ' ${widget.surveyListResponse.result[index].survey.name}',
//                     // style: kheadingStyle.apply(
//                     //   fontSizeDelta: 1.5,
//                     //   fontWeightDelta: 5,
//                     // ),
//                   )),
//                 ),
//               ),
//             );
//           },
//         ),
//       ),
//     );
//   }
// }

// class SurveyList extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return FutureBuilder(
//       future: ApiCall.getSurveyList(),
//       builder: (context, snapshot) {
//         if (snapshot.hasData) {
//           SurveyListResponse list = snapshot.data;

//           // var value = list.result.map((e) => print("value ka value hai: " + e.survey.fields[0].toString()));
//           // print("list is " + value.toString());
//           return SurveyList1(list);
//         }
//         return Scaffold(
//           body: Center(
//             child: CircularProgressIndicator(),
//           ),
//         );
//       },
//     );
//   }
// }
// //
// // class _SurveyListState extends State<SurveyList> {
// //   @override
// //   Widget build(BuildContext context) {
// //     return FutureBuilder(
// //       future: ApiCall.getSurveyList(),
// //       builder: (context, snapshot) {
// //         if (snapshot.hasData) {
// //           SurveyListResponse list = snapshot.data;
// //           return SurveyList1(list);
// //         }
// //         return Scaffold(
// //           body: Center(
// //             child: CircularProgressIndicator(),
// //           ),
// //         );
// //       },
// //     );
// //   }
// // }
