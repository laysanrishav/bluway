import 'package:shared_preferences/shared_preferences.dart';

class SignOut {
  SignOut() {
    signOut();
  }

  signOut() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.remove("accessToken");
    print("User is loged out");
    sharedPreferences.setBool("isSigned", false);
  }
}
