import 'package:blu_way/Functions/network/api_constants.dart';
import 'package:blu_way/Model/Common/settings.dart';
import 'package:blu_way/Widget/FetchingData.dart';
import 'package:blu_way/Widget/MyAppBar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class AboutUs extends StatefulWidget {
  @override
  _AboutUsState createState() => _AboutUsState();
}

class _AboutUsState extends State<AboutUs> {
  Future<Setting> getAboutUs() async {
    // Setting setting = new Setting();
    var response = await http.post(AppConstants.settings);
    // Map setting =
    // print("Setting response is: " + setting.toString());
    return Setting.fromJson(json.decode(response.body));
  }

  Setting setting = new Setting();

  @override
  void initState() {
    // TODO: implement initState
    getAboutUs();
    super.initState();
    // print()
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar("About Us"),
      body: FutureBuilder(
          future: getAboutUs(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              setting = snapshot.data;
              return ListView.builder(
                  itemCount: setting.data.length,
                  itemBuilder: (context, index) {
                    String name = setting.data[index].name;
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        child: (name == 'About')
                            ? Text(
                                '''${setting.data[index].value}''',
                                style: GoogleFonts.roboto(
                                  fontSize: 20,
                                  color: Colors.black,
                                ),
                              )
                            : Text(
                                "Please update the website info",
                                style: GoogleFonts.roboto(
                                  fontSize: 20,
                                  color: Colors.black,
                                ),
                              ),
                      ),
                    );
                  });
            } else {
              return FetchingData();
            }
          }),
    );
  }
}
