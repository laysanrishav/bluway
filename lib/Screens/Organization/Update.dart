import 'package:blu_way/Functions/network/api_constants.dart';
import 'package:blu_way/Screens/HomePage.dart';
import 'package:blu_way/Widget/MyAppBar.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:io' as Io;
import 'dart:io';
import 'package:blu_way/Constants/ConstantList.dart';
import 'package:blu_way/Constants/Style.dart';

import 'package:blu_way/Model/DistictList.dart';
import 'package:blu_way/Model/Organization/updateOrgModel.dart';
import 'package:blu_way/Model/StateList.dart';
import 'package:blu_way/Model/Supervisor/GetSupervisorModel.dart';
import 'package:blu_way/Model/hubListModel.dart';

import 'package:blu_way/utlis/Country.dart';

import 'package:blu_way/utlis/theme.dart';
import 'package:flutter/services.dart';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';


import 'package:shared_preferences/shared_preferences.dart';

import 'package:validators/validators.dart';

// import 'Supervisors.dart';

class UpdateOrganization extends StatefulWidget {
  // String id;
  // String fname;
  // String lname;

  // String email;
  // String gender;
  // String dob;
  // String father_name;
  // String address;
  // String village;
  // String post_office;
  // String thana;
  // String country;
  // String state_name;
  // String district_name;
  // String pincode;
  // String occupation;
  // String document;
  // String mobile;
  // String licence_number;
  // String document_image_data;
  // String image_data;

  // UpdateOrganization({
  // this.id,
  // this.address,
  // // this.country,
  // this.district_name,
  // // this.dob,
  // this.document,
  // this.document_image_data,
  // this.email,
  // // this.father_name,
  // this.fname,
  // // this.gender,
  // this.image_data,
  // this.licence_number,
  // this.lname,
  // this.mobile,
  // // this.occupation,
  // this.pincode,
  // this.post_office,
  // this.state_name,
  // this.thana,
  // this.village,
  // });

  @override
  _UpdateOrganizationState createState() => _UpdateOrganizationState();
}

class _UpdateOrganizationState extends State<UpdateOrganization> {
  final _formKey = GlobalKey<FormState>();
  List<HubResult> hublist = [];
  List<SupervisorResult> supervisorList = [];
  // List<SupervisorsList> supervisorList = [];
  StateList stateList = StateList();
  DistrictList distictList = DistrictList();
  List<StateResult> stateresult = [];
  List<DistrictResult> districtresult = [];
  DistrictResult test = DistrictResult();
  DateTime selectedDate = DateTime.now();
  TextEditingController dob = TextEditingController();

  // List<String> documenttype = [
  //   'Aadhar Card',
  //   'Voter Id Card',
  //   'PAN Card',
  //   'Driving License'
  // ];
  // List<String> supervisorList = List<String>();

  // GetSupervisorList(){

  // }

  getDatafromSharedPreference() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    organization_id = sharedPreferences.getString("Organization_id");
    orgName.text = sharedPreferences.getString("name");
    address1.text= sharedPreferences.getString("address");
    address2.text= sharedPreferences.getString("address");
    selectedOrg.text = sharedPreferences.getString("orgType");
    otherOrg.text = sharedPreferences.getString("orgOtherType");
    reg.text = sharedPreferences.getString("regNumber");
    gst.text = sharedPreferences.getString("gstNumber");
     state_id =sharedPreferences.getString("state");
    district_id = sharedPreferences.getString("district");
     pincode.text = sharedPreferences.getString("pincode");
     servicePincode.text = sharedPreferences.getString("service_pincode");



print("organizationname: "+ orgName.text + " address1: " + address1.text+ " address2: " +
    address2.text + " selectedOrg: "+ selectedOrg.text+ " otherOrg: "+ otherOrg.text+ " reg: "+ reg.text + " gst: "+ gst.text + "pincode: " + pincode.text);
    print("Organization id is: " + organization_id);
  }






  String organization_id = ' ';
  TextEditingController orgName = new TextEditingController();
  TextEditingController address1= new TextEditingController();
  TextEditingController address2= new TextEditingController();
  TextEditingController selectedOrg = new TextEditingController();
  TextEditingController otherOrg = new TextEditingController();
  TextEditingController reg = new TextEditingController();
  TextEditingController gst = new TextEditingController();
  String state_id = ' ';
  String district_id = ' ';
  TextEditingController pincode = new TextEditingController();
  TextEditingController servicePincode = new TextEditingController();



  List<String> listOfOrganization = [
    'Property firm',
    'Partnership firm',
    'LLB Pvt Ltd',
    'Pvt Ltd',
    'NGO',
    'Trust',
    'SHG',
    'Any Other'
  ];
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  Future<UpdateOrganizationModel> updateOrgdata(String id) async {
    EasyLoading.show(status: 'loading...');
    SharedPreferences sharedPref = await SharedPreferences.getInstance();
    var response = await http.post(
      Uri.encodeFull(AppConstants.updateOrg),
      body: jsonEncode({
        "id": organization_id,
        "state": state_id,
        "district": district_id,
        "name": orgName.text,
        "pincode": pincode.text,
        "org_type": selectedOrg.text,
        "org_other_type": otherOrg.text,
        "reg_number": reg.text,
        "gst_number": gst.text,
        "service_pincode": servicePincode.text,
        "address1": address1.text,
        "address2": address2.text,
      }),
    );

    if (response.statusCode == 200) {
      print("Response of updation is: "+ response.body);
      EasyLoading.dismiss();
      EasyLoading.showSuccess('Organization Updated!').then((_) =>  Navigator.push(context, MaterialPageRoute(builder: (context){
        return MyHomePage(title: "ORGANIZATION");
      })));
      UpdateOrganizationModel decoded = json.decode(response.body);
      if(decoded.status == "success")
        {
          EasyLoading.showSuccess('${decoded.message}');
          print("Updated name is: " + decoded.data.name);

       sharedPref.setString("name" , orgName.text);
           sharedPref.setString("address", address1.text);
          sharedPref.setString("address", address2.text);
          sharedPref.setString("orgType", selectedOrg.text );
          sharedPref.setString("orgOtherType", otherOrg.text );
          sharedPref.setString("regNumber",  reg.text );
          sharedPref.setString("gstNumber" ,  gst.text);
          sharedPref.setString("state",  state_id);
           sharedPref.setString("district",  district_id);
          sharedPref.setString("pincode",  pincode.text);
          sharedPref.setString("service_pincode", servicePincode.text);
        }
      return UpdateOrganizationModel.fromJson(json.decode(response.body));
    } else {
      throw "Not able to get it";
    }
  }

  // addEmployee() async {
  //   if (_formKey.currentState.validate() &&
  //       _userimagepath != null &&
  //       _documentimagepath != null) {
  //     _formKey.currentState.save();
  //     setState(() {
  //       Dialogs.showLoadingDialog(context, _keyLoader);
  //     });
  //     // 'Superviser',
  //     //                           'Sales Executive',
  //     if (usertype == 'Superviser') {
  //       final AddSupervisorModel supervisorModel =
  //           await ApiCall.addSupervisorModel(token,
  //               // id: organization_id,
  //               id: organization_id,
  //               fname: first_name,
  //               lname: last_name,
  //               email: email,
  //               gender: gender,
  //               dob: dob.text,
  //               fathername: father_name,
  //               postoffice: post_office,
  //               thana: thana,
  //               country: country,
  //               statename: state_id,
  //               districtname: district_id,
  //               pincode: pincode,
  //               occupation: occupation,
  //               doucment: document,
  //               documentimage: documentimg64,
  //               mobileNo: mobile,
  //               village: village,
  //               licenceno: licence_number,
  //               address: address,
  //               profileimage: userimg64);
  //       // .then((value) {
  //       print("Message is: " + supervisorModel.message);
  //       if (supervisorModel.status == 'success') {
  //         // print("Added supervisor is")
  //         Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
  //         print("sup status " + supervisorModel.status);
  //         Navigator.pushReplacement(context,
  //             MaterialPageRoute(builder: (context) {
  //           return SupervisorsList();
  //         }));

  //         // if (supervisorModel.token.isNotEmpty) {
  //         //   setState(() {
  //         //     Global.accessToken = supervisorModel.token;
  //         //   });
  //         // }
  //         // Toast.show(value.message, context);
  //         // _showSuccessDialog(value.message);
  //       } else {
  //         Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
  //         Toast.show(supervisorModel.message, context);
  //       }
  //     } else if (usertype == 'Sales Executive') {
  //       final AddSEModel addSEModel = await ApiCall.addSEModel(token,
  //           id: organization_id,
  //           fname: first_name,
  //           lname: last_name,
  //           email: email,
  //           gender: gender,
  //           dob: dob.text,
  //           fathername: father_name,
  //           postoffice: post_office,
  //           thana: thana,
  //           country: country,
  //           statename: state_id,
  //           districtname: district_id,
  //           pincode: pincode,
  //           occupation: occupation,
  //           doucment: document,
  //           documentimage: documentimg64,
  //           mobileNo: mobile,
  //           village: village,
  //           licenceno: licence_number,
  //           address: address,
  //           profileimage: userimg64,
  //           supervisorId: supervisorId);
  //       if (addSEModel.status == 'success') {
  //         Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
  //         print(addSEModel.status);
  //         Navigator.pushReplacement(context,
  //             MaterialPageRoute(builder: (context) {
  //           return SupervisorsList();
  //         }));

  //         if (addSEModel.token.isNotEmpty) {
  //           setState(() {
  //             Global.accessToken = addSEModel.token;
  //           });
  //         }
  //         // Toast.show(value.message, context);
  //         // _showSuccessDialog(value.message);
  //       } else {
  //         Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
  //         Toast.show(addSEModel.message, context);
  //       }
  //     }
  //     // });
  //   } else if (userimg64.isEmpty) {
  //     Toast.show('Please upload ProfilePicture', context);
  //   } else if (documentimg64.isEmpty) {
  //     Toast.show('Please upload Document', context);
  //   }
  // }

  // DateTime picked;

  initData() {
    fetchStateListModel().then((value) {
      setState(() {
        stateList = value;
        stateresult = stateList.result;
      });
    });


  }



  fetchDistrict(String id) {
    fetchDistrictListModel(id).then((value) {
      setState(() {
        distictList = value;
        districtresult = distictList.result;
        print(distictList.result.isEmpty);
        test = distictList.result[0];
      });
    });
  }

  // Future getUserImage(BuildContext context) async {
  //   showDialog(
  //       context: context,
  //       builder: (context) {
  //         return AlertDialog(
  //             content: SizedBox(
  //           height: 150,
  //           child: Column(
  //             crossAxisAlignment: CrossAxisAlignment.center,
  //             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  //             children: <Widget>[
  //               FlatButton(
  //                 child: Text(
  //                   "Camera",
  //                   style: TextStyle(fontSize: 26),
  //                 ),
  //                 onPressed: () async {
  //                   var image = await ImagePicker().getImage(
  //                     source: ImageSource.camera,
  //                     imageQuality: 50,
  //                   );
  //                   // var image = await ImagePicker()
  //                   //     .getImage(source: ImageSource.camera);
  //                   setState(() {
  //                     // _image = image;
  //                     _userimagepath = File(image.path);
  //                     List<int> bytes =
  //                         Io.File(_userimagepath.path).readAsBytesSync();
  //                     userimg64 = base64UrlEncode(bytes);
  //                     // var base64String =
  //                     //     base64UrlEncode(_image.readAsBytesSync());
  //                     // base64String = base64Encode(_image.readAsBytesSync());
  //                     // print('document $img64');
  //                     // print('base64String $base64String');
  //                     Navigator.pop(context);
  //                   });
  //                 },
  //               ),
  //               FlatButton(
  //                 child: Text(
  //                   "Gallery",
  //                   style: TextStyle(fontSize: 26),
  //                 ),
  //                 onPressed: () async {
  //                   var image = await ImagePicker().getImage(
  //                     source: ImageSource.gallery,
  //                     imageQuality: 50,
  //                   );
  //
  //                   setState(() {
  //                     _userimagepath = File(image.path);
  //                     // print(_image.uri.toString());
  //                     List<int> bytes =
  //                         Io.File(_userimagepath.path).readAsBytesSync();
  //                     userimg64 = base64UrlEncode(bytes);
  //                     print('img64 : $userimg64');
  //                     print('bytes $bytes');
  //
  //                     print('image1 : $_userimagepath');
  //                     Navigator.pop(context);
  //                   });
  //                 },
  //               ),
  //               _userimagepath != null
  //                   ? FlatButton(
  //                       child: Text(
  //                         "Remove Profile",
  //                         style: TextStyle(fontSize: 26),
  //                       ),
  //                       onPressed: () async {
  //                         setState(() {
  //                           _userimagepath = null;
  //                           Navigator.pop(context);
  //                         });
  //                       },
  //                     )
  //                   : Text(
  //                       "",
  //                     ),
  //             ],
  //           ),
  //         ));
  //       });
  // }
  //
  // Future getDocumentImage(BuildContext context) async {
  //   showDialog(
  //       context: context,
  //       builder: (context) {
  //         return AlertDialog(
  //             content: SizedBox(
  //           height: 150,
  //           child: Column(
  //             crossAxisAlignment: CrossAxisAlignment.center,
  //             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  //             children: <Widget>[
  //               FlatButton(
  //                 child: Text(
  //                   "Camera",
  //                   style: TextStyle(fontSize: 26),
  //                 ),
  //                 onPressed: () async {
  //                   var image = await ImagePicker().getImage(
  //                     source: ImageSource.camera,
  //                     imageQuality: 50,
  //                   );
  //                   // var image = await ImagePicker()
  //                   //     .getImage(source: ImageSource.camera);
  //                   setState(() {
  //                     // _image = image;
  //                     _documentimagepath = File(image.path);
  //                     List<int> bytes =
  //                         Io.File(_documentimagepath.path).readAsBytesSync();
  //                     documentimg64 = base64UrlEncode(bytes);
  //                     // var base64String =
  //                     //     base64UrlEncode(_image.readAsBytesSync());
  //                     // base64String = base64Encode(_image.readAsBytesSync());
  //                     // print('document $img64');
  //                     // print('base64String $base64String');
  //                     Navigator.pop(context);
  //                   });
  //                 },
  //               ),
  //               FlatButton(
  //                 child: Text(
  //                   "Gallery",
  //                   style: TextStyle(fontSize: 26),
  //                 ),
  //                 onPressed: () async {
  //                   var image = await ImagePicker().getImage(
  //                     source: ImageSource.gallery,
  //                     imageQuality: 50,
  //                   );
  //                   // var image = await ImagePicker()
  //                   //     .getImage(source: ImageSource.gallery);
  //                   setState(() {
  //                     _documentimagepath = File(image.path);
  //                     // print(_image.uri.toString());
  //                     List<int> bytes =
  //                         Io.File(_documentimagepath.path).readAsBytesSync();
  //                     documentimg64 = base64UrlEncode(bytes);
  //                     print('img64 : $documentimg64');
  //                     print('bytes $bytes');
  //                     // print('base64Image : $img64');
  //                     // base64String = base64Encode(_image.readAsBytesSync());
  //                     // print('document $img64');
  //                     // print('base64String $base64String');
  //                     print('image1 : $_documentimagepath');
  //                     Navigator.pop(context);
  //                   });
  //                 },
  //               ),
  //               _documentimagepath != null
  //                   ? FlatButton(
  //                       child: Text(
  //                         "Remove Profile",
  //                         style: TextStyle(fontSize: 26),
  //                       ),
  //                       onPressed: () async {
  //                         setState(() {
  //                           _documentimagepath = null;
  //                           Navigator.pop(context);
  //                         });
  //                       },
  //                     )
  //                   : Text(
  //                       "",
  //                     ),
  //             ],
  //           ),
  //         ));
  //       });
  // }

  @override
  void initState() {
    // TODO: implement initState
    initData();
    getDatafromSharedPreference();
    print("Organization id is: " + organization_id);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      builder: EasyLoading.init(),
      home: Scaffold(
        appBar: MyAppBar("UPDATE ORGANIZATION"),
        body: CustomContainer(
          child: Container(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 15),
              child: Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  // color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Form(
                  key: _formKey,
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'UPDATE ORGANIZATION',
                          style: TextStyle(
                            fontSize: 30,
                            fontFamily: 'Shruti',
                            color: Color(0xffaedcff),
                            shadows: <Shadow>[
                              Shadow(
                                offset: Offset(1.0, 1.0),
                                blurRadius: 3.0,
                                color: Color.fromARGB(255, 0, 0, 0),
                              ),
                              Shadow(
                                offset: Offset(1.0, 1.0),
                                blurRadius: 8.0,
                                color: Color.fromARGB(125, 0, 0, 255),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                       Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            TextFormInput(
                              textEditingController: orgName,
                              labelText: "Organization Name",
                              // hintText: orgName.text,
                              validator: (value) {
                                if (!isAlpha(value)) {
                                  return "Enter a valid name";
                                }
                                return null;
                              },
                              onSaved: (value) {
                                setState(() {
                                  orgName.text = value;
                                });
                              },
                            ),



                            SizedBox(
                              height: 20,
                            ),
                            TextFormInput(
                              textEditingController: address1,
                              labelText: "Address 1",
                              // hintText: address1.text,
                              validator: (value) {
                                if (!isAlpha(value)) {
                                  return "Enter a valid address";
                                } else
                                  return null;
                              },
                              onSaved: (value) {
                                setState(() {
                                  address1.text = value;
                                });
                              },
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            TextFormInput(
                              textEditingController: address2,
                              labelText: "Address 2",
                              // hintText: address2.text,
                              validator: (value) {
                                if (!isAlpha(value)) {
                                  return "Enter a valid address";
                                } else
                                  return null;
                              },
                              onSaved: (value) {
                                setState(() {
                                  address2.text = value;
                                });
                              },
                            ),
                            SizedBox(
                              height: 20,
                            ),

                            Text(
                              'Organization',
                              style: TextStyle(
                                  fontFamily: 'Shruti',
                                  fontWeight: FontWeight.w600,
                                  fontSize: 20,
                                  color:
                                  AppColors.textFeildcolor),
                            ),

                            Container(
                              width: double.infinity,
                              padding: EdgeInsets.fromLTRB(
                                  18, 0, 8, 8),
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: ExactAssetImage(
                                          'assets/images/Loginsignup/Field-01.png'),
                                      fit: BoxFit.fill)),
                              child: DropdownButtonFormField<
                                  String>(
                                items: listOfOrganization.map(
                                        (String
                                    dropDownStringItem) {
                                      return DropdownMenuItem<
                                          String>(
                                        value: dropDownStringItem,
                                        child: Text(
                                          dropDownStringItem,
                                          style: TextStyle(
                                              fontFamily: 'Shruti',
                                              fontWeight:
                                              FontWeight.w600,
                                              fontSize: 18,
                                              color: AppColors
                                                  .textFeildcolor),
                                        ),
                                      );
                                    }).toList(),
                                onChanged: (value) {
                                  setState(() {
                                    selectedOrg.text = value;
                                  });
                                },
                                validator: (value) =>
                                value == null
                                    ? 'Feild Required'
                                    : null,
                                hint: Text(
                                    'Select Organization',
                                    style: TextStyle(
                                        fontFamily: 'Shruti',
                                        fontWeight:
                                        FontWeight.w600,
                                        fontSize: 18,
                                        color: AppColors
                                            .textFeildcolor)),
                              ),


                            ),


                            (selectedOrg == 'Any Other')
                                ? Text(
                              'Organization Name',
                              style: TextStyle(
                                  fontFamily: 'Shruti',
                                  fontWeight:
                                  FontWeight.w600,
                                  fontSize: 20,
                                  color: AppColors
                                      .textFeildcolor),
                            )
                                : Container(),

                            (selectedOrg == 'Any Other')
                                ? TextFormField(
                              controller: otherOrg,
                              style: TextStyle(
                                  fontFamily: 'Arial',
                                  color: AppColors
                                      .whiteColor,
                                  fontSize: 22),
                              validator: (val) {
                                if (val.trim().isEmpty) {
                                  // Global.isLoading =
                                  // false;
                                  return 'Please enter Organization ';
                                }
                                return null;
                              },
                              onSaved: (value)
                              {
                                setState(() {
                                  otherOrg.text = value;
                                });
                              },
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                // labelText: 'Organization Name',
                                labelStyle: TextStyle(
                                    fontFamily: 'Shruti',
                                    fontWeight:
                                    FontWeight.w600,
                                    fontSize: 20,
                                    color: AppColors
                                        .textFeildcolor),
                                // border: InputBorder.none,
                              ),
                            )
                                : Container(),
                            (selectedOrg == 'Any Other')
                                ? Container(
                                child: Image.asset(
                                  'assets/images/Loginsignup/Line-1.png',
                                ))
                                : Container(),
                            // (selectedOrg == 'Any Other')
                            //     ? Container(
                            //     child: Image.asset(
                            //       'assets/images/Loginsignup/Line-1.png',
                            //     ))
                            //     : Container(),
                            TextFormInput(
                              textEditingController: reg,
                              labelText: "Reg Number",
                              // hintText: reg.text,
                              validator: (value) {
                                if (!isAlpha(value)) {
                                  return "Enter a valid reg name";
                                } else
                                  return null;
                              },
                              onSaved: (value) {
                                setState(() {
                                  reg.text = value;
                                });
                              },
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            TextFormInput(
                              textEditingController: reg,
                              labelText: "GST Number",
                              // hintText: reg.text,
                              validator: (value) {
                                if (!isAlpha(value)) {
                                  return "Enter a valid reg name";
                                } else
                                  return null;
                              },
                              onSaved: (value) {
                                setState(() {
                                  gst.text = value;
                                });
                              },
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Text(
                              'STATE',
                              style: TextStyle(
                                  fontFamily: 'Shruti',
                                  fontWeight: FontWeight.w600,
                                  fontSize: 20,
                                  color: AppColors.textFeildcolor),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.90,
                              padding: EdgeInsets.fromLTRB(18, 0, 8, 8),
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: ExactAssetImage(
                                          'assets/images/Loginsignup/Field-01.png'),
                                      fit: BoxFit.fill)),
                              child: new DropdownButtonFormField<StateResult>(
                                isDense: true,
                                style: TextStyle(
                                    fontFamily: 'Shruti',
                                    fontWeight: FontWeight.w600,
                                    fontSize: 20,
                                    color: AppColors.textFeildcolor),
                                hint: Text('Select State',
                                    style: TextStyle(
                                        fontFamily: 'Shruti',
                                        fontWeight: FontWeight.w600,
                                        fontSize: 18,
                                        color: AppColors.textFeildcolor)),
                                decoration: InputDecoration(
                                    enabledBorder: InputBorder.none),
                                icon: Padding(
                                  padding: const EdgeInsets.only(right: 8.0),
                                  child: Image.asset(
                                    'assets/images/Loginsignup/DropDown-Icon-01.png',
                                    height: 15,
                                    width: 15,
                                  ),
                                ),
                                validator: (value) =>
                                    value == null ? 'Feild Required' : null,
                                items: stateresult
                                    .map<DropdownMenuItem<StateResult>>(
                                        (StateResult item) {
                                  return DropdownMenuItem<StateResult>(
                                    value: item,
                                    child: Text(item.name),
                                  );
                                }).toList(),
                                onChanged: (value) {
                                  print(value.sId);
                                  setState(() {
                                    state_id = value.sId;
                                    fetchDistrict(state_id);
                                  });
                                },
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Text(
                              'DISTRICT',
                              style: TextStyle(
                                  fontFamily: 'Shruti',
                                  fontWeight: FontWeight.w600,
                                  fontSize: 20,
                                  color: AppColors.textFeildcolor),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.90,
                              padding: EdgeInsets.fromLTRB(18, 0, 8, 8),
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: ExactAssetImage(
                                          'assets/images/Loginsignup/Field-01.png'),
                                      fit: BoxFit.fill)),
                              child: new DropdownButtonFormField<DistrictResult>(
                                isDense: true,
                                value: test,
                                style: TextStyle(
                                    fontFamily: 'Shruti',
                                    fontWeight: FontWeight.w600,
                                    fontSize: 20,
                                    color: AppColors.textFeildcolor),
                                hint: Text('Select District',
                                    style: TextStyle(
                                        fontFamily: 'Shruti',
                                        fontWeight: FontWeight.w600,
                                        fontSize: 18,
                                        color: AppColors.textFeildcolor)),
                                decoration: InputDecoration(
                                    enabledBorder: InputBorder.none),
                                icon: Padding(
                                  padding: const EdgeInsets.only(right: 8.0),
                                  child: Image.asset(
                                    'assets/images/Loginsignup/DropDown-Icon-01.png',
                                    height: 15,
                                    width: 15,
                                  ),
                                ),
                                validator: (value) =>
                                    value == null ? 'Feild Required' : null,
                                items: districtresult
                                    .map<DropdownMenuItem<DistrictResult>>(
                                        (DistrictResult item) {
                                  return DropdownMenuItem<DistrictResult>(
                                    value: item,
                                    child: Text(item.name),
                                  );
                                }).toList(),
                                onChanged: state_id.isEmpty
                                    ? null
                                    : (value) {
                                        print(value.sId);
                                        setState(() {
                                          test = value;
                                          district_id = value.sId;
                                        });
                                      },
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            TextFormInput(
                              textEditingController: pincode,
                              labelText: "Pincode",
                              // hintText: pincode.text,
                              textInputType: TextInputType.number,
                              maxlength: 6,
                              validator: (value) {
                                if (!isNumeric(value)) {
                                  return "Enter a valid Pincode";
                                } else
                                  return null;
                              },
                              onSaved: (value) {
                                setState(() {
                                  pincode.text = value;
                                });
                              },
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            TextFormInput(
                              textEditingController: servicePincode,
                              labelText: "Service Pincode",
                              // hintText: servicePincode.text,
                              // textInputType: TextInputType.,
                              // maxlength: 6,
                              validator: (value) {
                                if (!isAlpha(value)) {
                                  return "Enter a valid Service Pincode";
                                } else
                                  return null;
                              },
                              onSaved: (value) {
                                setState(() {
                                  servicePincode.text = value;
                                });
                              },
                            ),
                            SizedBox(
                              height: 20,
                            ),






                            Padding(
                              padding: EdgeInsets.only(top: 30, bottom: 30),
                              child: Center(
                                child: GestureDetector(
                                  onTap: () {
                                    // addEmployee();
                                    updateOrgdata(organization_id);
                                  },
                                  child: Container(
                                    width: 230,
                                    decoration: BoxDecoration(
                                        image: DecorationImage(
                                            image: ExactAssetImage(
                                                'assets/images/Loginsignup/BT-1.png'),
                                            fit: BoxFit.fill)),
                                    child: Padding(
                                      padding: const EdgeInsets.all(15.0),
                                      child: Center(
                                        child: Text(
                                          'SUBMIT',
                                          style: TextStyle(
                                              fontFamily: 'Arial',
                                              fontWeight: FontWeight.w600,
                                              color: AppColors.whiteColor,
                                              fontSize: 18),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class TextFormInput extends StatefulWidget {
  final String labelText;
  final Function validator;
  final Function onSaved;
  final TextInputType textInputType;
  final int maxlength;
  final TextEditingController textEditingController;
  String hintText;

  // bool readOnly = false;

  TextFormInput({
    Key key,
    @required this.labelText,
    @required this.validator,
    @required this.onSaved,
    this.textInputType,
    this.maxlength,
    // this.hintText,
    this.textEditingController,
    // this.readOnly,
  }) : super(key: key);

  @override
  _TextFormInputState createState() => _TextFormInputState();
}

class _TextFormInputState extends State<TextFormInput> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TextFormField(
          controller: widget.textEditingController,
          readOnly: false,
          // initialValue: widget.hintText,

          inputFormatters: [
            new LengthLimitingTextInputFormatter(widget.maxlength),
          ],
          // autovalidateMode: autovalidate,
          keyboardType: widget.textInputType,
          style: TextStyle(
              fontFamily: 'Arial', color: AppColors.whiteColor, fontSize: 22),
          validator: widget.validator,
          onSaved: widget.onSaved,
          decoration: InputDecoration(
            // hintText: widget.hintText,
            hintStyle: TextStyle(
                fontFamily: 'Shruti',
                fontWeight: FontWeight.w600,
                fontSize: 20,
                color: AppColors.textFeildcolor),
            labelText: widget.labelText,
            labelStyle: TextStyle(
                fontFamily: 'Shruti',
                fontWeight: FontWeight.w600,
                fontSize: 20,
                color: AppColors.textFeildcolor),
            border: InputBorder.none,
          ),
        ),
        Container(
            child: Image.asset(
          'assets/images/Loginsignup/Line-1.png',
        )),
      ],
    );
  }
}
