import 'package:blu_way/Constants/ConstantList.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';

// class Registration extends StatefulWidget {
//   @override
//   _RegistrationState createState() => _RegistrationState();
// }

// class _RegistrationState extends State<Registration> {
//   List<String> genders = ['Male', 'Female', 'Rather not say'];
//   List<String> countryList = ['India'];
//   List<String> identityList = <String>[
//     'VOTER ID',
//     'PASSPORT',
//     'BANK ACCOUNT NO.',
//     'RATION CARD',
//     'AADHAAR CARD',
//     'DRIVING LICENSE',
//     'PAN CARD',
//   ];
//   List<String> typeOfOrganisation = <String>[
//     'Property Firm',
//     'Parternership Firm',
//     'LLB Pvt Ltd',
//     'Pvt Ltd',
//     'NGO',
//     'Trust',
//     'SHG',
//     'Any Other',
//   ];

//   StateListResponse stateList = StateListResponse();
//   List<Result> stateresult = [];
//   DistrictListResponse districtListResponse = new DistrictListResponse();
//   List<DistrictResult> districtResult = [];
//   DistrictResult test;
//   String districtId = '';
//   List<String> stateIds;
//   String selectedGender;
//   String selectedState;
//   String selectedStateId;
//   String selectedDistrict;
//   String selectedOrgType;
//   String stateId = '';
//   bool stateChange = true;
//   final _formKey = GlobalKey<FormState>();
//   String firstName,
//       lastName,
//       guardianName,
//       dob,
//       country,
//       address1,
//       village,
//       postOffice,
//       thana,
//       pincode,
//       state,
//       occupation,
//       mobile,
//       identity,
//       organization_name,
//       org_type,
//       org_other_type,
//       reg_number,
//       gst_number,
//       bank_name,
//       account_number,
//       ifsc,
//       hub;

//   File _image;
//   // Map stateList;
//   String img64;
//   DateTime selectedDate = DateTime.now();
//   final GlobalKey<State> _keyLoader = new GlobalKey<State>();
//   _selectDate(BuildContext context) async {
//     final DateTime picked = await showDatePicker(
//       context: context,
//       initialDate: selectedDate,
//       firstDate: DateTime(1900),
//       lastDate: DateTime.now(),
//     );
//     if (picked != null && picked != selectedDate)
//       setState(() {
//         selectedDate = picked;
//       });
//   }

//   @override
//   void initState() {
//     fetchStateListModel().then((value) {
//       setState(() {
//         stateList = value;
//         stateresult = stateList.hubResult;
//       });
//     });

//     super.initState();
//   }

//   fetchDistrictList(String id) {
//     fetchDistrictListModel(id).then((value) {
//       setState(() {
//         districtListResponse = value;
//         districtResult = districtListResponse.hubResult;
//         test = districtListResponse.hubResult[0];
//       });
//     });
//   }

//   Future getImage(BuildContext context) async {
//     showDialog(
//         context: context,
//         builder: (context) {
//           return AlertDialog(
//               content: SizedBox(
//             height: 150,
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.center,
//               mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//               children: <Widget>[
//                 FlatButton(
//                   child: Text(
//                     "Camera",
//                     style: TextStyle(fontSize: 26),
//                   ),
//                   onPressed: () async {
//                     var image = await ImagePicker().getImage(
//                       source: ImageSource.camera,
//                     );
//                     // var image = await ImagePicker()
//                     //     .getImage(source: ImageSource.camera);
//                     setState(() {
//                       // _image = image;
//                       _image = File(image.path);
//                       print(_image);
//                       List<int> bytes = Io.File(_image.path).readAsBytesSync();
//                       img64 = base64UrlEncode(bytes);
//                       // var base64String =
//                       //     base64UrlEncode(_image.readAsBytesSync());
//                       // base64String = base64Encode(_image.readAsBytesSync());
//                       print('document $img64');
//                       // print('base64String $base64String');
//                       Navigator.pop(context);
//                     });
//                   },
//                 ),
//                 FlatButton(
//                   child: Text(
//                     "Gallery",
//                     style: TextStyle(fontSize: 26),
//                   ),
//                   onPressed: () async {
//                     var image = await ImagePicker().getImage(
//                       source: ImageSource.gallery,
//                       imageQuality: 50,
//                     );
//                     // var image = await ImagePicker()
//                     //     .getImage(source: ImageSource.gallery);
//                     setState(() {
//                       _image = File(image.path);
//                       // print(_image.uri.toString());
//                       List<int> bytes = Io.File(_image.path).readAsBytesSync();
//                       img64 = base64UrlEncode(bytes);
//                       print('img64 : $img64');
//                       print('bytes $bytes');
//                       // print('base64Image : $img64');
//                       // base64String = base64Encode(_image.readAsBytesSync());
//                       // print('document $img64');
//                       // print('base64String $base64String');
//                       print('image1 : $_image');
//                       Navigator.pop(context);
//                     });
//                   },
//                 ),
//                 _image != null
//                     ? FlatButton(
//                         child: Text(
//                           "Remove Profile",
//                           style: TextStyle(fontSize: 26),
//                         ),
//                         onPressed: () async {
//                           setState(() {
//                             _image = null;
//                             img64 = null;
//                             Navigator.pop(context);
//                           });
//                         },
//                       )
//                     : Text(""),
//               ],
//             ),
//           ));
//         });
//   }

//   @override
//   Widget build(BuildContext context) {
//     // print("State result is " + stateresult[0].name.toString());
//     return Scaffold(
//       body: Stack(
//         children: [
//           Container(
//             width: MediaQuery.of(context).size.width,
//             child: SingleChildScrollView(
//               child: Form(
//                 key: _formKey,
//                 child: Padding(
//                   padding: const EdgeInsets.only(left: 16.0),
//                   child: Column(
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: [
//                       SizedBox(
//                         height: MediaQuery.of(context).size.height * 0.30,
//                       ),
//                       Text(
//                         "REGISTRATION",
//                         style: GoogleFonts.roboto(
//                           color: Color(0xffaedcff),
//                           fontSize: 40,
//                           shadows: <Shadow>[
//                             Shadow(
//                               offset: Offset(4.0, 4.0),
//                               // blurRadius: 1.0,
//                               color: Color.fromARGB(125, 0, 0, 0),
//                             ),
//                           ],
//                         ),
//                         textAlign: TextAlign.start,
//                       ),

//                       SizedBox(height: 30),
//                       InputRegistration(
//                           // isList: false,
//                           labeltext: "FIRST NAME",
//                           onSaved: (value) {
//                             setState(() {
//                               firstName = value;
//                             });
//                           }),
//                       SizedBox(height: 30),
//                       InputRegistration(
//                           // isList: false,
//                           labeltext: "LAST NAME",
//                           onSaved: (value) {
//                             setState(() {
//                               lastName = value;
//                             });
//                           }),
//                       SizedBox(height: 30),
//                       InputRegistration(
//                           // isList: false,
//                           labeltext: "FATHER'S NAME",
//                           onSaved: (value) {
//                             setState(() {
//                               guardianName = value;
//                             });
//                           }),
//                       SizedBox(height: 30),
//                       // InputRegistration(
//                       //   labeltext: "Gender",
//                       //   isList: true,
//                       //   list: genders,
//                       //   listChangedValue: this.selectedGender,
//                       //   listHintName: "Select Gender",
//                       // ),
//                       Container(
//                         decoration: BoxDecoration(
//                             border: Border.all(color: Colors.black)),
//                         width: MediaQuery.of(context).size.width * 0.85,
//                         child: Padding(
//                           padding: const EdgeInsets.symmetric(horizontal: 16.0),
//                           child: DropdownButton<String>(
//                             items: genders.map((value) {
//                               return DropdownMenuItem<String>(
//                                 value: value,
//                                 child: Text(
//                                   value,
//                                   style: GoogleFonts.actor(
//                                     color: Colors.blue,
//                                     fontSize: 20,
//                                     // fontWeight: FontWeight.bold,
//                                     // fontWeight: FontWeight.bold,
//                                   ),
//                                 ),
//                               );
//                             }).toList(),
//                             onChanged: (value) {
//                               setState(() {
//                                 this.selectedGender = value;
//                               });
//                             },
//                             value: selectedGender,
//                             hint: Text(
//                               "Select Genders",
//                               style: GoogleFonts.actor(
//                                   fontSize: 20, color: Colors.blue
//                                   // fontWeight: FontWeight.bold,
//                                   ),
//                             ),
//                           ),
//                         ),
//                       ),

//                       SizedBox(height: 30),
//                       InkWell(
//                         onTap: () => _selectDate(context),
//                         child: Container(
//                           child: Column(
//                             crossAxisAlignment: CrossAxisAlignment.start,
//                             children: [
//                               Text(
//                                 "DATE OF BIRTH",
//                                 style: GoogleFonts.roboto(
//                                     color: Color(0xff819ffd),
//                                     fontSize: 20,
//                                     fontWeight: FontWeight.w600),
//                               ),
//                               SizedBox(
//                                 height: 10,
//                               ),
//                               Row(
//                                 children: [
//                                   Text(
//                                     "${selectedDate.toLocal()}".split(' ')[0],
//                                     style: TextStyle(
//                                         fontSize: 20, color: Colors.blue),
//                                   ),
//                                   SizedBox(
//                                     width: 20,
//                                   ),
//                                   Icon(
//                                     Icons.edit,
//                                     color: Colors.white,
//                                   )
//                                 ],
//                               ),
//                             ],
//                           ),
//                         ),
//                       ),
//                       SizedBox(height: 30),
//                       InputRegistration(
//                           // isList: false,
//                           labeltext: "organization name".toUpperCase(),
//                           onSaved: (value) {
//                             setState(() {
//                               organization_name = value;
//                             });
//                           }),
//                       SizedBox(height: 30),
//                       // InputRegistration(
//                       //   isList: true,
//                       //   labeltext: "ORGANISATION TYPE",
//                       //   list: typeOfOrganisation,
//                       //   listChangedValue: this.selectedOrgType,
//                       //   listHintName: "Select Org Type",
//                       // ),
//                       Text(
//                         "ORGANISATION TYPE",
//                         style: GoogleFonts.actor(
//                           color: Color(0xff819ffd),
//                           fontSize: 20,
//                           fontWeight: FontWeight.w600,
//                           // fontWeight: FontWeight.bold,
//                         ),
//                       ),
//                       SizedBox(height: 10),
//                       Container(
//                         decoration: BoxDecoration(
//                             border: Border.all(color: Colors.black)),
//                         width: MediaQuery.of(context).size.width * 0.85,
//                         child: Padding(
//                           padding: const EdgeInsets.symmetric(horizontal: 16.0),
//                           child: DropdownButton<String>(
//                             items: typeOfOrganisation.map((value) {
//                               return DropdownMenuItem<String>(
//                                 value: value,
//                                 child: Text(
//                                   value,
//                                   style: GoogleFonts.actor(
//                                     color: Colors.blue,
//                                     fontSize: 20,
//                                     // fontWeight: FontWeight.bold,
//                                     // fontWeight: FontWeight.bold,
//                                   ),
//                                 ),
//                               );
//                             }).toList(),
//                             onChanged: (value) {
//                               setState(() {
//                                 this.selectedOrgType = value;
//                               });
//                             },
//                             value: selectedOrgType,
//                             hint: Text(
//                               "Select Org Type",
//                               style: GoogleFonts.actor(
//                                   fontSize: 20, color: Colors.blue
//                                   // fontWeight: FontWeight.bold,
//                                   ),
//                             ),
//                           ),
//                         ),
//                       ),
//                       SizedBox(height: 30),
//                       (this.selectedOrgType == 'Any Other')
//                           ? InputRegistration(
//                               // isList: false,
//                               // labeltext: "org other type".toUpperCase(),
//                               labeltext: this.selectedOrgType,
//                               onSaved: (value) {
//                                 setState(() {
//                                   org_other_type = value;
//                                 });
//                               })
//                           : Container(),

//                       SizedBox(height: 30),
//                       InputRegistration(
//                           // isList: false,
//                           labeltext: "reg number".toUpperCase(),
//                           onSaved: (value) {
//                             setState(() {
//                               reg_number = value;
//                             });
//                           }),
//                       SizedBox(height: 30),
//                       InputRegistration(
//                           // isList: false,
//                           labeltext: "gst number".toUpperCase(),
//                           onSaved: (value) {
//                             setState(() {
//                               gst_number = value;
//                             });
//                           }),
//                       SizedBox(height: 30),
//                       InputRegistration(
//                           // isList: false,
//                           labeltext: "bank_name".toUpperCase(),
//                           onSaved: (value) {
//                             setState(() {
//                               bank_name = value;
//                             });
//                           }),
//                       SizedBox(height: 30),
//                       InputRegistration(
//                         // isList: false,
//                         textInputType: TextInputType.number,
//                         labeltext: "account number".toUpperCase(),
//                         onSaved: (value) {
//                           setState(() {
//                             account_number = value;
//                           });
//                         },
//                         onValidate: (value) {
//                           if (!isNumeric(value)) {
//                             return "Enter a Valid Account Number";
//                           }
//                           return null;
//                         },
//                       ),

//                       SizedBox(height: 30),
//                       InputRegistration(
//                         // isList: false,
//                         labeltext: "ifsc".toUpperCase(),
//                         onSaved: (value) {
//                           setState(() {
//                             ifsc = value;
//                           });
//                         },
//                         maxlength: 11,
//                       ),
//                       SizedBox(height: 30),
//                       InputRegistration(
//                           // isList: false,
//                           labeltext: "hub".toUpperCase(),
//                           onSaved: (value) {
//                             setState(() {
//                               hub = "gete";
//                             });
//                           }),
//                       SizedBox(height: 30),
//                       // InputRegistration(
//                       //   labeltext: "Country",
//                       //   isList: true,
//                       //   list: countryList,
//                       //   listChangedValue: this.country,
//                       //   listHintName: "Select County",
//                       // ),
//                       Text(
//                         "COUNTRY",
//                         style: GoogleFonts.actor(
//                             color: Color(0xff819ffd),
//                             fontSize: 20,
//                             fontWeight: FontWeight.w600
//                             // fontWeight: FontWeight.bold,
//                             ),
//                       ),
//                       SizedBox(height: 10),
//                       Container(
//                         decoration: BoxDecoration(
//                             border: Border.all(color: Colors.black)),
//                         width: MediaQuery.of(context).size.width * 0.85,
//                         child: Padding(
//                           padding: const EdgeInsets.symmetric(horizontal: 16.0),
//                           child: DropdownButton<String>(
//                             items: countryList.map((value) {
//                               return DropdownMenuItem<String>(
//                                 value: value,
//                                 child: Text(
//                                   value,
//                                   style: GoogleFonts.actor(
//                                     color: Colors.blue,
//                                     fontSize: 20,
//                                     // fontWeight: FontWeight.bold,
//                                     // fontWeight: FontWeight.bold,
//                                   ),
//                                 ),
//                               );
//                             }).toList(),
//                             onChanged: (value) {
//                               setState(() {
//                                 this.country = value;
//                               });
//                             },
//                             value: country,
//                             hint: Text(
//                               "Select County",
//                               style: GoogleFonts.actor(
//                                 fontSize: 20,
//                                 color: Colors.blue,
//                                 // fontWeight: FontWeight.bold,
//                               ),
//                             ),
//                           ),
//                         ),
//                       ),
//                       SizedBox(
//                         height: 30,
//                       ),
//                       Text(
//                         "State",
//                         style: GoogleFonts.actor(
//                             color: Color(0xff819ffd),
//                             fontSize: 20,
//                             fontWeight: FontWeight.w600
//                             // fontWeight: FontWeight.bold,
//                             ),
//                       ),
//                       SizedBox(height: 10),
//                       Container(
//                         width: MediaQuery.of(context).size.width * 0.75,
//                         decoration: kSurveyFormContainerDecoration,
//                         child: Padding(
//                           padding: const EdgeInsets.symmetric(horizontal: 8.0),
//                           child: new DropdownButtonFormField<Result>(
//                             hint: Text(
//                               'Select State',
//                             ),
//                             validator: (value) =>
//                                 value == null ? 'Feild Required' : null,
//                             items: stateresult
//                                 .map<DropdownMenuItem<Result>>((Result item) {
//                               return DropdownMenuItem<Result>(
//                                 value: item,
//                                 child: Text(item.name),
//                               );
//                             }).toList(),
//                             onChanged: (value) {
//                               print(value.sId);
//                               setState(() {
//                                 stateId = value.sId;
//                                 print("state id is:" + stateId);
//                                 fetchDistrictList(stateId);
//                               });
//                             },
//                           ),
//                         ),
//                       ),

//                       Container(
//                         width: double.infinity,
//                         padding: EdgeInsets.fromLTRB(18, 0, 8, 8),
//                         decoration: BoxDecoration(
//                             image: DecorationImage(
//                                 image: ExactAssetImage(
//                                     'assets/images/Loginsignup/Field-01.png'),
//                                 fit: BoxFit.fill)),
//                         child: new DropdownButtonFormField<DistrictResult>(
//                           isDense: true,
//                           value: test,
//                           style: TextStyle(
//                             fontFamily: 'Shruti',
//                             fontWeight: FontWeight.w600,
//                             fontSize: 20,
//                           ),
//                           hint: Text('Select District',
//                               style: TextStyle(
//                                 fontFamily: 'Shruti',
//                                 fontWeight: FontWeight.w600,
//                                 fontSize: 18,
//                               )),
//                           decoration:
//                               InputDecoration(enabledBorder: InputBorder.none),
//                           icon: Padding(
//                             padding: const EdgeInsets.only(right: 8.0),
//                             child: Image.asset(
//                               'assets/images/Loginsignup/DropDown-Icon-01.png',
//                               height: 15,
//                               width: 15,
//                             ),
//                           ),
//                           validator: (value) =>
//                               value == null ? 'Feild Required' : null,
//                           items: districtResult
//                               .map<DropdownMenuItem<DistrictResult>>(
//                                   (DistrictResult item) {
//                             return DropdownMenuItem<DistrictResult>(
//                               value: item,
//                               child: Text(item.name),
//                             );
//                           }).toList(),
//                           onChanged: stateId.isEmpty
//                               ? null
//                               : (value) {
//                                   print(value.sId);
//                                   setState(() {
//                                     test = value;
//                                     districtId = value.sId;
//                                   });
//                                 },
//                         ),
//                       ),

//                       SizedBox(height: 30),
//                       Text(
//                         "District",
//                         style: GoogleFonts.actor(
//                             color: Color(0xff819ffd),
//                             fontSize: 20,
//                             fontWeight: FontWeight.w600
//                             // fontWeight: FontWeight.bold,
//                             ),
//                       ),
//                       // Container(
//                       //   decoration: kSurveyFormContainerDecoration,
//                       //   width: MediaQuery.of(context).size.width * 0.75,
//                       //   child: DropdownButtonFormField<DistrictResult>(
//                       //       validator: (value) =>
//                       //           value == null ? 'Feild Required' : null,
//                       //       items: districtResult
//                       //           .map<DropdownMenuItem<DistrictResult>>(
//                       //               (DistrictResult item) {
//                       //         return DropdownMenuItem(
//                       //           child: Text(item.name),
//                       //           value: item,
//                       //         );
//                       //       }).toList(),
//                       //       onChanged: stateId.isEmpty
//                       //           ? null
//                       //           : (value) {
//                       //               test = value;
//                       //               districtId = value.sId;
//                       //             }),
//                       // ),

//                       InputRegistration(
//                           // isList: false,
//                           labeltext: "ADDRESS1",
//                           onSaved: (value) {
//                             setState(() {
//                               address1 = value;
//                             });
//                           }),
//                       SizedBox(height: 30),
//                       InputRegistration(
//                         // isList: false,
//                         labeltext: "VILLAGE",
//                         onSaved: (value) {
//                           setState(() {
//                             village = value;
//                           });
//                         },
//                         onValidate: (value) {
//                           if (!isAlpha(value))
//                             return "Enter a valid village name";
//                           else if (value.length == null) {
//                             return "Village name cant be Empty";
//                           }
//                           return null;
//                         },
//                       ),
//                       SizedBox(height: 30),
//                       InputRegistration(
//                         // isList: false,
//                         labeltext: "POST OFFICE",
//                         onSaved: (value) {
//                           setState(() {
//                             postOffice = value;
//                           });
//                         },
//                         onValidate: (value) {
//                           if (!isAlpha(value)) {
//                             return "Enter a valid Post Office";
//                           } else if (value.length == null) {
//                             return "Post Office value cant be null";
//                           } else {
//                             return null;
//                           }
//                         },
//                       ),
//                       SizedBox(height: 30),
//                       InputRegistration(
//                           // isList: false,
//                           onValidate: (value) {
//                             if (!isAlpha(value)) {
//                               return "Enter a valid Thana";
//                             } else if (value.length == null) {
//                               return "Thana value cant be null";
//                             } else {
//                               return null;
//                             }
//                           },
//                           labeltext: "THANA",
//                           onSaved: (value) {
//                             setState(() {
//                               thana = value;
//                             });
//                           }),
//                       SizedBox(height: 30),
//                       InputRegistration(
//                           textInputType: TextInputType.number,
//                           maxlength: 6,
//                           // isList: false,
//                           onValidate: (value) {
//                             if (!isNumeric(value)) {
//                               return "Enter a valid Pincode";
//                             } else if (isNull(value)) {
//                               return "Pincode value cant be null";
//                             } else {
//                               return null;
//                             }
//                           },
//                           labeltext: "PINCODE",
//                           onSaved: (value) {
//                             setState(() {
//                               pincode = value;
//                             });
//                           }),
//                       SizedBox(height: 30),
//                       Text(
//                         "STATE",
//                         style: GoogleFonts.actor(
//                           color: Color(0xff819ffd),
//                           fontSize: 20,
//                           fontWeight: FontWeight.bold,
//                           // fontWeight: FontWeight.bold,
//                         ),
//                       ),
//                       SizedBox(height: 10),
//                       // Container(
//                       //   decoration: BoxDecoration(
//                       //       border: Border.all(color: Colors.black)),
//                       //   width: MediaQuery.of(context).size.width * 0.85,
//                       //   child: Padding(
//                       //     padding: const EdgeInsets.symmetric(horizontal: 16.0),
//                       //     child: _stateListResponse == null
//                       //         ? InkWell(
//                       //             onTap: () {
//                       //               showDialog(
//                       //                 barrierDismissible: false,
//                       //                 context: context,
//                       //                 builder: (context) {
//                       //                   ApiCall.postState()
//                       //                       .then((value) => setState(() {
//                       //                             _stateListResponse = value;
//                       //                             stateChange = true;
//                       //                             _districtListResponse = null;
//                       //                             Navigator.pop(context);
//                       //                           }));
//                       //                   return Align(
//                       //                     alignment: Alignment.center,
//                       //                     child: Container(
//                       //                       height: 40,
//                       //                       width: 40,
//                       //                       child: CircularProgressIndicator(),
//                       //                     ),
//                       //                   );
//                       //                 },
//                       //               );
//                       //             },
//                       //             child: Text(
//                       //               'Select State',
//                       //               style: GoogleFonts.actor(
//                       //                 color: Colors.blue,
//                       //                 fontSize: 20,
//                       //                 // fontWeight: FontWeight.bold,
//                       //                 // fontWeight: FontWeight.bold,
//                       //               ),
//                       //             ),
//                       //           )
//                       //         : DropdownButton<String>(
//                       //             items: _stateListResponse.data.map((value) {
//                       //               return DropdownMenuItem<String>(
//                       //                 value: value.name,
//                       //                 child: Text(
//                       //                   value.name,
//                       //                   style: GoogleFonts.actor(
//                       //                     color: Colors.blue,
//                       //                     fontSize: 20,
//                       //                     // fontWeight: FontWeight.bold,
//                       //                     // fontWeight: FontWeight.bold,
//                       //                   ),
//                       //                 ),
//                       //               );
//                       //             }).toList(),
//                       //             onChanged: (value) {
//                       //               setState(() {
//                       //                 this.selectedState = value;
//                       //                 this.selectedStateId =
//                       //                     stateId[selectedState];
//                       //                 this._districtListResponse = null;
//                       //                 stateChange = true;
//                       //                 this.selectedDistrict = null;
//                       //                 // print('State: ' + selectedState);
//                       //                 // print('StateId: ' + selectedStateId);
//                       //               });
//                       //             },
//                       //             value: selectedState,
//                       //             hint: Text(
//                       //               "Select State",
//                       //               style: GoogleFonts.actor(
//                       //                 color: Colors.blue,
//                       //                 fontSize: 20,
//                       //                 // fontWeight: FontWeight.bold,
//                       //               ),
//                       //             ),
//                       //           ),
//                       //   ),
//                       // ),

//                       SizedBox(height: 30),
//                       Text(
//                         "DISTRICT",
//                         style: GoogleFonts.actor(
//                           color: Color(0xff819ffd),
//                           fontSize: 20,
//                           fontWeight: FontWeight.bold,
//                           // fontWeight: FontWeight.bold,
//                         ),
//                       ),
//                       SizedBox(height: 10),
//                       // Container(
//                       //   decoration: BoxDecoration(
//                       //       border: Border.all(color: Colors.black)),
//                       //   width: MediaQuery.of(context).size.width * 0.85,
//                       //   child: Padding(
//                       //     padding: const EdgeInsets.symmetric(horizontal: 16.0),
//                       //     child: _districtListResponse == null || stateChange
//                       //         ? InkWell(
//                       //             onTap: () {
//                       //               showDialog(
//                       //                 barrierDismissible: false,
//                       //                 context: context,
//                       //                 builder: (context) {
//                       //                   ApiCall.postDistrict(selectedStateId)
//                       //                       .then((value) => setState(() {
//                       //                             _districtListResponse = value;
//                       //                             stateChange = false;
//                       //                             Navigator.pop(context);
//                       //                           }));
//                       //                   return Align(
//                       //                     alignment: Alignment.center,
//                       //                     child: Container(
//                       //                       height: 40,
//                       //                       width: 40,
//                       //                       child: CircularProgressIndicator(),
//                       //                     ),
//                       //                   );
//                       //                 },
//                       //               );
//                       //             },
//                       //             child: Text(
//                       //               'Select District',
//                       //               style: GoogleFonts.actor(
//                       //                 color: Colors.blue,
//                       //                 fontSize: 20,
//                       //                 // fontWeight: FontWeight.bold,
//                       //                 // fontWeight: FontWeight.bold,
//                       //               ),
//                       //             ),
//                       //           )
//                       //         : _districtListResponse.result.length == 0
//                       //             ? DropdownButton<String>(
//                       //                 items: ['No District'].map((value) {
//                       //                   return DropdownMenuItem<String>(
//                       //                     value: value,
//                       //                     child: Text(
//                       //                       value,
//                       //                       style: GoogleFonts.actor(
//                       //                         color: Colors.blue,
//                       //                         fontSize: 20,
//                       //                         // fontWeight: FontWeight.bold,
//                       //                         // fontWeight: FontWeight.bold,
//                       //                       ),
//                       //                     ),
//                       //                   );
//                       //                 }).toList(),
//                       //                 onChanged: (value) {
//                       //                   setState(() {
//                       //                     this.selectedDistrict = value;
//                       //                   });
//                       //                 },
//                       //                 value: selectedDistrict,
//                       //                 hint: Text(
//                       //                   "Select District",
//                       //                   style: GoogleFonts.actor(
//                       //                     fontSize: 20,
//                       //                     color: Colors.blue,
//                       //                     // fontWeight: FontWeight.bold,
//                       //                   ),
//                       //                 ),
//                       //               )
//                       //             : DropdownButton<String>(
//                       //                 items: _districtListResponse.result
//                       //                     .map((value) {
//                       //                   return DropdownMenuItem<String>(
//                       //                     value: value.name,
//                       //                     child: Text(
//                       //                       value.name,
//                       //                       style: GoogleFonts.actor(
//                       //                         color: Colors.blue,
//                       //                         fontSize: 20,
//                       //                         // fontWeight: FontWeight.bold,
//                       //                         // fontWeight: FontWeight.bold,
//                       //                       ),
//                       //                     ),
//                       //                   );
//                       //                 }).toList(),
//                       //                 onChanged: (value) {
//                       //                   setState(() {
//                       //                     this.selectedDistrict = value;
//                       //                   });
//                       //                 },
//                       //                 value: selectedDistrict,
//                       //                 hint: Text(
//                       //                   "Select District",
//                       //                   style: GoogleFonts.actor(
//                       //                       fontSize: 20, color: Colors.blue
//                       //                       // fontWeight: FontWeight.bold,
//                       //                       ),
//                       //                 ),
//                       //               ),
//                       //   ),
//                       // ),

//                       SizedBox(height: 30),
//                       InputRegistration(
//                           // isList: false,
//                           onValidate: (value) {
//                             if (!isAlpha(value)) {
//                               return "Enter a valid Occupation";
//                             } else if (isNull(value)) {
//                               return "Occupation  cant be null";
//                             } else {
//                               return null;
//                             }
//                           },
//                           labeltext: "Occupation",
//                           // labeltext: "OCCUPATION",
//                           onSaved: (value) {
//                             setState(() {
//                               occupation = value;
//                             });
//                           }),
//                       SizedBox(height: 30),
//                       InputRegistration(
//                           maxlength: 10,
//                           // isList: false,
//                           onValidate: (value) {
//                             if (!isNumeric(value)) {
//                               return "Enter a valid Number";
//                             } else if (isNull(value)) {
//                               return "Occupation value cant be null";
//                             } else {
//                               return null;
//                             }
//                           },
//                           labeltext: "MOBILE",
//                           textInputType: TextInputType.phone,
//                           onSaved: (value) {
//                             setState(() {
//                               mobile = value;
//                             });
//                           }),
//                       SizedBox(height: 30),
//                       // InputRegistration(
//                       //   labeltext: "IDENTITY DOCUMENT",
//                       //   isList: true,
//                       //   list: identityList,
//                       //   listChangedValue: this.identity,
//                       //   listHintName: "Select Identity Document",
//                       // ),
//                       Text(
//                         "IDENTITY DOCUMENT",
//                         style: GoogleFonts.actor(
//                           color: Color(0xff819ffd),
//                           fontSize: 20,
//                           fontWeight: FontWeight.w600,
//                           // fontWeight: FontWeight.bold,
//                         ),
//                       ),
//                       SizedBox(height: 10),
//                       Container(
//                         decoration: BoxDecoration(
//                             border: Border.all(color: Colors.black)),
//                         width: MediaQuery.of(context).size.width * 0.85,
//                         child: Padding(
//                           padding: const EdgeInsets.symmetric(horizontal: 16.0),
//                           child: DropdownButton<String>(
//                             items: identityList.map((value) {
//                               return DropdownMenuItem<String>(
//                                 value: value,
//                                 child: Text(
//                                   value,
//                                   style: GoogleFonts.actor(
//                                     color: Colors.blue,
//                                     fontSize: 20,
//                                     // fontWeight: FontWeight.bold,
//                                     // fontWeight: FontWeight.bold,
//                                   ),
//                                 ),
//                               );
//                             }).toList(),
//                             onChanged: (value) {
//                               setState(() {
//                                 this.identity = value;
//                               });
//                             },
//                             value: identity,
//                             hint: Text(
//                               "Select Identity Document",
//                               style: GoogleFonts.actor(
//                                   fontSize: 20, color: Colors.blue
//                                   // fontWeight: FontWeight.bold,
//                                   ),
//                             ),
//                           ),
//                         ),
//                       ),

//                       SizedBox(height: 30),
//                       InkWell(
//                         onTap: () => getImage(context),
//                         child: Container(
//                           child: Row(
//                             children: [
//                               Text(
//                                 "UPLOAD DOCUMENT",
//                                 style: GoogleFonts.roboto(
//                                     color: Color(0xff819ffd),
//                                     fontSize: 20,
//                                     fontWeight: FontWeight.bold),
//                               ),
//                               img64 == null
//                                   ? Icon(
//                                       Icons.cloud_upload,
//                                       color: Color(0xff819ffd),
//                                     )
//                                   : Icon(
//                                       Icons.check,
//                                       color: Color(0xff819ffd),
//                                     ),
//                             ],
//                           ),
//                         ),
//                       ),
//                       SizedBox(height: 30),
//                       Padding(
//                         padding: const EdgeInsets.symmetric(horizontal: 40),
//                         child: GestureDetector(
//                           onTap: () async {
//                             if (_formKey.currentState.validate()) {
//                               _formKey.currentState.save();
//                               print(firstName +
//                                   '\n' +
//                                   lastName +
//                                   '\n' +
//                                   guardianName +
//                                   '\n' +
//                                   selectedDate.day.toString() +
//                                   '/' +
//                                   (selectedDate.month + 1).toString() +
//                                   '/' +
//                                   selectedDate.year.toString() +
//                                   '\n' +
//                                   country +
//                                   '\n' +
//                                   address1 +
//                                   '\n' +
//                                   village +
//                                   '\n' +
//                                   postOffice +
//                                   '\n' +
//                                   thana +
//                                   '\n' +
//                                   pincode +
//                                   '\n' +
//                                   selectedState +
//                                   '\n' +
//                                   selectedDistrict +
//                                   '\n' +
//                                   occupation +
//                                   '\n' +
//                                   identity +
//                                   '\n' +
//                                   img64 +
//                                   '\n' +
//                                   account_number +
//                                   '\n' +
//                                   bank_name +
//                                   '\n' +
//                                   gst_number +
//                                   '\n' +
//                                   hub +
//                                   '\n' +
//                                   ifsc +
//                                   '\n' +
//                                   org_other_type +
//                                   '\n' +
//                                   selectedOrgType +
//                                   '\n' +
//                                   organization_name +
//                                   '\n' +
//                                   reg_number);
//                               Dialogs.showLoadingDialog(context, _keyLoader);
//                               final signUpResponse = await ApiCall.postSignUp(
//                                 account_number: account_number.toString(),
//                                 bank_name: bank_name.toString(),
//                                 gst_number: gst_number.toString(),
//                                 hub: hub.toString(),
//                                 ifsc: ifsc.toString(),
//                                 org_other_type: org_other_type,
//                                 org_type: selectedOrgType.toString(),
//                                 organization_name: organization_name.toString(),
//                                 reg_number: reg_number.toString(),
//                                 firstName: firstName.toString(),
//                                 lastName: lastName.toString(),
//                                 gender: selectedGender.toString(),
//                                 dob: selectedDate.day.toString() +
//                                     '/' +
//                                     (selectedDate.month + 1).toString() +
//                                     '/' +
//                                     selectedDate.year.toString(),
//                                 fatherName: guardianName,
//                                 address1: address1,
//                                 village: village,
//                                 postOffice: postOffice,
//                                 thana: thana,
//                                 country: country,
//                                 state: selectedState,
//                                 district: selectedDistrict,
//                                 pincode: pincode,
//                                 occupation: occupation,
//                                 identity: identity,
//                                 mobile: mobile,
//                                 document: 'data:image/jpg;base64,$img64',
//                               );
//                               if (signUpResponse == null) {
//                                 CustomView.showInDialog(
//                                     context, "Error", "Retry", () {
//                                   Navigator.of(context).pop();
//                                   Navigator.of(_keyLoader.currentContext,
//                                           rootNavigator: true)
//                                       .pop();
//                                 });
//                               } else if (signUpResponse.status == null) {
//                                 CustomView.showInDialog(
//                                     context, "Error", "Retry", () {
//                                   Navigator.of(context).pop();
//                                   Navigator.of(_keyLoader.currentContext,
//                                           rootNavigator: true)
//                                       .pop();
//                                 });
//                               } else if (signUpResponse.status == "api_error") {
//                                 CustomView.showInDialog(
//                                     context,
//                                     "Error",
//                                     // "Something is wrong at server end!"
//                                     signUpResponse.message, () {
//                                   Navigator.of(context).pop();
//                                   Navigator.of(_keyLoader.currentContext,
//                                           rootNavigator: true)
//                                       .pop();
//                                 });
//                               } else if (signUpResponse.status == "error") {
//                                 CustomView.showInDialog(context, "Error",
//                                     signUpResponse.message.toString(), () {
//                                   Navigator.of(context).pop();
//                                   Navigator.of(_keyLoader.currentContext,
//                                           rootNavigator: true)
//                                       .pop();
//                                 });
//                               } else if (signUpResponse.status ==
//                                   "validation_error") {
//                                 CustomView.showInDialog(
//                                     context, "Error", "Some fields missing!",
//                                     () {
//                                   Navigator.of(context).pop();
//                                   Navigator.of(_keyLoader.currentContext,
//                                           rootNavigator: true)
//                                       .pop();
//                                 });
//                               } else {
//                                 if (signUpResponse.status == "success") {
//                                   print(signUpResponse.status);
//                                   Navigator.of(_keyLoader.currentContext,
//                                           rootNavigator: true)
//                                       .pop();
//                                   Navigator.pushReplacementNamed(
//                                       context, 'login');
//                                   //IDHR SHOW kra de CUSTOMView DIalog for success.
//                                 }
//                               }
//                               print(signUpResponse.status);
//                               print(signUpResponse.message);
//                               print(signUpResponse.data);
//                             }
//                           },
//                           child: Align(
//                             alignment: Alignment.bottomRight,
//                             child: Card(
//                               elevation: 10,
//                               shape: BeveledRectangleBorder(
//                                 borderRadius: BorderRadius.circular(10.0),
//                               ),
//                               child: Container(
//                                 width: MediaQuery.of(context).size.width * 0.40,
//                                 child: Padding(
//                                   padding: const EdgeInsets.symmetric(
//                                       horizontal: 24.0, vertical: 8),
//                                   child: Center(
//                                     child: Text(
//                                       "SUBMIT",
//                                       style: GoogleFonts.roboto(
//                                         color: Colors.white,
//                                         fontSize: 20,
//                                         fontWeight: FontWeight.bold,
//                                       ),
//                                     ),
//                                   ),
//                                 ),
//                                 decoration: BoxDecoration(
//                                   color: Colors.blue[800],
//                                   borderRadius: BorderRadius.circular(10),
//                                 ),
//                               ),
//                             ),
//                           ),
//                         ),
//                       ),
//                       SizedBox(
//                         height: 20,
//                       ),
//                     ],
//                   ),
//                 ),
//               ),
//             ),
//             decoration: BoxDecoration(
//               image: DecorationImage(
//                   fit: BoxFit.fill,
//                   image: AssetImage('assets/images/background.png')),
//             ),
//           ),
//           Positioned(
//             top: 0,
//             bottom: MediaQuery.of(context).size.height * 0.60,
//             left: 0,
//             right: 0,
//             child: Container(
//               width: MediaQuery.of(context).size.width,
//               // height: MediaQuery.of(context).size.height*0.40,
//               child: Container(
//                 height: 200,
//                 width: 200,
//                 padding: const EdgeInsets.all(24.0),
//                 child: Image.asset('assets/images/logologin.png'),
//               ),
//               alignment: Alignment.topRight,
//               decoration: BoxDecoration(
//                   image: DecorationImage(
//                       fit: BoxFit.fill,
//                       image: AssetImage('assets/images/logintopsegment.png'))),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }

class InputRegistration extends StatefulWidget {
  final String labeltext;
  final onSaved;
  final int maxlength;
  final onValidate;
  // final bool isList;
  // List list = new List();
  // String listChangedValue;
  // String listHintName;

  TextInputType textInputType;

  InputRegistration(
      {@required this.labeltext,
      this.onSaved,
      this.textInputType,
      this.onValidate,
      // @required this.isList,
      // this.list,
      // this.listChangedValue,
      // this.listHintName,
      this.maxlength});

  @override
  _InputRegistrationState createState() => _InputRegistrationState();
}

class _InputRegistrationState extends State<InputRegistration> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            child: Align(
              alignment: Alignment.topLeft,
              child: Text(
                widget.labeltext,
                textAlign: TextAlign.start,
                style: GoogleFonts.roboto(
                    color: Color(0xff819ffd),
                    fontSize: 20,
                    fontWeight: FontWeight.w600),
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          // (widget.isList == true)
          //     ? Container(
          //         decoration:
          //             BoxDecoration(border: Border.all(color: Colors.black)),
          //         width: MediaQuery.of(context).size.width * 0.85,
          //         child: Padding(
          //           padding: const EdgeInsets.symmetric(horizontal: 16.0),
          //           child: DropdownButton<String>(
          //             items: widget.list.map((value) {
          //               return DropdownMenuItem<String>(
          //                 value: value,
          //                 child: Text(
          //                   value,
          //                   style: GoogleFonts.actor(
          //                     color: Colors.blue,
          //                     fontSize: 20,
          //                     // fontWeight: FontWeight.bold,
          //                     // fontWeight: FontWeight.bold,
          //                   ),
          //                 ),
          //               );
          //             }).toList(),
          //             onChanged: (value) {
          //               setState(() {
          //                 widget.listChangedValue = value;
          //               });
          //             },
          //             value: widget.listChangedValue,
          //             hint: Text(
          //               widget.listHintName,
          //               style:
          //                   GoogleFonts.actor(fontSize: 20, color: Colors.blue
          //                       // fontWeight: FontWeight.bold,
          //                       ),
          //             ),
          //           ),
          //         ),
          //       )
          //     :
          Container(
            decoration: kSurveyFormContainerDecoration,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: TextFormField(
                inputFormatters: [
                  new LengthLimitingTextInputFormatter(widget.maxlength),
                ],
                // maxLength: widget.maxlength,
                validator: widget.onValidate,
                onSaved: widget.onSaved,
                keyboardType: widget.textInputType == null
                    ? TextInputType.text
                    : widget.textInputType,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  // new UnderlineInputBorder(
                  //   borderSide: new BorderSide(color: Color(0xff3469FA)),
                  // ),
                  // labelText: labeltext,
                  labelStyle: GoogleFonts.roboto(
                      color: Color(0xff819ffd),
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ),
        ],
      ),
      width: MediaQuery.of(context).size.width * 0.75,
    );
  }
}
