import 'package:blu_way/Constants/ConstantList.dart';
import 'package:blu_way/Model/Auth/VarifyOrganizationModel.dart';
import 'package:blu_way/Model/Auth/VarifySEModel.dart';
import 'package:blu_way/Model/Auth/VarifySupervisorModel.dart';
import 'package:blu_way/Model/Auth/fetchOtpModel.dart';
import 'package:blu_way/Model/verifyResponse.dart';
import 'package:blu_way/Widget/Dialog.dart';
import 'package:blu_way/utlis/global.dart';
import 'package:blu_way/utlis/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:validators/validators.dart' as validator;
import 'package:google_fonts/google_fonts.dart';

import 'package:validators/validators.dart';
import 'HomePage.dart';
import 'Registration.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  String num, status, otp, serverOTP, tokenCall;
  String response;
  VerifyResponse numVerifyResponse;
  String groupvalue = '';
  String actualGroupValue = '';

  Map<String, dynamic> verify;
  bool isLoading;
  String accessToken;
  bool nameChange;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  getotp() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      if (groupvalue != null) {
        (groupvalue == 'Organization')
            ? fetchOrganizationOtpModel(mobileNo: num).then((value) {
                setState(() {
                  Global.isLoading = false;
                });
                if (value.status == 'success') {
                  Navigator.of(_keyLoader.currentContext, rootNavigator: true)
                      .pop();
                  setState(() {
                    otp = value.oTP.toString();
                    actualGroupValue = groupvalue;

                    Toast.show(
                      value.message,
                      context,
                      backgroundColor: Colors.white,
                      textColor: Colors.black,
                      duration: 2,
                    );
                  });
                } else {
                  Navigator.of(_keyLoader.currentContext, rootNavigator: true)
                      .pop();
                  Toast.show(
                    value.message,
                    context,
                    backgroundColor: Colors.white,
                    textColor: Colors.black,
                    duration: 2,
                  );
                }
              })
            : (groupvalue == 'Supervisor')
                ? fetchSupervisorOtpModel(mobileNo: num).then((value) {
                    setState(() {
                      Global.isLoading = false;
                    });
                    if (value.status == 'success') {
                      Navigator.of(_keyLoader.currentContext,
                              rootNavigator: true)
                          .pop();
                      setState(() {
                        otp = value.oTP.toString();
                        actualGroupValue = groupvalue;

                        Toast.show(
                          value.message,
                          context,
                          backgroundColor: Colors.white,
                          textColor: Colors.black,
                          duration: 2,
                        );
                      });
                    } else {
                      Navigator.of(_keyLoader.currentContext,
                              rootNavigator: true)
                          .pop();
                      Toast.show(
                        value.message,
                        context,
                        backgroundColor: Colors.white,
                        textColor: Colors.black,
                        duration: 2,
                      );
                    }
                  })
                : fetchSEOtpModel(mobileNo: num).then((value) {
                    setState(() {
                      Global.isLoading = false;
                    });
                    if (value.status == 'success') {
                      Navigator.of(_keyLoader.currentContext,
                              rootNavigator: true)
                          .pop();
                      setState(() {
                        otp = value.oTP.toString();
                        actualGroupValue = groupvalue;

                        Toast.show(
                          value.message,
                          context,
                          backgroundColor: Colors.white,
                          textColor: Colors.black,
                          duration: 2,
                        );
                      });
                    } else {
                      Navigator.of(_keyLoader.currentContext,
                              rootNavigator: true)
                          .pop();
                      Toast.show(
                        value.message,
                        context,
                        backgroundColor: Colors.white,
                        textColor: Colors.black,
                        duration: 2,
                      );
                    }
                  });
      } else {
        Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
        Toast.show(
          "Choose Atleast one user",
          context,
          backgroundColor: Colors.white,
          textColor: Colors.black,
          duration: 2,
        );
      }
    }
  }

  loginSubmitted() {
    if (actualGroupValue == "Organization") {
      varifyOrganizationModel(otp: otp, mobileNo: num).then((value) async {
        setState(() => Global.isLoading = false);
        if (value.status == 'success') {
          Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
          Global.id = value.result.sId;
          Global.loginModel = value;
          // authProvider.settoken(value: value.token);
          Global.accessToken = value.token;

          SharedPreferences sharedPreferences =
              await SharedPreferences.getInstance();
          sharedPreferences.setString("token", value.token);
          sharedPreferences.setBool("isSigned", true);
          print("Login token is: " + value.token);
          sharedPreferences.setString('LoginId', value.result.sId);

          // sharedPreferences.setString("", value)
          sharedPreferences.setString(
              "Organization_id", value.result.organizations.sId);
          String status = sharedPreferences.getBool("isSigned").toString();

          sharedPreferences.setString("first_name", value.result.firstName);
          sharedPreferences.setString("last_name", value.result.lastName);
          sharedPreferences.setString("village", value.result.village);
          sharedPreferences.setString("post_office", value.result.postOffice);
          sharedPreferences.setString("thana", value.result.thana);
          sharedPreferences.setString("state", value.result.state);
          sharedPreferences.setString("district", value.result.district);
          sharedPreferences.setString("pincode", value.result.pincode);
          sharedPreferences.setString("document", value.result.document);
          sharedPreferences.setString("mobile", value.result.mobile);
          sharedPreferences.setString('email', value.result.email);
          sharedPreferences.setString('father_name', value.result.fatherName);
          sharedPreferences.setString('occupation', value.result.occupation);
          sharedPreferences.setString('address', value.result.address);
          sharedPreferences.setString(
              "profile_image", value.result.profileImage);
          // String state;
          // String district;
          // String name;
          // String pincode;
          // String orgType;
          // String orgOtherType;
          // String regNumber;
          // String gstNumber;
          sharedPreferences.setString("name", value.result.organizations.name);
          // sharedPreferences.setString("state", value.orgData.state);
          // sharedPreferences.setString("district", value.orgData.district);
          sharedPreferences.setString("pincode", value.result.organizations.pincode);
          sharedPreferences.setString("orgType", value.result.organizations.orgType);
          sharedPreferences.setString("orgOtherType", value.result.organizations.orgOtherType);
          sharedPreferences.setString("regNumber", value.result.organizations.regNumber);
          sharedPreferences.setString("gstNumber", value.result.organizations.gstNumber);

          print("User logged in status " + status);
          // saveTransporterData(value);
          Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (context) {
            return MyHomePage(
              title: "ORGANIZATION",
              token: value.token,
              model: value,
            );
          }));
        } else {
          Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
          Toast.show(
            value.message,
            context,
            backgroundColor: Colors.white,
            textColor: Colors.black,
            duration: 2,
          );
        }
      });
    } else if (actualGroupValue == "Supervisor") {
      varifySupervisorModel(otp: otp, mobileNo: num).then((value) async {
        setState(() => Global.isLoading = false);
        if (value.status == 'success') {
          Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
          Global.id = value.result.sId;
          Global.supervisorModel = value;
          // authProvider.settoken(value: value.token);
          Global.accessToken = value.token;

          SharedPreferences sharedPreferences =
              await SharedPreferences.getInstance();
          sharedPreferences.setString("token", value.token);
          sharedPreferences.setBool("isSigned", true);
          sharedPreferences.setString("Supervisor_id", value.result.sId);
          String status = sharedPreferences.getBool("isSigned").toString();

          sharedPreferences.setString("first_name", value.result.firstName);
          sharedPreferences.setString("last_name", value.result.lastName);
          sharedPreferences.setString("village", value.result.village);
          sharedPreferences.setString("post_office", value.result.postOffice);
          sharedPreferences.setString("thana", value.result.thana);
          sharedPreferences.setString("state", value.result.state);
          sharedPreferences.setString("district", value.result.district);
          sharedPreferences.setString("pincode", value.result.pincode);
          sharedPreferences.setString("document", value.result.document);
          sharedPreferences.setString("mobile", value.result.mobile);
          sharedPreferences.setString('email', value.result.email);
          sharedPreferences.setString('father_name', value.result.fatherName);
          sharedPreferences.setString('occupation', value.result.occupation);
          sharedPreferences.setString('address', value.result.address);
          sharedPreferences.setString(
              "profile_image", value.result.profileImage);

          print("User logged in status " + status);
          // saveTransporterData(value);
          Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (context) {
            return MyHomePage(
              title: "SUPERVISOR",
              token: value.token,
              model: value,
            );
          }));
        } else {
          Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
          Toast.show(
            value.message,
            context,
            backgroundColor: Colors.white,
            textColor: Colors.black,
            duration: 2,
          );
        }
      });
    } else if (actualGroupValue == "Sales Executive") {
      varifySEModel(otp: otp, mobileNo: num).then((value) async {
        setState(() => Global.isLoading = false);
        if (value.status == 'success') {
          Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
          Global.id = value.result.sId;
          Global.seModel = value;
          // authProvider.settoken(value: value.token);
          Global.accessToken = value.token;

          SharedPreferences sharedPreferences =
              await SharedPreferences.getInstance();
          sharedPreferences.setString("token", value.token);
          sharedPreferences.setBool("isSigned", true);
          sharedPreferences.setString("Supervisor_id", value.result.sId);
          String status = sharedPreferences.getBool("isSigned").toString();

          sharedPreferences.setString("first_name", value.result.firstName);
          sharedPreferences.setString("last_name", value.result.lastName);
          sharedPreferences.setString("village", value.result.village);
          sharedPreferences.setString("post_office", value.result.postOffice);
          sharedPreferences.setString("thana", value.result.thana);
          sharedPreferences.setString("state", value.result.state);
          sharedPreferences.setString("district", value.result.district);
          sharedPreferences.setString("pincode", value.result.pincode);
          sharedPreferences.setString("document", value.result.document);
          sharedPreferences.setString("mobile", value.result.mobile);
          sharedPreferences.setString('email', value.result.email);
          sharedPreferences.setString('father_name', value.result.fatherName);
          sharedPreferences.setString('occupation', value.result.occupation);
          sharedPreferences.setString('address', value.result.address);
          sharedPreferences.setString(
              "profile_image", value.result.profileImage);

          print("User logged in status " + status);
          // saveTransporterData(value);
          Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (context) {
            return MyHomePage(
              title: "SALES EXECUTIVE",
              token: value.token,
              model: value,
            );
          }));
        } else {
          Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
          Toast.show(
            value.message,
            context,
            backgroundColor: Colors.white,
            textColor: Colors.black,
            duration: 2,
          );
        }
      });
    } else if (actualGroupValue == null) {
      Navigator.of(context).pop();
      Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
      Toast.show(
        "Please Generate OTP First",
        context,
        backgroundColor: Colors.white,
        textColor: Colors.black,
        duration: 5,
      );
    }
  }

  @override
  void initState() {
    super.initState();
    isLoading = true;
    num = '';
    nameChange = true;
    status = '';
    verify = {};
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Stack(
          children: [
            CustomContainer(
              child: ListView(
                children: [
                  Container(
                    height: 200,
                    child: Stack(
                      children: [
                        Container(
                          height: 200,
                          // width: MediaQuery.of(context).size.width,
                          padding: const EdgeInsets.all(24.0),
                          // child: Image.asset('assets/images/logintopsegment.png'),
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  fit: BoxFit.fill,
                                  image: AssetImage(
                                      'assets/images/logintopsegment.png'))),
                        ),
                        Positioned(
                          top: 30,
                          right: 20,
                          child: Container(
                            height: 80,
                            width: 140,
                            padding: const EdgeInsets.all(24.0),
                            // child: Image.asset('assets/images/logintopsegment.png'),
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    fit: BoxFit.fill,
                                    image: AssetImage(
                                        'assets/images/logologin.png'))),
                          ),
                        )
                        // ),
                      ],
                    ),
                  ),

                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    child: Text(
                      'LOGIN',
                      style: TextStyle(
                        fontSize: 50,
                        fontFamily: 'Shruti',
                        color: Color(0xffaedcff),
                        shadows: <Shadow>[
                          Shadow(
                            offset: Offset(1.0, 1.0),
                            blurRadius: 3.0,
                            color: Color.fromARGB(255, 0, 0, 0),
                          ),
                          Shadow(
                            offset: Offset(1.0, 1.0),
                            blurRadius: 8.0,
                            color: Color.fromARGB(125, 0, 0, 255),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    child: Container(
                      // height: MediaQuery.of(context).size.height,
                      child: Form(
                        key: _formKey,
                        child: Column(
                          children: [
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                TextFormField(
                                  // controller: fname,
                                  // autovalidateMode: autovalidate,
                                  keyboardType: TextInputType.number,
                                  style: TextStyle(
                                      fontFamily: 'Arial',
                                      color: AppColors.whiteColor,
                                      fontSize: 22),
                                  validator: (val) {
                                    if (val.trim().isEmpty) {
                                      // Global.isLoading = false;
                                      return 'Please enter your Number';
                                    } else if (!isNumeric(val)) {
                                      return "Enter a valid Number";
                                    }

                                    return null;
                                  },
                                  onSaved: (val) {
                                    setState(() {
                                      num = val;
                                    });
                                  },
                                  decoration: InputDecoration(
                                    labelText: 'MOBILE NUMBER',
                                    labelStyle: TextStyle(
                                        fontFamily: 'Shruti',
                                        fontWeight: FontWeight.w600,
                                        fontSize: 20,
                                        color: AppColors.textFeildcolor),
                                    border: InputBorder.none,
                                  ),
                                ),
                                Container(
                                    child: Image.asset(
                                  'assets/images/Loginsignup/Line-1.png',
                                )),
                                SizedBox(height: 10),
                                TextFormField(
                                  // controller: fname,
                                  // autovalidateMode: autovalidate,
                                  keyboardType: TextInputType.number,
                                  maxLength: 6,
                                  style: TextStyle(
                                      fontFamily: 'Arial',
                                      color: AppColors.whiteColor,
                                      fontSize: 22),
                                  onSaved: (val) {
                                    setState(() {
                                      otp = val;
                                    });
                                  },
                                  decoration: InputDecoration(
                                    labelText: 'OTP',
                                    labelStyle: TextStyle(
                                        fontFamily: 'Shruti',
                                        fontWeight: FontWeight.w600,
                                        fontSize: 20,
                                        color: AppColors.textFeildcolor),
                                    border: InputBorder.none,
                                  ),
                                ),
                                Container(
                                    child: Image.asset(
                                  'assets/images/Loginsignup/Line-1.png',
                                )),
                                SizedBox(height: 30),
                                // Container(
                                //   child: Column(
                                //     crossAxisAlignment:
                                //         CrossAxisAlignment.start,
                                //     children: [
                                //       Row(
                                //         children: [
                                //           Radio(
                                //               value: Constants.userType[0],
                                //               groupValue: groupvalue,
                                //               onChanged: (val) {
                                //                 setState(() {
                                //                   groupvalue = val;
                                //                 });
                                //               }),
                                //           Text(
                                //             Constants.userType[0],
                                //             style: TextStyle(
                                //               color: Color(0xff819ffd),
                                //               fontSize: 20,
                                //             ),
                                //           ),
                                //         ],
                                //       ),
                                //       Row(
                                //         children: [
                                //           Radio(
                                //               value: Constants.userType[1],
                                //               groupValue: groupvalue,
                                //               onChanged: (val) {
                                //                 setState(() {
                                //                   groupvalue = val;
                                //                 });
                                //               }),
                                //           Text(
                                //             Constants.userType[1],
                                //             style: TextStyle(
                                //               color: Color(0xff819ffd),
                                //               fontSize: 20,
                                //             ),
                                //           ),
                                //         ],
                                //       ),
                                //       Row(
                                //         children: [
                                //           Radio(
                                //               value: Constants.userType[2],
                                //               groupValue: groupvalue,
                                //               onChanged: (val) {
                                //                 setState(() {
                                //                   groupvalue = val;
                                //                 });
                                //               }),
                                //           Text(
                                //             Constants.userType[2],
                                //             style: TextStyle(
                                //               color: Color(0xff819ffd),
                                //               fontSize: 20,
                                //             ),
                                //           ),
                                //         ],
                                //       ),
                                //     ],
                                //   ),
                                // ),
                                // Container(
                                //     child: Image.asset(
                                //       'assets/images/Loginsignup/Line-1.png',
                                //     )),
                                Container(
                                  padding: EdgeInsets.fromLTRB(18, 0, 8, 8),
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                          image: ExactAssetImage(
                                              'assets/images/Loginsignup/Field-01.png'),
                                          fit: BoxFit.fill)),
                                  child: DropdownButtonFormField<String>(
                                    isDense: true,
                                    style: TextStyle(
                                        fontFamily: 'Shruti',
                                        fontWeight: FontWeight.w600,
                                        fontSize: 20,
                                        color: Color(0xff819ffd)),
                                    hint: Text('SELECT USER',
                                        style: TextStyle(
                                            fontFamily: 'Shruti',
                                            fontWeight: FontWeight.w600,
                                            fontSize: 18,
                                            color: Color(0xff819ffd))),
                                    decoration: InputDecoration(
                                        enabledBorder: InputBorder.none),
                                    icon: Padding(
                                      padding:
                                          const EdgeInsets.only(right: 8.0),
                                      child: Image.asset(
                                        'assets/images/Loginsignup/DropDown-Icon-01.png',
                                        height: 15,
                                        width: 15,
                                      ),
                                    ),
                                    validator: (value) =>
                                        value == null ? 'Feild Required' : null,
                                    items: <String>[
                                      'Organization',
                                      'Supervisor',
                                      'Sales Executive',
                                    ].map((String value) {
                                      return new DropdownMenuItem<String>(
                                        value: value,
                                        child: new Text(value),
                                      );
                                    }).toList(),
                                    onChanged: (val) {
                                      setState(() {
                                        // gender = val;
                                        groupvalue = val;
                                      });
                                    },
                                  ),
                                ),
                                // Container(
                                //     child: Image.asset(
                                //       'assets/images/Loginsignup/Line-1.png',
                                //     )),
                                SizedBox(
                                  height: 20,
                                ),
                              ],
                            ),
                            Column(
                              children: [
                                GestureDetector(
                                  onTap: () async {
                                    if (_formKey.currentState.validate() &&
                                        groupvalue.isNotEmpty) {
                                      _formKey.currentState.save();
                                      Dialogs.showLoadingDialog(
                                          context, _keyLoader);
                                      getotp();
                                    } else {
                                      Toast.show(
                                        "Please Select User Type",
                                        context,
                                        backgroundColor: Colors.white,
                                        textColor: Colors.black,
                                        gravity: Toast.CENTER,
                                        duration: 2,
                                      );
                                    }
                                  },
                                  child: Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.70,
                                    child: Stack(
                                      alignment: Alignment.center,
                                      children: [
                                        Image.asset(
                                            'assets/images/Loginsignup/BT-2.png'),
                                        Text(
                                          (nameChange)
                                              ? "GENERATE OTP"
                                              : "RESEND OTP",
                                          style: TextStyle(
                                              fontFamily: 'Arial',
                                              fontSize: 30,
                                              fontWeight: FontWeight.bold,
                                              color: Color(0xff045890)),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(height: 30),
                                GestureDetector(
                                  onTap: () async {
                                    if (_formKey.currentState.validate() &&
                                        actualGroupValue.isNotEmpty) {
                                      _formKey.currentState.save();
                                      Dialogs.showLoadingDialog(
                                          context, _keyLoader);
                                      loginSubmitted();
                                    } else {
                                      Toast.show(
                                        "Please Generate OTP First",
                                        context,
                                        backgroundColor: Colors.white,
                                        textColor: Colors.black,
                                        gravity: Toast.CENTER,
                                        duration: 5,
                                      );
                                    }
                                  },
                                  child: Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.75,
                                    child: Stack(
                                      alignment: Alignment.center,
                                      children: [
                                        Image.asset(
                                            'assets/images/EditProfile/buttonBG.png'),
                                        Text(
                                          "LOGIN",
                                          style: TextStyle(
                                              fontFamily: 'Arial',
                                              fontSize: 30,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                GestureDetector(
                                  onTap: () {
                                    Navigator.pushReplacementNamed(
                                        context, 'registration');
                                  },
                                  child: Container(
                                    margin: EdgeInsets.only(bottom: 20),
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 24.0, vertical: 8),
                                      child: Center(
                                        child: RichText(
                                          text: TextSpan(
                                              style: GoogleFonts.roboto(
                                                color: Colors.white,
                                                fontSize: 15,
                                                // fontWeight: FontWeight.bold,
                                              ),
                                              children: [
                                                TextSpan(
                                                    text:
                                                        ''' Don't have an account'''),
                                                TextSpan(
                                                  text: ''' Create One''',
                                                  style: GoogleFonts.roboto(
                                                    color: Colors.white,
                                                    fontSize: 20,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                                TextSpan(text: ''' here'''),
                                              ]),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  )
                  // Container(
                  //   height: MediaQuery.of(context).size.height,
                  //   child: ListView(
                  //     children: [
                  //
                  //     ],
                  //   ),
                  // ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
