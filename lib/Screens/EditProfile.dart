import 'dart:convert';
import 'dart:io';
import 'package:blu_way/Functions/network/api_constants.dart';
import 'package:cool_alert/cool_alert.dart';
import 'package:http/http.dart' as http;

import 'package:blu_way/Constants/ConstantList.dart';
import 'package:blu_way/Constants/Style.dart';
import 'package:blu_way/Model/Auth/UpdateProfile.dart';
import 'package:blu_way/Model/Auth/VarifyOrganizationModel.dart';
import 'package:blu_way/Widget/FetchingData.dart';
import 'package:blu_way/Widget/MyAppBar.dart';
import 'package:blu_way/utlis/global.dart';
import 'package:blu_way/utlis/theme.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:validators/validators.dart';
import 'dart:io' as Io;

class EditProfile extends StatefulWidget {
  var model;

  EditProfile({@required this.model});

  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  VarifyOrganizationModel varifyOrganizationModel = VarifyOrganizationModel();
  final _formKey = GlobalKey<FormState>();
  var decoded;
  // double height = appBar.preferredSize.height;
  String fname = ' ';
  String lname = ' ';
  String gender = "Male";
  String img64 = ' ';
  String father_name = ' ';

  String address = ' ';
  String email = ' ';
  String village = ' ';
  String postOffice = ' ';
  String thana = ' ';
  String state = ' ';
  String district = ' ';
  String pincode = ' ';
  String occupation = ' ';
  String document = ' ';
  String mobile = ' ';
  String profile_image = ' ';
  String id = ' ';
  String header = ' ';
  File _image;
  TextEditingController dob = TextEditingController();
  DateTime selectedDate = DateTime.now();
  Color textFieldLabelColor = Color(0xff004fc1);

  _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime(1980),
      lastDate: DateTime(2025),
    ).then((selectedDate) {
      if (selectedDate != null) {
        dob.text = DateFormat('yyyy-MM-dd').format(selectedDate);
      }
      return;
    });
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  Future<String> getImage(BuildContext context) async {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
              content: SizedBox(
            height: 150,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                FlatButton(
                  child: Text(
                    "Camera",
                    style: TextStyle(fontSize: 26),
                  ),
                  onPressed: () async {
                    var image = await ImagePicker().getImage(
                      source: ImageSource.camera,
                      imageQuality: 50,
                    );
                    // var image = await ImagePicker()
                    //     .getImage(source: ImageSource.camera);
                    setState(() {
                      // _image = image;
                      _image = File(image.path);
                      List<int> bytes = Io.File(_image.path).readAsBytesSync();
                      img64 = base64UrlEncode(bytes);
                      // var base64String =
                      //     base64UrlEncode(_image.readAsBytesSync());
                      // base64String = base64Encode(_image.readAsBytesSync());
                      // print('document $img64');
                      // print('base64String $base64String');
                      Navigator.pop(context);
                    });
                  },
                ),
                FlatButton(
                  child: Text(
                    "Gallery",
                    style: TextStyle(fontSize: 26),
                  ),
                  onPressed: () async {
                    var image = await ImagePicker().getImage(
                      source: ImageSource.gallery,
                      imageQuality: 50,
                    );
                    // var image = await ImagePicker()
                    //     .getImage(source: ImageSource.gallery);
                    setState(() {
                      _image = File(image.path);
                      // print(_image.uri.toString());
                      List<int> bytes = Io.File(_image.path).readAsBytesSync();
                      img64 = base64UrlEncode(bytes);
                      print('img64 : $img64');
                      print('bytes $bytes');
                      // print('base64Image : $img64');
                      // base64String = base64Encode(_image.readAsBytesSync());
                      // print('document $img64');
                      // print('base64String $base64String');
                      print('image1 : $_image');
                      Navigator.pop(context);
                    });
                  },
                ),
                _image != null
                    ? FlatButton(
                        child: Text(
                          "Remove Profile",
                          style: TextStyle(fontSize: 26),
                        ),
                        onPressed: () async {
                          setState(() {
                            _image = null;
                            Navigator.pop(context);
                          });
                        },
                      )
                    : Text(
                        "",
                      ),
              ],
            ),
          ));
        });
  }

  getDataFromSharedPreference() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    id = sharedPreferences.getString("LoginId");
    header = sharedPreferences.getString("token");
    fname = sharedPreferences.get('first_name');
    email = sharedPreferences.get('email');
    father_name = sharedPreferences.get('father_name');
    address = sharedPreferences.get('address');
    occupation = sharedPreferences.get('occupation');
    lname = sharedPreferences.getString('last_name');
    village = sharedPreferences.getString('village');
    postOffice = sharedPreferences.getString('post_office');
    thana = sharedPreferences.getString('thana');
    state = sharedPreferences.getString('state');
    district = sharedPreferences.getString('district');
    pincode = sharedPreferences.getString('pincode');
    document = sharedPreferences.getString('document');
    mobile = sharedPreferences.getString('mobile');
    profile_image = sharedPreferences.getString('profile_image');
    print("Data from shared Preference:" + fname);
    Map map = {
      'fname': fname,
      'lname': lname,
      'village': village,
      'postOffice': postOffice,
      'thana': thana,
      'state': state,
      'district': district,
      'pincode': pincode,
      'document': document,
      'mobile': mobile,
      'profile_image': profile_image
    };
    return map;
  }

  Future<UpdateProfile> submitdata() async {
    var response = await http.post(
      Uri.encodeFull(AppConstants.updateProfile),
      headers: {
        "accept": "application/json",
        "authorization": "$header",
      },
      body: json.encode({
        "id": id,
        "first_name": fname,
        "last_name": lname,
        "email": email,
        "gender": gender,
        "dob": dob.text,
        "father_name": father_name,
        "address1": address,
        "village": village,
        "post_office": postOffice,
        "thana": thana,
        "state": state,
        "district": district,
        "pincode": pincode,
        "occupation": occupation,
        "document": document,
        "mobile": mobile,
        "profile_image": "data:image/png;base64, $profile_image",
      }),
    );
    if (response.statusCode == 200) {
      decoded = json.decode(response.body);

      return UpdateProfile.fromJson(json.decode(response.body));
    } else {
      print("There is an error with ${response.statusCode} status code");
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getDataFromSharedPreference();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: MyAppBar("UPDATE PROFILE"),
        body: FutureBuilder(
            future: getDataFromSharedPreference(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                Map mapValue = snapshot.data;
                return Stack(
                  children: [
                    CustomContainer(),
                    Column(
                      children: [
                        Container(
                          height: 6,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                              gradient: LinearGradient(colors: [
                            Color(0xffa73628),
                            Color(0xfff2d367),
                          ])),
                        ),
                        Container(
                          height: MediaQuery.of(context).size.height * 0.35,
                          child: Row(
                            children: [
                              GestureDetector(
                                child: Column(
                                  children: [
                                    Container(
                                      margin:
                                          EdgeInsets.only(top: 32, left: 24),
                                      // alignment: Alignment.topLeft,
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Colors.white,
                                      ),
                                      height: 150,
                                      width: 150,
                                      child: Padding(
                                        padding: const EdgeInsets.all(24.0),
                                        child: (_image == null)
                                            ? Image.asset(
                                                'assets/images/EditProfile/Icon.png',
                                                fit: BoxFit.contain,
                                              )
                                            : Image.file(
                                                _image,
                                                fit: BoxFit.contain,
                                              ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 16.0, top: 8),
                                      child: Container(
                                        child: Text(
                                          "UPLOAD DOCUMENT",
                                          style: GoogleFonts.roboto(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                              fontSize: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.025),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                                onTap: () {
                                  getImage(context);
                                },
                              ),
                              SizedBox(
                                width: 30,
                              ),
                              Text(
                                mapValue['fname'].toUpperCase() +
                                    " " +
                                    mapValue['lname'].toUpperCase(),
                                style: GoogleFonts.roboto(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize:
                                        MediaQuery.of(context).size.height *
                                            0.04),
                              )
                            ],
                          ),
                        ),
                        Container(
                          height: 6,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                              gradient: LinearGradient(colors: [
                            Color(0xffa73628),
                            Color(0xfff2d367),
                          ])),
                        ),
                        Expanded(
                          child: Container(
                            color: Colors.white,
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 16.0),
                              child: Form(
                                key: _formKey,
                                child: SingleChildScrollView(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      SizedBox(
                                        height: 30,
                                      ),
                                      TextFormField(
                                        keyboardType: TextInputType.name,
                                        style: GoogleFonts.roboto(
                                            color: textFieldLabelColor),
                                        // validator: (val) {
                                        //   if (val.trim().isEmpty) {
                                        //     return 'Please enter your Name';
                                        //   } else if (!isAlpha(val)) {
                                        //     return "Enter a valid Name";
                                        //   }

                                        //   return null;
                                        // },
                                        onSaved: (val) {
                                          setState(() {
                                            fname = val;
                                          });
                                        },
                                        decoration: InputDecoration(
                                          labelText: mapValue['fname'],
                                          labelStyle: TextStyle(
                                              fontFamily: 'Shruti',
                                              fontWeight: FontWeight.w600,
                                              fontSize: 20,
                                              color: textFieldLabelColor),
                                          border: InputBorder.none,
                                        ),
                                      ),
                                      Container(
                                        height: 1,
                                        width:
                                            MediaQuery.of(context).size.width,
                                        color: textFieldLabelColor,
                                      ),
                                      TextFormField(
                                        keyboardType: TextInputType.name,
                                        style: GoogleFonts.roboto(
                                            color: textFieldLabelColor),
                                        // validator: (val) {
                                        //   if (val.trim().isEmpty) {
                                        //     return 'Please enter your Number';
                                        //   } else if (!isAlpha(val)) {
                                        //     return "Enter a valid Number";
                                        //   }

                                        //   return null;
                                        // },
                                        onSaved: (val) {
                                          setState(() {
                                            lname = val;
                                          });
                                        },
                                        decoration: InputDecoration(
                                          labelText: mapValue['lname'],
                                          labelStyle: TextStyle(
                                              fontFamily: 'Shruti',
                                              fontWeight: FontWeight.w600,
                                              fontSize: 20,
                                              color: textFieldLabelColor),
                                          border: InputBorder.none,
                                        ),
                                      ),
                                      Container(
                                        height: 1,
                                        width:
                                            MediaQuery.of(context).size.width,
                                        color: textFieldLabelColor,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            top: 8.0, bottom: 10),
                                        child: DropdownButtonFormField<String>(
                                          isDense: true,
                                          style: TextStyle(
                                              fontFamily: 'Shruti',
                                              fontWeight: FontWeight.w600,
                                              fontSize: 20,
                                              color: textFieldLabelColor),
                                          hint: Text('SELECT GENDER',
                                              style: TextStyle(
                                                  fontFamily: 'Shruti',
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 18,
                                                  color: textFieldLabelColor)),
                                          decoration: InputDecoration(
                                              enabledBorder: InputBorder.none),
                                          icon: Padding(
                                            padding: const EdgeInsets.only(
                                                right: 8.0),
                                            child: Image.asset(
                                              'assets/images/Loginsignup/DropDown-Icon-01.png',
                                              height: 15,
                                              width: 15,
                                            ),
                                          ),
                                          validator: (value) => value == null
                                              ? 'Feild Required'
                                              : null,
                                          items: <String>[
                                            'Male',
                                            'Female',
                                            'Not Specify',
                                          ].map((String value) {
                                            return new DropdownMenuItem<String>(
                                              value: value,
                                              child: new Text(value),
                                            );
                                          }).toList(),
                                          onChanged: (val) {
                                            setState(() {
                                              // gender = val;
                                            });
                                          },
                                        ),
                                      ),
                                      Container(
                                        height: 1,
                                        width:
                                            MediaQuery.of(context).size.width,
                                        color: textFieldLabelColor,
                                      ),
                                      Row(
                                        children: [
                                          Expanded(
                                            child: TextFormField(
                                              controller: dob,

                                              // autovalidateMode:
                                              // autovalidate,
                                              keyboardType: TextInputType.phone,
                                              style: TextStyle(
                                                  fontFamily: 'Arial',
                                                  color:
                                                      AppColors.textFeildcolor,
                                                  fontSize: 22),
                                              validator: (val) {
                                                if (val.trim().isEmpty) {
                                                  Global.isLoading = false;
                                                  return 'Please enter your Date of Birth';
                                                }

                                                return null;
                                              },
                                              // inputFormatters: [DateTextFormatter],
                                              decoration: InputDecoration(
                                                labelText: 'DATE OF BIRTH',
                                                hintText: '1989/12/16',
                                                hintStyle: TextStyle(
                                                    fontFamily: 'Shruti',
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 20,
                                                    color: Colors.grey[200]),
                                                labelStyle: TextStyle(
                                                    fontFamily: 'Shruti',
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 20,
                                                    color: textFieldLabelColor),
                                                border: InputBorder.none,
                                              ),
                                            ),
                                          ),
                                          GestureDetector(
                                              onTap: () {
                                                _selectDate(context);
                                              },
                                              child: Image.asset(
                                                'assets/images/Loginsignup/Calender-Icon-01.png',
                                                height: 30,
                                                width: 30,
                                                color: textFieldLabelColor,
                                              ))
                                        ],
                                      ),
                                      Container(
                                        height: 1,
                                        width:
                                            MediaQuery.of(context).size.width,
                                        color: textFieldLabelColor,
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          if (_formKey.currentState
                                              .validate()) {
                                            _formKey.currentState.save();
                                            submitdata().then(
                                              (value) => CoolAlert.show(
                                                context: context,
                                                type: CoolAlertType.success,
                                                text:
                                                    "Success ${'\n'} ${value.message}",
                                              ),
                                            );
                                          }
                                        },
                                        child: Container(
                                          height: 50,
                                          child: Stack(
                                            alignment: Alignment.center,
                                            children: [
                                              Image.asset(
                                                  'assets/images/EditProfile/buttonBG.png'),
                                              Text(
                                                "UPDATE",
                                                style: GoogleFonts.roboto(
                                                    fontSize: 30,
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.white),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                );
              } else {
                return FetchingData();
              }
            }));
  }
}
