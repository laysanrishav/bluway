import 'dart:collection';
import 'package:blu_way/Constants/ConstantList.dart';
import 'package:blu_way/Model/GetAllProductModel.dart';
import 'package:blu_way/Model/Organization/AddRequisition.dart';
import 'package:blu_way/Screens/submitOrder.dart';
import 'package:blu_way/Widget/FetchingData.dart';
import 'package:cool_alert/cool_alert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';

import 'HomePage.dart';

class CreateOrder extends StatefulWidget {
  final String orgId;
  final String token;
  final String name;
  // final List<ProductResult> productList;
  static List<HashMap<String, String>> product =
      List<HashMap<String, String>>();

  CreateOrder({
    Key key,
    @required this.orgId,
    @required this.token,
    @required this.name,
    // this.productList,
  }) : super(key: key);
  @override
  _CreateOrderState createState() => _CreateOrderState();
}

class _CreateOrderState extends State<CreateOrder> {
  // List<SupervisorResult> supervisorList = [];
  List<ProductResult> products = new List<ProductResult>();
  String number = '';
  List<ProductResult> searchedProducts = new List<ProductResult>();
  GetAllProductModel productList = new GetAllProductModel();
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  String seId = '';

  submitRequisition() {
    EasyLoading.show(status: 'loading...');
    addRequisition(CreateOrder.product, widget.orgId, widget.token)
        .then((value) {
      if (value.status == "success" &&
          value.message == "Requisition created.") {
        EasyLoading.dismiss();
        CoolAlert.show(
            context: context,
            type: CoolAlertType.success,
            text: "${value.message}",
            onConfirmBtnTap: () {
              Navigator.of(context)
                  .pushReplacement(MaterialPageRoute(builder: (context) {
                return MyHomePage(
                  title: "ORGANIZATION",
                );
              }));
            });
      }
    });
  }

  @override
  void initState() {
    // TODO: implement initState

    getProductList(widget.orgId, widget.token).then((value) {
      setState(() {
        productList = value;
        products.addAll(value.result);
        searchedProducts = products;
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      builder: EasyLoading.init(),
      home: SafeArea(
        child: Scaffold(
          // appBar: MyAppBar("Create Order"),
          body: CustomContainer(
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16.0, vertical: 0),
                child: (productList.status == "success")
                    ? Column(
                        children: [
                          Expanded(
                            child: SingleChildScrollView(
                              child: Column(
                                children: [
                                  Container(
                                      width: MediaQuery.of(context).size.width -
                                          30,
                                      height:
                                          MediaQuery.of(context).size.height,
                                      child: ListView.builder(
                                          itemCount:
                                              searchedProducts.length + 1,
                                          itemBuilder: (context, index) {
                                            return (index == 0)
                                                ? Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            8.0),
                                                    child: Expanded(
                                                      flex: 5,
                                                      child: Container(
                                                        decoration:
                                                            BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(6),
                                                          color: Colors.white,
                                                        ),
                                                        height: 50,
                                                        child: Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                  .all(8.0),
                                                          child: TextField(
                                                            keyboardType:
                                                                TextInputType
                                                                    .name,
                                                            style: GoogleFonts
                                                                .roboto(
                                                                    color: Color(
                                                                        0xff3e4368)),
                                                            decoration: InputDecoration(
                                                                border:
                                                                    InputBorder
                                                                        .none,
                                                                hintText:
                                                                    "Search Product..",
                                                                hintStyle: GoogleFonts.roboto(
                                                                    color: Color(
                                                                        0xff3e4368),
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold)),
                                                            onChanged: (text) {
                                                              text = text
                                                                  .toLowerCase();
                                                              setState(() {
                                                                searchedProducts =
                                                                    products.where(
                                                                        (localCustomers) {
                                                                  var title =
                                                                      localCustomers
                                                                          .name
                                                                          .toLowerCase();

                                                                  return title
                                                                      .contains(
                                                                          text);
                                                                }).toList();
                                                              });
                                                            },
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  )
                                                : Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            8.0),
                                                    child: ProductList(
                                                        index - 1,
                                                        searchedProducts),
                                                  );
                                          }))
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Card(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              child: GestureDetector(
                                  onTap: () {
                                    (widget.name == "CreateOrder")
                                        ? Navigator.of(context).push(
                                            MaterialPageRoute(
                                                builder: (context) {
                                            return SubmitOrder(
                                              // seResult:widget.s,
                                              token: widget.token,
                                              orgId: widget.orgId,
                                              product: CreateOrder.product,
                                            );
                                          }))
                                        : submitRequisition();
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: Colors.blue[900],
                                        borderRadius:
                                            BorderRadius.circular(10)),
                                    height: MediaQuery.of(context).size.height *
                                        0.09,
                                    child: Center(
                                      child: (widget.name == "CreateOrder")
                                          ? Text("NEXT")
                                          : Text("SUBMIT"),
                                    ),
                                  )),
                            ),
                          ),
                        ],
                      )
                    : Container(
                        child: FetchingData(),
                      ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

// class ProductList extends ConsumerWidget {
//   int index;
//   List searchedProductList;

//   ProductList(this.index, this.searchedProductList);
//   final numberStateProvider = StateProvider<int>((ref) {
//     return 0;
//   });
//   @override
//   Widget build(BuildContext context, ScopedReader watch) {
//     int qty = watch(numberStateProvider).state;

//     return Padding(
//       padding: const EdgeInsets.all(2.0),
//       child: Card(
//         color: Colors.blue[100],
//         shape: RoundedRectangleBorder(
//           borderRadius: BorderRadius.circular(8.0),
//         ),
//         child: Container(
//           decoration: BoxDecoration(
//             color: Colors.blue[100],
//           ),
//           margin: EdgeInsets.all(8),
//           width: MediaQuery.of(context).size.width * 0.80,
//           child: Padding(
//             padding: const EdgeInsets.all(4.0),
//             child: Row(
//               mainAxisAlignment: MainAxisAlignment.center,
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: [
//                 CircleAvatar(
//                   maxRadius: 40,
//                   minRadius: 40,
//                   backgroundColor: Colors.transparent,
//                   child: Image.network(
//                       '''http://139.59.75.40:4040/uploads/product/${searchedProductList[index].image}'''),
//                 ),
//                 SizedBox(
//                   width: 20,
//                 ),
//                 Expanded(
//                   child: Column(
//                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: [
//                       RichText(
//                         text: TextSpan(
//                             style: GoogleFonts.roboto(
//                                 color: Colors.black,
//                                 fontSize: 20,
//                                 fontWeight: FontWeight.bold
//                                 // fontWeight: FontWeight.bold,
//                                 ),
//                             children: [
//                               TextSpan(text: searchedProductList[index].name),
//                             ]),
//                       ),
//                       SizedBox(
//                         height: 5,
//                       ),
//                       RichText(
//                         text: TextSpan(
//                             style: GoogleFonts.roboto(
//                               color: Colors.black,
//                               fontSize: 15,
//                             ),
//                             children: [
//                               TextSpan(
//                                 text: "BRAND: ",
//                               ),
//                               TextSpan(
//                                 text: searchedProductList[index]
//                                     .brand
//                                     .name
//                                     .toString(),
//                                 style: GoogleFonts.roboto(
//                                   color: Colors.black,
//                                   fontSize: 15,
//                                   fontWeight: FontWeight.bold,
//                                 ),
//                               )
//                             ]),
//                       ),
//                       SizedBox(
//                         height: 5,
//                       ),
//                       SizedBox(
//                         height: 5,
//                       ),
//                       RichText(
//                         text: TextSpan(
//                             style: GoogleFonts.roboto(
//                               color: Colors.black,
//                               fontSize: 15,
//                             ),
//                             children: [
//                               TextSpan(
//                                 text: "PRICE: ",
//                               ),
//                               TextSpan(
//                                 text: "₹ " +
//                                     searchedProductList[index]
//                                         .sellingPrice
//                                         .toString(),
//                                 style: GoogleFonts.roboto(
//                                   color: Colors.black,
//                                   fontSize: 15,
//                                   fontWeight: FontWeight.bold,
//                                 ),
//                               )
//                             ]),
//                       ),
//                       SizedBox(
//                         height: 5,
//                       ),
//                       Row(
//                         children: [
//                           IconButton(
//                               icon: Icon(Icons.remove),
//                               onPressed: () {
//                                 context.read(numberStateProvider).state--;

//                                 Map<String, String> temp =
//                                     new Map<String, String>();
//                                 temp['qty'] = qty.toString();
//                                 temp['product'] =
//                                     searchedProductList[index].sId.toString();
//                                 CreateOrder.product.add(temp);
//                               }),
//                           Container(
//                             decoration: BoxDecoration(
//                                 color: Colors.white,
//                                 borderRadius: BorderRadius.circular(4)),
//                             child: Padding(
//                               padding: const EdgeInsets.symmetric(
//                                   horizontal: 16, vertical: 6),
//                               child: Text("$qty"),
//                             ),
//                           ),
//                           IconButton(
//                               icon: Icon(Icons.add),
//                               onPressed: () {
//                                 context.read(numberStateProvider).state++;

//                                 HashMap<String, String> temp =
//                                     new HashMap<String, String>();
//                                 temp['qty'] = '${qty + 1}'.toString();
//                                 temp['product'] =
//                                     searchedProductList[index].sId.toString();

//                                 // if (CreateOrder.product.isEmpty) {
//                                 //   CreateOrder.product.add(temp);
//                                 // } else {
//                                 //   if (!CreateOrder.product[index].containsValue(
//                                 //       searchedProductList[index]
//                                 //           .sId
//                                 //           .toString())) {
//                                 //     CreateOrder.product.add(temp);
//                                 //   }
//                                 // }
//                                 CreateOrder.product.add(temp);
//                                 // if(!CreateOrder.product[index].containsKey(searchedProductList[index].sId.toString()) )
//                               })
//                         ],
//                       )
//                     ],
//                   ),
//                 ),
//               ],
//             ),
//           ),
//         ),
//       ),
//     );
//   }

// void increment(BuildContext context) {
//   context.read(numberStateProvider).state;
// }
// }
// BottomSheet bottomSheet = B

class ProductList extends StatefulWidget {
  int index;
  List searchedProductList;

  ProductList(this.index, this.searchedProductList);
  @override
  _ProductListState createState() => _ProductListState();
}

class _ProductListState extends State<ProductList> {
  getMapValue() {
    Map<String, String> temp = new Map<String, String>();
    temp['qty'] = qty.toString();
    temp['product'] = widget.searchedProductList[widget.index].sId.toString();

    CreateOrder.product.add(temp);
  }

  int qty = 0;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(2.0),
      child: Card(
        color: Colors.blue[100],
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.blue[100],
          ),
          margin: EdgeInsets.all(8),
          width: MediaQuery.of(context).size.width * 0.80,
          child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CircleAvatar(
                  maxRadius: 40,
                  minRadius: 40,
                  backgroundColor: Colors.transparent,
                  child: Image.network(
                      '''http://139.59.75.40:4040/uploads/product/${widget.searchedProductList[widget.index].image}'''),
                ),
                SizedBox(
                  width: 20,
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      RichText(
                        text: TextSpan(
                            style: GoogleFonts.roboto(
                                color: Colors.black,
                                fontSize: 20,
                                fontWeight: FontWeight.bold
                                // fontWeight: FontWeight.bold,
                                ),
                            children: [
                              TextSpan(
                                  text: widget
                                      .searchedProductList[widget.index].name),
                            ]),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      RichText(
                        text: TextSpan(
                            style: GoogleFonts.roboto(
                              color: Colors.black,
                              fontSize: 15,
                            ),
                            children: [
                              TextSpan(
                                text: "BRAND: ",
                              ),
                              TextSpan(
                                text: widget.searchedProductList[widget.index]
                                    .brand.name
                                    .toString(),
                                style: GoogleFonts.roboto(
                                  color: Colors.black,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                ),
                              )
                            ]),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      RichText(
                        text: TextSpan(
                            style: GoogleFonts.roboto(
                              color: Colors.black,
                              fontSize: 15,
                            ),
                            children: [
                              TextSpan(
                                text: "PRICE: ",
                              ),
                              TextSpan(
                                text: "₹ " +
                                    widget.searchedProductList[widget.index]
                                        .sellingPrice
                                        .toString(),
                                style: GoogleFonts.roboto(
                                  color: Colors.black,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                ),
                              )
                            ]),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        children: [
                          IconButton(
                              icon: Icon(Icons.remove),
                              onPressed: () {
                                setState(() {
                                  qty = qty - 1;
                                });
                              }),
                          Container(
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(4)),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 16, vertical: 6),
                              child: Text("$qty"),
                            ),
                          ),
                          IconButton(
                              icon: Icon(Icons.add),
                              onPressed: () {
                                // context.read(numberStateProvider).state++;
                                setState(() {
                                  qty = qty + 1;
                                });
                                HashMap<String, String> temp =
                                    new HashMap<String, String>();
                                temp['qty'] = qty.toString();
                                temp['product'] = widget
                                    .searchedProductList[widget.index].sId
                                    .toString();

                                // if (CreateOrder.product.isEmpty) {
                                //   CreateOrder.product.add(temp);
                                // } else {
                                //   if (!CreateOrder.product[index].containsValue(
                                //       searchedProductList[index]
                                //           .sId
                                //           .toString())) {
                                //     CreateOrder.product.add(temp);
                                //   }
                                // }
                                CreateOrder.product.add(temp);
                                // if(!CreateOrder.product[index].containsKey(searchedProductList[index].sId.toString()) )
                              })
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
