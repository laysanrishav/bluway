import 'package:blu_way/Model/CustomerList.dart';
import 'package:blu_way/Model/GetSEListResponse.dart';
import 'package:blu_way/Screens/AddCustomer.dart';
import 'package:blu_way/Screens/submitOrder.dart';
import 'package:blu_way/Widget/FetchingData.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class SearchCustomer extends SearchDelegate {
  final List<Map<String, String>> product;
  final String orgId;
  final List<SEResult> seResult;
  final String token;
  SearchCustomer(
      {this.product, this.seResult, this.token, @required this.orgId})
      : super(
          searchFieldLabel: "Enter Mobile Number of Customer",
          // keyboardType: TextInputType.number
        );
  List<Widget> buildActions(BuildContext context) {
    return [
      GestureDetector(
        onTap: () {
          Future.delayed(Duration.zero, () {
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return AddCustomer();
            }));
          });
        },
        child: Container(
            color: Color(0xff7093bd),
            child: Row(
              children: [
                IconButton(
                  icon: Icon(Icons.add, color: Colors.white),
                  onPressed: () {
                    // Navigator.of(context)
                    //     .push(MaterialPageRoute(builder: (context) {
                    //   return AddCustomer();
                    // }));
                  },
                ),
                Text(
                  "Add",
                  style: GoogleFonts.roboto(color: Colors.white),
                )
              ],
            )),
      ),
      // Container(
      //   color: Color(0xff7093bd),
      //   child: Center(child: Text("Add")),
      // )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
        icon: Icon(Icons.arrow_back),
        onPressed: () {
          close(context, null);
        });
  }

  @override
  Widget buildResults(BuildContext context) {
    if (query.length < 3) {
      return Center(
          child: Container(
        child: Text("Query length should be more than 3"),
      ));
    } else {
      return FutureBuilder(
          future: getCustomerList("6002957ac2f631188c610c60"),
          builder: (context, snapshot) {
            int count = 0;
            if (snapshot.hasData) {
              CustomerList customerList = snapshot.data;
              for (int i = 0; i < customerList.result.length; i++) {
                if (customerList.result[i].mobile
                    .toLowerCase()
                    .contains('$query')) {
                  count++;
                }
              }

              return ListView.builder(
                  itemCount: count,
                  itemBuilder: (context, index) {
                    if ((customerList.result[index].mobile
                        .toLowerCase()
                        .contains('$query'))) {
                      return GestureDetector(
                        onTap: () {
                          SubmitOrder(
                              product: product,
                              seResult: seResult,
                              orgId: orgId,
                              token: token);
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Card(
                            child: Container(
                              padding: EdgeInsets.only(left: 10),
                              decoration: BoxDecoration(
                                color: Color(0xffCBECFF),
                                borderRadius: BorderRadius.circular(10),
                              ),
                              height: 100,
                              child: Row(
                                children: [
                                  CircleAvatar(
                                    maxRadius: 40,
                                    backgroundColor: Color(0xff6e90bc),
                                    child: Container(
                                      width: 60,
                                      height: 60,
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 16.0, top: 8),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Text(
                                          customerList.result[index].fullName,
                                          style: GoogleFonts.roboto(
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20,
                                          ),
                                        ),
                                        RichText(
                                            text: TextSpan(children: [
                                          TextSpan(
                                            text: "Mobile: ",
                                            style: GoogleFonts.roboto(
                                              color: Colors.black,
                                              fontSize: 15,
                                            ),
                                          ),
                                          TextSpan(
                                            text: customerList
                                                .result[index].mobile,
                                            style: GoogleFonts.roboto(
                                              color: Colors.black,
                                              fontSize: 15,
                                            ),
                                          )
                                        ])),
                                        RichText(
                                            text: TextSpan(children: [
                                          TextSpan(
                                            text: "Address: ",
                                            style: GoogleFonts.roboto(
                                              color: Colors.black,
                                              fontSize: 15,
                                            ),
                                          ),
                                          TextSpan(
                                            text: customerList
                                                .result[index].address,
                                            style: GoogleFonts.roboto(
                                              color: Colors.black,
                                              fontSize: 15,
                                            ),
                                          ),
                                        ])),
                                        RichText(
                                            text: TextSpan(children: [
                                          TextSpan(
                                            text: "PIN: ",
                                            style: GoogleFonts.roboto(
                                              color: Colors.black,
                                              fontSize: 15,
                                            ),
                                          ),
                                          TextSpan(
                                            text: customerList
                                                .result[index].pincode,
                                            style: GoogleFonts.roboto(
                                              color: Colors.black,
                                              fontSize: 15,
                                            ),
                                          )
                                        ]))
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      );
                    } else {
                      return Container(
                        child: Text("There is no such Please Add"),
                      );
                    }
                  });
            } else {
              FetchingData();
            }
          });
    }
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return Container(
      child: Text("$query"),
    );
  }
}
