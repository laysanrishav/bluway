import 'package:flutter/material.dart';

class NewPage extends StatelessWidget {
  String query;
  NewPage({this.query});
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(
        query,
        style: TextStyle(fontSize: 50),
      ),
    );
  }
}
