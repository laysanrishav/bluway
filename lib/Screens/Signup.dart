import 'dart:ui';

import 'package:blu_way/utlis/Size_config.dart';
import 'package:blu_way/utlis/global.dart';
import 'package:blu_way/utlis/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class Signup extends StatefulWidget {
  Signup({Key key, this.globalOtp}) : super(key: key);
  int globalOtp;
  // Registration registration;

  @override
  _SignupState createState() => _SignupState();
}

class _SignupState extends State<Signup> {
  // String Otp_value = '';
  final _formKey = GlobalKey<FormState>();
  TextEditingController mobileNo = TextEditingController();
  TextEditingController otp = TextEditingController();
  bool autoValidate = false;
  AutovalidateMode autovalidate = AutovalidateMode.disabled;
  // AuthProvider authProvider = AuthProvider();

  void _showDialog() {
    // flutter defined function

    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Thanks"),
          content: new Text(
              """Your registration for Blu Way is completed. You can "LOGIN" once your profile get activated"""),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Ok"),
              onPressed: () {
                // Navigator.of(context).pop();

                Navigator.of(context).pushReplacementNamed('login');
              },
            ),
          ],
        );
      },
    );
  }

  // getDataFromSharedPrefs() async {
  //   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  //
  //   setState(() {
  //     Otp_value = sharedPreferences.getString("Register_otp");
  //   });
  //   // print("OTP of Registration is: "+ Otp_value);
  // }

  // getOtp()
  // {
  //   Otp_value
  // }

  @override
  void initState() {
    // getDataFromSharedPrefs();
    super.initState();
    mobileNo.text = Global.signupno;
    // Otp_value = Global.otp;
    print("Global otp is: " + widget.globalOtp.toString());
    // print("Global otp is: " + Otp_value);
  }

  void loginSubmitted() {
    if (_formKey.currentState.validate()) {
      setState(() => Global.isLoading = true);
      _showDialog();
    } else {}
  }

  @override
  Widget build(BuildContext context) {
    // print("Global otp is: " + Otp_value.toString());
    SizeConfig().init(context);
    return ModalProgressHUD(
        inAsyncCall: Global.isLoading,
        color: AppColors.buttonBg,
        progressIndicator: CircularProgressIndicator(),
        child: GestureDetector(
            onTap: () => FocusScope.of(context).unfocus(),
            child: SafeArea(
                child: WillPopScope(
              onWillPop: () async => false,
              child: Scaffold(
                  resizeToAvoidBottomInset: true,
                  body: Form(
                    key: _formKey,
                    child: Container(
                        width: double.infinity,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              image: ExactAssetImage(
                                  'assets/images/login/BG-1.png'),
                              fit: BoxFit.fill),
                        ),
                        height: SizeConfig.screenHeight,
                        child: LayoutBuilder(builder: (BuildContext context,
                            BoxConstraints viewportConstraints) {
                          return SingleChildScrollView(
                            child: ConstrainedBox(
                              constraints: BoxConstraints(
                                minHeight: viewportConstraints.maxHeight,
                              ),
                              child: Container(
                                child: Stack(
                                  children: [
                                    Container(
                                        child: Image.asset(
                                            'assets/images/login/BG-2.png',
                                            width: SizeConfig.screenWidth)),
                                    Positioned(
                                      right: 20,
                                      top: 0,
                                      child: Padding(
                                        padding: EdgeInsets.only(
                                            top: SizeConfig.safeBlockVertical *
                                                1,
                                            left: 10),
                                        child: Container(
                                            child: Image.asset(
                                          'assets/images/Loginsignup/Blu-logo.png',
                                          height: 140,
                                          width: 140,
                                        )),
                                      ),
                                    ),
                                    Container(
                                        //height: SizeConfig.safeBlockVertical*76,
                                        child: Padding(
                                      padding: EdgeInsets.only(
                                          left: 12.0,
                                          right: 12.0,
                                          top: SizeConfig.safeBlockVertical *
                                              22),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                right: 8.0),
                                            child: Text(
                                              'REGISTRATION',
                                              style: TextStyle(
                                                fontSize: 30,
                                                fontFamily: 'Shruti',
                                                color: Color(0xffaedcff),
                                                shadows: <Shadow>[
                                                  Shadow(
                                                    offset: Offset(1.0, 1.0),
                                                    blurRadius: 3.0,
                                                    color: Color.fromARGB(
                                                        255, 0, 0, 0),
                                                  ),
                                                  Shadow(
                                                    offset: Offset(1.0, 1.0),
                                                    blurRadius: 8.0,
                                                    color: Color.fromARGB(
                                                        125, 0, 0, 255),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          TextFormField(
                                            controller: mobileNo,
                                            readOnly: true,
                                            autovalidateMode: autovalidate,
                                            keyboardType: TextInputType.phone,
                                            style: TextStyle(
                                                fontFamily: 'Arial',
                                                color: AppColors.whiteColor,
                                                fontSize: 22),
                                            validator: (val) {
                                              if (val.trim().isEmpty) {
                                                return 'Please enter Mobile no';
                                              } else if (val.length > 10 ||
                                                  val.length < 10) {
                                                return 'Please enter valid mobile No';
                                              }
                                              return null;
                                            },
                                            decoration: InputDecoration(
                                              labelText: 'MOBILE NUMBER',
                                              labelStyle: TextStyle(
                                                  fontFamily: 'Shruti',
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 20,
                                                  color:
                                                      AppColors.textFeildcolor),
                                              border: InputBorder.none,
                                            ),
                                          ),
                                          Container(
                                              child: Image.asset(
                                            'assets/images/Loginsignup/Line-1.png',
                                          )),
                                          Padding(
                                            padding:
                                                const EdgeInsets.only(top: 8.0),
                                            child: TextFormField(
                                              controller: otp,
                                              keyboardType: TextInputType.phone,
                                              style: TextStyle(
                                                  fontFamily: 'Arial',
                                                  color: AppColors.whiteColor,
                                                  fontSize: 22),
                                              validator: (val) {
                                                if (val.trim().isEmpty) {
                                                  return 'Please enter Otp ';
                                                } else if (val !=
                                                    widget.globalOtp
                                                        .toString()) {
                                                  return "Otp doesn't match";
                                                }
                                                return null;
                                              },
                                              decoration: InputDecoration(
                                                labelText: 'OTP',
                                                labelStyle: TextStyle(
                                                    fontFamily: 'Shruti',
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 20,
                                                    color: AppColors
                                                        .textFeildcolor),
                                                border: InputBorder.none,
                                              ),
                                            ),
                                          ),
                                          Container(
                                              child: Image.asset(
                                            'assets/images/Loginsignup/Line-1.png',
                                          )),
                                          Center(
                                            child: Padding(
                                              padding: EdgeInsets.only(
                                                  top: SizeConfig
                                                          .safeBlockVertical *
                                                      3,
                                                  bottom: 10),
                                              child: GestureDetector(
                                                onTap: loginSubmitted,
                                                child: Container(
                                                  width: 230,
                                                  decoration: BoxDecoration(
                                                      image: DecorationImage(
                                                          image: ExactAssetImage(
                                                              'assets/images/Loginsignup/BT-1.png'),
                                                          fit: BoxFit.fill)),
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            15.0),
                                                    child: Center(
                                                      child: Text(
                                                        'Register',
                                                        style: TextStyle(
                                                            fontFamily: 'Arial',
                                                            fontWeight:
                                                                FontWeight.w600,
                                                            color: AppColors
                                                                .whiteColor,
                                                            fontSize: 18),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    )),
                                  ],
                                ),
                              ),
                            ),
                          );
                        })),
                  )),
            ))));
  }
}
