// import 'dart:collection';
// import 'dart:io';
// import 'dart:io' as Io;
// import 'dart:convert';

// import 'package:flutter/material.dart';
// import 'package:google_fonts/google_fonts.dart';
// import 'package:image_picker/image_picker.dart';
// import 'package:navigationdrawer/Constants/ConstantList.dart';
// import 'package:navigationdrawer/Constants/Style.dart';
// import 'package:navigationdrawer/Functions/network/api_call.dart';
// import 'package:navigationdrawer/Model/districtListResponse.dart';
// import 'package:navigationdrawer/Model/stateListResponse.dart';

// import 'package:navigationdrawer/Widget/custom_view.dart';
// import 'package:navigationdrawer/utlis/values/styles.dart';

// import 'ActiveSurveyForm.dart';

// class ActiveFormStatic extends StatefulWidget {
//   const ActiveFormStatic(
//       {Key key,
//       this.surveyForm,
//       this.products,
//       this.surveyName,
//       this.surveyId,
//       this.stateListResponse})
//       : super(key: key);

//   @override
//   _ActiveFormStaticState createState() => _ActiveFormStaticState();
//   final List surveyForm, products;
//   final String surveyName;
//   final String surveyId;
//   final StateListResponse stateListResponse;
// }

// class _ActiveFormStaticState extends State<ActiveFormStatic> {
//   List<String> genders = ['Male', 'Female', 'Rather not say'];
//   List<String> countryList = ['India'];
//   String country;
//   String image;
//   String fullName;
//   String address;
//   String village;
//   String postOffice;
//   String thana;
//   String state;
//   String district;
//   String pincode;
//   String mobileNumber;
//   // List<String> identityList = <String>[
//   //   'VOTER ID',
//   //   'PASSPORT',
//   //   'BANK ACCOUNT NO.',
//   //   'RATION CARD',
//   //   'AADHAAR CARD',
//   //   'DRIVING LICENSE',
//   //   'PAN CARD',
//   // ];
//   StateListResponse _stateListResponse;
//   DistrictListResponse _districtListResponse;
//   List<String> stateIds;
//   String selectedGender;
//   String selectedState;
//   String selectedStateId;
//   String selectedDistrict;
//   HashMap<String, String> stateId;
//   bool stateChange = true;
//   final _formKey = GlobalKey<FormState>();

//   File _image;
//   String img64;
//   DateTime selectedDate = DateTime.now();
//   final GlobalKey<State> _keyLoader = new GlobalKey<State>();
//   _selectDate(BuildContext context) async {
//     final DateTime picked = await showDatePicker(
//       context: context,
//       initialDate: selectedDate,
//       firstDate: DateTime(1900),
//       lastDate: DateTime.now(),
//     );
//     if (picked != null && picked != selectedDate)
//       setState(() {
//         selectedDate = picked;
//       });
//   }

//   @override
//   void initState() {
//     stateId = new HashMap<String, String>();
//     setState(() {
//       for (dynamic e in widget.stateListResponse.hubResult) {
//         print('In For Loop');
//         stateId.putIfAbsent(e.name, () => e.id);
//       }
//     });
//     // ApiCall.postState().then((value) => setState(() {
//     //       _stateListResponse = value;
//     //     }));
//     super.initState();
//   }

//   Future getImage(BuildContext context) async {
//     showDialog(
//         context: context,
//         builder: (context) {
//           return AlertDialog(
//               content: SizedBox(
//             height: 150,
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.center,
//               mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//               children: <Widget>[
//                 FlatButton(
//                   child: Text(
//                     "Camera",
//                     style: TextStyle(fontSize: 26),
//                   ),
//                   onPressed: () async {
//                     var image = await ImagePicker().getImage(
//                       source: ImageSource.camera,
//                     );
//                     // var image = await ImagePicker()
//                     //     .getImage(source: ImageSource.camera);
//                     setState(() {
//                       // _image = image;
//                       _image = File(image.path);
//                       print(_image);
//                       List<int> bytes = Io.File(_image.path).readAsBytesSync();
//                       img64 = base64UrlEncode(bytes);
//                       // var base64String =
//                       //     base64UrlEncode(_image.readAsBytesSync());
//                       // base64String = base64Encode(_image.readAsBytesSync());
//                       print('document $img64');
//                       // print('base64String $base64String');
//                       Navigator.pop(context);
//                     });
//                   },
//                 ),
//                 FlatButton(
//                   child: Text(
//                     "Gallery",
//                     style: TextStyle(fontSize: 26),
//                   ),
//                   onPressed: () async {
//                     var image = await ImagePicker().getImage(
//                       source: ImageSource.gallery,
//                       imageQuality: 50,
//                     );
//                     // var image = await ImagePicker()
//                     //     .getImage(source: ImageSource.gallery);
//                     setState(() {
//                       _image = File(image.path);
//                       // print(_image.uri.toString());
//                       List<int> bytes = Io.File(_image.path).readAsBytesSync();
//                       img64 = base64UrlEncode(bytes);
//                       print('img64 : $img64');
//                       print('bytes $bytes');
//                       // print('base64Image : $img64');
//                       // base64String = base64Encode(_image.readAsBytesSync());
//                       // print('document $img64');
//                       // print('base64String $base64String');
//                       print('image1 : $_image');
//                       Navigator.pop(context);
//                     });
//                   },
//                 ),
//                 _image != null
//                     ? FlatButton(
//                         child: Text(
//                           "Remove Profile",
//                           style: TextStyle(fontSize: 26),
//                         ),
//                         onPressed: () async {
//                           setState(() {
//                             _image = null;
//                             img64 = null;
//                             Navigator.pop(context);
//                           });
//                         },
//                       )
//                     : Text(""),
//               ],
//             ),
//           ));
//         });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         flexibleSpace: Container(
//           decoration: BoxDecoration(gradient: kAppBarGradient),
//           child: SafeArea(
//             child: Row(
//               mainAxisAlignment: MainAxisAlignment.spaceBetween,
//               children: [
//                 Padding(
//                   padding: const EdgeInsets.all(8.0),
//                   child: Stack(
//                     alignment: Alignment.center,
//                     children: [
//                       Image.asset('assets/images/Icon-Holder.png'),
//                       // Icon(Icons.chevron_left_sharp, color: Colors.white,),
//                     ],
//                   ),
//                 ),
//                 Padding(
//                   padding: const EdgeInsets.symmetric(vertical: 3.0),
//                   child: Image.asset('assets/images/Header-Logo.png'),
//                 ),
//                 SizedBox(
//                   width: 20,
//                 ),
//                 Center(
//                   child: Text(
//                     "Survey Form",
//                     style: GoogleFonts.roboto(
//                         color: Color(0xff031e48),
//                         fontSize: 20,
//                         fontWeight: FontWeight.bold
//                         // fontFamily: 'Roboto'
//                         ),
//                   ),
//                 ),
//                 SizedBox(
//                   width: 60,
//                 ),
//                 Padding(
//                   padding: const EdgeInsets.all(8.0),
//                   child: Stack(
//                     alignment: Alignment.center,
//                     children: [
//                       Image.asset('assets/images/Icon-Holder.png'),
//                       Icon(
//                         Icons.close,
//                         color: Colors.white,
//                       ),
//                     ],
//                   ),
//                 ),
//               ],
//             ),
//           ),
//         ),
//       ),
//       body: CustomContainer(
//         child: Padding(
//           padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 15),
//           child: Container(
//             decoration: BoxDecoration(
//                 borderRadius: BorderRadius.circular(10), color: Colors.white),
//             child: Form(
//               key: _formKey,
//               onChanged: () {
//                 Form.of(primaryFocus.context).save();
//                 print('FULLNAME : ' + fullName);
//                 print('ADDRESS : ' + address);
//                 print('VILLAGE : ' + village);
//                 print('POST OFFICE : ' + postOffice);
//                 print('THANA : ' + thana);
//                 print('PINCODE : ' + pincode);
//                 print('MOBILE : ' + mobileNumber);
//               },
//               child: Padding(
//                 padding: const EdgeInsets.symmetric(horizontal: 16.0),
//                 child: Column(
//                   children: [
//                     Expanded(
//                       child: ListView(
//                         // crossAxisAlignment: CrossAxisAlignment.start,
//                         children: [
//                           InputRegistration(
//                               labelText: "FULL NAME",
//                               onSaved: (value) {
//                                 fullName = value;
//                                 // setState(() {
//                                 // });
//                               }),
//                           SizedBox(height: 30),
//                           Text(
//                             "COUNTRY",
//                             style: GoogleFonts.actor(
//                               color: Color(0xff819ffd),
//                               fontSize: 20,
//                               fontWeight: FontWeight.bold,
//                               // fontWeight: FontWeight.bold,
//                             ),
//                           ),
//                           SizedBox(height: 10),
//                           Container(
//                             decoration: BoxDecoration(
//                                 border: Border.all(color: Colors.black)),
//                             width: MediaQuery.of(context).size.width * 0.85,
//                             child: Padding(
//                               padding:
//                                   const EdgeInsets.symmetric(horizontal: 16.0),
//                               child: DropdownButton<String>(
//                                 items: countryList.map((value) {
//                                   return DropdownMenuItem<String>(
//                                     value: value,
//                                     child: Text(
//                                       value,
//                                       style: GoogleFonts.actor(
//                                         color: Colors.blue,
//                                         fontSize: 20,
//                                         // fontWeight: FontWeight.bold,
//                                         // fontWeight: FontWeight.bold,
//                                       ),
//                                     ),
//                                   );
//                                 }).toList(),
//                                 onChanged: (value) {
//                                   setState(() {
//                                     country = value;
//                                   });
//                                 },
//                                 value: country,
//                                 hint: Text(
//                                   "Select County",
//                                   style: GoogleFonts.actor(
//                                     fontSize: 20,
//                                     fontWeight: FontWeight.bold,
//                                   ),
//                                 ),
//                               ),
//                             ),
//                           ),
//                           SizedBox(height: 30),
//                           InputRegistration(
//                               labelText: "ADDRESS",
//                               onSaved: (value) {
//                                 address = value;
//                                 // setState(() {
//                                 // });
//                               }),
//                           SizedBox(height: 30),
//                           InputRegistration(
//                               labelText: "VILLAGE",
//                               onSaved: (value) {
//                                 village = value;
//                                 // setState(() {
//                                 // });
//                               }),
//                           SizedBox(height: 30),
//                           InputRegistration(
//                               labelText: "POST OFFICE",
//                               onSaved: (value) {
//                                 postOffice = value;
//                                 // setState(() {
//                                 // });
//                               }),
//                           SizedBox(height: 30),
//                           InputRegistration(
//                               labelText: "THANA",
//                               onSaved: (value) {
//                                 thana = value;
//                                 // setState(() {
//                                 // });
//                               }),
//                           SizedBox(height: 30),
//                           InputRegistration(
//                               labelText: "PINCODE",
//                               textInputType: TextInputType.number,
//                               onSaved: (value) {
//                                 pincode = value;
//                                 // setState(() {
//                                 // });
//                               }),
//                           SizedBox(height: 30),
//                           Text(
//                             "STATE",
//                             style: GoogleFonts.actor(
//                               color: Color(0xff819ffd),
//                               fontSize: 20,
//                               fontWeight: FontWeight.bold,
//                               // fontWeight: FontWeight.bold,
//                             ),
//                           ),
//                           SizedBox(height: 10),
//                           Container(
//                             decoration: BoxDecoration(
//                                 border: Border.all(color: Colors.black)),
//                             width: MediaQuery.of(context).size.width * 0.85,
//                             child: DropdownButton<String>(
//                               items: widget.stateListResponse.hubResult
//                                   .map((value) {
//                                 return DropdownMenuItem<String>(
//                                   value: value.name,
//                                   child: Text(
//                                     value.name,
//                                     style: GoogleFonts.actor(
//                                       color: Colors.blue,
//                                       fontSize: 10,
//                                       fontWeight: FontWeight.bold,
//                                       // fontWeight: FontWeight.bold,
//                                     ),
//                                   ),
//                                 );
//                               }).toList(),
//                               onChanged: (value) {
//                                 setState(() {
//                                   state = value;
//                                   this.selectedStateId = stateId[state];
//                                   this._districtListResponse = null;
//                                   stateChange = true;
//                                   this.selectedDistrict = null;
//                                   // print('State: ' + selectedState);
//                                   // print('StateId: ' + selectedStateId);
//                                 });
//                               },
//                               value: state,
//                               hint: Text(
//                                 "Select State",
//                                 style: GoogleFonts.actor(
//                                   fontSize: 10,
//                                   fontWeight: FontWeight.bold,
//                                 ),
//                               ),
//                             ),
//                           ),
//                           SizedBox(height: 30),
//                           Text(
//                             "DISTRICT",
//                             style: GoogleFonts.actor(
//                               color: Color(0xff819ffd),
//                               fontSize: 20,
//                               fontWeight: FontWeight.bold,
//                               // fontWeight: FontWeight.bold,
//                             ),
//                           ),
//                           SizedBox(height: 10),
//                           Container(
//                             decoration: BoxDecoration(
//                                 border: Border.all(color: Colors.black)),
//                             width: MediaQuery.of(context).size.width * 0.85,
//                             child: Padding(
//                               padding:
//                                   const EdgeInsets.symmetric(horizontal: 16.0),
//                               child: _districtListResponse == null ||
//                                       stateChange
//                                   ? InkWell(
//                                       onTap: () {
//                                         showDialog(
//                                           barrierDismissible: false,
//                                           context: context,
//                                           builder: (context) {
//                                             ApiCall.postDistrict(
//                                                     selectedStateId)
//                                                 .then((value) => setState(() {
//                                                       _districtListResponse =
//                                                           value;
//                                                       stateChange = false;
//                                                       Navigator.pop(context);
//                                                     }));
//                                             return Align(
//                                               alignment: Alignment.center,
//                                               child: Container(
//                                                 height: 40,
//                                                 width: 40,
//                                                 child:
//                                                     CircularProgressIndicator(),
//                                               ),
//                                             );
//                                           },
//                                         );
//                                       },
//                                       child: Text(
//                                         'Select District',
//                                         style: GoogleFonts.actor(
//                                           color: Colors.blue,
//                                           fontSize: 20,
//                                           // fontWeight: FontWeight.bold,
//                                           // fontWeight: FontWeight.bold,
//                                         ),
//                                       ),
//                                     )
//                                   : _districtListResponse.hubResult.length == 0
//                                       ? DropdownButton<String>(
//                                           items: ['No District'].map((value) {
//                                             return DropdownMenuItem<String>(
//                                               value: value,
//                                               child: Text(
//                                                 value,
//                                                 style: GoogleFonts.actor(
//                                                   color: Colors.blue,
//                                                   fontSize: 20,
//                                                   // fontWeight: FontWeight.bold,
//                                                   // fontWeight: FontWeight.bold,
//                                                 ),
//                                               ),
//                                             );
//                                           }).toList(),
//                                           onChanged: (value) {
//                                             setState(() {
//                                               selectedDistrict = value;
//                                             });
//                                           },
//                                           value: selectedDistrict,
//                                           hint: Text(
//                                             "Select District",
//                                             style: GoogleFonts.actor(
//                                                 fontSize: 20, color: Colors.blue
//                                                 // fontWeight: FontWeight.bold,
//                                                 ),
//                                           ),
//                                         )
//                                       : DropdownButton<String>(
//                                           items: _districtListResponse.hubResult
//                                               .map((value) {
//                                             return DropdownMenuItem<String>(
//                                               value: value.name,
//                                               child: Text(
//                                                 value.name,
//                                                 style: GoogleFonts.actor(
//                                                   color: Colors.blue,
//                                                   fontSize: 20,
//                                                   // fontWeight: FontWeight.bold,
//                                                   // fontWeight: FontWeight.bold,
//                                                 ),
//                                               ),
//                                             );
//                                           }).toList(),
//                                           onChanged: (value) {
//                                             setState(() {
//                                               this.selectedDistrict = value;
//                                             });
//                                           },
//                                           value: selectedDistrict,
//                                           hint: Text(
//                                             "Select District",
//                                             style: GoogleFonts.actor(
//                                               fontSize: 20,
//                                               fontWeight: FontWeight.bold,
//                                             ),
//                                           ),
//                                         ),
//                             ),
//                           ),
//                           SizedBox(height: 30),
//                           InputRegistration(
//                               labelText: "MOBILE",
//                               textInputType: TextInputType.phone,
//                               onSaved: (value) {
//                                 mobileNumber = value;
//                                 // setState(() {
//                                 // });
//                               }),
//                           SizedBox(height: 30),
//                           Padding(
//                             padding: const EdgeInsets.symmetric(horizontal: 40),
//                             child: GestureDetector(
//                               onTap: () async {
//                                 if (_formKey.currentState.validate()) {
//                                   _formKey.currentState.save();
//                                   print('PRODUCST : ' +
//                                       widget.products.toString());
//                                   print('surveyForm : ' +
//                                       widget.surveyForm.toString());
//                                   print('surveyName : ' + widget.surveyName);
//                                   print('surveyId : ' + widget.surveyId);
//                                   print('country : ' + country);
//                                   print('district : ' + selectedDistrict);
//                                   print('state : ' + state);
//                                   Navigator.push(context,
//                                       MaterialPageRoute(builder: (context) {
//                                     return ActiveSurveyForm(
//                                       products: widget.products,
//                                       surveyForm: widget.surveyForm,
//                                       surveyName: widget.surveyName,
//                                       surveyId: widget.surveyId,
//                                       stateListResponse:
//                                           widget.stateListResponse,
//                                       address: address,
//                                       country: country,
//                                       district: selectedDistrict,
//                                       fullName: fullName,
//                                       mobileNumber: mobileNumber,
//                                       pincode: pincode,
//                                       postOffice: postOffice,
//                                       state: state,
//                                       thana: thana,
//                                       village: village,
//                                     );
//                                   }));
//                                 }
//                               },
//                               child: Container(
//                                 // margin: EdgeInsets.all(value),
//                                 width: MediaQuery.of(context).size.width * 0.70,
//                                 child: Padding(
//                                   padding: const EdgeInsets.symmetric(
//                                       horizontal: 24.0, vertical: 8),
//                                   child: Center(
//                                     child: Text(
//                                       "SUBMIT",
//                                       style: GoogleFonts.roboto(
//                                         color: Colors.white,
//                                         fontSize: 30,
//                                         fontWeight: FontWeight.bold,
//                                       ),
//                                     ),
//                                   ),
//                                 ),
//                                 decoration: BoxDecoration(
//                                   image: DecorationImage(
//                                       fit: BoxFit.fill,
//                                       image: AssetImage(
//                                           'assets/images/ButtonBG.png')),
//                                 ),
//                               ),
//                             ),
//                           ),
//                           SizedBox(
//                             height: 20,
//                           ),
//                         ],
//                       ),
//                     ),
//                   ],
//                 ),
//               ),
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }

// class InputRegistration extends StatefulWidget {
//   String labelText;
//   final onSaved;
//   TextInputType textInputType;
//   InputRegistration(
//       {@required this.labelText, @required this.onSaved, this.textInputType});
//   @override
//   _InputRegistrationState createState() => _InputRegistrationState();
// }

// class _InputRegistrationState extends State<InputRegistration> {
//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10),
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           Container(
//             child: Text(
//               widget.labelText,
//               style: GoogleFonts.actor(
//                 fontSize: 25,
//                 color: Colors.blue,
//               ),
//             ),
//           ),
//           Container(
//             width: MediaQuery.of(context).size.width * 0.90,
//             margin: const EdgeInsets.only(top: 20, bottom: 20),
//             decoration: kSurveyFormContainerDecoration,
//             child: Padding(
//               padding: const EdgeInsets.all(8.0),
//               child: new TextFormField(
//                 onSaved: widget.onSaved,
//                 keyboardType: widget.textInputType == null
//                     ? TextInputType.text
//                     : widget.textInputType,
//                 decoration: InputDecoration(
//                   border: new UnderlineInputBorder(
//                     borderSide: new BorderSide(color: Color(0xff3469FA)),
//                   ),
//                   // labelText: labeltext,
//                   labelStyle: GoogleFonts.roboto(
//                       color: Color(0xff819ffd),
//                       fontSize: 20,
//                       fontWeight: FontWeight.bold),
//                 ),
//               ),
//             ),
//             // width: MediaQuery.of(context).size.width * 0.75,
//           ),
//         ],
//       ),
//     );
//   }
// }

// class StaticPage extends StatefulWidget {
//   final List surveyForm, products;
//   final String surveyName;
//   final String surveyId;
//   final StateListResponse stateListResponse;

//   const StaticPage(
//       {Key key,
//       this.surveyForm,
//       this.products,
//       this.surveyName,
//       this.surveyId,
//       this.stateListResponse})
//       : super(key: key);

//   @override
//   _StaticPageState createState() => _StaticPageState();
// }

// class _StaticPageState extends State<StaticPage> {
//   List<String> genders = ['Male', 'Female', 'Rather not say'];
//   List<String> countryList = ['India'];
//   String country;
//   String image;
//   String fullName;
//   String address;
//   String village;
//   String postOffice;
//   String thana;
//   String state;
//   String district;
//   String pincode;
//   String mobileNumber;

//   DistrictListResponse districtListResponse;
//   List<String> stateIds;
//   String selectedGender;
//   String selectedState;
//   String selectedStateId;
//   String selectedDistrict;
//   HashMap<String, String> stateId;
//   bool stateChange = true;
//   final _formKey = GlobalKey<FormState>();

//   final TextEditingController fullNameController = TextEditingController();
//   final TextEditingController villageController = TextEditingController();
//   final TextEditingController thanaController = TextEditingController();
//   final TextEditingController mobileController = TextEditingController();
//   final TextEditingController addressController = TextEditingController();
//   final TextEditingController postOfficeController = TextEditingController();
//   final TextEditingController pincodeController = TextEditingController();

//   File _image;
//   String img64;

//   @override
//   void initState() {
//     stateId = new HashMap<String, String>();
//     // setState(() {
//     //   for (dynamic e in widget.stateListResponse.data) {
//     //     print('In For Loop ==> ' +
//     //         widget.stateListResponse.data.length.toString());
//     //     stateId.putIfAbsent(e.name, () => e.id);
//     //   }
//     // });
//     super.initState();
//   }

//   Future getImage(BuildContext context) async {
//     showDialog(
//         context: context,
//         builder: (context) {
//           return AlertDialog(
//               content: SizedBox(
//             height: 150,
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.center,
//               mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//               children: <Widget>[
//                 FlatButton(
//                   child: Text(
//                     "Camera",
//                     style: TextStyle(fontSize: 26),
//                   ),
//                   onPressed: () async {
//                     var image = await ImagePicker().getImage(
//                       source: ImageSource.camera,
//                     );
//                     // var image = await ImagePicker()
//                     //     .getImage(source: ImageSource.camera);
//                     setState(() {
//                       // _image = image;
//                       _image = File(image.path);
//                       print(_image);
//                       List<int> bytes = Io.File(_image.path).readAsBytesSync();
//                       img64 = base64UrlEncode(bytes);
//                       // var base64String =
//                       //     base64UrlEncode(_image.readAsBytesSync());
//                       // base64String = base64Encode(_image.readAsBytesSync());
//                       print('document $img64');
//                       // print('base64String $base64String');
//                       Navigator.pop(context);
//                     });
//                   },
//                 ),
//                 FlatButton(
//                   child: Text(
//                     "Gallery",
//                     style: TextStyle(fontSize: 26),
//                   ),
//                   onPressed: () async {
//                     var image = await ImagePicker().getImage(
//                       source: ImageSource.gallery,
//                       imageQuality: 50,
//                     );
//                     // var image = await ImagePicker()
//                     //     .getImage(source: ImageSource.gallery);
//                     setState(() {
//                       _image = File(image.path);
//                       // print(_image.uri.toString());
//                       List<int> bytes = Io.File(_image.path).readAsBytesSync();
//                       img64 = base64UrlEncode(bytes);
//                       print('img64 : $img64');
//                       print('bytes $bytes');
//                       // print('base64Image : $img64');
//                       // base64String = base64Encode(_image.readAsBytesSync());
//                       // print('document $img64');
//                       // print('base64String $base64String');
//                       print('image1 : $_image');
//                       Navigator.pop(context);
//                     });
//                   },
//                 ),
//                 _image != null
//                     ? FlatButton(
//                         child: Text(
//                           "Remove Profile",
//                           style: TextStyle(fontSize: 26),
//                         ),
//                         onPressed: () async {
//                           setState(() {
//                             _image = null;
//                             img64 = null;
//                             Navigator.pop(context);
//                           });
//                         },
//                       )
//                     : Text(""),
//               ],
//             ),
//           ));
//         });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         flexibleSpace: Container(
//           decoration: BoxDecoration(gradient: kAppBarGradient),
//           child: SafeArea(
//             child: Row(
//               mainAxisAlignment: MainAxisAlignment.spaceBetween,
//               children: [
//                 Padding(
//                   padding: const EdgeInsets.all(8.0),
//                   child: Stack(
//                     alignment: Alignment.center,
//                     children: [
//                       Image.asset('assets/images/Icon-Holder.png'),
//                       // Icon(Icons.chevron_left_sharp, color: Colors.white,),
//                     ],
//                   ),
//                 ),
//                 Padding(
//                   padding: const EdgeInsets.symmetric(vertical: 3.0),
//                   child: Image.asset('assets/images/Header-Logo.png'),
//                 ),
//                 SizedBox(
//                   width: 20,
//                 ),
//                 Center(
//                   child: Text(
//                     "Survey Form",
//                     style: GoogleFonts.roboto(
//                         color: Color(0xff031e48),
//                         fontSize: 20,
//                         fontWeight: FontWeight.bold
//                         // fontFamily: 'Roboto'
//                         ),
//                   ),
//                 ),
//                 SizedBox(
//                   width: 60,
//                 ),
//                 Padding(
//                   padding: const EdgeInsets.all(8.0),
//                   child: Stack(
//                     alignment: Alignment.center,
//                     children: [
//                       Image.asset('assets/images/Icon-Holder.png'),
//                       Icon(
//                         Icons.close,
//                         color: Colors.white,
//                       ),
//                     ],
//                   ),
//                 ),
//               ],
//             ),
//           ),
//         ),
//       ),
//       body: CustomContainer(
//         child: Padding(
//           padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 15),
//           child: Container(
//             decoration: BoxDecoration(
//                 borderRadius: BorderRadius.circular(10), color: Colors.white),
//             child: Padding(
//               padding: const EdgeInsets.symmetric(horizontal: 16.0),
//               child: Column(
//                 children: [
//                   Expanded(
//                     child: ListView(
//                       // crossAxisAlignment: CrossAxisAlignment.start,
//                       children: [
//                         Container(
//                           margin: const EdgeInsets.only(top: 20, left: 20),
//                           child: Row(
//                             mainAxisAlignment: MainAxisAlignment.spaceAround,
//                             children: [
//                               _image == null
//                                   ? Container(
//                                       // alignment: Alignment.topLeft,
//                                       decoration: BoxDecoration(
//                                         color: Colors.transparent,
//                                       ),
//                                       child: CircleAvatar(
//                                         radius: 30.0,
//                                         backgroundImage: AssetImage(
//                                             "assets/icons/Upload-Icon.png"),
//                                       ),
//                                     )
//                                   : Container(
//                                       // alignment: Alignment.topLeft,
//                                       decoration: BoxDecoration(
//                                         color: Colors.transparent,
//                                       ),
//                                       child: CircleAvatar(
//                                         radius: 30.0,
//                                         backgroundImage: FileImage(
//                                           _image,
//                                         ),
//                                       ),
//                                     ),
//                               InkWell(
//                                 onTap: () => getImage(context),
//                                 child: Container(
//                                   child: Row(
//                                     children: [
//                                       Text(
//                                         "UPLOAD PICTURE",
//                                         style: kheadingStyle.apply(
//                                           fontWeightDelta: 3,
//                                           fontSizeFactor: 1.2,
//                                         ),
//                                       ),
//                                       img64 == null
//                                           ? Icon(
//                                               Icons.cloud_upload,
//                                               color: Color(0xff819ffd),
//                                             )
//                                           : Icon(
//                                               Icons.check,
//                                               color: Color(0xff819ffd),
//                                             ),
//                                     ],
//                                   ),
//                                 ),
//                               ),
//                             ],
//                           ),
//                         ),
//                         Container(
//                           margin: const EdgeInsets.only(top: 20, left: 20),
//                           child: Text(
//                             'FULL NAME'.toUpperCase(),
//                             style: kheadingStyle.apply(
//                               fontWeightDelta: 3,
//                               fontSizeFactor: 1.2,
//                             ),
//                           ),
//                         ),
//                         Container(
//                           margin: const EdgeInsets.only(top: 20),
//                           child: CustomView.editTextField(
//                               controller: fullNameController,
//                               color: Colors.red,
//                               keyborad: TextInputType.text,
//                               // labelText: 'NAME',
//                               lengthLimiting: 20,
//                               fn: (input) {
//                                 setState(() {
//                                   fullName = input;
//                                 });
//                               }),
//                         ),
//                         SizedBox(height: 30),
//                         Container(
//                           margin: const EdgeInsets.only(top: 20, left: 20),
//                           child: Text(
//                             'COUNTRY'.toUpperCase(),
//                             style: kheadingStyle.apply(
//                               fontWeightDelta: 3,
//                               fontSizeFactor: 1.2,
//                             ),
//                           ),
//                         ),
//                         SizedBox(height: 10),
//                         Container(
//                           margin: const EdgeInsets.only(
//                               top: 20, left: 20, right: 20),
//                           decoration: BoxDecoration(
//                               border: Border.all(color: Colors.black)),
//                           width: MediaQuery.of(context).size.width * 0.85,
//                           child: Padding(
//                             padding:
//                                 const EdgeInsets.symmetric(horizontal: 16.0),
//                             child: DropdownButton<String>(
//                               items: countryList.map((value) {
//                                 return DropdownMenuItem<String>(
//                                   value: value,
//                                   child: Text(
//                                     value,
//                                     style: GoogleFonts.actor(
//                                       color: Colors.blue,
//                                       fontSize: 20,
//                                       // fontWeight: FontWeight.bold,
//                                       // fontWeight: FontWeight.bold,
//                                     ),
//                                   ),
//                                 );
//                               }).toList(),
//                               onChanged: (value) {
//                                 setState(() {
//                                   country = value;
//                                 });
//                               },
//                               value: country,
//                               hint: Text(
//                                 "Select County",
//                                 style: GoogleFonts.actor(
//                                     fontSize: 20, color: Colors.blue
//                                     // fontWeight: FontWeight.bold,
//                                     ),
//                               ),
//                             ),
//                           ),
//                         ),
//                         Container(
//                           margin: const EdgeInsets.only(top: 20, left: 20),
//                           child: Text(
//                             'Address'.toUpperCase(),
//                             style: kheadingStyle.apply(
//                               fontWeightDelta: 3,
//                               fontSizeFactor: 1.2,
//                             ),
//                           ),
//                         ),
//                         Container(
//                           margin: const EdgeInsets.only(top: 20),
//                           child: CustomView.editTextField(
//                               controller: addressController,
//                               color: Colors.red,
//                               keyborad: TextInputType.text,
//                               // labelText: 'LAST NAME',
//                               lengthLimiting: 20,
//                               fn: (input) {
//                                 setState(() {
//                                   address = input;
//                                 });
//                               }),
//                         ),
//                         Container(
//                           margin: const EdgeInsets.only(top: 20, left: 20),
//                           child: Text(
//                             'Village'.toUpperCase(),
//                             style: kheadingStyle.apply(
//                               fontWeightDelta: 3,
//                               fontSizeFactor: 1.2,
//                             ),
//                           ),
//                         ),
//                         Container(
//                           margin: const EdgeInsets.only(top: 20),
//                           child: CustomView.editTextField(
//                               controller: villageController,
//                               color: Colors.red,
//                               keyborad: TextInputType.text,
//                               // labelText: 'NAME',
//                               lengthLimiting: 20,
//                               fn: (input) {
//                                 setState(() {
//                                   village = input;
//                                 });
//                               }),
//                         ),
//                         SizedBox(height: 30),
//                         Container(
//                           margin: const EdgeInsets.only(top: 20, left: 20),
//                           child: Text(
//                             'Post Office'.toUpperCase(),
//                             style: kheadingStyle.apply(
//                               fontWeightDelta: 3,
//                               fontSizeFactor: 1.2,
//                             ),
//                           ),
//                         ),
//                         Container(
//                           margin: const EdgeInsets.only(top: 20),
//                           child: CustomView.editTextField(
//                               controller: postOfficeController,
//                               color: Colors.red,
//                               keyborad: TextInputType.text,
//                               // labelText: 'NAME',
//                               lengthLimiting: 20,
//                               fn: (input) {
//                                 setState(() {
//                                   postOffice = input;
//                                 });
//                               }),
//                         ),
//                         SizedBox(height: 30),
//                         Container(
//                           margin: const EdgeInsets.only(top: 20, left: 20),
//                           child: Text(
//                             'Thana'.toUpperCase(),
//                             style: kheadingStyle.apply(
//                               fontWeightDelta: 3,
//                               fontSizeFactor: 1.2,
//                             ),
//                           ),
//                         ),
//                         Container(
//                           margin: const EdgeInsets.only(top: 20),
//                           child: CustomView.editTextField(
//                               controller: thanaController,
//                               color: Colors.red,
//                               keyborad: TextInputType.text,
//                               // labelText: 'NAME',
//                               lengthLimiting: 20,
//                               fn: (input) {
//                                 setState(() {
//                                   thana = input;
//                                 });
//                               }),
//                         ),
//                         SizedBox(height: 30),
//                         Container(
//                           margin: const EdgeInsets.only(top: 20, left: 20),
//                           child: Text(
//                             'PIN CODE'.toUpperCase(),
//                             style: kheadingStyle.apply(
//                               fontWeightDelta: 3,
//                               fontSizeFactor: 1.2,
//                             ),
//                           ),
//                         ),
//                         Container(
//                           margin: const EdgeInsets.only(top: 20),
//                           child: CustomView.editTextField(
//                               controller: pincodeController,
//                               maxLength: 6,
//                               color: Colors.red,
//                               keyborad: TextInputType.number,
//                               // labelText: 'NAME',
//                               lengthLimiting: 20,
//                               fn: (input) {
//                                 setState(() {
//                                   pincode = input.toString();
//                                 });
//                               }),
//                         ),
//                         SizedBox(height: 30),
//                         Container(
//                           margin: const EdgeInsets.only(top: 20, left: 20),
//                           child: Text(
//                             'STATE'.toUpperCase(),
//                             style: kheadingStyle.apply(
//                               fontWeightDelta: 3,
//                               fontSizeFactor: 1.2,
//                             ),
//                           ),
//                         ),
//                         SizedBox(height: 10),
//                         Container(
//                           margin: const EdgeInsets.only(top: 20, left: 20),
//                           decoration: BoxDecoration(
//                               border: Border.all(color: Colors.black)),
//                           width: MediaQuery.of(context).size.width * 0.85,
//                           child: Padding(
//                             padding:
//                                 const EdgeInsets.symmetric(horizontal: 16.0),
//                             child: DropdownButton<String>(
//                               items: widget.stateListResponse.hubResult
//                                   .map((value) {
//                                 return DropdownMenuItem<String>(
//                                   value: value.name,
//                                   child: Text(
//                                     value.name,
//                                     style: GoogleFonts.actor(
//                                       color: Colors.blue,
//                                       fontSize: 20,

//                                       // fontWeight: FontWeight.bold,
//                                       // fontWeight: FontWeight.bold,
//                                     ),
//                                   ),
//                                 );
//                               }).toList(),
//                               onChanged: (value) {
//                                 setState(() {
//                                   selectedState = value;
//                                   this.selectedStateId = stateId[selectedState];
//                                   this.districtListResponse = null;
//                                   stateChange = true;
//                                   this.selectedDistrict = null;
//                                 });
//                               },
//                               value: selectedState,
//                               hint: Text(
//                                 "Select State",
//                                 style: GoogleFonts.actor(
//                                     fontSize: 20, color: Colors.blue
//                                     // fontWeight: FontWeight.bold,
//                                     ),
//                               ),
//                             ),
//                           ),
//                         ),
//                         SizedBox(height: 30),
//                         Container(
//                           margin: const EdgeInsets.only(top: 20, left: 20),
//                           child: Text(
//                             'DISTRICT'.toUpperCase(),
//                             style: kheadingStyle.apply(
//                               fontWeightDelta: 3,
//                               fontSizeFactor: 1.2,
//                             ),
//                           ),
//                         ),
//                         SizedBox(height: 10),
//                         Container(
//                           margin: const EdgeInsets.only(top: 20, left: 20),
//                           decoration: BoxDecoration(
//                               border: Border.all(color: Colors.black)),
//                           width: MediaQuery.of(context).size.width * 0.85,
//                           child: Padding(
//                             padding:
//                                 const EdgeInsets.symmetric(horizontal: 16.0),
//                             child: districtListResponse == null || stateChange
//                                 ? InkWell(
//                                     onTap: () {
//                                       showDialog(
//                                         barrierDismissible: false,
//                                         context: context,
//                                         builder: (context) {
//                                           ApiCall.postDistrict(selectedStateId)
//                                               .then((value) => setState(() {
//                                                     districtListResponse =
//                                                         value;
//                                                     stateChange = false;
//                                                     Navigator.pop(context);
//                                                   }));
//                                           return Align(
//                                             alignment: Alignment.center,
//                                             child: Container(
//                                               height: 40,
//                                               width: 40,
//                                               child:
//                                                   CircularProgressIndicator(),
//                                             ),
//                                           );
//                                         },
//                                       );
//                                     },
//                                     child: Text(
//                                       'Select District',
//                                       style: GoogleFonts.actor(
//                                         color: Colors.blue,
//                                         fontSize: 20,
//                                         // fontWeight: FontWeight.bold,
//                                         // fontWeight: FontWeight.bold,
//                                       ),
//                                     ),
//                                   )
//                                 : districtListResponse.hubResult.length == 0
//                                     ? DropdownButton<String>(
//                                         items: ['No District'].map((value) {
//                                           return DropdownMenuItem<String>(
//                                             value: value,
//                                             child: Text(
//                                               value,
//                                               style: GoogleFonts.actor(
//                                                 color: Colors.blue,
//                                                 fontSize: 20,
//                                                 // fontWeight: FontWeight.bold,
//                                                 // fontWeight: FontWeight.bold,
//                                               ),
//                                             ),
//                                           );
//                                         }).toList(),
//                                         onChanged: (value) {
//                                           setState(() {
//                                             selectedDistrict = value;
//                                           });
//                                         },
//                                         value: selectedDistrict,
//                                         hint: Text(
//                                           "Select District",
//                                           style: GoogleFonts.actor(
//                                               fontSize: 20, color: Colors.blue
//                                               // fontWeight: FontWeight.bold,
//                                               ),
//                                         ),
//                                       )
//                                     : DropdownButton<String>(
//                                         items: districtListResponse.hubResult
//                                             .map((value) {
//                                           return DropdownMenuItem<String>(
//                                             value: value.name,
//                                             child: Text(
//                                               value.name,
//                                               style: GoogleFonts.actor(
//                                                 color: Colors.blue,
//                                                 fontSize: 20,
//                                                 // fontWeight: FontWeight.bold,
//                                                 // fontWeight: FontWeight.bold,
//                                               ),
//                                             ),
//                                           );
//                                         }).toList(),
//                                         onChanged: (value) {
//                                           setState(() {
//                                             this.selectedDistrict = value;
//                                           });
//                                         },
//                                         value: selectedDistrict,
//                                         hint: Text(
//                                           "Select District",
//                                           style: GoogleFonts.actor(
//                                             fontSize: 20,
//                                             fontWeight: FontWeight.bold,
//                                           ),
//                                         ),
//                                       ),
//                           ),
//                         ),
//                         SizedBox(height: 30),
//                         Container(
//                           margin: const EdgeInsets.only(top: 20, left: 20),
//                           child: Text(
//                             'Mobile Number'.toUpperCase(),
//                             style: kheadingStyle.apply(
//                               fontWeightDelta: 3,
//                               fontSizeFactor: 1.2,
//                             ),
//                           ),
//                         ),
//                         Container(
//                           margin: const EdgeInsets.only(top: 20),
//                           child: CustomView.editTextField(
//                               controller: mobileController,
//                               maxLength: 10,
//                               color: Colors.red,
//                               keyborad: TextInputType.number,
//                               // labelText: 'NAME',
//                               lengthLimiting: 20,
//                               fn: (input) {
//                                 setState(() {
//                                   mobileNumber = input.toString();
//                                 });
//                               }),
//                         ),
//                         SizedBox(height: 30),
//                         Padding(
//                           padding: const EdgeInsets.symmetric(horizontal: 40),
//                           child: GestureDetector(
//                             onTap: () async {
//                               print('PRODUCST : ' + widget.products.toString());
//                               print('surveyForm : ' +
//                                   widget.surveyForm.toString());
//                               print('surveyName : ' + widget.surveyName);
//                               print('surveyId : ' + widget.surveyId);
//                               print('country : ' + country);
//                               print('district : ' + selectedDistrict);
//                               print('state : ' + selectedState);
//                               print('img64 : ' + img64);
//                               Navigator.pushReplacement(context,
//                                   MaterialPageRoute(builder: (context) {
//                                 return ActiveSurveyForm(
//                                   products: widget.products,
//                                   surveyForm: widget.surveyForm,
//                                   surveyName: widget.surveyName,
//                                   surveyId: widget.surveyId,
//                                   stateListResponse: widget.stateListResponse,
//                                   address: addressController.text.toString(),
//                                   country: country,
//                                   district: selectedDistrict,
//                                   fullName: fullNameController.text.toString(),
//                                   mobileNumber:
//                                       mobileController.text.toString(),
//                                   pincode: pincodeController.text.toString(),
//                                   postOffice:
//                                       postOfficeController.text.toString(),
//                                   state: selectedState,
//                                   thana: thanaController.text.toString(),
//                                   village: villageController.text.toString(),
//                                   img64: img64,
//                                 );
//                               }));
//                             },
//                             child: Align(
//                               alignment: Alignment.bottomRight,
//                               child: Card(
//                                 elevation: 10,
//                                 shape: BeveledRectangleBorder(
//                                   borderRadius: BorderRadius.circular(10),
//                                 ),
//                                 child: Container(
//                                   // margin: EdgeInsets.all(value),
//                                   // width:
//                                   // MediaQuery.of(context).size.width * 0.70,
//                                   child: Padding(
//                                     padding: const EdgeInsets.symmetric(
//                                         horizontal: 24.0, vertical: 8),
//                                     child: Center(
//                                       child: Text(
//                                         "SUBMIT",
//                                         style: GoogleFonts.roboto(
//                                           color: Colors.white,
//                                           fontSize: 30,
//                                           fontWeight: FontWeight.bold,
//                                         ),
//                                       ),
//                                     ),
//                                   ),
//                                   decoration: BoxDecoration(
//                                     color: Colors.blue[800],
//                                     borderRadius: BorderRadius.circular(10),
//                                     // image: DecorationImage(
//                                     //     fit: BoxFit.fill,
//                                     //     image: AssetImage(
//                                     //         'assets/images/ButtonBG.png')),
//                                   ),
//                                 ),
//                               ),
//                             ),
//                           ),
//                         ),
//                         SizedBox(
//                           height: 20,
//                         ),
//                       ],
//                     ),
//                   ),
//                 ],
//               ),
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }
