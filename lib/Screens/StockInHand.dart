import 'package:blu_way/Constants/ConstantList.dart';
import 'package:blu_way/Constants/Style.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class StockInHand extends StatefulWidget {
  @override
  _StockInHandState createState() => _StockInHandState();
}

class _StockInHandState extends State<StockInHand> {
  String selectedvaluevendors;
  String selectedvaluevproducts;
  String selectedvaluedates;
  var vendors = ['Alpha', 'Beta', 'Gamma'];
  List<String> products = ['Car', 'Bike', 'Cycle'];
  List<String> Dates = ['Today', 'Tomorrow', 'Yesterday'];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: Container(
          decoration: BoxDecoration(gradient: kAppBarGradient),
          child: SafeArea(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Image.asset('assets/images/Icon-Holder.png'),
                      // Icon(Icons.chevron_left_sharp, color: Colors.white,),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 3.0),
                  child: Image.asset('assets/images/Header-Logo.png'),
                ),
                SizedBox(
                  width: 20,
                ),
                Center(
                  child: Text(
                    "STOCK IN HAND",
                    style: GoogleFonts.roboto(
                        color: Color(0xff031e48),
                        fontSize: 20,
                        fontWeight: FontWeight.bold
                        // fontFamily: 'Roboto'
                        ),
                  ),
                ),
                SizedBox(
                  width: 60,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Image.asset('assets/images/Icon-Holder.png'),
                      Icon(
                        Icons.close,
                        color: Colors.white,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      body: CustomContainer(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 15),
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
            ),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Align(
                      alignment: Alignment.topRight,
                      child: Container(
                        width: 120,
                        height: 34,
                        child: Center(
                          child: Padding(
                            padding: const EdgeInsets.all(1.0),
                            child: Text(
                              "RESET",
                              style: GoogleFonts.actor(
                                fontSize: 20,
                                color: Colors.grey[700],
                                // fontWeight: FontWeight.bold
                              ),
                            ),
                          ),
                        ),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(color: Colors.orange, width: 5)),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "ALL VENDORS",
                        style: GoogleFonts.actor(
                          color: Colors.blue[800],
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 16.0, vertical: 10),
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black)),
                        width: MediaQuery.of(context).size.width * 0.85,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16.0),
                          child: DropdownButton<String>(
                            items: vendors.map((value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(
                                  value,
                                  style: GoogleFonts.actor(
                                    color: Colors.black,
                                    fontSize: 20,
                                    // fontWeight: FontWeight.bold,
                                  ),
                                ),
                              );
                            }).toList(),
                            onChanged: (value) {
                              setState(() {
                                this.selectedvaluevendors = value;
                              });
                            },
                            value: selectedvaluevendors,
                            hint: Text(
                              "Vendors",
                              style: GoogleFonts.actor(
                                fontSize: 30,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "PRODUCT",
                        style: GoogleFonts.actor(
                          color: Colors.blue[800],
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 16.0, vertical: 10),
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black)),
                        width: MediaQuery.of(context).size.width * 0.85,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16.0),
                          child: DropdownButton<String>(
                            items: products.map((value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(
                                  value,
                                  style: GoogleFonts.actor(
                                    color: Colors.black,
                                    fontSize: 20,
                                    // fontWeight: FontWeight.bold,
                                  ),
                                ),
                              );
                            }).toList(),
                            onChanged: (value) {
                              setState(() {
                                this.selectedvaluevproducts = value;
                              });
                            },
                            value: selectedvaluevproducts,
                            hint: Text(
                              "Products",
                              style: GoogleFonts.actor(
                                fontSize: 30,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "DATE",
                        style: GoogleFonts.actor(
                          color: Colors.blue[800],
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 16.0, vertical: 10),
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black)),
                        width: MediaQuery.of(context).size.width * 0.85,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16.0),
                          child: DropdownButton<String>(
                            items: Dates.map((value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(
                                  value,
                                  style: GoogleFonts.actor(
                                    color: Colors.black,
                                    fontSize: 20,
                                    // fontWeight: FontWeight.bold,
                                  ),
                                ),
                              );
                            }).toList(),
                            onChanged: (value) {
                              setState(() {
                                this.selectedvaluedates = value;
                              });
                            },
                            value: selectedvaluedates,
                            hint: Text(
                              "Products",
                              style: GoogleFonts.actor(
                                fontSize: 30,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),

                  // DataTable(
                  //   columns: [
                  //     DataColumn(label: Expanded(
                  //       child: Container(
                  //         color: Colors.blue[800],
                  //           child: Text('Variant')),
                  //     )),
                  //     DataColumn(label: Container(child: Text('P-Nos'))),
                  //     DataColumn(label: Container(child: Text('Stock'))),
                  //     DataColumn(label: Container(child: Text('Stock'))),
                  //   ],
                  //   rows: [
                  //
                  //   ],
                  // ),

                  //     rows: [
                  //       DataRow(cells: [
                  //         DataCell(Text('1')),
                  //         DataCell(Text('Arya')),
                  //         DataCell(Text('6')),
                  //       ]),
                  //       DataRow(cells: [
                  //         DataCell(Text('12')),
                  //         DataCell(Text('John')),
                  //         DataCell(Text('9')),
                  //       ]),
                  //       DataRow(cells: [
                  //         DataCell(Text('42')),
                  //         DataCell(Text('Tony')),
                  //         DataCell(Text('8')),
                  //       ]),
                  //     ],
                  //   ),
                  // ])),
                  //
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
