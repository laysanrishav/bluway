import 'dart:async';

import 'package:blu_way/Model/Auth/SEModel.dart';
import 'package:blu_way/Model/Auth/SupervisorModel.dart';
import 'package:blu_way/Model/CustomerList.dart';
import 'package:blu_way/Model/SurveyListResponse.dart';
import 'package:blu_way/Model/surveySubmitResponse.dart';
import 'package:blu_way/Model/verifyResponse.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter/foundation.dart';

import 'api_constants.dart';

class ApiCall {
  static Map<String, dynamic> verifyData;
  static String token, tokenCall;
  static Map<String, dynamic> stateData;
  static List<dynamic> stateList;
  static List<String> listData = [];
  static Map<String, dynamic> loginData;
  static String otpByLogin, statusData;
  static List districData;
  static List<String> districtList = [];
  static Map<String, dynamic> aboutData;

  static Future<SurveyListResponse> getSurveyList() async {
    final response = await http.post(
      Uri.encodeFull(AppConstants.surveyList),
      headers: {
        "accept": "application/json",
        "authorization": "$token",
      },
      body: json.encode({
        "blu_angel": tokenCall,
      }),
    );
    if (response.statusCode == 200) {
      final String responseString = response.body;
      print('response $response');
      print('response ${response.statusCode}');
      print('responseString $responseString');
      return surveyListResponseFromJson(responseString);
    } else {
      return null;
    }
  }

  static Future<SurveySubmitResponse> getSurveySubmit({
    @required String surveyId,
    @required String fullName,
    @required String address,
    @required String village,
    @required String postOffice,
    @required String thana,
    @required String country,
    @required String state,
    @required String district,
    @required String pincode,
    @required String mobileNumber,
    @required Map<String, dynamic> formData,
    @required String products,
    @required String qty,
    @required String lng,
    @required String lat,
    @required String image,
  }) async {
    final response = await http.post(
      Uri.encodeFull(AppConstants.surveySubmit),
      headers: {
        "accept": "application/json",
        "authorization": "$token",
      },
      body: json.encode({
        "surveys": surveyId,
        "blu_angel": tokenCall,
        "full_name": fullName,
        "address": address,
        "village": village,
        "post_office": postOffice,
        "thana": thana,
        "country": country,
        "state": state,
        "district": district,
        "pincode": pincode,
        "mobile": mobileNumber,
        "form_data": formData,
        "product": products,
        "qty": qty,
        "image": image,
        "lat": lat,
        "lng": lng,
      }),
    );
    print("APICall ==> " +
        json.encode({
          "surveys": surveyId,
          "blu_angel": tokenCall,
          "full_name": fullName,
          "address": address,
          "village": village,
          "post_office": postOffice,
          "thana": thana,
          "country": country,
          "state": state,
          "district": district,
          "pincode": pincode,
          "mobile": mobileNumber,
          "form_data": formData,
          "product": products,
          "qty": qty,
          "image": image,
          "lat": lat,
          "lng": lng,
        }));
    if (response.statusCode == 200) {
      final String responseString = response.body;
      return surveySubmitResponseFromJson(responseString);
    } else {
      return null;
    }
  }

  static Future<AddSupervisorModel> addSupervisorModel(
      @required String tokenvalue,
      {String fname,
      String lname,
      String id,
      String mobileNo,
      String email,
      String gender,
      String dob,
      String fathername,
      String address,
      String village,
      String postoffice,
      String thana,
      String country,
      String statename,
      String districtname,
      String pincode,
      String occupation,
      String doucment,
      String licenceno,
      String documentimage,
      String profileimage}) async {
    // SharedPreferences sharedPreferences =
    // await SharedPreferences.getInstance();
    // Global.accessToken =  sharedPreferences.getString("token");
    // var header = {'authorization': Global.accessToken};
    // print(header);
    var body = jsonEncode({
      "id": id,
      // "id": Global.id,
      "first_name": fname,
      "last_name": lname,
      "email": email,
      "gender": gender,
      "dob": dob,
      "father_name": fathername,
      "address": address,
      "village": village,
      "post_office": postoffice,
      "thana": thana,
      "country": country,
      "state_name": statename,
      "district_name": districtname,
      "pincode": pincode,
      "occupation": occupation,
      "document": doucment,
      "mobile": mobileNo,
      "licence_number": licenceno,
      "document_image_data": "data:image/png;base64, $documentimage",
      "image_data": "data:image/png;base64, $profileimage"
    });
    print(body);
    // print("Authotization: "+header.toString());
    final response = await http.post(
        'http://139.59.75.40:4040/app/add-supervisor',
        body: body,
        headers: {
          "accept": "application/json",
          "authorization": "$tokenvalue",
        });
    // print("Authotization: "+header.toString());
    print(response.body);
    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      print(response.body);

      return AddSupervisorModel.fromJson(json.decode(response.body));
      // print("Status OK");
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }

  // static Future<

  static Future<AddSEModel> addSEModel(String tokenvalue,
      {String fname,
      String lname,
      String id,
      String supervisorId,
      String mobileNo,
      String email,
      String gender,
      String dob,
      String fathername,
      String address,
      String village,
      String postoffice,
      String thana,
      String country,
      String statename,
      String districtname,
      String pincode,
      String occupation,
      String doucment,
      String licenceno,
      String documentimage,
      String profileimage}) async {
    // SharedPreferences sharedPreferences =
    // await SharedPreferences.getInstance();
    // Global.accessToken =  sharedPreferences.getString("token");
    // var header = {'authorization': Global.accessToken};
    // print(header);
    var body = jsonEncode({
      "id": id,
      "supervisor": supervisorId,
      // "id": Global.id,
      "first_name": fname,
      "last_name": lname,
      "email": email,
      "gender": gender,
      "dob": dob,
      "father_name": fathername,
      "address": address,
      "village": village,
      "post_office": postoffice,
      "thana": thana,
      "country": country,
      "state_name": statename,
      "district_name": districtname,
      "pincode": pincode,
      "occupation": occupation,
      "document": doucment,
      "mobile": mobileNo,
      "licence_number": licenceno,
      "document_image_data": "data:image/png;base64, $documentimage",
      "image_data": "data:image/png;base64, $profileimage"
    });
    print(body);
    // print("Authotization: "+header.toString());
    final response = await http
        .post('http://139.59.75.40:4040/app/add-se', body: body, headers: {
      "accept": "application/json",
      "authorization": "$tokenvalue",
    });
    // print("Authotization: "+header.toString());
    print(response.body);
    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      print(response.body);

      return AddSEModel.fromJson(json.decode(response.body));
      // print("Status OK");
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }

//   static Future<GetSupervisorModel> getSupervisors({
//
// }) async {
//
// }

  // login reponse api
  // static Future<LoginResponse> postLogin({
  //   @required String mobileNumber,
  // }) async {
  //   final response = await http
  //       .post(Uri.encodeFull(AppConstants.login),
  //           headers: {
  //             "accept": "application/json",
  //             // "authorization": "$token",
  //           },
  //           body: json.encode({
  //             "mobile": mobileNumber,
  //           }))
  //       .timeout(
  //         Duration(seconds: 5),
  //       );
  //   if (response.statusCode == 200) {
  //     final String responseString = response.body;
  //     return loginResponseFromJson(responseString);
  //   } else {
  //     return null;
  //   }
  // }

  // verify api
  static Future<VerifyResponse> postVerify(
      {@required String mobileNumber, @required String otp}) async {
    final response = await http.post(Uri.encodeFull(AppConstants.verify),
        headers: {
          "accept": "application/json",
          "authorization": "$token",
        },
        body: json.encode({
          "mobile": mobileNumber,
          "otp": otp,
        }));
    if (response.statusCode == 200) {
      final String responseString = response.body;
      return verifyResponseFromJson(responseString);
    } else {
      return null;
    }
  }

  // List<CustomerResult> parseCustomers(String responseBody) {
  //   var l = json.decode(responseBody) as List<dynamic>;
  //   var customers = l.map((model) => CustomerResult.fromJson(model)).toList();
  //   return customers;
  // }
  //
  // static Future<List<CustomerResult>> getCustomerList(String orgId) async {
  //   List<CustomerResult> customerResult = new List<CustomerResult>();
  //   var response = await http.post(Uri.encodeFull(AppConstants.customerList),
  //       body: json.encode({"organization": orgId}));
  //   if (response.statusCode == 200) {
  //     CustomerList decoded = json.decode(response.body);
  //
  //     customerResult = decoded.result;
  //     return customerResult;
  //   } else {
  //     print("There is an error with status code ${response.statusCode}");
  //   }
  // }

// sign up api
// static Future<SignUpResponse> postSignUp({
//   @required String firstName,
//   @required String lastName,
//   @required String gender,
//   @required String dob,
//   @required String fatherName,
//   @required String address1,
//   @required String village,
//   @required String postOffice,
//   @required String thana,
//   @required String country,
//   @required String state,
//   @required String district,
//   @required String pincode,
//   @required String occupation,
//   @required String identity,
//   @required String mobile,
//   @required String document,
//   @required String organization_name,
//   @required String org_type,
//   @required String org_other_type,
//   @required String reg_number,
//   @required String bank_name,
//   @required String account_number,
//   @required String gst_number,
//   @required String ifsc,
//   @required String hub,
//   // @required File image,
// }) async {
//   // var base64String = base64Encode(document.readAsBytesSync());
//   // var base64String = base64UrlEncode(image.readAsBytesSync());
//   // print(base64String);
//   final response = await http.post(
//     Uri.encodeFull(AppConstants.signUp),
//     headers: {
//       "accept": "text/plain",
//       "authorization": "$token",
//     },
//     body: json.encode({
//       'organization_name': organization_name,
//       'org_type': org_type,
//       'org_other_type': org_other_type,
//       'reg_number': reg_number,
//       'gst_number': gst_number,
//       'bank_name': bank_name,
//       'account_number': account_number,
//       'ifsc': ifsc,
//       "hub": hub,
//       'first_name': firstName,
//       'last_name': lastName,
//       'gender': gender,
//       'dob': dob,
//       'father_name': fatherName,
//       'address1': address1,
//       'village': village,
//       'post_office': postOffice,
//       'thana': thana,
//       'country': country,
//       'state': state,
//       'district': district,
//       'pincode': pincode,
//       'occupation': occupation,
//       'identity': identity,
//       'mobile': mobile,
//       "document": document,
//     }),
//   );
//   if (response.statusCode == 200) {
//     final String responseString = response.body;
//     print(responseString);
//     // print(base64String);
//     // print(CustomView.base64String(document.readAsBytesSync()));
//     try {
//       return signUpResponseFromJson(responseString);
//     } catch (e) {
//       print(e);
//       return SignUpResponse(
//           status: "error", message: "Mobile already exists!");
//     }
//   } else {
//     print(response.statusCode);
//     return SignUpResponse(
//         status: "api_error", message: "Mobile already exists!");
//     // return null;
//   }
// }

// state list api
  /// static Future<StateListResponse> getState() async {
//   final response = await http.post(
//     Uri.encodeFull(AppConstants.stateList),
//     headers: {
//       "accept": "application/json",
//     },
//   );
//   if (response.statusCode == 200) {
//     final String responseString = response.body;
//     return stateListResponseFromJson(responseString);
//   } else {
//     return null;
//   }
// }

// district api
//  static Future<DistrictListResponse> posttDistrictData(String id) async {
//   final response = await http.post(Uri.encodeFull(AppConstants.districtList),
//       headers: {
//         "accept": "application/json",
//       },
//       body: json.encode({
//         "id": id,
//       }));
//   if (response.statusCode == 200) {
//     final String responseString = response.body;
//     return districtListResponseFromJson(responseString);
//   } else {
//     return null;
//   }
// }

  /// static Future<BannerResponse> getBanner() async {
//   final response = await http.post(
//     Uri.encodeFull(AppConstants.banner),
//     headers: {
//       "accept": "application/json",
//       "authorization": "$token",
//     },
//   );
//   if (response.statusCode == 200) {
//     final String responseString = response.body;
//     return bannerResponseFromJson(responseString);
//   } else {
//     return null;
//   }
// }

// static Future<StateListResponse> postState() async {
//   final response = await http.post(
//     Uri.encodeFull(AppConstants.stateList),
//     headers: {
//       "accept": "application/json",
//     },
//   );
//   if (response.statusCode == 200) {
//     final String responseString = response.body;
//     return stateListResponseFromJson(responseString);
//   } else {
//     return null;
//   }
// }

// static Future<DistrictListResponse> postDistrict(String id) async {
//   final response = await http.post(Uri.encodeFull(AppConstants.districtList),
//       headers: {
//         "accept": "application/json",
//       },
//       body: json.encode({
//         "id": id,
//       }));
//   if (response.statusCode == 200) {
//     final String responseString = response.body;
//     return districtListResponseFromJson(responseString);
//   } else {
//     return null;
//   }
// }

  /// static var uri = AppConstants.baseUrl;

  /// static BaseOptions options = BaseOptions(
//     baseUrl: uri,
//     responseType: ResponseType.plain,
//     connectTimeout: 30000,
//     receiveTimeout: 30000,
//     validateStatus: (code) {
//       if (code >= 200) {
//         return true;
//       } else {
//         return false;
//       }
//     });

  /// static Dio dio = Dio(options);

// static Future<SignUpResponse> signUP({
//   @required String firstName,
//   @required String lastName,
//   @required String gender,
//   @required String dob,
//   @required String fatherName,
//   @required String address1,
//   @required String village,
//   @required String postOffice,
//   @required String thana,
//   @required String country,
//   @required String state,
//   @required String district,
//   @required String pincode,
//   @required String occupation,
//   @required String identity,
//   @required String mobile,
//   @required String document,
// }) async {
//   try {
//     Options options = Options(
//       contentType: 'application/json',
//     );
//     // var base64String = base64Encode(document.readAsBytesSync());
//     FormData formData = new FormData.fromMap({
//       "first_name": firstName,
//       "last_name": lastName,
//       "gender": gender,
//       "dob": dob,
//       "father_name": fatherName,
//       "address1": address1,
//       "village": village,
//       "post_office": postOffice,
//       "thana": thana,
//       "country": country,
//       "state": state,
//       "district": district,
//       "pincode": pincode,
//       "occupation": occupation,
//       "identity": identity,
//       "mobile": mobile,
//       "document": document,
//     });
//     // print(await MultipartFile.fromFile(document.path,
//     //     filename: document.path.split('/').last));
//     var response =
//         await dio.post('register', data: formData, options: options);

//     if (response.statusCode == 200 || response.statusCode == 201) {
//       // var responseJson = json.decode(response.data);
//       var responseJson = response.data;
//       return signUpResponseFromJson(responseJson);
//     } else {
//       print('response.statusCode ${response.statusCode}');
//     }
//   } on DioError catch (exception) {
//     if (exception == null ||
//         exception.toString().contains('SocketException')) {
//       print("Error Network Error");
//     } else if (exception.type == DioErrorType.RECEIVE_TIMEOUT ||
//         exception.type == DioErrorType.CONNECT_TIMEOUT) {
//       print(
//           "Error Could'nt connect, please ensure you have a stable network.");
//     } else {
//       return null;
//     }
//   }
// }
}
