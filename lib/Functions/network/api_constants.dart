class AppConstants {
  static final String baseUrl = 'http://139.59.75.40:4040/app/';
  // 'https://api.bluangel.in/app/';

  static final String stateList = baseUrl + 'state-list';

  static final String districtList = baseUrl + 'district-list';

  static final String signUp = baseUrl + 'organization-register';

  static final String verify = baseUrl + 'verify';

  static final String login = baseUrl + 'login';

  // static final String resetPassword = baseUrl + 'reset-password';

  static final String aboutPage = baseUrl + 'about-page';

  static final String surveyList = baseUrl + 'my-surevy-list';

  static final String surveyReport = baseUrl + 'my-surevy-report';

  static final String surveySubmit = baseUrl + 'survey-submit';

  static final String getCustomerData = baseUrl + 'get-customer-data';

  static final String updateProfile = baseUrl + 'update-profile';

  static final String getLevel = baseUrl + 'get-level';

  static final String adminRequest = baseUrl + 'admin-request';

  static final String banner = baseUrl + 'settings';

  static final String allProducts = baseUrl + 'get-all-product';

  static final String addCustomerorder = baseUrl + 'add-customer-order';

  static final String seList = baseUrl + 'get-se-list';

  static final String settings = baseUrl + 'settings';

  static final String customerList = baseUrl + 'organization-customer-list';

  static final String createCustomer = baseUrl + 'create-customer';

  static final String deleteSupervisor = baseUrl + 'delete-supervisor';

  static final String updateOrg = baseUrl + 'update-organization';

  static final String addRequisition = baseUrl + 'add-requisition';
}
